DROP TABLE IF EXISTS [FrmMainText];
CREATE TABLE [FrmMainText] (
  [ControlName] text NOT NULL UNIQUE
, [Text] text NOT NULL
, CONSTRAINT [PK_FrmMainText] PRIMARY KEY ([ControlName])
);
