DROP TABLE IF EXISTS [FrmMainInternalText];
CREATE TABLE [FrmMainInternalText] (
  [ControlName] text NOT NULL UNIQUE
, [Text] text NOT NULL
, CONSTRAINT [PK_FrmMainInternalText] PRIMARY KEY ([ControlName])
);
