DROP TABLE IF EXISTS [CrystalNames];
CREATE TABLE [CrystalNames] (
  [ID] INTEGER  NOT NULL UNIQUE
, [CrystalName] text NOT NULL
, CONSTRAINT [PK_CrystalNames] PRIMARY KEY ([ID])
);
