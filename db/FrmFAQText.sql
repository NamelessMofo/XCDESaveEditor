DROP TABLE IF EXISTS [FrmFAQText];
CREATE TABLE [FrmFAQText] (
  [ControlName] text NOT NULL UNIQUE
, [Text] text NOT NULL
, CONSTRAINT [PK_FrmFAQText] PRIMARY KEY ([ControlName])
);
