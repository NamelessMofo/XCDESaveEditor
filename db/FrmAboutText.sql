DROP TABLE IF EXISTS [FrmAboutText];
CREATE TABLE [FrmAboutText] (
  [ControlName] text NOT NULL UNIQUE
, [Text] text NOT NULL
, CONSTRAINT [PK_FrmAboutText] PRIMARY KEY ([ControlName])
);
