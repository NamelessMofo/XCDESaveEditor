using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XCDESaveEditor
{
    public static class XCDEData
    {
        private static SQLiteConnection dbConn = new SQLiteConnection (@"Data Source=XCDEData.db; Read Only=True;");
        private static SQLiteDataAdapter dbAdapter;
        private static DataSet ds;

        public static DataTable Arts()
        {
            string query =
                "SELECT ID, Name FROM Arts WHERE CharID = 0 " +
                "UNION ALL " +
                "SELECT ID, Name || ' [' || ID || ']' AS Name " +
                "FROM Arts WHERE CharID <> 0";

            return GetDataSet (query).Tables[0];
        }

        public static DataTable ArtsUnlockLvls()
        {
            string query = "SELECT ID, Name FROM ArtsUnlockLvls";

            return GetDataSet (query).Tables[0];
        }

        public static DataTable Characters()
        {
            string query = "SELECT ID, Name FROM Characters";

            return GetDataSet (query).Tables[0];
        }

        public static DataTable CharRange (ushort min, ushort max)
        {
            string query =
                "SELECT ID, ID || ': ' || Name AS Name FROM Characters " +
                "WHERE ID BETWEEN " + min + " AND " + max;

            return GetDataSet (query).Tables[0];
        }

        public static DataTable Elements()
        {
            string query = "SELECT ID, Name FROM Elements";

            return GetDataSet (query).Tables[0];
        }

        public static DataTable SkillTrees()
        {
            string query =
                "SELECT ID, CharID, TreeID, Name FROM SkillTrees WHERE CharID = 0 " +
                "UNION ALL " +
                "SELECT ID, CharID, TreeID, SkillID, Name || ' [' || ID || ']' AS Name " +
                "FROM SkillTrees WHERE CharID <> 0";

            return GetDataSet (query).Tables[0];
        }

        public static DataTable Skills()
        {
            string query =
                "SELECT ID, CharID, TreeID, SkillID, Name FROM Skills WHERE CharID = 0 " +
                "UNION ALL " +
                "SELECT ID, CharID, TreeID, SkillID, Name || ' [' || ID || ']' AS Name " +
                "FROM Skills WHERE CharID <> 0";

            return GetDataSet (query).Tables[0];
        }

        public static bool ContainsChar (UInt16 id)
        {
            DataTable dr = GetDataSet ("SELECT ID FROM Characters").Tables[0];
            List<UInt16> IDs = new List<UInt16>();
            foreach (DataRow r in dr.Rows)
                IDs.Add (Convert.ToUInt16 (r[0]));

            return IDs.Contains (id);
        }

        public static DataTable Items()
        {
            string query =
                "SELECT ID, " +
                "Name || ' [' || ID || ']' AS Name " +
                "FROM Items";

            return GetDataSet (query).Tables[0];
        }

        public static DataTable ItemTypes()
        {
            string query = "SELECT ID, Name FROM ItemTypes";

            return GetDataSet (query).Tables[0];
        }

        public static DataTable Weapons()
        {
            string query =
                "SELECT ID, Name FROM Items WHERE ItemTypeID = 0 " +
                "UNION ALL " +
                "SELECT ID, Name || ' [' || ID || ']' AS Name " +
                "FROM Items WHERE ItemTypeID = 2";

            return GetDataSet (query).Tables[0];
        }

        public static DataTable HeadArmour()
        {
            string query =
                "SELECT ID, Name FROM Items WHERE ItemTypeID = 0 " +
                "UNION ALL " +
                "SELECT ID, Name || ' [' || ID || ']' AS Name " +
                "FROM Items WHERE ItemTypeID = 4";

            return GetDataSet (query).Tables[0];
        }

        public static DataTable TorsoArmour()
        {
            string query =
                "SELECT ID, Name FROM Items WHERE ItemTypeID = 0 " +
                "UNION ALL " +
                "SELECT ID, Name || ' [' || ID || ']' AS Name " +
                "FROM Items WHERE ItemTypeID = 5";

            return GetDataSet (query).Tables[0];
        }

        public static DataTable ArmArmour()
        {
            string query =
                "SELECT ID, Name FROM Items WHERE ItemTypeID = 0 " +
                "UNION ALL " +
                "SELECT ID, Name || ' [' || ID || ']' AS Name " +
                "FROM Items WHERE ItemTypeID = 6";

            return GetDataSet (query).Tables[0];
        }

        public static DataTable LegArmour()
        {
            string query =
                "SELECT ID, Name FROM Items WHERE ItemTypeID = 0 " +
                "UNION ALL " +
                "SELECT ID, Name || ' [' || ID || ']' AS Name " +
                "FROM Items WHERE ItemTypeID = 7";

            return GetDataSet (query).Tables[0];
        }

        public static DataTable FootArmour()
        {
            string query =
                "SELECT ID, Name FROM Items WHERE ItemTypeID = 0 " +
                "UNION ALL " +
                "SELECT ID, Name || ' [' || ID || ']' AS Name " +
                "FROM Items WHERE ItemTypeID = 8";

            return GetDataSet (query).Tables[0];
        }

        public static DataTable Crystals()
        {
            string query =
                "SELECT ID, Name FROM Items WHERE ItemTypeID = 0 " +
                "UNION ALL " +
                "SELECT ID, Name || ' [' || ID || ']' AS Name " +
                "FROM Items WHERE ItemTypeID = 9";

            return GetDataSet(query).Tables[0];
        }

        public static DataTable CrystalNames ()
        {
            string query = "SELECT ID, CrystalName FROM CrystalNames";

            return GetDataSet (query).Tables[0];
        }

        public static DataTable Gems()
        {
            string query =
                "SELECT ID, Name FROM Items WHERE ItemTypeID = 0 " +
                "UNION ALL " +
                "SELECT ID, Name || ' [' || ID || ']' AS Name " +
                "FROM Items WHERE ItemTypeID = 3";

            return GetDataSet (query).Tables[0];
        }

        public static DataTable RankNames()
        {
            string query = "SELECT ID, Name FROM RankNames";

            return GetDataSet (query).Tables[0];
        }

        public static DataTable Buffs()
        {
            string query = "SELECT ID, ElementID, Name FROM Buffs";

            return GetDataSet (query).Tables[0];
        }

        public static DataTable Collectables()
        {
            string query =
                "SELECT ID, Name FROM Items WHERE ItemTypeID = 0 " +
                "UNION ALL " +
                "SELECT ID, Name || ' [' || ID || ']' AS Name " +
                "FROM Items WHERE ItemTypeID = 10";

            return GetDataSet (query).Tables[0];
        }

        public static DataTable Materials()
        {
            string query =
                "SELECT ID, Name FROM Items WHERE ItemTypeID = 0 " +
                "UNION ALL " +
                "SELECT ID, Name || ' [' || ID || ']' AS Name " +
                "FROM Items WHERE ItemTypeID = 11";

            return GetDataSet (query).Tables[0];
        }

        public static DataTable KeyItems()
        {
            string query =
                "SELECT ID, Name FROM Items WHERE ItemTypeID = 0 " +
                "UNION ALL " +
                "SELECT ID, Name || ' [' || ID || ']' AS Name " +
                "FROM Items WHERE ItemTypeID = 12";

            return GetDataSet (query).Tables[0];
        }

        public static DataTable ArtsManuals()
        {
            string query =
                "SELECT ID, Name FROM Items WHERE ItemTypeID = 0 " +
                "UNION ALL " +
                "SELECT ID, Name || ' [' || ID || ']' AS Name " +
                "FROM Items WHERE ItemTypeID = 13";

            return GetDataSet (query).Tables[0];
        }

        public static Dictionary<string, string> GetFrmAboutText()
        {
            string query =
                "SELECT ControlName, Text FROM FrmAboutText";

            DataTable dt = GetDataSet(query).Tables[0];
            Dictionary<string, string> dict = new Dictionary<string, string>();

            foreach (DataRow r in dt.Rows)
                dict.Add(r[0].ToString(), r[1].ToString());

            return dict;
        }

        public static Dictionary<string, string> GetFrmFAQText()
        {
            string query =
                "SELECT ControlName, Text FROM FrmFAQText";

            DataTable dt = GetDataSet(query).Tables[0];
            Dictionary<string, string> dict = new Dictionary<string, string>();

            foreach (DataRow r in dt.Rows)
                dict.Add(r[0].ToString(), r[1].ToString());

            return dict;
        }

        public static Dictionary<string, string> GetFrmMainText()
        {
            string query =
                "SELECT ControlName, Text FROM FrmMainText";

            DataTable dt = GetDataSet(query).Tables[0];
            Dictionary<string, string> dict = new Dictionary<string, string>();

            foreach (DataRow r in dt.Rows)
                dict.Add(r[0].ToString(), r[1].ToString());

            return dict;
        }

        public static Dictionary<string, string> GetFrmMainInternalText()
        {
            string query =
                "SELECT ControlName, Text FROM FrmMainInternalText";

            DataTable dt = GetDataSet(query).Tables[0];
            Dictionary<string, string> dict = new Dictionary<string, string>();

            foreach (DataRow r in dt.Rows)
                dict.Add(r[0].ToString(), r[1].ToString());

            return dict;
        }

        /// <summary>
        /// Method for getting data from database and binding to DataSet
        /// </summary>
        /// <param name="query">SQL query to send to the database</param>
        public static DataSet GetDataSet (string query)
        {
            ds = new DataSet();                                 // Make new DataSet
            dbAdapter = new SQLiteDataAdapter (query, dbConn);  // Make new DbAdapter using supplied query

            dbConn.Open();                                      // Open connection to database
            dbAdapter.Fill (ds);                                // Fill the DataSet with obtained data
            dbConn.Close();                                     // Close connection to database

            return ds;
        }
    }
}
