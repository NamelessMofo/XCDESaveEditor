using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static XCDESaveEditor.DataGridViewColumnFactory;

namespace XCDESaveEditor
{
    public partial class FrmMain : Form
    {
        #region Globals
        private XCDESaveData SaveData;

        private bool IsReloadingParty = false;
        private bool IsReloadingItems = false;
        private bool dgvIsEditing = false;

        private Dictionary<string, string> config;

        private Dictionary<string, string> FrmMainText;
        private Dictionary<string, string> FrmMainInternalText;
        #endregion Globals

        #region Constructor
        public FrmMain()
        {
            if (!(File.Exists (@"XCDEData.db")))
            {
                string title = "";
                string message = "";

                title = "Database Error!";
                message =
                    "ERROR: XCDEData.db not found!" + Environment.NewLine +
                    "Please ensure that XCDEData.db exists in same folder as this program!" + Environment.NewLine +
                    Environment.NewLine +
                    "Program will now exit.";

                MessageBox.Show (message, title, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit (1);
            }
            InitializeComponent();
        }
        #endregion Constructor

        #region Methods

        #region Main
        private void SetupComboBoxes()
        {
            #region Main
            int CbxPartyMembersIndex = CbxPartyMembers.SelectedIndex;
            CbxPartyMembers.Items.Clear();
            for (int i = 0; i < Party.Count; i++)
                CbxPartyMembers.Items.Add (FrmMainInternalText["PartyMembers"] + (i + 1).ToString());
            CbxPartyMembers.SelectedIndex = CbxPartyMembersIndex;

            CbxPartyMemberChar.BindDataTable (XCDEData.Characters());
            #endregion Main

            #region Character Main
            CbxCharID.BindDataTable (XCDEData.Characters());
            CbxCharWeaponID.BindDataTable (XCDEData.Weapons());
            CbxCharWeaponType.BindDataTable (XCDEData.ItemTypes());
            CbxCharHeadArmourID.BindDataTable (XCDEData.HeadArmour());
            CbxCharHeadArmourType.BindDataTable (XCDEData.ItemTypes());
            CbxCharTorsoArmourID.BindDataTable (XCDEData.TorsoArmour());
            CbxCharTorsoArmourType.BindDataTable (XCDEData.ItemTypes());
            CbxCharArmArmourID.BindDataTable (XCDEData.ArmArmour());
            CbxCharArmArmourType.BindDataTable (XCDEData.ItemTypes());
            CbxCharLegArmourID.BindDataTable (XCDEData.LegArmour());
            CbxCharLegArmourType.BindDataTable (XCDEData.ItemTypes());
            CbxCharFootArmourID.BindDataTable (XCDEData.FootArmour());
            CbxCharFootArmourType.BindDataTable (XCDEData.ItemTypes());
            #endregion Character Main

            #region Character Arts
            CbxCharNormalArt1ID.BindDataTable (XCDEData.Arts());
            CbxCharNormalArt2ID.BindDataTable (XCDEData.Arts());
            CbxCharNormalArt3ID.BindDataTable (XCDEData.Arts());
            CbxCharNormalArt4ID.BindDataTable (XCDEData.Arts());
            CbxCharTalentArtID.BindDataTable  (XCDEData.Arts());
            CbxCharNormalArt5ID.BindDataTable (XCDEData.Arts());
            CbxCharNormalArt6ID.BindDataTable (XCDEData.Arts());
            CbxCharNormalArt7ID.BindDataTable (XCDEData.Arts());
            CbxCharNormalArt8ID.BindDataTable (XCDEData.Arts());
            CbxCharMonadoArt1ID.BindDataTable (XCDEData.Arts());
            CbxCharMonadoArt2ID.BindDataTable (XCDEData.Arts());
            CbxCharMonadoArt3ID.BindDataTable (XCDEData.Arts());
            CbxCharMonadoArt4ID.BindDataTable (XCDEData.Arts());
            CbxCharMonTalentArtID.BindDataTable (XCDEData.Arts());
            CbxCharMonadoArt5ID.BindDataTable (XCDEData.Arts());
            CbxCharMonadoArt6ID.BindDataTable (XCDEData.Arts());
            CbxCharMonadoArt7ID.BindDataTable (XCDEData.Arts());
            CbxCharMonadoArt8ID.BindDataTable (XCDEData.Arts());
            CbxCharArtID.BindDataTable        (XCDEData.Arts());
            CbxCharArtUnlockLvl.BindDataTable (XCDEData.ArtsUnlockLvls());
            #endregion Character Arts

            #region Party
            #endregion Party

            #region Items
            UpdateNewItemComboBox();
            #endregion Items
        }
        private void LoadSaveData (string path)
        {
            try
            {
                SaveData = XCDESaveSerial.Deserialize (File.ReadAllBytes (path));

                #region Main
                NudMoney.Value = SaveData.Money;
                NudNoponstones.Value = SaveData.Noponstones;

                NudSaveTimeYear.Value = SaveData.SaveTime.Year;
                NudSaveTimeMonth.Value = SaveData.SaveTime.Month;
                NudSaveTimeDay.Value = SaveData.SaveTime.Day;
                NudSaveTimeHour.Value = SaveData.SaveTime.Hour;
                NudSaveTimeMinute.Value = SaveData.SaveTime.Minute;
                NudSaveTimeSecond.Value = SaveData.SaveTime.Second;
                NudSaveTimeMSecond.Value = SaveData.SaveTime.MSecond;
                NudSaveTimeUnkA.Value = SaveData.SaveTime.UnkA;
                NudSaveTimeUnkB.Value = SaveData.SaveTime.UnkB;

                NudInGameTimeDay.Value = SaveData.InGameTime.Days;
                NudInGameTimeHour.Value = SaveData.InGameTime.Hours;
                NudInGameTimeMinute.Value = SaveData.InGameTime.Minutes;
                NudInGameTimeSecond.Value = SaveData.InGameTime.Seconds;

                NudPlayTimeHour.Value = SaveData.PlayTime.Hours;
                NudPlayTimeMinute.Value = SaveData.PlayTime.Minutes;
                NudPlayTimeSecond.Value = SaveData.PlayTime.Seconds;
                #endregion Main

                #region Characters                
                RefreshPartyList();
                LbxChars.SelectedIndex = 0;
                LoadPartyMember (LbxChars.SelectedIndex);
                #endregion Characters

                #region Items
                // SetupEquipBox (DgvWeaponBox,      SaveData.WeaponBox,      XCDEData.Weapons());
                // SetupEquipBox (DgvHeadArmourBox,  SaveData.HeadArmourBox,  XCDEData.HeadArmour());
                // SetupEquipBox (DgvTorsoArmourBox, SaveData.TorsoArmourBox, XCDEData.TorsoArmour());
                // SetupEquipBox (DgvArmArmourBox,   SaveData.ArmArmourBox,   XCDEData.ArmArmour());
                // SetupEquipBox (DgvLegArmourBox,   SaveData.LegArmourBox,   XCDEData.LegArmour());
                // SetupEquipBox (DgvFootArmourBox,  SaveData.FootArmourBox,  XCDEData.FootArmour());
                // FIXME: need to consider better name generation for crystals & gems
                // SetupCrystalBox (DgvCrystalBox, SaveData.CrystalBox, XCDEData.Crystals());
                // SetupCrystalBox (DgvGemBox,    SaveData.GemBox, XCDEData.Gems());
                // SetupItemBox  (DgvCollectBox,     SaveData.CollectableBox, XCDEData.Collectables());
                // SetupItemBox  (DgvMaterialBox,    SaveData.MaterialBox,    XCDEData.Materials());
                SetupItemBox  (DgvKeyItemBox,     SaveData.KeyItemBox,     XCDEData.KeyItems());
                // SetupItemBox  (DgvArtsManBox,     SaveData.ArtsManualBox,  XCDEData.ArtsManuals());

                NudItemsSerial1.Value  = SaveData.Serials[0];
                NudItemsSerial2.Value  = SaveData.Serials[1];
                NudItemsSerial3.Value  = SaveData.Serials[2];
                NudItemsSerial4.Value  = SaveData.Serials[3];
                NudItemsSerial5.Value  = SaveData.Serials[4];
                NudItemsSerial6.Value  = SaveData.Serials[5];
                NudItemsSerial7.Value  = SaveData.Serials[6];
                NudItemsSerial8.Value  = SaveData.Serials[7];
                NudItemsSerial9.Value  = SaveData.Serials[8];
                NudItemsSerial10.Value = SaveData.Serials[9];
                NudItemsSerial11.Value = SaveData.Serials[10];
                NudItemsSerial12.Value = SaveData.Serials[11];
                NudItemsSerial13.Value = SaveData.Serials[12];
                #endregion Items

                #region Party
                // force refresh
                CbxPartyMembers.SelectedIndex = 1;
                CbxPartyMembers.SelectedIndex = 0;
                #endregion Party

                #region Unknown
                HbxUnk_0x000000.ByteProvider = new FixedLengthByteProvider (SaveData.Unk_0x000000);
                HbxUnk_0x000014.ByteProvider = new FixedLengthByteProvider (SaveData.Unk_0x000014);
                HbxUnk_0x03b5b0.ByteProvider = new FixedLengthByteProvider (SaveData.Unk_0x03b5b0);
                HbxUnk_0x04693c.ByteProvider = new FixedLengthByteProvider (SaveData.Unk_0x04693c);
                HbxUnk_0x151b44.ByteProvider = new FixedLengthByteProvider (SaveData.Unk_0x151b44);
                HbxUnk_0x152331.ByteProvider = new FixedLengthByteProvider (SaveData.Unk_0x152331);
                #endregion Unknown

                TbcMain.Enabled = true;
                SaveFileToolStripMenuItem.Enabled = true;
                BtnSave.Enabled = true;
                Console.WriteLine ("File Loaded.");
            }
            catch (Exception ex)
            {
                string message =
                    FrmMainInternalText["UnableToLoadSaveFile"] + Environment.NewLine +
                    Environment.NewLine +
                    ex.Message;
                string title = FrmMainInternalText["ErrorCannotLoadSaveFile"];

                FrmErrorBox ebx = new FrmErrorBox (message, title, ex.StackTrace);
                ebx.ShowDialog();
                //throw ex;
            }
        }
        private void LoadFile()
        {
            DialogResult = OfdSaveFile.ShowDialog();
            if (DialogResult == DialogResult.OK)
                LoadSaveData (OfdSaveFile.FileName);
        }
        private void SaveFile()
        {
            DialogResult result = SfdSaveFile.ShowDialog();

            if (result == DialogResult.OK)
            {
                try
                {
                    File.WriteAllBytes (SfdSaveFile.FileName, XCDESaveSerial.Serialize (SaveData));
                }
                catch (Exception ex)
                {
                    string title = FrmMainInternalText["ErrorCannotSaveSaveFile"];
                    string message =
                        FrmMainInternalText["UnableToSaveSaveFile"] + Environment.NewLine +
                        Environment.NewLine +
                        ex.Message;

                    FrmErrorBox ebx = new FrmErrorBox (message, title, ex.StackTrace);
                    ebx.ShowDialog();
                    //throw ex;
                }
            }
        }
        private void UpdateControlText (Control ctl)
        {
            if (FrmMainText.ContainsKey (ctl.Name))
                ctl.Text = FrmMainText[ctl.Name].Replace (@"\n", Environment.NewLine);

            foreach (Control chctl in ctl.Controls)
                UpdateControlText (chctl);
        }
        private void UpdateMenuStripText (MenuStrip ms)
        {
            foreach (ToolStripMenuItem tsmi in ms.Items)
            {
                if (FrmMainText.ContainsKey (tsmi.Name))
                    tsmi.Text = FrmMainText[tsmi.Name].Replace (@"\n", Environment.NewLine);

                foreach (ToolStripDropDownItem ddi in tsmi.DropDownItems)
                    UpdateDropDownItemText (ddi);
            }
        }
        private void UpdateDropDownItemText (ToolStripDropDownItem ddi)
        {
            if (FrmMainText.ContainsKey (ddi.Name))
                ddi.Text = FrmMainText[ddi.Name].Replace (@"\n", Environment.NewLine);

            foreach (ToolStripDropDownItem d in ddi.DropDownItems)
                UpdateDropDownItemText (d);
        }
        private void ReloadLanguage()
        {
            FrmMainText = XCDEData.GetFrmMainText();
            FrmMainInternalText = XCDEData.GetFrmMainInternalText();
            UpdateControlText (this);
            UpdateMenuStripText (MenuStrip);

            SetupComboBoxes();

            OfdSaveFile.Filter = FrmMainInternalText["XCDESaveFileDesc"] + "|*.sav|" + FrmMainInternalText["AllFilesDesc"] + "|*.*";
            SfdSaveFile.Filter = FrmMainInternalText["XCDESaveFileDesc"] + "|*.sav|" + FrmMainInternalText["AllFilesDesc"] + "|*.*";
        }
        private void ConfigEditReadOnlyUpdate()
        {
            config["EditReadOnlyData"] = AllowEditOfROValsToolStripMenuItem.Checked ? "true" : "false";
            SaveConfig (config, "settings.cfg");
        }
        private Dictionary<string, string> CreateNewConfig()
        {
            Dictionary<string, string> conf = new Dictionary<string, string>
            {
                { "EditReadOnlyData", "false" }
            };

            return conf;
        }
        private Dictionary<string, string> LoadConfig (string path)
        {
            try
            {
                Dictionary<string, string> conf = new Dictionary<string, string>();
                string[] data = File.ReadAllLines (path);

                foreach (string line in data)
                {
                    if (line.Contains ("="))
                    {
                        string[] setting = line.Split ('=');
                        conf.Add (setting[0], setting[1]);
                    }
                }

                return conf;
            }
            catch (FileNotFoundException)
            {
                string message = FrmMainInternalText["SettingsFileNotFound"];

                MessageBox.Show (message, FrmMainInternalText["WarningSettingsFileNotFound"], MessageBoxButtons.OK, MessageBoxIcon.Warning);
                Dictionary<string, string> conf = CreateNewConfig();
                SaveConfig (conf, "settings.cfg");

                return conf;
            }

            catch (Exception e)
            {
                string message =
                    FrmMainInternalText["UnableToLoadSettingsFile"] + Environment.NewLine +
                    e.Message;

                MessageBox.Show (message, FrmMainInternalText["WarningCannotLoadSettingsFile"], MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return CreateNewConfig();
            }
        }
        private void SaveConfig (Dictionary<string, string> conf, string path)
        {
            string output = "";
            foreach (KeyValuePair<string, string> kv in conf)
                output += kv.Key + "=" + kv.Value + Environment.NewLine;

            try
            {
                File.WriteAllText (path, output);
            }
            catch (Exception ex)
            {
                string title = FrmMainInternalText["ErrorCannotSaveSettingsFile"];
                string message =
                    FrmMainInternalText["UnableToSaveSettingsFile"] + Environment.NewLine +
                    Environment.NewLine +
                    ex.Message;

                FrmErrorBox ebx = new FrmErrorBox (message, title, ex.StackTrace);
                ebx.ShowDialog();
            }
        }
        #endregion Main

        #region Characters
        private void RefreshPartyList()
        {
            IsReloadingParty = true;
            int index = LbxChars.SelectedIndex;

            LbxChars.DataSource = null;
            LbxChars.DisplayMember = "Name";
            // LbxChars.DataSource = SaveData.PartyMembers;
            LbxChars.DataSource = XCDEData.CharRange (1, 15);

            if (LbxChars.SelectedIndex != index)
                LbxChars.SelectedIndex = index;
            if (LbxChars.SelectedIndex < 0)
                LbxChars.SelectedIndex = 0;

            IsReloadingParty = false;
        }
        private void LoadPartyMember (int index)
        {
            PartyMember pm = SaveData.PartyMembers[index];
            ushort CharID = (ushort) (index + 1);

            #region Character Main
            NudCharID.Value         = CharID;
            CbxCharID.SelectedValue = CharID;
            ChkCharInParty.Checked  = CheckIfCharInParty (CharID);

            NudCharLevel.Value        = pm.Level;
            NudCharExpertLevel.Value  = pm.ExpertModeLevel;

            NudCharExp.Value          = pm.EXP;
            NudCharExpertExp.Value    = pm.ExpertModeEXP;
            NudCharExpertRsvExp.Value = pm.ExpertModeReserveEXP;
            #endregion Character Main

            #region Character Equipment
            NudCharWeaponID.Value     = SaveData.WeaponBox[SaveData.PartyMembers[LbxChars.SelectedIndex].WeaponEquip.ID].FullID.ID;
            NudCharWeaponType.Value   = (ushort) SaveData.PartyMembers[LbxChars.SelectedIndex].WeaponEquip.TypeID;
            NudCharWeaponIndex.Value  = SaveData.PartyMembers[LbxChars.SelectedIndex].WeaponEquip.ID;

            NudCharHeadArmourID.Value    = SaveData.HeadArmourBox[SaveData.PartyMembers[LbxChars.SelectedIndex].HeadEquip.ID].FullID.ID;
            NudCharHeadArmourType.Value  = (ushort) SaveData.PartyMembers[LbxChars.SelectedIndex].HeadEquip.TypeID;
            NudCharHeadArmourIndex.Value = SaveData.PartyMembers[LbxChars.SelectedIndex].HeadEquip.ID;

            NudCharTorsoArmourID.Value    = SaveData.TorsoArmourBox[SaveData.PartyMembers[LbxChars.SelectedIndex].TorsoEquip.ID].FullID.ID;
            NudCharTorsoArmourType.Value  = (ushort) SaveData.PartyMembers[LbxChars.SelectedIndex].TorsoEquip.TypeID;
            NudCharTorsoArmourIndex.Value = SaveData.PartyMembers[LbxChars.SelectedIndex].TorsoEquip.ID;

            NudCharArmArmourID.Value    = SaveData.ArmArmourBox[SaveData.PartyMembers[LbxChars.SelectedIndex].ArmEquip.ID].FullID.ID;
            NudCharArmArmourType.Value  = (ushort) SaveData.PartyMembers[LbxChars.SelectedIndex].ArmEquip.TypeID;
            NudCharArmArmourIndex.Value = SaveData.PartyMembers[LbxChars.SelectedIndex].ArmEquip.ID;

            NudCharLegArmourID.Value    = SaveData.LegArmourBox[SaveData.PartyMembers[LbxChars.SelectedIndex].LegEquip.ID].FullID.ID;
            NudCharLegArmourType.Value  = (ushort) SaveData.PartyMembers[LbxChars.SelectedIndex].LegEquip.TypeID;
            NudCharLegArmourIndex.Value = SaveData.PartyMembers[LbxChars.SelectedIndex].LegEquip.ID;

            NudCharFootArmourID.Value    = SaveData.FootArmourBox[SaveData.PartyMembers[LbxChars.SelectedIndex].FootEquip.ID].FullID.ID;
            NudCharFootArmourType.Value  = (ushort) SaveData.PartyMembers[LbxChars.SelectedIndex].FootEquip.TypeID;
            NudCharFootArmourIndex.Value = SaveData.PartyMembers[LbxChars.SelectedIndex].FootEquip.ID;
            #endregion Character Equipment

            #region Character Arts
            NudCharNormalArt1ID.Value = SaveData.PartyMembers[LbxChars.SelectedIndex].Arts[1];
            NudCharNormalArt2ID.Value = SaveData.PartyMembers[LbxChars.SelectedIndex].Arts[2];
            NudCharNormalArt3ID.Value = SaveData.PartyMembers[LbxChars.SelectedIndex].Arts[3];
            NudCharNormalArt4ID.Value = SaveData.PartyMembers[LbxChars.SelectedIndex].Arts[4];
            NudCharTalentArtID.Value  = SaveData.PartyMembers[LbxChars.SelectedIndex].Arts[0];
            NudCharNormalArt5ID.Value = SaveData.PartyMembers[LbxChars.SelectedIndex].Arts[5];
            NudCharNormalArt6ID.Value = SaveData.PartyMembers[LbxChars.SelectedIndex].Arts[6];
            NudCharNormalArt7ID.Value = SaveData.PartyMembers[LbxChars.SelectedIndex].Arts[7];
            NudCharNormalArt8ID.Value = SaveData.PartyMembers[LbxChars.SelectedIndex].Arts[8];

            NudCharMonadoArt1ID.Value = SaveData.PartyMembers[LbxChars.SelectedIndex].MonadoArts[1];
            NudCharMonadoArt2ID.Value = SaveData.PartyMembers[LbxChars.SelectedIndex].MonadoArts[2];
            NudCharMonadoArt3ID.Value = SaveData.PartyMembers[LbxChars.SelectedIndex].MonadoArts[3];
            NudCharMonadoArt4ID.Value = SaveData.PartyMembers[LbxChars.SelectedIndex].MonadoArts[4];
            NudCharMonTalentArtID.Value = SaveData.PartyMembers[LbxChars.SelectedIndex].MonadoArts[0];
            NudCharMonadoArt5ID.Value = SaveData.PartyMembers[LbxChars.SelectedIndex].MonadoArts[5];
            NudCharMonadoArt6ID.Value = SaveData.PartyMembers[LbxChars.SelectedIndex].MonadoArts[6];
            NudCharMonadoArt7ID.Value = SaveData.PartyMembers[LbxChars.SelectedIndex].MonadoArts[7];
            NudCharMonadoArt8ID.Value = SaveData.PartyMembers[LbxChars.SelectedIndex].MonadoArts[8];

            NudCharArtID.Value        = 0;
            NudCharArtUnlockLvl.Value = 0;
            NudCharArtLevel.Value     = 0;
            NudCharArtPoints.Value    = SaveData.PartyMembers[LbxChars.SelectedIndex].AP;
            #endregion Character Arts

            #region Character Unknown
            HbxCharUnk_0x010.ByteProvider = new FixedLengthByteProvider (SaveData.PartyMembers[LbxChars.SelectedIndex].Unk1);
            HbxCharUnk_0x02c.ByteProvider = new FixedLengthByteProvider (SaveData.PartyMembers[LbxChars.SelectedIndex].Unk2);
            HbxCharUnk_0x06c.ByteProvider = new FixedLengthByteProvider (SaveData.PartyMembers[LbxChars.SelectedIndex].Unk3);
            HbxCharUnk_0x0a4.ByteProvider = new FixedLengthByteProvider (SaveData.PartyMembers[LbxChars.SelectedIndex].Unk4);
            HbxCharUnk_0x0f8.ByteProvider = new FixedLengthByteProvider (SaveData.PartyMembers[LbxChars.SelectedIndex].Unk5);
            #endregion Character Unknown

            ToggleCharEditControls (AllowEditOfROValsToolStripMenuItem.Checked);
        }
        private bool CheckIfCharInParty (UInt16 charid)
        {
            for (uint i = 0; i < SaveData.Party.Characters.Length; i++)
            {
                if (SaveData.Party.Characters[i] == (Character) charid)
                    return true;
            }
            return false;
        }
        private void CreateCharArtComboBox (ComboBox box, int value = 0)
        {
            box.DataSource    = XCDEData.Arts();
            box.DisplayMember = "Name";
            box.ValueMember   = "ID";
            box.SelectedValue = value;
        }
        private void CreateCharSkillComboBox (ComboBox box, int value = 0)
        {
            box.DataSource    = XCDEData.Skills();
            box.DisplayMember = "Name";
            box.ValueMember   = "ID";
            box.SelectedValue = value;
        }
        private void ToggleCharEditControls (bool enabled)
        {
            NudPartyMemberCount.Enabled  = enabled;
            NudCharID.Enabled            = enabled;
            CbxCharID.Enabled            = enabled;
            ChkCharInParty.Enabled       = enabled;
            NudCharWeaponID.Enabled      = enabled;
            CbxCharWeaponID.Enabled      = enabled;
            NudCharHeadArmourID.Enabled  = enabled;
            CbxCharHeadArmourID.Enabled  = enabled;
            NudCharTorsoArmourID.Enabled = enabled;
            CbxCharTorsoArmourID.Enabled = enabled;
            NudCharArmArmourID.Enabled   = enabled;
            CbxCharArmArmourID.Enabled   = enabled;
            NudCharLegArmourID.Enabled   = enabled;
            CbxCharLegArmourID.Enabled   = enabled;
            NudCharFootArmourID.Enabled  = enabled;
            CbxCharFootArmourID.Enabled  = enabled;
        }
        #endregion Characters

        #region Items
        private DataGridView SetupItemBox (DataGridView dgv, Item[] items, DataTable ItemIDs)
        {
            dgv.CellFormatting += new DataGridViewCellFormattingEventHandler (Dgv_CellFormatting);
            dgv.AutoGenerateColumns = false;
            dgv.DataSource = null;

            BindingList<Item> ds = new BindingList<Item> (items);

            dgv.Columns.Clear();
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Index",     FrmMainInternalText["ItemColumnIndex"],    "ushort", null, 50, 0, 500));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "ID",        FrmMainInternalText["ItemColumnID"],       "UInt16", null, 50, 0, 5120));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.ComboBox,      "ID",        FrmMainInternalText["ItemColumnName"],     null,     ItemIDs, 175));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Type",      FrmMainInternalText["ItemColumnType"],     "Byte",   null, 50, 0, 63));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.ComboBox,      "Type",      FrmMainInternalText["ItemColumnTypeName"], null,     XCDEData.ItemTypes(), 150));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Quantity",  FrmMainInternalText["ItemColumnQty"],      "UInt16", null, 50, 0, 99));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.CheckBox,      "Exists",    FrmMainInternalText["ItemColumnExists"],   null,     null, 55));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.CheckBox,      "Favourite", FrmMainInternalText["ItemColumnFav"],      null,     null, 55));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Serial",    FrmMainInternalText["ItemColumnSerial"],   "UInt32", null, 75, 0, 67108863));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Unk1",      FrmMainInternalText["ItemColumnUnk1"],     "UInt16", null, 50, 0, 65535));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Unk2",      FrmMainInternalText["ItemColumnUnk2"],     "UInt16", null, 50, 0, 65535));
            dgv.DataSource = ds;

            return dgv;
        }
        private DataGridView SetupEquipBox (DataGridView dgv, EquipItem[] items, DataTable ItemIDs)
        {
            dgv.CellFormatting += new DataGridViewCellFormattingEventHandler (Dgv_CellFormatting);
            dgv.AutoGenerateColumns = false;
            dgv.DataSource = null;

            BindingList<EquipItem> ds = new BindingList<EquipItem> (items);

            dgv.Columns.Clear();
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Index",       FrmMainInternalText["ItemColumnIndex"],           "UInt16", null, 50, 0, 500));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "ID",          FrmMainInternalText["ItemColumnID"],              "UInt16", null, 50, 0, 8191));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.ComboBox,      "ID",          FrmMainInternalText["ItemColumnName"],            null,     ItemIDs, 175));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Type",        FrmMainInternalText["ItemColumnType"],            "Byte",   null, 50, 0, 63));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.ComboBox,      "Type",        FrmMainInternalText["ItemColumnTypeName"],        null,     XCDEData.ItemTypes(), 150));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Qty",         FrmMainInternalText["ItemColumnQty"],             "UInt16", null, 50, 0, 1023));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.CheckBox,      "Exists",      FrmMainInternalText["ItemColumnExists"],          null,     null, 55));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.CheckBox,      "Favourite",   FrmMainInternalText["ItemColumnFav"],             null,     null, 55));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Serial",      FrmMainInternalText["ItemColumnSerial"],          "UInt32", null, 75, 0, 67108863));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.CheckBox,      "Equipped",    FrmMainInternalText["ItemColumnEquipped"],        null,     null, 55));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Weight",      FrmMainInternalText["ItemColumnWeight"],          "Byte",   null, 50, 0, 63));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Slots",       FrmMainInternalText["ItemColumnSlots"],           "Byte",   null, 50, 0, 63));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Gem1",        FrmMainInternalText["ItemColumnGem1"],            "UInt16", null, 50, 0, 8191));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Gem1Type",    FrmMainInternalText["ItemColumnGem1Type"],        "UInt16", null, 50, 0, 63));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.ComboBox,      "Gem1Type",    FrmMainInternalText["ItemColumnGem1TypeName"],    null,     XCDEData.ItemTypes(), 150));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Gem1Alt",     FrmMainInternalText["ItemColumnGem1Alt"],         "UInt16", null, 50, 0, 8191));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Gem1AltType", FrmMainInternalText["ItemColumnGem1AltType"],     "UInt16", null, 50, 0, 63));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.ComboBox,      "Gem1AltType", FrmMainInternalText["ItemColumnGem1AltTypeName"], null,     XCDEData.ItemTypes(), 150));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Gem2",        FrmMainInternalText["ItemColumnGem2"],            "UInt16", null, 50, 0, 8191));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Gem2Type",    FrmMainInternalText["ItemColumnGem2Type"],        "UInt16", null, 50, 0, 63));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.ComboBox,      "Gem2Type",    FrmMainInternalText["ItemColumnGem2TypeName"],    null,     XCDEData.ItemTypes(), 150));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Gem2Alt",     FrmMainInternalText["ItemColumnGem2Alt"],         "UInt16", null, 50, 0, 8191));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Gem2AltType", FrmMainInternalText["ItemColumnGem2AltType"],     "UInt16", null, 50, 0, 63));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.ComboBox,      "Gem2AltType", FrmMainInternalText["ItemColumnGem2AltTypeName"], null,     XCDEData.ItemTypes(), 150));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Gem3",        FrmMainInternalText["ItemColumnGem3"],            "UInt16", null, 50, 0, 8191));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Gem3Type",    FrmMainInternalText["ItemColumnGem3Type"],        "UInt16", null, 50, 0, 63));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.ComboBox,      "Gem3Type",    FrmMainInternalText["ItemColumnGem3TypeName"],    null,     XCDEData.ItemTypes(), 150));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Gem3Alt",     FrmMainInternalText["ItemColumnGem3Alt"],         "UInt16", null, 50, 0, 8191));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Gem3AltType", FrmMainInternalText["ItemColumnGem3AltType"],     "UInt16", null, 50, 0, 63));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.ComboBox,      "Gem3AltType", FrmMainInternalText["ItemColumnGem3AltTypeName"], null,     XCDEData.ItemTypes(), 150));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Unk1",        FrmMainInternalText["ItemColumnUnk1"],            "UInt16", null, 50, 0, 65535));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Unk2",        FrmMainInternalText["ItemColumnUnk2"],            "UInt16", null, 50, 0, 65535));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Unk3",        FrmMainInternalText["ItemColumnUnk3"],            "UInt16", null, 50, 0, 65535));
            dgv.DataSource = ds;

            return dgv;
        }
        private DataGridView SetupCrystalBox (DataGridView dgv, CrystalItem[] items, DataTable ItemIDs)
        {
            dgv.CellFormatting += new DataGridViewCellFormattingEventHandler (Dgv_CellFormatting);
            dgv.AutoGenerateColumns = false;
            dgv.DataSource = null;

            BindingList<CrystalItem> ds = new BindingList<CrystalItem> (items);

            dgv.Columns.Clear();
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Index",      FrmMainInternalText["ItemColumnIndex"],       "UInt16", null, 50, 0, 500));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "ID",         FrmMainInternalText["ItemColumnID"],          "UInt16", null, 50, 0, 8191));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.ComboBox,      "ID",         FrmMainInternalText["ItemColumnName"],        null,     ItemIDs, 175));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Type",       FrmMainInternalText["ItemColumnType"],        "Byte",   null, 50, 0, 63));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.ComboBox,      "Type",       FrmMainInternalText["ItemColumnTypeName"],    null,     XCDEData.ItemTypes(), 150));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Qty",        FrmMainInternalText["ItemColumnQty"],         "UInt16", null, 50, 0, 1023));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.CheckBox,      "Exists",     FrmMainInternalText["ItemColumnExists"],      null,     null, 55));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.CheckBox,      "Favourite",  FrmMainInternalText["ItemColumnFav"],         null,     null, 55));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Serial",     FrmMainInternalText["ItemColumnSerial"],      "UInt32", null, 75, 0, 67108863));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.CheckBox,      "Equipped",   FrmMainInternalText["ItemColumnEquipped"],    null,     null, 55));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "CrystalNameID", FrmMainInternalText["ItemColumnCrystalNameID"], "UInt16", null, 50, 0, 8191));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Rank",       FrmMainInternalText["ItemColumnRank"],        "Byte",   null, 50, 0, 15));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Element",    FrmMainInternalText["ItemColumnElement"],     "Byte",   null, 50, 0, 31));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.ComboBox,      "Element",    FrmMainInternalText["ItemColumnElementName"], null,     XCDEData.Elements(), 150));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.CheckBox,      "IsCylinder", FrmMainInternalText["ItemColumnIsCylinder"],  null,     null, 60));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Buffs",      FrmMainInternalText["ItemColumnBuffs"],       "UInt16", null, 50, 0, 63));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Buff1ID",    FrmMainInternalText["ItemColumnBuff1ID"],     "UInt16", null, 50, 0, 255));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.ComboBox,      "Buff1ID",    FrmMainInternalText["ItemColumnBuff1IDName"], null,     XCDEData.Buffs(), 150));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Buff1Value", FrmMainInternalText["ItemColumnBuff1Value"],  "UInt16", null, 50, 0, 1023));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Buff2ID",    FrmMainInternalText["ItemColumnBuff2ID"],     "UInt16", null, 50, 0, 255));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.ComboBox,      "Buff2ID",    FrmMainInternalText["ItemColumnBuff2IDName"], null,     XCDEData.Buffs(), 150));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Buff2Value", FrmMainInternalText["ItemColumnBuff2Value"],  "UInt16", null, 50, 0, 1023));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Buff3ID",    FrmMainInternalText["ItemColumnBuff3ID"],     "UInt16", null, 50, 0, 255));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.ComboBox,      "Buff3ID",    FrmMainInternalText["ItemColumnBuff3IDName"], null,     XCDEData.Buffs(), 150));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Buff3Value", FrmMainInternalText["ItemColumnBuff3Value"],  "UInt16", null, 50, 0, 1023));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Buff4ID",    FrmMainInternalText["ItemColumnBuff4ID"],     "UInt16", null, 50, 0, 255));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.ComboBox,      "Buff4ID",    FrmMainInternalText["ItemColumnBuff4IDName"], null,     XCDEData.Buffs(), 150));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Buff4Value", FrmMainInternalText["ItemColumnBuff4Value"],  "UInt16", null, 50, 0, 1023));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Unk1",       FrmMainInternalText["ItemColumnUnk1"],        "UInt16", null, 50, 0, 65535));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Unk2",       FrmMainInternalText["ItemColumnUnk2"],        "UInt16", null, 50, 0, 65535));
            dgv.Columns.Add (CreateDgvColumn (DgvColumnType.NumericUpDown, "Unk3",       FrmMainInternalText["ItemColumnUnk3"],        "Byte",   null, 50, 0, 255));
            dgv.DataSource = ds;

            return dgv;
        }
        private void UpdateNewItemComboBox()
        {
            CbxItemAddNewID.DataSource = null;
            switch (TbcItems.SelectedIndex)
            {
                case 0:
                    CbxItemAddNewID.BindDataTable (XCDEData.Weapons());
                    break;
                case 1:
                    CbxItemAddNewID.BindDataTable (XCDEData.HeadArmour());
                    break;
                case 2:
                    CbxItemAddNewID.BindDataTable (XCDEData.TorsoArmour());
                    break;
                case 3:
                    CbxItemAddNewID.BindDataTable (XCDEData.ArmArmour());
                    break;
                case 4:
                    CbxItemAddNewID.BindDataTable (XCDEData.LegArmour());
                    break;
                case 5:
                    CbxItemAddNewID.BindDataTable (XCDEData.FootArmour());
                    break;
                case 6:
                    CbxItemAddNewID.BindDataTable (XCDEData.Crystals());
                    break;
                case 7:
                    CbxItemAddNewID.BindDataTable (XCDEData.Gems());
                    break;
                case 8:
                    CbxItemAddNewID.BindDataTable (XCDEData.Collectables());
                    break;
                case 9:
                    CbxItemAddNewID.BindDataTable (XCDEData.Materials());
                    break;
                case 10:
                    CbxItemAddNewID.BindDataTable (XCDEData.KeyItems());
                    break;
                case 11:
                    CbxItemAddNewID.BindDataTable (XCDEData.ArtsManuals());
                    break;
            }

            CbxItemAddNewID.SelectedValue = (UInt16) NudItemAddNewID.Value;
        }
        private UInt32 ItemGetNewSerial (DataGridView dgv)
        {
            List<UInt32> serials = new List<UInt32>();
            foreach (DataGridViewRow row in dgv.Rows)
                serials.Add ((UInt32) row.Cells[8].Value);

            UInt32 serial = 1;
            while (serials.Contains (serial))
                serial++;

            return serial;
        }
        private void FindInItemBox()
        {
            DataGridView dgv = ((DataGridView) TbcItems.SelectedTab.Controls[0]);
            DataTable items;

            switch (TbcItems.SelectedIndex)
            {
                case 0:
                    items = XCDEData.Weapons();
                    break;
                case 1:
                    items = XCDEData.HeadArmour();
                    break;
                case 2:
                    items = XCDEData.TorsoArmour();
                    break;
                case 3:
                    items = XCDEData.ArmArmour();
                    break;
                case 4:
                    items = XCDEData.LegArmour();
                    break;
                case 5:
                    items = XCDEData.FootArmour();
                    break;
                case 6:
                    items = XCDEData.Crystals();
                    break;
                case 7:
                    items = XCDEData.Gems();
                    break;
                case 8:
                    items = XCDEData.Collectables();
                    break;
                case 9:
                    items = XCDEData.Materials();
                    break;
                case 10:
                    items = XCDEData.KeyItems();
                    break;
                case 11:
                    items = XCDEData.ArtsManuals();
                    break;
                default:
                    return;
            }

            items.PrimaryKey = new DataColumn[] { items.Columns[0] };
            List<string> itemNames = new List<string>();

            foreach (DataGridViewRow row in dgv.Rows)
            {
                DataRow[] rows = items.Select ("ID = " + row.Cells[1].Value);
                string name = "";
                if (rows.Length > 0)
                    name = (string) (rows)[0]["Name"];
                itemNames.Add (name);
            }

            int index = itemNames.FindIndex (s => s.ToLower().Contains (TxtItemSearch.Text.ToLower()));
            Console.WriteLine (index);
            if (index >= 0)
            {
                dgv.ClearSelection();
                dgv.CurrentCell = dgv.Rows[index].Cells[1];
                dgv.Rows[index].Selected = true;
            }
        }
        #endregion Items

        #endregion Methods

        #region Events

        #region Main
        private void frmMain_Load (object sender, EventArgs e)
        {
            Text = Application.ProductName + " ver" + Application.ProductVersion + " by " + Application.CompanyName;

            ReloadLanguage();   // have to do twice to have SOME default text
            config = LoadConfig ("settings.cfg");

            bool editReadOnlyData = false;
            if (!config.ContainsKey ("EditReadOnlyData") || ! Boolean.TryParse (config["EditReadOnlyData"], out editReadOnlyData))
            {
                AllowEditOfROValsToolStripMenuItem.Checked = editReadOnlyData;
                ConfigEditReadOnlyUpdate();
            }
            else
            {
                AllowEditOfROValsToolStripMenuItem.Checked = Boolean.Parse (config["EditReadOnlyData"]);
            }
            ReloadLanguage();
        }
        private void aboutToolStripMenuItem_Click (object sender, EventArgs e)
        {
            frmAbout frmAbout = new frmAbout (FrmMainInternalText["AppDescBf2"], this.Icon);
            frmAbout.ShowDialog();
        }
        private void fAQToolStripMenuItem_Click (object sender, EventArgs e)
        {
            frmFAQ frmFAQ = new frmFAQ (this.Icon);
            frmFAQ.ShowDialog();
        }
        private void LoadFileToolStripMenuItem_Click (object sender, EventArgs e) => LoadFile();
        private void SaveFileToolStripMenuItem_Click (object sender, EventArgs e) => SaveFile();
        private void ExitToolStripMenuItem_Click (object sender, EventArgs e) => Application.Exit();
        private void AllowEditOfROValsToolStripMenuItem_Click (object sender, EventArgs e)
        {
            AllowEditOfROValsToolStripMenuItem.Checked = !AllowEditOfROValsToolStripMenuItem.Checked;
            ToggleCharEditControls (AllowEditOfROValsToolStripMenuItem.Checked);
            ConfigEditReadOnlyUpdate();
        }
        private void BtnLoad_Click (object sender, EventArgs e) => LoadFile();
        private void BtnSave_Click (object sender, EventArgs e) => SaveFile();
        #endregion Main

        #region Main
        private void NudMoney_ValueChanged       (object sender, EventArgs e) => SaveData.Money       = (UInt32) NudMoney.Value;
        private void NudNoponstones_ValueChanged (object sender, EventArgs e) => SaveData.Noponstones = (UInt32) NudNoponstones.Value;

        private void NudSaveTimeYear_ValueChanged    (object sender, EventArgs e) => SaveData.SaveTime.Year    = (UInt16) NudSaveTimeYear.Value;
        private void NudSaveTimeMonth_ValueChanged   (object sender, EventArgs e) => SaveData.SaveTime.Month   = (Byte) NudSaveTimeMonth.Value;
        private void NudSaveTimeDay_ValueChanged     (object sender, EventArgs e) => SaveData.SaveTime.Day     = (Byte) NudSaveTimeDay.Value;
        private void NudSaveTimeHour_ValueChanged    (object sender, EventArgs e) => SaveData.SaveTime.Hour    = (Byte) NudSaveTimeHour.Value;
        private void NudSaveTimeMinute_ValueChanged  (object sender, EventArgs e) => SaveData.SaveTime.Minute  = (Byte) NudSaveTimeMinute.Value;
        private void NudSaveTimeSecond_ValueChanged  (object sender, EventArgs e) => SaveData.SaveTime.Second  = (Byte) NudSaveTimeSecond.Value;
        private void NudSaveTimeMSecond_ValueChanged (object sender, EventArgs e) => SaveData.SaveTime.MSecond = (UInt16) NudSaveTimeMSecond.Value;
        private void NudSaveTimeUnkA_ValueChanged    (object sender, EventArgs e) => SaveData.SaveTime.UnkA    = (UInt16) NudSaveTimeUnkA.Value;
        private void NudSaveTimeUnkB_ValueChanged    (object sender, EventArgs e) => SaveData.SaveTime.UnkB    = (Byte) NudSaveTimeUnkB.Value;

        private void NudInGameTimeDay_ValueChanged    (object sender, EventArgs e) => SaveData.InGameTime.Days    = (UInt16) NudInGameTimeDay.Value;
        private void NudInGameTimeHour_ValueChanged   (object sender, EventArgs e) => SaveData.InGameTime.Hours   = (Byte) NudInGameTimeHour.Value;
        private void NudInGameTimeMinute_ValueChanged (object sender, EventArgs e) => SaveData.InGameTime.Minutes = (Byte) NudInGameTimeMinute.Value;
        private void NudInGameTimeSecond_ValueChanged (object sender, EventArgs e) => SaveData.InGameTime.Seconds = (Byte) NudInGameTimeSecond.Value;

        private void NudPlayTimeHour_ValueChanged   (object sender, EventArgs e) => SaveData.PlayTime.Hours   = (UInt16) NudPlayTimeHour.Value;
        private void NudPlayTimeMinute_ValueChanged (object sender, EventArgs e) => SaveData.PlayTime.Minutes = (Byte) NudPlayTimeMinute.Value;
        private void NudPlayTimeSecond_ValueChanged (object sender, EventArgs e) => SaveData.PlayTime.Seconds = (Byte) NudPlayTimeSecond.Value;

        private void CbxPartyMembers_SelectedIndexChanged (object sender, EventArgs e)
        {
            NudPartyMemberChar.Value = (decimal) SaveData.Party.Characters[CbxPartyMembers.SelectedIndex];
            CbxPartyMemberChar.SelectedValue = SaveData.Party.Characters[CbxPartyMembers.SelectedIndex];
        }
        private void NudPartyMemberChar_ValueChanged (object sender, EventArgs e)
        {
            SaveData.Party.Characters[CbxPartyMembers.SelectedIndex] = (Character) NudPartyMemberChar.Value;
            CbxPartyMemberChar.SelectedValue = (UInt16) NudPartyMemberChar.Value;
            NudPartyMemberCount_UpdatePartyMemberCount();
        }
        private void CbxPartyMemberChar_SelectionChangeCommitted (object sender, EventArgs e) => NudPartyMemberChar.Value = Convert.ToUInt16 (CbxPartyMemberChar.SelectedValue);
        private void NudPartyMemberCount_UpdatePartyMemberCount()
        {
            Byte count = 0;

            for (byte i = 0; i < SaveData.Party.Characters.Length; i++)
                if (SaveData.Party.Characters[i] != Character.None)
                    count++;
                else
                    break;
            NudPartyMemberCount.Value = count;
            SaveData.Party.PartyMembersCount = count;
        }
        #endregion Main

        #region Characters

        #region Characters Main
        private void LbxChars_SelectedIndexChanged (object sender, EventArgs e)
        {
            if (!IsReloadingParty)
                LoadPartyMember (LbxChars.SelectedIndex);
        }
        #endregion Characters Main

        #region Characters Stats
        private void NudCharLevel_ValueChanged        (object sender, EventArgs e) => SaveData.PartyMembers[LbxChars.SelectedIndex].Level           = (UInt32) NudCharLevel.Value;
        private void NudCharExpertLevel_ValueChanged  (object sender, EventArgs e) => SaveData.PartyMembers[LbxChars.SelectedIndex].ExpertModeLevel = (UInt32) NudCharExpertLevel.Value;
        private void NudCharExp_ValueChanged          (object sender, EventArgs e) => SaveData.PartyMembers[LbxChars.SelectedIndex].EXP             = (UInt32) NudCharExp.Value;
        private void NudCharExpertExp_ValueChanged    (object sender, EventArgs e) => SaveData.PartyMembers[LbxChars.SelectedIndex].ExpertModeEXP   = (UInt32) NudCharExpertExp.Value;
        private void NudCharExpertRsvExp_ValueChanged (object sender, EventArgs e) => SaveData.PartyMembers[LbxChars.SelectedIndex].ExpertModeReserveEXP = (UInt32) NudCharExpertRsvExp.Value;
        #endregion Characters Stats

        #region Character Equipment
        private void NudCharWeaponType_ValueChanged (object sender, EventArgs e)
        {
            Item[] box = null;

            switch ((ItemType) NudCharWeaponType.Value)
            {
                case ItemType.Weapon:
                    box = SaveData.WeaponBox;
                    break;
                case ItemType.Gem:
                    box = SaveData.GemBox;
                    break;
                case ItemType.HeadArmour:
                    box = SaveData.HeadArmourBox;
                    break;
                case ItemType.TorsoArmour:
                    box = SaveData.TorsoArmourBox;
                    break;
                case ItemType.ArmArmour:
                    box = SaveData.ArmArmourBox;
                    break;
                case ItemType.LegArmour:
                    box = SaveData.LegArmourBox;
                    break;
                case ItemType.FootArmour:
                    box = SaveData.FootArmourBox;
                    break;
                case ItemType.Crystal:
                    box = SaveData.CrystalBox;
                    break;
                case ItemType.Collectable:
                    box = SaveData.CollectableBox;
                    break;
                case ItemType.Material:
                    box = SaveData.MaterialBox;
                    break;
                case ItemType.KeyItem:
                    box = SaveData.KeyItemBox;
                    break;
                case ItemType.ArtsManual:
                    box = SaveData.ArtsManualBox;
                    break;
                default:
                    box = SaveData.WeaponBox;
                    break;
            }
            NudCharWeaponID.Value           = Convert.ToUInt16 (box[(int) NudCharWeaponIndex.Value].FullID.ID);
            CbxCharWeaponID.SelectedValue   = (UInt16) NudCharWeaponID.Value;
            CbxCharWeaponType.SelectedValue = (Byte) NudCharWeaponType.Value;
            SaveData.PartyMembers[LbxChars.SelectedIndex].WeaponEquip.TypeID = (ItemType) NudCharWeaponType.Value;
        }
        private void CbxCharWeaponType_SelectionChangeCommitted (object sender, EventArgs e)
        {
            NudCharWeaponType.Value = Convert.ToUInt16 (CbxCharWeaponType.SelectedValue);
        }
        private void NudCharWeaponIndex_ValueChanged (object sender, EventArgs e)
        {
            Item[] box = null;

            switch (SaveData.PartyMembers[LbxChars.SelectedIndex].WeaponEquip.TypeID)
            {
                case ItemType.Weapon:
                    box = SaveData.WeaponBox;
                    break;
                case ItemType.Gem:
                    box = SaveData.GemBox;
                    break;
                case ItemType.HeadArmour:
                    box = SaveData.HeadArmourBox;
                    break;
                case ItemType.TorsoArmour:
                    box = SaveData.TorsoArmourBox;
                    break;
                case ItemType.ArmArmour:
                    box = SaveData.ArmArmourBox;
                    break;
                case ItemType.LegArmour:
                    box = SaveData.LegArmourBox;
                    break;
                case ItemType.FootArmour:
                    box = SaveData.FootArmourBox;
                    break;
                case ItemType.Crystal:
                    box = SaveData.CrystalBox;
                    break;
                case ItemType.Collectable:
                    box = SaveData.CollectableBox;
                    break;
                case ItemType.Material:
                    box = SaveData.MaterialBox;
                    break;
                case ItemType.KeyItem:
                    box = SaveData.KeyItemBox;
                    break;
                case ItemType.ArtsManual:
                    box = SaveData.ArtsManualBox;
                    break;
                default:
                    box = SaveData.WeaponBox;
                    break;
            }
            NudCharWeaponID.Value         = Convert.ToUInt16 (box[(int) NudCharWeaponIndex.Value].FullID.ID);
            CbxCharWeaponID.SelectedValue = (UInt16) NudCharWeaponID.Value;
            SaveData.PartyMembers[LbxChars.SelectedIndex].WeaponEquip.ID = (UInt16) NudCharWeaponIndex.Value;
        }
        private void NudCharHeadArmourType_ValueChanged (object sender, EventArgs e)
        {
            Item[] box = null;

            switch ((ItemType) NudCharHeadArmourType.Value)
            {
                case ItemType.Weapon:
                    box = SaveData.WeaponBox;
                    break;
                case ItemType.Gem:
                    box = SaveData.GemBox;
                    break;
                case ItemType.HeadArmour:
                    box = SaveData.HeadArmourBox;
                    break;
                case ItemType.TorsoArmour:
                    box = SaveData.TorsoArmourBox;
                    break;
                case ItemType.ArmArmour:
                    box = SaveData.ArmArmourBox;
                    break;
                case ItemType.LegArmour:
                    box = SaveData.LegArmourBox;
                    break;
                case ItemType.FootArmour:
                    box = SaveData.FootArmourBox;
                    break;
                case ItemType.Crystal:
                    box = SaveData.CrystalBox;
                    break;
                case ItemType.Collectable:
                    box = SaveData.CollectableBox;
                    break;
                case ItemType.Material:
                    box = SaveData.MaterialBox;
                    break;
                case ItemType.KeyItem:
                    box = SaveData.KeyItemBox;
                    break;
                case ItemType.ArtsManual:
                    box = SaveData.ArtsManualBox;
                    break;
                default:
                    box = SaveData.HeadArmourBox;
                    break;
            }
            NudCharHeadArmourID.Value           = Convert.ToUInt16 (box[(int) NudCharHeadArmourIndex.Value].FullID.ID);
            CbxCharHeadArmourID.SelectedValue   = (UInt16) NudCharHeadArmourID.Value;
            CbxCharHeadArmourType.SelectedValue = (Byte) NudCharHeadArmourType.Value;
            SaveData.PartyMembers[LbxChars.SelectedIndex].HeadEquip.TypeID = (ItemType) NudCharHeadArmourType.Value;
        }
        private void CbxCharHeadArmourType_SelectionChangeCommitted (object sender, EventArgs e)
        {
            NudCharHeadArmourType.Value = Convert.ToUInt16 (CbxCharHeadArmourType.SelectedValue);
        }
        private void NudCharHeadArmourIndex_ValueChanged (object sender, EventArgs e)
        {
            Item[] box = null;

            switch (SaveData.PartyMembers[LbxChars.SelectedIndex].HeadEquip.TypeID)
            {
                case ItemType.Weapon:
                    box = SaveData.WeaponBox;
                    break;
                case ItemType.Gem:
                    box = SaveData.GemBox;
                    break;
                case ItemType.HeadArmour:
                    box = SaveData.HeadArmourBox;
                    break;
                case ItemType.TorsoArmour:
                    box = SaveData.TorsoArmourBox;
                    break;
                case ItemType.ArmArmour:
                    box = SaveData.ArmArmourBox;
                    break;
                case ItemType.LegArmour:
                    box = SaveData.LegArmourBox;
                    break;
                case ItemType.FootArmour:
                    box = SaveData.FootArmourBox;
                    break;
                case ItemType.Crystal:
                    box = SaveData.CrystalBox;
                    break;
                case ItemType.Collectable:
                    box = SaveData.CollectableBox;
                    break;
                case ItemType.Material:
                    box = SaveData.MaterialBox;
                    break;
                case ItemType.KeyItem:
                    box = SaveData.KeyItemBox;
                    break;
                case ItemType.ArtsManual:
                    box = SaveData.ArtsManualBox;
                    break;
                default:
                    box = SaveData.HeadArmourBox;
                    break;
            }
            NudCharHeadArmourID.Value         = Convert.ToUInt16 (box[(int) NudCharHeadArmourIndex.Value].FullID.ID);
            CbxCharHeadArmourID.SelectedValue = (UInt16) NudCharHeadArmourID.Value;
            SaveData.PartyMembers[LbxChars.SelectedIndex].HeadEquip.ID = (UInt16) NudCharHeadArmourIndex.Value;
        }
        private void NudCharTorsoArmourType_ValueChanged (object sender, EventArgs e)
        {
            Item[] box = null;

            switch ((ItemType) NudCharTorsoArmourType.Value)
            {
                case ItemType.Weapon:
                    box = SaveData.WeaponBox;
                    break;
                case ItemType.Gem:
                    box = SaveData.GemBox;
                    break;
                case ItemType.HeadArmour:
                    box = SaveData.HeadArmourBox;
                    break;
                case ItemType.TorsoArmour:
                    box = SaveData.TorsoArmourBox;
                    break;
                case ItemType.ArmArmour:
                    box = SaveData.ArmArmourBox;
                    break;
                case ItemType.LegArmour:
                    box = SaveData.LegArmourBox;
                    break;
                case ItemType.FootArmour:
                    box = SaveData.FootArmourBox;
                    break;
                case ItemType.Crystal:
                    box = SaveData.CrystalBox;
                    break;
                case ItemType.Collectable:
                    box = SaveData.CollectableBox;
                    break;
                case ItemType.Material:
                    box = SaveData.MaterialBox;
                    break;
                case ItemType.KeyItem:
                    box = SaveData.KeyItemBox;
                    break;
                case ItemType.ArtsManual:
                    box = SaveData.ArtsManualBox;
                    break;
                default:
                    box = SaveData.TorsoArmourBox;
                    break;
            }
            NudCharTorsoArmourID.Value           = Convert.ToUInt16 (box[(int) NudCharTorsoArmourIndex.Value].FullID.ID);
            CbxCharTorsoArmourID.SelectedValue   = (UInt16) NudCharTorsoArmourID.Value;
            CbxCharTorsoArmourType.SelectedValue = (Byte) NudCharTorsoArmourType.Value;
            SaveData.PartyMembers[LbxChars.SelectedIndex].TorsoEquip.TypeID = (ItemType) NudCharTorsoArmourType.Value;
        }
        private void CbxCharTorsoArmourType_SelectionChangeCommitted (object sender, EventArgs e)
        {
            NudCharTorsoArmourType.Value = Convert.ToUInt16 (CbxCharTorsoArmourType.SelectedValue);
        }
        private void NudCharTorsoArmourIndex_ValueChanged (object sender, EventArgs e)
        {
            Item[] box = null;

            switch (SaveData.PartyMembers[LbxChars.SelectedIndex].TorsoEquip.TypeID)
            {
                case ItemType.Weapon:
                    box = SaveData.WeaponBox;
                    break;
                case ItemType.Gem:
                    box = SaveData.GemBox;
                    break;
                case ItemType.HeadArmour:
                    box = SaveData.HeadArmourBox;
                    break;
                case ItemType.TorsoArmour:
                    box = SaveData.TorsoArmourBox;
                    break;
                case ItemType.ArmArmour:
                    box = SaveData.ArmArmourBox;
                    break;
                case ItemType.LegArmour:
                    box = SaveData.LegArmourBox;
                    break;
                case ItemType.FootArmour:
                    box = SaveData.FootArmourBox;
                    break;
                case ItemType.Crystal:
                    box = SaveData.CrystalBox;
                    break;
                case ItemType.Collectable:
                    box = SaveData.CollectableBox;
                    break;
                case ItemType.Material:
                    box = SaveData.MaterialBox;
                    break;
                case ItemType.KeyItem:
                    box = SaveData.KeyItemBox;
                    break;
                case ItemType.ArtsManual:
                    box = SaveData.ArtsManualBox;
                    break;
                default:
                    box = SaveData.TorsoArmourBox;
                    break;
            }
            NudCharTorsoArmourID.Value         = Convert.ToUInt16 (box[(int) NudCharTorsoArmourIndex.Value].FullID.ID);
            CbxCharTorsoArmourID.SelectedValue = (UInt16) NudCharTorsoArmourID.Value;
            SaveData.PartyMembers[LbxChars.SelectedIndex].TorsoEquip.ID = (UInt16) NudCharTorsoArmourIndex.Value;
        }
        private void NudCharArmArmourType_ValueChanged (object sender, EventArgs e)
        {
            Item[] box = null;

            switch ((ItemType) NudCharArmArmourType.Value)
            {
                case ItemType.Weapon:
                    box = SaveData.WeaponBox;
                    break;
                case ItemType.Gem:
                    box = SaveData.GemBox;
                    break;
                case ItemType.HeadArmour:
                    box = SaveData.HeadArmourBox;
                    break;
                case ItemType.TorsoArmour:
                    box = SaveData.TorsoArmourBox;
                    break;
                case ItemType.ArmArmour:
                    box = SaveData.ArmArmourBox;
                    break;
                case ItemType.LegArmour:
                    box = SaveData.LegArmourBox;
                    break;
                case ItemType.FootArmour:
                    box = SaveData.FootArmourBox;
                    break;
                case ItemType.Crystal:
                    box = SaveData.CrystalBox;
                    break;
                case ItemType.Collectable:
                    box = SaveData.CollectableBox;
                    break;
                case ItemType.Material:
                    box = SaveData.MaterialBox;
                    break;
                case ItemType.KeyItem:
                    box = SaveData.KeyItemBox;
                    break;
                case ItemType.ArtsManual:
                    box = SaveData.ArtsManualBox;
                    break;
                default:
                    box = SaveData.ArmArmourBox;
                    break;
            }
            NudCharArmArmourID.Value           = Convert.ToUInt16 (box[(int) NudCharArmArmourIndex.Value].FullID.ID);
            CbxCharArmArmourID.SelectedValue   = (UInt16) NudCharArmArmourID.Value;
            CbxCharArmArmourType.SelectedValue = (Byte) NudCharArmArmourType.Value;
            SaveData.PartyMembers[LbxChars.SelectedIndex].ArmEquip.TypeID = (ItemType) NudCharArmArmourType.Value;
        }
        private void CbxCharArmArmourType_SelectionChangeCommitted (object sender, EventArgs e)
        {
            NudCharArmArmourType.Value = Convert.ToUInt16 (CbxCharArmArmourType.SelectedValue);
        }
        private void NudCharArmArmourIndex_ValueChanged (object sender, EventArgs e)
        {
            Item[] box = null;

            switch (SaveData.PartyMembers[LbxChars.SelectedIndex].ArmEquip.TypeID)
            {
                case ItemType.Weapon:
                    box = SaveData.WeaponBox;
                    break;
                case ItemType.Gem:
                    box = SaveData.GemBox;
                    break;
                case ItemType.HeadArmour:
                    box = SaveData.HeadArmourBox;
                    break;
                case ItemType.TorsoArmour:
                    box = SaveData.TorsoArmourBox;
                    break;
                case ItemType.ArmArmour:
                    box = SaveData.ArmArmourBox;
                    break;
                case ItemType.LegArmour:
                    box = SaveData.LegArmourBox;
                    break;
                case ItemType.FootArmour:
                    box = SaveData.FootArmourBox;
                    break;
                case ItemType.Crystal:
                    box = SaveData.CrystalBox;
                    break;
                case ItemType.Collectable:
                    box = SaveData.CollectableBox;
                    break;
                case ItemType.Material:
                    box = SaveData.MaterialBox;
                    break;
                case ItemType.KeyItem:
                    box = SaveData.KeyItemBox;
                    break;
                case ItemType.ArtsManual:
                    box = SaveData.ArtsManualBox;
                    break;
                default:
                    box = SaveData.ArmArmourBox;
                    break;
            }
            NudCharArmArmourID.Value         = Convert.ToUInt16 (box[(int) NudCharArmArmourIndex.Value].FullID.ID);
            CbxCharArmArmourID.SelectedValue = (UInt16) NudCharArmArmourID.Value;
            SaveData.PartyMembers[LbxChars.SelectedIndex].ArmEquip.ID = (UInt16) NudCharArmArmourIndex.Value;
        }
        private void NudCharLegArmourType_ValueChanged (object sender, EventArgs e)
        {
            Item[] box = null;

            switch ((ItemType) NudCharLegArmourType.Value)
            {
                case ItemType.Weapon:
                    box = SaveData.WeaponBox;
                    break;
                case ItemType.Gem:
                    box = SaveData.GemBox;
                    break;
                case ItemType.HeadArmour:
                    box = SaveData.HeadArmourBox;
                    break;
                case ItemType.TorsoArmour:
                    box = SaveData.TorsoArmourBox;
                    break;
                case ItemType.ArmArmour:
                    box = SaveData.ArmArmourBox;
                    break;
                case ItemType.LegArmour:
                    box = SaveData.LegArmourBox;
                    break;
                case ItemType.FootArmour:
                    box = SaveData.FootArmourBox;
                    break;
                case ItemType.Crystal:
                    box = SaveData.CrystalBox;
                    break;
                case ItemType.Collectable:
                    box = SaveData.CollectableBox;
                    break;
                case ItemType.Material:
                    box = SaveData.MaterialBox;
                    break;
                case ItemType.KeyItem:
                    box = SaveData.KeyItemBox;
                    break;
                case ItemType.ArtsManual:
                    box = SaveData.ArtsManualBox;
                    break;
                default:
                    box = SaveData.LegArmourBox;
                    break;
            }
            NudCharLegArmourID.Value           = Convert.ToUInt16 (box[(int) NudCharLegArmourIndex.Value].FullID.ID);
            CbxCharLegArmourID.SelectedValue   = (UInt16) NudCharLegArmourID.Value;
            CbxCharLegArmourType.SelectedValue = (Byte) NudCharLegArmourType.Value;
            SaveData.PartyMembers[LbxChars.SelectedIndex].LegEquip.TypeID = (ItemType) NudCharLegArmourType.Value;
        }
        private void CbxCharLegArmourType_SelectionChangeCommitted (object sender, EventArgs e)
        {
            NudCharLegArmourType.Value = Convert.ToUInt16 (CbxCharLegArmourType.SelectedValue);
        }
        private void NudCharLegArmourIndex_ValueChanged (object sender, EventArgs e)
        {
            Item[] box = null;

            switch (SaveData.PartyMembers[LbxChars.SelectedIndex].LegEquip.TypeID)
            {
                case ItemType.Weapon:
                    box = SaveData.WeaponBox;
                    break;
                case ItemType.Gem:
                    box = SaveData.GemBox;
                    break;
                case ItemType.HeadArmour:
                    box = SaveData.HeadArmourBox;
                    break;
                case ItemType.TorsoArmour:
                    box = SaveData.TorsoArmourBox;
                    break;
                case ItemType.ArmArmour:
                    box = SaveData.ArmArmourBox;
                    break;
                case ItemType.LegArmour:
                    box = SaveData.LegArmourBox;
                    break;
                case ItemType.FootArmour:
                    box = SaveData.FootArmourBox;
                    break;
                case ItemType.Crystal:
                    box = SaveData.CrystalBox;
                    break;
                case ItemType.Collectable:
                    box = SaveData.CollectableBox;
                    break;
                case ItemType.Material:
                    box = SaveData.MaterialBox;
                    break;
                case ItemType.KeyItem:
                    box = SaveData.KeyItemBox;
                    break;
                case ItemType.ArtsManual:
                    box = SaveData.ArtsManualBox;
                    break;
                default:
                    box = SaveData.LegArmourBox;
                    break;
            }
            NudCharLegArmourID.Value         = Convert.ToUInt16 (box[(int) NudCharLegArmourIndex.Value].FullID.ID);
            CbxCharLegArmourID.SelectedValue = (UInt16) NudCharLegArmourID.Value;
            SaveData.PartyMembers[LbxChars.SelectedIndex].LegEquip.ID = (UInt16) NudCharLegArmourIndex.Value;
        }
        private void NudCharFootArmourType_ValueChanged (object sender, EventArgs e)
        {
            Item[] box = null;

            switch ((ItemType) NudCharFootArmourType.Value)
            {
                case ItemType.Weapon:
                    box = SaveData.WeaponBox;
                    break;
                case ItemType.Gem:
                    box = SaveData.GemBox;
                    break;
                case ItemType.HeadArmour:
                    box = SaveData.HeadArmourBox;
                    break;
                case ItemType.TorsoArmour:
                    box = SaveData.TorsoArmourBox;
                    break;
                case ItemType.ArmArmour:
                    box = SaveData.ArmArmourBox;
                    break;
                case ItemType.LegArmour:
                    box = SaveData.LegArmourBox;
                    break;
                case ItemType.FootArmour:
                    box = SaveData.FootArmourBox;
                    break;
                case ItemType.Crystal:
                    box = SaveData.CrystalBox;
                    break;
                case ItemType.Collectable:
                    box = SaveData.CollectableBox;
                    break;
                case ItemType.Material:
                    box = SaveData.MaterialBox;
                    break;
                case ItemType.KeyItem:
                    box = SaveData.KeyItemBox;
                    break;
                case ItemType.ArtsManual:
                    box = SaveData.ArtsManualBox;
                    break;
                default:
                    box = SaveData.FootArmourBox;
                    break;
            }
            NudCharFootArmourID.Value           = Convert.ToUInt16 (box[(int) NudCharFootArmourIndex.Value].FullID.ID);
            CbxCharFootArmourID.SelectedValue   = (UInt16) NudCharFootArmourID.Value;
            CbxCharFootArmourType.SelectedValue = (Byte) NudCharFootArmourType.Value;
            SaveData.PartyMembers[LbxChars.SelectedIndex].FootEquip.TypeID = (ItemType) NudCharFootArmourType.Value;
        }
        private void CbxCharFootArmourType_SelectionChangeCommitted (object sender, EventArgs e)
        {
            NudCharFootArmourType.Value = Convert.ToUInt16 (CbxCharFootArmourType.SelectedValue);
        }
        private void NudCharFootArmourIndex_ValueChanged (object sender, EventArgs e)
        {
            Item[] box = null;

            switch (SaveData.PartyMembers[LbxChars.SelectedIndex].FootEquip.TypeID)
            {
                case ItemType.Weapon:
                    box = SaveData.WeaponBox;
                    break;
                case ItemType.Gem:
                    box = SaveData.GemBox;
                    break;
                case ItemType.HeadArmour:
                    box = SaveData.HeadArmourBox;
                    break;
                case ItemType.TorsoArmour:
                    box = SaveData.TorsoArmourBox;
                    break;
                case ItemType.ArmArmour:
                    box = SaveData.ArmArmourBox;
                    break;
                case ItemType.LegArmour:
                    box = SaveData.LegArmourBox;
                    break;
                case ItemType.FootArmour:
                    box = SaveData.FootArmourBox;
                    break;
                case ItemType.Crystal:
                    box = SaveData.CrystalBox;
                    break;
                case ItemType.Collectable:
                    box = SaveData.CollectableBox;
                    break;
                case ItemType.Material:
                    box = SaveData.MaterialBox;
                    break;
                case ItemType.KeyItem:
                    box = SaveData.KeyItemBox;
                    break;
                case ItemType.ArtsManual:
                    box = SaveData.ArtsManualBox;
                    break;
                default:
                    box = SaveData.FootArmourBox;
                    break;
            }
            NudCharFootArmourID.Value         = Convert.ToUInt16 (box[(int) NudCharFootArmourIndex.Value].FullID.ID);
            CbxCharFootArmourID.SelectedValue = (UInt16) NudCharFootArmourID.Value;
            SaveData.PartyMembers[LbxChars.SelectedIndex].FootEquip.ID = (UInt16) NudCharFootArmourIndex.Value;
        }
        #endregion Character Equipment

        #region Characters Arts
        private void NudCharNormalArt1ID_ValueChanged (object sender, EventArgs e)
        {
            CbxCharNormalArt1ID.SelectedValue = (UInt32) NudCharNormalArt1ID.Value;
            SaveData.PartyMembers[LbxChars.SelectedIndex].Arts[1] = (UInt16) NudCharNormalArt1ID.Value;
        }
        private void NudCharNormalArt2ID_ValueChanged (object sender, EventArgs e)
        {
            CbxCharNormalArt2ID.SelectedValue = (UInt32) NudCharNormalArt2ID.Value;
            SaveData.PartyMembers[LbxChars.SelectedIndex].Arts[2] = (UInt16) NudCharNormalArt2ID.Value;
        }
        private void NudCharNormalArt3ID_ValueChanged (object sender, EventArgs e)
        {
            CbxCharNormalArt3ID.SelectedValue = (UInt32) NudCharNormalArt3ID.Value;
            SaveData.PartyMembers[LbxChars.SelectedIndex].Arts[3] = (UInt16) NudCharNormalArt3ID.Value;
        }
        private void NudCharNormalArt4ID_ValueChanged (object sender, EventArgs e)
        {
            CbxCharNormalArt4ID.SelectedValue = (UInt32) NudCharNormalArt4ID.Value;
            SaveData.PartyMembers[LbxChars.SelectedIndex].Arts[4] = (UInt16) NudCharNormalArt4ID.Value;
        }
        private void NudCharTalentArtID_ValueChanged (object sender, EventArgs e)
        {
            CbxCharTalentArtID.SelectedValue = (UInt32) NudCharTalentArtID.Value;
            SaveData.PartyMembers[LbxChars.SelectedIndex].Arts[0] = (UInt16) NudCharTalentArtID.Value;
        }
        private void NudCharNormalArt5ID_ValueChanged (object sender, EventArgs e)
        {
            CbxCharNormalArt5ID.SelectedValue = (UInt32) NudCharNormalArt5ID.Value;
            SaveData.PartyMembers[LbxChars.SelectedIndex].Arts[5] = (UInt16) NudCharNormalArt5ID.Value;
        }
        private void NudCharNormalArt6ID_ValueChanged (object sender, EventArgs e)
        {
            CbxCharNormalArt6ID.SelectedValue = (UInt32) NudCharNormalArt6ID.Value;
            SaveData.PartyMembers[LbxChars.SelectedIndex].Arts[6] = (UInt16) NudCharNormalArt6ID.Value;
        }
        private void NudCharNormalArt7ID_ValueChanged (object sender, EventArgs e)
        {
            CbxCharNormalArt7ID.SelectedValue = (UInt32) NudCharNormalArt7ID.Value;
            SaveData.PartyMembers[LbxChars.SelectedIndex].Arts[7] = (UInt16) NudCharNormalArt7ID.Value;
        }
        private void NudCharNormalArt8ID_ValueChanged (object sender, EventArgs e)
        {
            CbxCharNormalArt8ID.SelectedValue = (UInt32) NudCharNormalArt8ID.Value;
            SaveData.PartyMembers[LbxChars.SelectedIndex].Arts[8] = (UInt16) NudCharNormalArt8ID.Value;
        }
        private void CbxCharNormalArt1ID_SelectionChangeCommitted (object sender, EventArgs e)
        {
            NudCharNormalArt1ID.Value = Convert.ToUInt32 (CbxCharNormalArt1ID.SelectedValue);
            SaveData.PartyMembers[LbxChars.SelectedIndex].Arts[1] = (UInt16) NudCharNormalArt1ID.Value;
        }
        private void CbxCharNormalArt2ID_SelectionChangeCommitted (object sender, EventArgs e)
        {
            NudCharNormalArt2ID.Value = Convert.ToUInt32 (CbxCharNormalArt2ID.SelectedValue);
            SaveData.PartyMembers[LbxChars.SelectedIndex].Arts[2] = (UInt16) NudCharNormalArt2ID.Value;
        }
        private void CbxCharNormalArt3ID_SelectionChangeCommitted (object sender, EventArgs e)
        {
            NudCharNormalArt3ID.Value = Convert.ToUInt32 (CbxCharNormalArt3ID.SelectedValue);
            SaveData.PartyMembers[LbxChars.SelectedIndex].Arts[3] = (UInt16) NudCharNormalArt3ID.Value;
        }
        private void CbxCharNormalArt4ID_SelectionChangeCommitted (object sender, EventArgs e)
        {
            NudCharNormalArt4ID.Value = Convert.ToUInt32 (CbxCharNormalArt4ID.SelectedValue);
            SaveData.PartyMembers[LbxChars.SelectedIndex].Arts[4] = (UInt16) NudCharNormalArt4ID.Value;
        }
        private void CbxCharTalentArtID_SelectionChangeCommitted (object sender, EventArgs e)
        {
            NudCharTalentArtID.Value = Convert.ToUInt32 (CbxCharTalentArtID.SelectedValue);
            SaveData.PartyMembers[LbxChars.SelectedIndex].Arts[0] = (UInt16) NudCharTalentArtID.Value;
        }
        private void CbxCharNormalArt5ID_SelectionChangeCommitted (object sender, EventArgs e)
        {
            NudCharNormalArt5ID.Value = Convert.ToUInt32 (CbxCharNormalArt5ID.SelectedValue);
            SaveData.PartyMembers[LbxChars.SelectedIndex].Arts[5] = (UInt16) NudCharNormalArt5ID.Value;
        }
        private void CbxCharNormalArt6ID_SelectionChangeCommitted (object sender, EventArgs e)
        {
            NudCharNormalArt6ID.Value = Convert.ToUInt32 (CbxCharNormalArt6ID.SelectedValue);
            SaveData.PartyMembers[LbxChars.SelectedIndex].Arts[6] = (UInt16) NudCharNormalArt6ID.Value;
        }
        private void CbxCharNormalArt7ID_SelectionChangeCommitted (object sender, EventArgs e)
        {
            NudCharNormalArt7ID.Value = Convert.ToUInt32 (CbxCharNormalArt7ID.SelectedValue);
            SaveData.PartyMembers[LbxChars.SelectedIndex].Arts[7] = (UInt16) NudCharNormalArt7ID.Value;
        }
        private void CbxCharNormalArt8ID_SelectionChangeCommitted (object sender, EventArgs e)
        {
            NudCharNormalArt8ID.Value = Convert.ToUInt32 (CbxCharNormalArt8ID.SelectedValue);
            SaveData.PartyMembers[LbxChars.SelectedIndex].Arts[8] = (UInt16) NudCharNormalArt8ID.Value;
        }

        private void NudCharMonadoArt1ID_ValueChanged (object sender, EventArgs e)
        {
            CbxCharMonadoArt1ID.SelectedValue = (UInt32) NudCharMonadoArt1ID.Value;
            SaveData.PartyMembers[LbxChars.SelectedIndex].MonadoArts[1] = (UInt16) NudCharMonadoArt1ID.Value;
        }
        private void NudCharMonadoArt2ID_ValueChanged (object sender, EventArgs e)
        {
            CbxCharMonadoArt2ID.SelectedValue = (UInt32) NudCharMonadoArt2ID.Value;
            SaveData.PartyMembers[LbxChars.SelectedIndex].MonadoArts[2] = (UInt16) NudCharMonadoArt2ID.Value;
        }
        private void NudCharMonadoArt3ID_ValueChanged (object sender, EventArgs e)
        {
            CbxCharMonadoArt3ID.SelectedValue = (UInt32) NudCharMonadoArt3ID.Value;
            SaveData.PartyMembers[LbxChars.SelectedIndex].MonadoArts[3] = (UInt16) NudCharMonadoArt3ID.Value;
        }
        private void NudCharMonadoArt4ID_ValueChanged (object sender, EventArgs e)
        {
            CbxCharMonadoArt4ID.SelectedValue = (UInt32) NudCharMonadoArt4ID.Value;
            SaveData.PartyMembers[LbxChars.SelectedIndex].MonadoArts[4] = (UInt16) NudCharMonadoArt4ID.Value;
        }
        private void NudCharMonTalentArtID_ValueChanged (object sender, EventArgs e)
        {
            CbxCharMonTalentArtID.SelectedValue = (UInt32) NudCharMonTalentArtID.Value;
            SaveData.PartyMembers[LbxChars.SelectedIndex].MonadoArts[0] = (UInt16) NudCharMonTalentArtID.Value;
        }
        private void NudCharMonadoArt5ID_ValueChanged (object sender, EventArgs e)
        {
            CbxCharMonadoArt5ID.SelectedValue = (UInt32) NudCharMonadoArt5ID.Value;
            SaveData.PartyMembers[LbxChars.SelectedIndex].MonadoArts[5] = (UInt16) NudCharMonadoArt5ID.Value;
        }
        private void NudCharMonadoArt6ID_ValueChanged (object sender, EventArgs e)
        {
            CbxCharMonadoArt6ID.SelectedValue = (UInt32) NudCharMonadoArt6ID.Value;
            SaveData.PartyMembers[LbxChars.SelectedIndex].MonadoArts[6] = (UInt16) NudCharMonadoArt6ID.Value;
        }
        private void NudCharMonadoArt7ID_ValueChanged (object sender, EventArgs e)
        {
            CbxCharMonadoArt7ID.SelectedValue = (UInt32) NudCharMonadoArt7ID.Value;
            SaveData.PartyMembers[LbxChars.SelectedIndex].MonadoArts[7] = (UInt16) NudCharMonadoArt7ID.Value;
        }
        private void NudCharMonadoArt8ID_ValueChanged (object sender, EventArgs e)
        {
            CbxCharMonadoArt8ID.SelectedValue = (UInt32) NudCharMonadoArt8ID.Value;
            SaveData.PartyMembers[LbxChars.SelectedIndex].MonadoArts[8] = (UInt16) NudCharMonadoArt8ID.Value;
        }
        private void CbxCharMonadoArt1ID_SelectionChangeCommitted (object sender, EventArgs e)
        {
            NudCharMonadoArt1ID.Value = Convert.ToUInt32 (CbxCharMonadoArt1ID.SelectedValue);
            SaveData.PartyMembers[LbxChars.SelectedIndex].MonadoArts[1] = (UInt16) NudCharMonadoArt1ID.Value;
        }
        private void CbxCharMonadoArt2ID_SelectionChangeCommitted (object sender, EventArgs e)
        {
            NudCharMonadoArt2ID.Value = Convert.ToUInt32 (CbxCharMonadoArt2ID.SelectedValue);
            SaveData.PartyMembers[LbxChars.SelectedIndex].MonadoArts[2] = (UInt16) NudCharMonadoArt2ID.Value;
        }
        private void CbxCharMonadoArt3ID_SelectionChangeCommitted (object sender, EventArgs e)
        {
            NudCharMonadoArt3ID.Value = Convert.ToUInt32 (CbxCharMonadoArt3ID.SelectedValue);
            SaveData.PartyMembers[LbxChars.SelectedIndex].MonadoArts[3] = (UInt16) NudCharMonadoArt3ID.Value;
        }
        private void CbxCharMonadoArt4ID_SelectionChangeCommitted (object sender, EventArgs e)
        {
            NudCharMonadoArt4ID.Value = Convert.ToUInt32 (CbxCharMonadoArt4ID.SelectedValue);
            SaveData.PartyMembers[LbxChars.SelectedIndex].MonadoArts[4] = (UInt16) NudCharMonadoArt4ID.Value;
        }
        private void CbxCharMonTalentArtID_SelectionChangeCommitted (object sender, EventArgs e)
        {
            NudCharMonTalentArtID.Value = Convert.ToUInt32 (CbxCharMonTalentArtID.SelectedValue);
            SaveData.PartyMembers[LbxChars.SelectedIndex].MonadoArts[0] = (UInt16) NudCharMonTalentArtID.Value;
        }
        private void CbxCharMonadoArt5ID_SelectionChangeCommitted (object sender, EventArgs e)
        {
            NudCharMonadoArt5ID.Value = Convert.ToUInt32 (CbxCharMonadoArt5ID.SelectedValue);
            SaveData.PartyMembers[LbxChars.SelectedIndex].MonadoArts[5] = (UInt16) NudCharMonadoArt5ID.Value;
        }
        private void CbxCharMonadoArt6ID_SelectionChangeCommitted (object sender, EventArgs e)
        {
            NudCharMonadoArt6ID.Value = Convert.ToUInt32 (CbxCharMonadoArt6ID.SelectedValue);
            SaveData.PartyMembers[LbxChars.SelectedIndex].MonadoArts[6] = (UInt16) NudCharMonadoArt6ID.Value;
        }
        private void CbxCharMonadoArt7ID_SelectionChangeCommitted (object sender, EventArgs e)
        {
            NudCharMonadoArt7ID.Value = Convert.ToUInt32 (CbxCharMonadoArt7ID.SelectedValue);
            SaveData.PartyMembers[LbxChars.SelectedIndex].MonadoArts[7] = (UInt16) NudCharMonadoArt7ID.Value;
        }
        private void CbxCharMonadoArt8ID_SelectionChangeCommitted (object sender, EventArgs e)
        {
            NudCharMonadoArt8ID.Value = Convert.ToUInt32 (CbxCharMonadoArt8ID.SelectedValue);
            SaveData.PartyMembers[LbxChars.SelectedIndex].MonadoArts[8] = (UInt16) NudCharMonadoArt8ID.Value;
        }

        private void NudCharArtID_ValueChanged (object sender, EventArgs e)
        {
            CbxCharArtID.SelectedValue        = (UInt32) NudCharArtID.Value;
            NudCharArtUnlockLvl.Value         = Convert.ToUInt32 (SaveData.ArtsLevels[(int) NudCharArtID.Value].MaxLevelUnlocked);
            CbxCharArtUnlockLvl.SelectedValue = (UInt32) NudCharArtUnlockLvl.Value;
            NudCharArtLevel.Value             = Convert.ToUInt32 (SaveData.ArtsLevels[(int) NudCharArtID.Value].Level);
        }
        private void CbxCharArtID_SelectionChangeCommitted (object sender, EventArgs e)
        {
            NudCharArtID.Value                = Convert.ToUInt32 (CbxCharArtID.SelectedValue);
            NudCharArtUnlockLvl.Value         = Convert.ToUInt32 (SaveData.ArtsLevels[(int) NudCharArtID.Value].MaxLevelUnlocked);
            CbxCharArtUnlockLvl.SelectedValue = (UInt32) NudCharArtUnlockLvl.Value;
            NudCharArtLevel.Value             = Convert.ToUInt32 (SaveData.ArtsLevels[(int) NudCharArtID.Value].Level);
        }
        private void CbxCharArtUnlockLvl_SelectionChangeCommitted (object sender, EventArgs e)
        {
            NudCharArtUnlockLvl.Value = Convert.ToUInt32 (CbxCharArtUnlockLvl.SelectedValue);
            SaveData.ArtsLevels[(int) NudCharArtID.Value].MaxLevelUnlocked = (ArtsLevelUnlocked) NudCharArtUnlockLvl.Value;
        }
        private void NudCharArtLevel_ValueChanged  (object sender, EventArgs e) => SaveData.ArtsLevels[(int) NudCharArtID.Value].Level = (Byte) NudCharArtLevel.Value;
        private void NudCharArtPoints_ValueChanged (object sender, EventArgs e) => SaveData.PartyMembers[LbxChars.SelectedIndex].AP    = (UInt32) NudCharArtPoints.Value;
        #endregion Characters Arts

        #endregion Characters

        #region Items
        private void TbcItems_SelectedIndexChanged (object sender, EventArgs e) => UpdateNewItemComboBox();
        private void Dgv_CellFormatting (object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (! dgvIsEditing)
            {
                dgvIsEditing = true;
                int columnIndex = e.ColumnIndex;
                int rowIndex    = e.RowIndex;

                DataGridView dgv = (DataGridView) sender;

                switch (columnIndex)
                {
                    case 1:
                        if (Convert.ToUInt32 (dgv.Rows[rowIndex].Cells[1].Value) < ((DataGridViewComboBoxCell) dgv.Rows[rowIndex].Cells[2]).Items.Count)
                            dgv.Rows[rowIndex].Cells[2].Value = ((DataRowView) ((DataGridViewComboBoxCell) dgv.Rows[rowIndex].Cells[2]).Items[Convert.ToInt32 (dgv.Rows[rowIndex].Cells[1].Value)])[0];
                        break;

                    case 3:
                        if (Convert.ToUInt32 (dgv.Rows[rowIndex].Cells[3].Value) < ((DataGridViewComboBoxCell) dgv.Rows[rowIndex].Cells[4]).Items.Count)
                            dgv.Rows[rowIndex].Cells[4].Value = ((DataRowView) ((DataGridViewComboBoxCell) dgv.Rows[rowIndex].Cells[4]).Items[Convert.ToInt32 (dgv.Rows[rowIndex].Cells[3].Value)])[0];
                        break;
                }
                dgvIsEditing = false;
            }
        }
        private void DgvItemBox_CellValueChanged (object sender, DataGridViewCellEventArgs e)
        {
            if (! dgvIsEditing)
            {
                dgvIsEditing = true;

                DataGridView dgv = (DataGridView) sender;

                switch (e.ColumnIndex)
                {
                    case 1:
                        if (Convert.ToUInt32 (dgv.Rows[e.RowIndex].Cells[1].Value) < ((DataGridViewComboBoxCell) dgv.Rows[e.RowIndex].Cells[2]).Items.Count)
                            dgv.Rows[e.RowIndex].Cells[2].Value = ((DataRowView) ((DataGridViewComboBoxCell) dgv.Rows[e.RowIndex].Cells[2]).Items[Convert.ToInt32 (dgv.Rows[e.RowIndex].Cells[1].Value)])[0];
                        break;

                    case 2:
                        dgv.Rows[e.RowIndex].Cells[1].Value = dgv.Rows[e.RowIndex].Cells[2].Value;
                        break;

                    case 3:
                        if (Convert.ToUInt32 (dgv.Rows[e.RowIndex].Cells[3].Value) < ((DataGridViewComboBoxCell) dgv.Rows[e.RowIndex].Cells[4]).Items.Count)
                            dgv.Rows[e.RowIndex].Cells[4].Value = ((DataRowView) ((DataGridViewComboBoxCell) dgv.Rows[e.RowIndex].Cells[4]).Items[Convert.ToInt32 (dgv.Rows[e.RowIndex].Cells[3].Value)])[0];
                        break;

                    case 4:
                        dgv.Rows[e.RowIndex].Cells[3].Value = dgv.Rows[e.RowIndex].Cells[4].Value;
                        break;
                }

                dgvIsEditing = false;
            }
        }
        private void DgvItemBox_CellContextMenuStripNeeded (object sender, DataGridViewCellContextMenuStripNeededEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridView dgv = (DataGridView) sender;

                foreach (DataGridViewRow r in dgv.Rows)
                    r.Selected = false;
                dgv.Rows[e.RowIndex].Selected = true;

                ContextMenuStripItems.Tag = dgv;
                ContextMenuStripItems.Show (dgv, dgv.PointToClient (Cursor.Position));
            }
        }
        private void DgvItemBox_ColumnHeaderMouseDoubleClick (object sender, DataGridViewCellMouseEventArgs e)
        {
            if ((TbcItems.SelectedIndex >= 0) && (TbcItems.SelectedIndex <= 5))
                DgvEquipItemBox_ColumnHeaderMouseDoubleClick (sender, e);
            else if ((TbcItems.SelectedIndex >= 6) && (TbcItems.SelectedIndex <= 7))
                DgvCrystalItemBox_ColumnHeaderMouseDoubleClick (sender, e);
            else if ((TbcItems.SelectedIndex >= 8) && (TbcItems.SelectedIndex <= 11))
                DgvBaseItemBox_ColumnHeaderMouseDoubleClick (sender, e);
        }
        private void DgvBaseItemBox_ColumnHeaderMouseDoubleClick (object sender, DataGridViewCellMouseEventArgs e)
        {
            int c = e.ColumnIndex;

            DataGridView dgv = (DataGridView) sender;
            Item[] box = null;

            switch (TbcItems.SelectedIndex)
            {
                case 8:
                    box = SaveData.CollectableBox;
                    break;
                case 9:
                    box = SaveData.MaterialBox;
                    break;
                case 10:
                    box = SaveData.KeyItemBox;
                    break;
                case 11:
                    box = SaveData.ArtsManualBox;
                    break;
            }

            if (dgv.Columns[c].Tag == null)
                dgv.Columns[c].Tag = false;

            switch (c)
            {
                // Columns 0-8 are common to all item types
                case 1:
                    Array.Sort (box, Item.GetComparer (Item.CmpField.ID, (bool) dgv.Columns[c].Tag));
                    break;
                case 2:
                    string[] item_names = new string[box.Length];
                    for (int i = 0; i < item_names.Length; i++)
                    {
                        // FIXME: need to check if [2] is right index or not
                        DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell) dgv.Rows[i].Cells[2];
                        item_names[i] = (string) ((DataRowView) cell.Items[Convert.ToInt32 (cell.Value)]).Row[2];
                    }
                    Array.Sort (item_names, box, Item.GetComparer (Item.CmpField.Name, (bool) dgv.Columns[c].Tag));
                    break;
                case 5:
                    Array.Sort (box, Item.GetComparer (Item.CmpField.Quantity, (bool) dgv.Columns[c].Tag));
                    break;
                case 6:
                    Array.Sort (box, Item.GetComparer (Item.CmpField.Exists, (bool) dgv.Columns[c].Tag));
                    break;
                case 7:
                    Array.Sort (box, Item.GetComparer (Item.CmpField.Favourite, (bool) dgv.Columns[c].Tag));
                    break;
                case 8:
                    Array.Sort (box, Item.GetComparer (Item.CmpField.Serial, (bool) dgv.Columns[c].Tag));
                    break;
                default:
                    return;
            }

            // After sorting by any of the above supported fields, the Index
            // fields in the Item structures will be all jumbled up
            // So as a post process, need to reassign the Index entries to be
            // equal to their new positions in the array
            // This is required for the sorting to take effect in-game
            for (uint i = 0; i < box.Length; i++)
            {
                if (box[i].Exists)
                    box[i].Index = (ushort) i;
            }

            dgv.Columns[c].Tag = ! (bool) dgv.Columns[c].Tag;

            ((BindingList<Item>) dgv.DataSource).ResetBindings();
        }
        private void DgvEquipItemBox_ColumnHeaderMouseDoubleClick (object sender, DataGridViewCellMouseEventArgs e)
        {
            int c = e.ColumnIndex;

            DataGridView dgv = (DataGridView) sender;
            EquipItem[] box = null;

            switch (TbcItems.SelectedIndex)
            {
                case 0:
                    box = SaveData.WeaponBox;
                    break;
                case 1:
                    box = SaveData.HeadArmourBox;
                    break;
                case 2:
                    box = SaveData.TorsoArmourBox;
                    break;
                case 3:
                    box = SaveData.ArmArmourBox;
                    break;
                case 4:
                    box = SaveData.LegArmourBox;
                    break;
                case 5:
                    box = SaveData.FootArmourBox;
                    break;
            }

            if (dgv.Columns[c].Tag == null)
                dgv.Columns[c].Tag = false;

            switch (c)
            {
                // Columns 0-8 are common to all item types
                case 1:
                    Array.Sort (box, EquipItem.GetComparer (EquipItem.CmpField.ID, (bool) dgv.Columns[c].Tag));
                    break;
                case 2:
                    string[] item_names = new string[box.Length];
                    for (int i = 0; i < item_names.Length; i++)
                    {
                        DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell) dgv.Rows[i].Cells[2];
                        item_names[i] = (string) ((DataRowView) cell.Items[Convert.ToInt32 (cell.Value)]).Row[2];
                    }
                    Array.Sort (item_names, box, EquipItem.GetComparer (EquipItem.CmpField.Name, (bool) dgv.Columns[c].Tag));
                    break;
                case 5:
                    Array.Sort (box, EquipItem.GetComparer (EquipItem.CmpField.Quantity, (bool) dgv.Columns[c].Tag));
                    break;
                case 6:
                    Array.Sort (box, EquipItem.GetComparer (EquipItem.CmpField.Exists, (bool) dgv.Columns[c].Tag));
                    break;
                case 7:
                    Array.Sort (box, EquipItem.GetComparer (EquipItem.CmpField.Favourite, (bool) dgv.Columns[c].Tag));
                    break;
                case 8:
                    Array.Sort (box, EquipItem.GetComparer (EquipItem.CmpField.Serial, (bool) dgv.Columns[c].Tag));
                    break;
                // Columns 9-10 apply to equipment and crystal items only
                case 9:
                    Array.Sort (box, EquipItem.GetComparer (EquipItem.CmpField.Weight, (bool) dgv.Columns[c].Tag));
                    break;
                case 10:
                    Array.Sort (box, EquipItem.GetComparer (EquipItem.CmpField.GemSlots, (bool) dgv.Columns[c].Tag));
                    break;
                default:
                    return;
            }

            // After sorting by any of the above supported fields, the Index
            // fields in the Item structures will be all jumbled up
            // So as a post process, need to reassign the Index entries to be
            // equal to their new positions in the array
            // This is required for the sorting to take effect in-game
            for (uint i = 0; i < box.Length; i++)
            {
                if (box[i].Exists)
                    box[i].Index = (ushort) i;
            }

            dgv.Columns[c].Tag = ! (bool) dgv.Columns[c].Tag;

            ((BindingList<Item>) dgv.DataSource).ResetBindings();
        }
        private void DgvCrystalItemBox_ColumnHeaderMouseDoubleClick (object sender, DataGridViewCellMouseEventArgs e)
        {
            int c = e.ColumnIndex;

            DataGridView dgv = (DataGridView) sender;
            CrystalItem[] box = null;

            switch (TbcItems.SelectedIndex)
            {
                case 6:
                    box = SaveData.CrystalBox;
                    break;
                case 7:
                    box = SaveData.GemBox;
                    break;
            }

            if (dgv.Columns[c].Tag == null)
                dgv.Columns[c].Tag = false;

            switch (c)
            {
                // Columns 0-8 are common to all item types
                case 1:
                    Array.Sort (box, CrystalItem.GetComparer (CrystalItem.CmpField.ID, (bool) dgv.Columns[c].Tag));
                    break;
                case 2:
                    string[] item_names = new string[box.Length];
                    for (int i = 0; i < item_names.Length; i++)
                    {
                        DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell) dgv.Rows[i].Cells[2];
                        item_names[i] = (string) ((DataRowView) cell.Items[Convert.ToInt32 (cell.Value)]).Row[2];
                    }
                    Array.Sort (item_names, box, CrystalItem.GetComparer (CrystalItem.CmpField.Name, (bool) dgv.Columns[c].Tag));
                    break;
                case 5:
                    Array.Sort (box, CrystalItem.GetComparer (CrystalItem.CmpField.Quantity, (bool) dgv.Columns[c].Tag));
                    break;
                case 6:
                    Array.Sort (box, CrystalItem.GetComparer (CrystalItem.CmpField.Exists, (bool) dgv.Columns[c].Tag));
                    break;
                case 7:
                    Array.Sort (box, CrystalItem.GetComparer (CrystalItem.CmpField.Favourite, (bool) dgv.Columns[c].Tag));
                    break;
                case 8:
                    Array.Sort (box, CrystalItem.GetComparer (CrystalItem.CmpField.Serial, (bool) dgv.Columns[c].Tag));
                    break;
                // Columns 9-10 apply to equipment and crystal items only
                case 9:
                    Array.Sort (box, CrystalItem.GetComparer (CrystalItem.CmpField.CrystalNameID, (bool) dgv.Columns[c].Tag));
                    break;
                case 10:
                    Array.Sort (box, CrystalItem.GetComparer (CrystalItem.CmpField.Rank, (bool) dgv.Columns[c].Tag));
                    break;
                // Columns 11-14 apply to crystal items only
                case 11:
                    Array.Sort (box, CrystalItem.GetComparer (CrystalItem.CmpField.Element, (bool) dgv.Columns[c].Tag));
                    break;
                case 12:
                    string[] element_names = new string[box.Length];
                    for (int i = 0; i < element_names.Length; i++)
                    {
                        DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell) dgv.Rows[i].Cells[12];
                        element_names[i] = (string) ((DataRowView) cell.Items[Convert.ToInt32 (cell.Value)]).Row[12];
                    }
                    Array.Sort (element_names, box, CrystalItem.GetComparer (CrystalItem.CmpField.ElementName, (bool) dgv.Columns[c].Tag));
                    break;
                case 13:
                    Array.Sort (box, CrystalItem.GetComparer (CrystalItem.CmpField.IsCylinder, (bool) dgv.Columns[c].Tag));
                    break;
                case 14:
                    Array.Sort (box, CrystalItem.GetComparer (CrystalItem.CmpField.Buffs, (bool) dgv.Columns[c].Tag));
                    break;
                default:
                    return;
            }

            // After sorting by any of the above supported fields, the Index
            // fields in the Item structures will be all jumbled up
            // So as a post process, need to reassign the Index entries to be
            // equal to their new positions in the array
            // This is required for the sorting to take effect in-game
            for (uint i = 0; i < box.Length; i++)
            {
                if (box[i].Exists)
                    box[i].Index = (ushort) i;
            }

            dgv.Columns[c].Tag = ! (bool) dgv.Columns[c].Tag;

            ((BindingList<Item>) dgv.DataSource).ResetBindings();
        }

        private void tsmiItemGetNewSerial_Click (object sender, EventArgs e)
        {
            DataGridView dgv = (DataGridView) ((ToolStripMenuItem) sender).GetCurrentParent().Tag;
            dgv.Rows[dgv.SelectedRows[0].Index].Cells[8].Value = ItemGetNewSerial (dgv);
        }

        private void btnItemSearch_Click (object sender, EventArgs e) => FindInItemBox();
        private void txtItemSearch_KeyUp (object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                FindInItemBox();
        }
        private void nudItemAddNewID_ValueChanged (object sender, EventArgs e) => CbxItemAddNewID.SelectedValue = (UInt16) NudItemAddNewID.Value;
        private void cbxItemAddNewID_SelectionChangeCommitted (object sender, EventArgs e) => NudItemAddNewID.Value = Convert.ToUInt16 (CbxItemAddNewID.SelectedValue);
        private void btnItemAddNew_Click (object sender, EventArgs e)
        {
            DataGridView dgv = ((DataGridView) TbcItems.SelectedTab.Controls[0]);

            for (int i = 0; i < dgv.Rows.Count; i++)
            {
                if ((UInt16) dgv.Rows[i].Cells[0].Value == 0)
                {
                    dgv.Rows[i].Cells[0].Value = NudItemAddNewID.Value;

                    switch (TbcItems.SelectedIndex)
                    {
                        case 0:
                            dgv.Rows[i].Cells[2].Value = 2;
                            break;

                        case 1:
                            dgv.Rows[i].Cells[2].Value = 4;
                            break;

                        case 2:
                            dgv.Rows[i].Cells[2].Value = 5;
                            break;

                        case 3:
                            dgv.Rows[i].Cells[2].Value = 6;
                            break;

                        case 4:
                            dgv.Rows[i].Cells[2].Value = 7;
                            break;

                        case 5:
                            dgv.Rows[i].Cells[2].Value = 8;
                            break;

                        case 6:
                            dgv.Rows[i].Cells[2].Value = 9;
                            break;

                        case 7:
                            dgv.Rows[i].Cells[2].Value = 3;
                            break;

                        case 8:
                            dgv.Rows[i].Cells[2].Value = 10;
                            break;

                        case 9:
                            dgv.Rows[i].Cells[2].Value = 11;
                            break;

                        case 10:
                            dgv.Rows[i].Cells[2].Value = 12;
                            break;

                        case 11:
                            dgv.Rows[i].Cells[2].Value = 13;
                            break;
                    }

                    // Initialize Quantity, Exists, & Serial
                    dgv.Rows[i].Cells[4].Value = NudItemAddNewQty.Value;
                    dgv.Rows[i].Cells[5].Value = true;
                    dgv.Rows[i].Cells[7].Value = ItemGetNewSerial (dgv);

                    // FIXME: need to initialize Weight for armor
                    // FIXME: need to initialize Rank & Element for crystals/cylinders

                    dgv.ClearSelection();
                    dgv.CurrentCell = dgv.Rows[i].Cells[0];
                    dgv.Rows[i].Selected = true;

                    return;
                }
            }

            string message =
                "Unable to add New Item to " + TbcItems.TabPages[TbcItems.SelectedIndex].Text + ", ItemBox is full.";

            MessageBox.Show (message, "Add New Item Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void btnItemCheatMaxQty_Click (object sender, EventArgs e)
        {
            if (TbcItems.TabPages[TbcItems.SelectedIndex].Controls[0].GetType() == typeof (DataGridView))
            {
                BindingList<Item> items = ((BindingList<Item>) ((DataGridView) TbcItems.TabPages[TbcItems.SelectedIndex].Controls[0]).DataSource);

                foreach (Item i in items)
                    if (!i.IsEmptyItem())
                        i.Quantity = 99;

                items.ResetBindings();
            }
        }
        private void nudItemsSerial1_ValueChanged  (object sender, EventArgs e) => SaveData.Serials[0]  = (UInt32) NudItemsSerial1.Value;
        private void nudItemsSerial2_ValueChanged  (object sender, EventArgs e) => SaveData.Serials[1]  = (UInt32) NudItemsSerial2.Value;
        private void nudItemsSerial3_ValueChanged  (object sender, EventArgs e) => SaveData.Serials[2]  = (UInt32) NudItemsSerial3.Value;
        private void nudItemsSerial4_ValueChanged  (object sender, EventArgs e) => SaveData.Serials[3]  = (UInt32) NudItemsSerial4.Value;
        private void nudItemsSerial5_ValueChanged  (object sender, EventArgs e) => SaveData.Serials[4]  = (UInt32) NudItemsSerial5.Value;
        private void nudItemsSerial6_ValueChanged  (object sender, EventArgs e) => SaveData.Serials[5]  = (UInt32) NudItemsSerial6.Value;
        private void nudItemsSerial7_ValueChanged  (object sender, EventArgs e) => SaveData.Serials[6]  = (UInt32) NudItemsSerial7.Value;
        private void nudItemsSerial8_ValueChanged  (object sender, EventArgs e) => SaveData.Serials[7]  = (UInt32) NudItemsSerial8.Value;
        private void nudItemsSerial9_ValueChanged  (object sender, EventArgs e) => SaveData.Serials[8]  = (UInt32) NudItemsSerial9.Value;
        private void nudItemsSerial10_ValueChanged (object sender, EventArgs e) => SaveData.Serials[9]  = (UInt32) NudItemsSerial10.Value;
        private void nudItemsSerial11_ValueChanged (object sender, EventArgs e) => SaveData.Serials[10] = (UInt32) NudItemsSerial11.Value;
        private void nudItemsSerial12_ValueChanged (object sender, EventArgs e) => SaveData.Serials[11] = (UInt32) NudItemsSerial12.Value;
        private void nudItemsSerial13_ValueChanged (object sender, EventArgs e) => SaveData.Serials[12] = (UInt32) NudItemsSerial13.Value;
        #endregion Items

        #endregion Events
    }
}
