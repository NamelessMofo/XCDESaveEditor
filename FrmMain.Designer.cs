namespace XCDESaveEditor
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose (bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose (disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager (typeof (FrmMain));
            this.BtnLoad = new System.Windows.Forms.Button();
            this.LblSaveTimeDate = new System.Windows.Forms.Label();
            this.LblSaveTimeDateDash1 = new System.Windows.Forms.Label();
            this.LblSaveTimeUnkB = new System.Windows.Forms.Label();
            this.GbxTimeData = new System.Windows.Forms.GroupBox();
            this.GbxPlayTime = new System.Windows.Forms.GroupBox();
            this.NudPlayTimeSecond = new System.Windows.Forms.NumericUpDown();
            this.LblPlayTimeTime = new System.Windows.Forms.Label();
            this.NudPlayTimeMinute = new System.Windows.Forms.NumericUpDown();
            this.NudPlayTimeHour = new System.Windows.Forms.NumericUpDown();
            this.LblPlayTimeColon2 = new System.Windows.Forms.Label();
            this.LblPlayTimeColon1 = new System.Windows.Forms.Label();
            this.GbxInGameTime = new System.Windows.Forms.GroupBox();
            this.NudInGameTimeDay = new System.Windows.Forms.NumericUpDown();
            this.LblInGameTimeDay = new System.Windows.Forms.Label();
            this.NudInGameTimeSecond = new System.Windows.Forms.NumericUpDown();
            this.LblInGameTimeTime = new System.Windows.Forms.Label();
            this.NudInGameTimeMinute = new System.Windows.Forms.NumericUpDown();
            this.NudInGameTimeHour = new System.Windows.Forms.NumericUpDown();
            this.LblInGameTimeTimeColon2 = new System.Windows.Forms.Label();
            this.LblInGameTimeTimeColon1 = new System.Windows.Forms.Label();
            this.GbxSaveTime = new System.Windows.Forms.GroupBox();
            this.NudSaveTimeYear = new System.Windows.Forms.NumericUpDown();
            this.NudSaveTimeUnkB = new System.Windows.Forms.NumericUpDown();
            this.NudSaveTimeUnkA = new System.Windows.Forms.NumericUpDown();
            this.NudSaveTimeMSecond = new System.Windows.Forms.NumericUpDown();
            this.LblSaveTimeTimeDecimal = new System.Windows.Forms.Label();
            this.LblSaveTimeDateDash2 = new System.Windows.Forms.Label();
            this.NudSaveTimeSecond = new System.Windows.Forms.NumericUpDown();
            this.LblSaveTimeTime = new System.Windows.Forms.Label();
            this.NudSaveTimeMinute = new System.Windows.Forms.NumericUpDown();
            this.LblSaveTimeUnkA = new System.Windows.Forms.Label();
            this.NudSaveTimeHour = new System.Windows.Forms.NumericUpDown();
            this.NudSaveTimeMonth = new System.Windows.Forms.NumericUpDown();
            this.LblSaveTimeTimeColon2 = new System.Windows.Forms.Label();
            this.NudSaveTimeDay = new System.Windows.Forms.NumericUpDown();
            this.LblSaveTimeTimeColon1 = new System.Windows.Forms.Label();
            this.LblMoney = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.TbcMain = new System.Windows.Forms.TabControl();
            this.TabMain = new System.Windows.Forms.TabPage();
            this.GbxMoney = new System.Windows.Forms.GroupBox();
            this.NudNoponstones = new System.Windows.Forms.NumericUpDown();
            this.LblNoponstones = new System.Windows.Forms.Label();
            this.NudMoney = new System.Windows.Forms.NumericUpDown();
            this.GbxParty = new System.Windows.Forms.GroupBox();
            this.GbxPartyMembers = new System.Windows.Forms.GroupBox();
            this.NudPartyMemberCount = new System.Windows.Forms.NumericUpDown();
            this.LblPartyMembersCount = new System.Windows.Forms.Label();
            this.NudPartyMemberChar = new System.Windows.Forms.NumericUpDown();
            this.LblPartyMembersMember = new System.Windows.Forms.Label();
            this.CbxPartyMemberChar = new System.Windows.Forms.ComboBox();
            this.LblPartyMembersChar = new System.Windows.Forms.Label();
            this.CbxPartyMembers = new System.Windows.Forms.ComboBox();
            this.TabChars = new System.Windows.Forms.TabPage();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.LbxChars = new System.Windows.Forms.ListBox();
            this.TbcChars = new System.Windows.Forms.TabControl();
            this.TabCharsMain = new System.Windows.Forms.TabPage();
            this.GbxCharExp = new System.Windows.Forms.GroupBox();
            this.NudCharExpertRsvExp = new System.Windows.Forms.NumericUpDown();
            this.LblCharExpertRsvExp = new System.Windows.Forms.Label();
            this.NudCharExp = new System.Windows.Forms.NumericUpDown();
            this.LblCharExpertExp = new System.Windows.Forms.Label();
            this.LblCharExp = new System.Windows.Forms.Label();
            this.NudCharExpertExp = new System.Windows.Forms.NumericUpDown();
            this.GbxCharacter = new System.Windows.Forms.GroupBox();
            this.ChkCharInParty = new System.Windows.Forms.CheckBox();
            this.LblCharID = new System.Windows.Forms.Label();
            this.CbxCharID = new System.Windows.Forms.ComboBox();
            this.NudCharID = new System.Windows.Forms.NumericUpDown();
            this.GbxCharEquipment = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.CbxCharFootArmourType = new System.Windows.Forms.ComboBox();
            this.CbxCharFootArmourID = new System.Windows.Forms.ComboBox();
            this.LblCharFootArmourID = new System.Windows.Forms.Label();
            this.NudCharFootArmourIndex = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.NudCharFootArmourType = new System.Windows.Forms.NumericUpDown();
            this.LblCharFootArmourIndex = new System.Windows.Forms.Label();
            this.NudCharFootArmourID = new System.Windows.Forms.NumericUpDown();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.CbxCharLegArmourType = new System.Windows.Forms.ComboBox();
            this.CbxCharLegArmourID = new System.Windows.Forms.ComboBox();
            this.LblCharLegArmourID = new System.Windows.Forms.Label();
            this.NudCharLegArmourIndex = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.NudCharLegArmourType = new System.Windows.Forms.NumericUpDown();
            this.LblCharLegArmourIndex = new System.Windows.Forms.Label();
            this.NudCharLegArmourID = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.CbxCharArmArmourType = new System.Windows.Forms.ComboBox();
            this.CbxCharArmArmourID = new System.Windows.Forms.ComboBox();
            this.LblCharArmArmourID = new System.Windows.Forms.Label();
            this.NudCharArmArmourIndex = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.NudCharArmArmourType = new System.Windows.Forms.NumericUpDown();
            this.LblCharArmArmourIndex = new System.Windows.Forms.Label();
            this.NudCharArmArmourID = new System.Windows.Forms.NumericUpDown();
            this.GbxCharTorsoArmor = new System.Windows.Forms.GroupBox();
            this.CbxCharTorsoArmourType = new System.Windows.Forms.ComboBox();
            this.CbxCharTorsoArmourID = new System.Windows.Forms.ComboBox();
            this.LblCharTorsoArmourID = new System.Windows.Forms.Label();
            this.NudCharTorsoArmourIndex = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.NudCharTorsoArmourType = new System.Windows.Forms.NumericUpDown();
            this.LblCharTorsoArmourIndex = new System.Windows.Forms.Label();
            this.NudCharTorsoArmourID = new System.Windows.Forms.NumericUpDown();
            this.GbxCharHeadArmour = new System.Windows.Forms.GroupBox();
            this.CbxCharHeadArmourType = new System.Windows.Forms.ComboBox();
            this.CbxCharHeadArmourID = new System.Windows.Forms.ComboBox();
            this.LblCharHeadArmourID = new System.Windows.Forms.Label();
            this.NudCharHeadArmourIndex = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.NudCharHeadArmourType = new System.Windows.Forms.NumericUpDown();
            this.LblCharHeadArmourIndex = new System.Windows.Forms.Label();
            this.NudCharHeadArmourID = new System.Windows.Forms.NumericUpDown();
            this.GbxCharWeapon = new System.Windows.Forms.GroupBox();
            this.CbxCharWeaponType = new System.Windows.Forms.ComboBox();
            this.CbxCharWeaponID = new System.Windows.Forms.ComboBox();
            this.LblCharWeaponID = new System.Windows.Forms.Label();
            this.NudCharWeaponIndex = new System.Windows.Forms.NumericUpDown();
            this.LblCharWeaponType = new System.Windows.Forms.Label();
            this.NudCharWeaponType = new System.Windows.Forms.NumericUpDown();
            this.LblCharWeaponIndex = new System.Windows.Forms.Label();
            this.NudCharWeaponID = new System.Windows.Forms.NumericUpDown();
            this.GbxCharStats = new System.Windows.Forms.GroupBox();
            this.NudCharExpertLevel = new System.Windows.Forms.NumericUpDown();
            this.LblExpertLvl = new System.Windows.Forms.Label();
            this.NudCharLevel = new System.Windows.Forms.NumericUpDown();
            this.LblCharLevel = new System.Windows.Forms.Label();
            this.TabCharsArts = new System.Windows.Forms.TabPage();
            this.GbxCharSetMonadoArts = new System.Windows.Forms.GroupBox();
            this.CbxCharMonadoArt8ID = new System.Windows.Forms.ComboBox();
            this.NudCharMonadoArt8ID = new System.Windows.Forms.NumericUpDown();
            this.LblCharMonadoArt8ID = new System.Windows.Forms.Label();
            this.CbxCharMonadoArt7ID = new System.Windows.Forms.ComboBox();
            this.CbxCharMonadoArt6ID = new System.Windows.Forms.ComboBox();
            this.CbxCharMonadoArt5ID = new System.Windows.Forms.ComboBox();
            this.NudCharMonadoArt7ID = new System.Windows.Forms.NumericUpDown();
            this.NudCharMonadoArt6ID = new System.Windows.Forms.NumericUpDown();
            this.NudCharMonadoArt5ID = new System.Windows.Forms.NumericUpDown();
            this.LblCharMonadoArt7ID = new System.Windows.Forms.Label();
            this.LblCharMonadoArt5ID = new System.Windows.Forms.Label();
            this.LblCharMonadoArt6ID = new System.Windows.Forms.Label();
            this.CbxCharMonTalentArtID = new System.Windows.Forms.ComboBox();
            this.NudCharMonTalentArtID = new System.Windows.Forms.NumericUpDown();
            this.LblCharMonTalentArtID = new System.Windows.Forms.Label();
            this.CbxCharMonadoArt4ID = new System.Windows.Forms.ComboBox();
            this.NudCharMonadoArt4ID = new System.Windows.Forms.NumericUpDown();
            this.LblCharMonadoArt4ID = new System.Windows.Forms.Label();
            this.CbxCharMonadoArt3ID = new System.Windows.Forms.ComboBox();
            this.CbxCharMonadoArt2ID = new System.Windows.Forms.ComboBox();
            this.CbxCharMonadoArt1ID = new System.Windows.Forms.ComboBox();
            this.NudCharMonadoArt3ID = new System.Windows.Forms.NumericUpDown();
            this.NudCharMonadoArt2ID = new System.Windows.Forms.NumericUpDown();
            this.NudCharMonadoArt1ID = new System.Windows.Forms.NumericUpDown();
            this.LblCharMonadoArt3ID = new System.Windows.Forms.Label();
            this.LblCharMonadoArt1ID = new System.Windows.Forms.Label();
            this.LblCharMonadoArt2ID = new System.Windows.Forms.Label();
            this.GbxCharArtLevels = new System.Windows.Forms.GroupBox();
            this.CbxCharArtUnlockLvl = new System.Windows.Forms.ComboBox();
            this.LblCharArtUnlkLvl = new System.Windows.Forms.Label();
            this.NudCharArtUnlockLvl = new System.Windows.Forms.NumericUpDown();
            this.NudCharArtPoints = new System.Windows.Forms.NumericUpDown();
            this.LblCharArtPoints = new System.Windows.Forms.Label();
            this.NudCharArtLevel = new System.Windows.Forms.NumericUpDown();
            this.LblCharArtLevel = new System.Windows.Forms.Label();
            this.CbxCharArtID = new System.Windows.Forms.ComboBox();
            this.LblCharArtID = new System.Windows.Forms.Label();
            this.NudCharArtID = new System.Windows.Forms.NumericUpDown();
            this.GbxCharSetNormalArts = new System.Windows.Forms.GroupBox();
            this.CbxCharNormalArt8ID = new System.Windows.Forms.ComboBox();
            this.NudCharNormalArt8ID = new System.Windows.Forms.NumericUpDown();
            this.LblCharNormalArt8ID = new System.Windows.Forms.Label();
            this.CbxCharNormalArt7ID = new System.Windows.Forms.ComboBox();
            this.CbxCharNormalArt6ID = new System.Windows.Forms.ComboBox();
            this.CbxCharNormalArt5ID = new System.Windows.Forms.ComboBox();
            this.NudCharNormalArt7ID = new System.Windows.Forms.NumericUpDown();
            this.NudCharNormalArt6ID = new System.Windows.Forms.NumericUpDown();
            this.NudCharNormalArt5ID = new System.Windows.Forms.NumericUpDown();
            this.LblCharNormalArt7ID = new System.Windows.Forms.Label();
            this.LblCharNormalArt5ID = new System.Windows.Forms.Label();
            this.LblCharNormalArt6ID = new System.Windows.Forms.Label();
            this.CbxCharTalentArtID = new System.Windows.Forms.ComboBox();
            this.NudCharTalentArtID = new System.Windows.Forms.NumericUpDown();
            this.LblCharTalentArtID = new System.Windows.Forms.Label();
            this.CbxCharNormalArt4ID = new System.Windows.Forms.ComboBox();
            this.NudCharNormalArt4ID = new System.Windows.Forms.NumericUpDown();
            this.LblCharNormalArt4ID = new System.Windows.Forms.Label();
            this.CbxCharNormalArt3ID = new System.Windows.Forms.ComboBox();
            this.CbxCharNormalArt2ID = new System.Windows.Forms.ComboBox();
            this.CbxCharNormalArt1ID = new System.Windows.Forms.ComboBox();
            this.NudCharNormalArt3ID = new System.Windows.Forms.NumericUpDown();
            this.NudCharNormalArt2ID = new System.Windows.Forms.NumericUpDown();
            this.NudCharNormalArt1ID = new System.Windows.Forms.NumericUpDown();
            this.LblCharNormalArt3ID = new System.Windows.Forms.Label();
            this.LblCharNormalArt1ID = new System.Windows.Forms.Label();
            this.LblCharNormalArt2ID = new System.Windows.Forms.Label();
            this.TabCharsSkillTrees = new System.Windows.Forms.TabPage();
            this.gbxDriverSkillPoints = new System.Windows.Forms.GroupBox();
            this.lblDriverCurrentSkillPoints = new System.Windows.Forms.Label();
            this.nudDriverCurrentSkillPoints = new System.Windows.Forms.NumericUpDown();
            this.nudDriverTotalSkillPoints = new System.Windows.Forms.NumericUpDown();
            this.lblDriverTotalSkillPoints = new System.Windows.Forms.Label();
            this.gbxDriverHiddenSkills = new System.Windows.Forms.GroupBox();
            this.nudDriverHiddenSkill_c5_Level = new System.Windows.Forms.NumericUpDown();
            this.lblDriverHiddenSkillsColumn5Level = new System.Windows.Forms.Label();
            this.nudDriverHiddenSkill_c5_ColumnNo = new System.Windows.Forms.NumericUpDown();
            this.lblDriverHiddenSkillsColumn5No = new System.Windows.Forms.Label();
            this.nudDriverHiddenSkill_c4_Level = new System.Windows.Forms.NumericUpDown();
            this.lblDriverHiddenSkillsColumn4Level = new System.Windows.Forms.Label();
            this.nudDriverHiddenSkill_c4_ColumnNo = new System.Windows.Forms.NumericUpDown();
            this.lblDriverHiddenSkillsColumn4No = new System.Windows.Forms.Label();
            this.nudDriverHiddenSkill_c3_Level = new System.Windows.Forms.NumericUpDown();
            this.lblDriverHiddenSkillsColumn3Level = new System.Windows.Forms.Label();
            this.nudDriverHiddenSkill_c3_ColumnNo = new System.Windows.Forms.NumericUpDown();
            this.lblDriverHiddenSkillsColumn3No = new System.Windows.Forms.Label();
            this.nudDriverHiddenSkill_c2_Level = new System.Windows.Forms.NumericUpDown();
            this.lblDriverHiddenSkillsColumn2Level = new System.Windows.Forms.Label();
            this.nudDriverHiddenSkill_c2_ColumnNo = new System.Windows.Forms.NumericUpDown();
            this.lblDriverHiddenSkillsColumn2No = new System.Windows.Forms.Label();
            this.nudDriverHiddenSkill_c1_Level = new System.Windows.Forms.NumericUpDown();
            this.lblDriverHiddenSkillsColumn1Level = new System.Windows.Forms.Label();
            this.nudDriverHiddenSkill_c1_ColumnNo = new System.Windows.Forms.NumericUpDown();
            this.lblDriverHiddenSkillsColumn1No = new System.Windows.Forms.Label();
            this.lblDriverHiddenSkillsRow3 = new System.Windows.Forms.Label();
            this.lblDriverHiddenSkillsRow2 = new System.Windows.Forms.Label();
            this.lblDriverHiddenSkillsRow1 = new System.Windows.Forms.Label();
            this.cbxDriverHiddenSkill_c5r3 = new System.Windows.Forms.ComboBox();
            this.nudDriverHiddenSkill_c5r1 = new System.Windows.Forms.NumericUpDown();
            this.cbxDriverHiddenSkill_c5r2 = new System.Windows.Forms.ComboBox();
            this.nudDriverHiddenSkill_c5r2 = new System.Windows.Forms.NumericUpDown();
            this.cbxDriverHiddenSkill_c5r1 = new System.Windows.Forms.ComboBox();
            this.nudDriverHiddenSkill_c5r3 = new System.Windows.Forms.NumericUpDown();
            this.cbxDriverHiddenSkill_c4r3 = new System.Windows.Forms.ComboBox();
            this.nudDriverHiddenSkill_c4r1 = new System.Windows.Forms.NumericUpDown();
            this.cbxDriverHiddenSkill_c4r2 = new System.Windows.Forms.ComboBox();
            this.nudDriverHiddenSkill_c4r2 = new System.Windows.Forms.NumericUpDown();
            this.cbxDriverHiddenSkill_c4r1 = new System.Windows.Forms.ComboBox();
            this.nudDriverHiddenSkill_c4r3 = new System.Windows.Forms.NumericUpDown();
            this.cbxDriverHiddenSkill_c3r3 = new System.Windows.Forms.ComboBox();
            this.nudDriverHiddenSkill_c3r1 = new System.Windows.Forms.NumericUpDown();
            this.cbxDriverHiddenSkill_c3r2 = new System.Windows.Forms.ComboBox();
            this.nudDriverHiddenSkill_c3r2 = new System.Windows.Forms.NumericUpDown();
            this.cbxDriverHiddenSkill_c3r1 = new System.Windows.Forms.ComboBox();
            this.nudDriverHiddenSkill_c3r3 = new System.Windows.Forms.NumericUpDown();
            this.cbxDriverHiddenSkill_c2r3 = new System.Windows.Forms.ComboBox();
            this.nudDriverHiddenSkill_c2r1 = new System.Windows.Forms.NumericUpDown();
            this.cbxDriverHiddenSkill_c2r2 = new System.Windows.Forms.ComboBox();
            this.nudDriverHiddenSkill_c2r2 = new System.Windows.Forms.NumericUpDown();
            this.cbxDriverHiddenSkill_c2r1 = new System.Windows.Forms.ComboBox();
            this.nudDriverHiddenSkill_c2r3 = new System.Windows.Forms.NumericUpDown();
            this.cbxDriverHiddenSkill_c1r3 = new System.Windows.Forms.ComboBox();
            this.nudDriverHiddenSkill_c1r1 = new System.Windows.Forms.NumericUpDown();
            this.cbxDriverHiddenSkill_c1r2 = new System.Windows.Forms.ComboBox();
            this.nudDriverHiddenSkill_c1r2 = new System.Windows.Forms.NumericUpDown();
            this.cbxDriverHiddenSkill_c1r1 = new System.Windows.Forms.ComboBox();
            this.nudDriverHiddenSkill_c1r3 = new System.Windows.Forms.NumericUpDown();
            this.gbxDriverOvertSkills = new System.Windows.Forms.GroupBox();
            this.nudDriverOvertSkill_c5_Level = new System.Windows.Forms.NumericUpDown();
            this.lblDriverOvertSkillsColumn5Level = new System.Windows.Forms.Label();
            this.nudDriverOvertSkill_c5_ColumnNo = new System.Windows.Forms.NumericUpDown();
            this.lblDriverOvertSkillsColumn5No = new System.Windows.Forms.Label();
            this.nudDriverOvertSkill_c4_Level = new System.Windows.Forms.NumericUpDown();
            this.lblDriverOvertSkillsColumn4Level = new System.Windows.Forms.Label();
            this.nudDriverOvertSkill_c4_ColumnNo = new System.Windows.Forms.NumericUpDown();
            this.lblDriverOvertSkillsColumn4No = new System.Windows.Forms.Label();
            this.nudDriverOvertSkill_c3_Level = new System.Windows.Forms.NumericUpDown();
            this.lblDriverOvertSkillsColumn3Level = new System.Windows.Forms.Label();
            this.nudDriverOvertSkill_c3_ColumnNo = new System.Windows.Forms.NumericUpDown();
            this.lblDriverOvertSkillsColumn3No = new System.Windows.Forms.Label();
            this.nudDriverOvertSkill_c2_Level = new System.Windows.Forms.NumericUpDown();
            this.lblDriverOvertSkillsColumn2Level = new System.Windows.Forms.Label();
            this.nudDriverOvertSkill_c2_ColumnNo = new System.Windows.Forms.NumericUpDown();
            this.lblDriverOvertSkillsColumn2No = new System.Windows.Forms.Label();
            this.nudDriverOvertSkill_c1_Level = new System.Windows.Forms.NumericUpDown();
            this.lblDriverOvertSkillsColumn1Level = new System.Windows.Forms.Label();
            this.nudDriverOvertSkill_c1_ColumnNo = new System.Windows.Forms.NumericUpDown();
            this.lblDriverOvertSkillsColumn1No = new System.Windows.Forms.Label();
            this.lblDriverOvertSkillsRow3 = new System.Windows.Forms.Label();
            this.lblDriverOvertSkillsRow2 = new System.Windows.Forms.Label();
            this.lblDriverOvertSkillsRow1 = new System.Windows.Forms.Label();
            this.cbxDriverOvertSkill_c5r3 = new System.Windows.Forms.ComboBox();
            this.nudDriverOvertSkill_c5r1 = new System.Windows.Forms.NumericUpDown();
            this.cbxDriverOvertSkill_c5r2 = new System.Windows.Forms.ComboBox();
            this.nudDriverOvertSkill_c5r2 = new System.Windows.Forms.NumericUpDown();
            this.cbxDriverOvertSkill_c5r1 = new System.Windows.Forms.ComboBox();
            this.nudDriverOvertSkill_c5r3 = new System.Windows.Forms.NumericUpDown();
            this.cbxDriverOvertSkill_c4r3 = new System.Windows.Forms.ComboBox();
            this.nudDriverOvertSkill_c4r1 = new System.Windows.Forms.NumericUpDown();
            this.cbxDriverOvertSkill_c4r2 = new System.Windows.Forms.ComboBox();
            this.nudDriverOvertSkill_c4r2 = new System.Windows.Forms.NumericUpDown();
            this.cbxDriverOvertSkill_c4r1 = new System.Windows.Forms.ComboBox();
            this.nudDriverOvertSkill_c4r3 = new System.Windows.Forms.NumericUpDown();
            this.cbxDriverOvertSkill_c3r3 = new System.Windows.Forms.ComboBox();
            this.nudDriverOvertSkill_c3r1 = new System.Windows.Forms.NumericUpDown();
            this.cbxDriverOvertSkill_c3r2 = new System.Windows.Forms.ComboBox();
            this.nudDriverOvertSkill_c3r2 = new System.Windows.Forms.NumericUpDown();
            this.cbxDriverOvertSkill_c3r1 = new System.Windows.Forms.ComboBox();
            this.nudDriverOvertSkill_c3r3 = new System.Windows.Forms.NumericUpDown();
            this.cbxDriverOvertSkill_c2r3 = new System.Windows.Forms.ComboBox();
            this.nudDriverOvertSkill_c2r1 = new System.Windows.Forms.NumericUpDown();
            this.cbxDriverOvertSkill_c2r2 = new System.Windows.Forms.ComboBox();
            this.nudDriverOvertSkill_c2r2 = new System.Windows.Forms.NumericUpDown();
            this.cbxDriverOvertSkill_c2r1 = new System.Windows.Forms.ComboBox();
            this.nudDriverOvertSkill_c2r3 = new System.Windows.Forms.NumericUpDown();
            this.cbxDriverOvertSkill_c1r3 = new System.Windows.Forms.ComboBox();
            this.nudDriverOvertSkill_c1r1 = new System.Windows.Forms.NumericUpDown();
            this.cbxDriverOvertSkill_c1r2 = new System.Windows.Forms.ComboBox();
            this.nudDriverOvertSkill_c1r2 = new System.Windows.Forms.NumericUpDown();
            this.cbxDriverOvertSkill_c1r1 = new System.Windows.Forms.ComboBox();
            this.nudDriverOvertSkill_c1r3 = new System.Windows.Forms.NumericUpDown();
            this.TabCharsSkillLinks = new System.Windows.Forms.TabPage();
            this.tbcDriver150Ext = new System.Windows.Forms.TabControl();
            this.tabDriver150ExtUnk_0x00B4 = new System.Windows.Forms.TabPage();
            this.hbxDriver150ExtUnk_0x00B4 = new Be.Windows.Forms.HexBox();
            this.tabDriver150ExtUnk_0x0247 = new System.Windows.Forms.TabPage();
            this.hbxDriver150ExtUnk_0x0247 = new Be.Windows.Forms.HexBox();
            this.TabCharsUnknown = new System.Windows.Forms.TabPage();
            this.GbxCharUnk_0x0f8 = new System.Windows.Forms.GroupBox();
            this.HbxCharUnk_0x0f8 = new Be.Windows.Forms.HexBox();
            this.GbxCharUnk_0x0a4 = new System.Windows.Forms.GroupBox();
            this.HbxCharUnk_0x0a4 = new Be.Windows.Forms.HexBox();
            this.GbxCharUnk_0x06c = new System.Windows.Forms.GroupBox();
            this.HbxCharUnk_0x06c = new Be.Windows.Forms.HexBox();
            this.GbxCharUnk_0x02c = new System.Windows.Forms.GroupBox();
            this.HbxCharUnk_0x02c = new Be.Windows.Forms.HexBox();
            this.GbxCharUnk_0x010 = new System.Windows.Forms.GroupBox();
            this.HbxCharUnk_0x010 = new Be.Windows.Forms.HexBox();
            this.TabItems = new System.Windows.Forms.TabPage();
            this.splitContainer5 = new System.Windows.Forms.SplitContainer();
            this.LblItemSortTip = new System.Windows.Forms.Label();
            this.GbxItemCheat = new System.Windows.Forms.GroupBox();
            this.BtnItemCheatMaxQty = new System.Windows.Forms.Button();
            this.GbxItemAddNew = new System.Windows.Forms.GroupBox();
            this.BtnItemAddNew = new System.Windows.Forms.Button();
            this.LblItemAddNewItemQty = new System.Windows.Forms.Label();
            this.LblItemAddNewItemID = new System.Windows.Forms.Label();
            this.NudItemAddNewQty = new System.Windows.Forms.NumericUpDown();
            this.CbxItemAddNewID = new System.Windows.Forms.ComboBox();
            this.NudItemAddNewID = new System.Windows.Forms.NumericUpDown();
            this.GbxItemFind = new System.Windows.Forms.GroupBox();
            this.LblItemFindName = new System.Windows.Forms.Label();
            this.BtnItemSearch = new System.Windows.Forms.Button();
            this.TxtItemSearch = new System.Windows.Forms.TextBox();
            this.TbcItems = new System.Windows.Forms.TabControl();
            this.TabWeaponBox = new System.Windows.Forms.TabPage();
            this.DgvWeaponBox = new System.Windows.Forms.DataGridView();
            this.TabHeadArmourBox = new System.Windows.Forms.TabPage();
            this.DgvHeadArmourBox = new System.Windows.Forms.DataGridView();
            this.TabTorsoArmourBox = new System.Windows.Forms.TabPage();
            this.DgvTorsoArmourBox = new System.Windows.Forms.DataGridView();
            this.TabArmArmourBox = new System.Windows.Forms.TabPage();
            this.DgvArmArmourBox = new System.Windows.Forms.DataGridView();
            this.TabLegArmourBox = new System.Windows.Forms.TabPage();
            this.DgvLegArmourBox = new System.Windows.Forms.DataGridView();
            this.TabFootArmourBox = new System.Windows.Forms.TabPage();
            this.DgvFootArmourBox = new System.Windows.Forms.DataGridView();
            this.TabCrystalBox = new System.Windows.Forms.TabPage();
            this.DgvCrystalBox = new System.Windows.Forms.DataGridView();
            this.TabGemBox = new System.Windows.Forms.TabPage();
            this.DgvGemBox = new System.Windows.Forms.DataGridView();
            this.TabCollectBox = new System.Windows.Forms.TabPage();
            this.DgvCollectBox = new System.Windows.Forms.DataGridView();
            this.TabMaterialBox = new System.Windows.Forms.TabPage();
            this.DgvMaterialBox = new System.Windows.Forms.DataGridView();
            this.TabKeyItemBox = new System.Windows.Forms.TabPage();
            this.DgvKeyItemBox = new System.Windows.Forms.DataGridView();
            this.TabArtsManBox = new System.Windows.Forms.TabPage();
            this.DgvArtsManBox = new System.Windows.Forms.DataGridView();
            this.TabItemsSerials = new System.Windows.Forms.TabPage();
            this.gbxItemBoxSerials = new System.Windows.Forms.GroupBox();
            this.NudItemsSerial1 = new System.Windows.Forms.NumericUpDown();
            this.LblItemsSerial1 = new System.Windows.Forms.Label();
            this.LblItemsSerial2 = new System.Windows.Forms.Label();
            this.NudItemsSerial2 = new System.Windows.Forms.NumericUpDown();
            this.LblItemsSerial3 = new System.Windows.Forms.Label();
            this.NudItemsSerial3 = new System.Windows.Forms.NumericUpDown();
            this.LblItemsSerial4 = new System.Windows.Forms.Label();
            this.NudItemsSerial4 = new System.Windows.Forms.NumericUpDown();
            this.LblItemsSerial5 = new System.Windows.Forms.Label();
            this.NudItemsSerial5 = new System.Windows.Forms.NumericUpDown();
            this.LblItemsSerial6 = new System.Windows.Forms.Label();
            this.NudItemsSerial6 = new System.Windows.Forms.NumericUpDown();
            this.LblItemsSerial7 = new System.Windows.Forms.Label();
            this.NudItemsSerial13 = new System.Windows.Forms.NumericUpDown();
            this.NudItemsSerial7 = new System.Windows.Forms.NumericUpDown();
            this.LblItemsSerial13 = new System.Windows.Forms.Label();
            this.LblItemsSerial8 = new System.Windows.Forms.Label();
            this.NudItemsSerial12 = new System.Windows.Forms.NumericUpDown();
            this.NudItemsSerial8 = new System.Windows.Forms.NumericUpDown();
            this.LblItemsSerial12 = new System.Windows.Forms.Label();
            this.LblItemsSerial9 = new System.Windows.Forms.Label();
            this.NudItemsSerial11 = new System.Windows.Forms.NumericUpDown();
            this.NudItemsSerial9 = new System.Windows.Forms.NumericUpDown();
            this.LblItemsSerial11 = new System.Windows.Forms.Label();
            this.LblItemsSerial10 = new System.Windows.Forms.Label();
            this.NudItemsSerial10 = new System.Windows.Forms.NumericUpDown();
            this.TabAffChart = new System.Windows.Forms.TabPage();
            this.tbcFlagData = new System.Windows.Forms.TabControl();
            this.tabFlags1248 = new System.Windows.Forms.TabPage();
            this.gbxFlags1Bit = new System.Windows.Forms.GroupBox();
            this.dgvFlags1Bit = new System.Windows.Forms.DataGridView();
            this.gbxFlags2Bit = new System.Windows.Forms.GroupBox();
            this.dgvFlags2Bit = new System.Windows.Forms.DataGridView();
            this.gbxFlags4Bit = new System.Windows.Forms.GroupBox();
            this.dgvFlags4Bit = new System.Windows.Forms.DataGridView();
            this.gbxFlags8Bit = new System.Windows.Forms.GroupBox();
            this.dgvFlags8Bit = new System.Windows.Forms.DataGridView();
            this.tabFlags1632 = new System.Windows.Forms.TabPage();
            this.gbxFlags16Bit = new System.Windows.Forms.GroupBox();
            this.dgvFlags16Bit = new System.Windows.Forms.DataGridView();
            this.gbxFlags32Bit = new System.Windows.Forms.GroupBox();
            this.dgvFlags32Bit = new System.Windows.Forms.DataGridView();
            this.TabTimeAtk = new System.Windows.Forms.TabPage();
            this.tabUnknown = new System.Windows.Forms.TabPage();
            this.TbcUnknown = new System.Windows.Forms.TabControl();
            this.TabUnkMain = new System.Windows.Forms.TabPage();
            this.GbxUnk_0x000000 = new System.Windows.Forms.GroupBox();
            this.HbxUnk_0x000000 = new Be.Windows.Forms.HexBox();
            this.GbxUnk_0x152331 = new System.Windows.Forms.GroupBox();
            this.HbxUnk_0x152331 = new Be.Windows.Forms.HexBox();
            this.TabUnk_0x000014 = new System.Windows.Forms.TabPage();
            this.HbxUnk_0x000014 = new Be.Windows.Forms.HexBox();
            this.TabUnk_0x03b5b0 = new System.Windows.Forms.TabPage();
            this.HbxUnk_0x03b5b0 = new Be.Windows.Forms.HexBox();
            this.TabUnk_0x04693c = new System.Windows.Forms.TabPage();
            this.HbxUnk_0x04693c = new Be.Windows.Forms.HexBox();
            this.TabUnk_0x151b44 = new System.Windows.Forms.TabPage();
            this.HbxUnk_0x151b44 = new Be.Windows.Forms.HexBox();
            this.OfdSaveFile = new System.Windows.Forms.OpenFileDialog();
            this.BtnSave = new System.Windows.Forms.Button();
            this.SfdSaveFile = new System.Windows.Forms.SaveFileDialog();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.MenuStrip = new System.Windows.Forms.MenuStrip();
            this.FileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.LoadFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SaveFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ExitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AllowEditOfROValsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.HelpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.FaqToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextMenuStripItems = new System.Windows.Forms.ContextMenuStrip (this.components);
            this.tsmiItemGetNewSerial = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiItemUpdateTime = new System.Windows.Forms.ToolStripMenuItem();
            this.GbxTimeData.SuspendLayout();
            this.GbxPlayTime.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.NudPlayTimeSecond)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudPlayTimeMinute)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudPlayTimeHour)).BeginInit();
            this.GbxInGameTime.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.NudInGameTimeDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudInGameTimeSecond)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudInGameTimeMinute)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudInGameTimeHour)).BeginInit();
            this.GbxSaveTime.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.NudSaveTimeYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudSaveTimeUnkB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudSaveTimeUnkA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudSaveTimeMSecond)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudSaveTimeSecond)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudSaveTimeMinute)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudSaveTimeHour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudSaveTimeMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudSaveTimeDay)).BeginInit();
            this.TbcMain.SuspendLayout();
            this.TabMain.SuspendLayout();
            this.GbxMoney.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.NudNoponstones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudMoney)).BeginInit();
            this.GbxParty.SuspendLayout();
            this.GbxPartyMembers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.NudPartyMemberCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudPartyMemberChar)).BeginInit();
            this.TabChars.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.TbcChars.SuspendLayout();
            this.TabCharsMain.SuspendLayout();
            this.GbxCharExp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharExpertRsvExp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharExp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharExpertExp)).BeginInit();
            this.GbxCharacter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharID)).BeginInit();
            this.GbxCharEquipment.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharFootArmourIndex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharFootArmourType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharFootArmourID)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharLegArmourIndex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharLegArmourType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharLegArmourID)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharArmArmourIndex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharArmArmourType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharArmArmourID)).BeginInit();
            this.GbxCharTorsoArmor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharTorsoArmourIndex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharTorsoArmourType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharTorsoArmourID)).BeginInit();
            this.GbxCharHeadArmour.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharHeadArmourIndex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharHeadArmourType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharHeadArmourID)).BeginInit();
            this.GbxCharWeapon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharWeaponIndex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharWeaponType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharWeaponID)).BeginInit();
            this.GbxCharStats.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharExpertLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharLevel)).BeginInit();
            this.TabCharsArts.SuspendLayout();
            this.GbxCharSetMonadoArts.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharMonadoArt8ID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharMonadoArt7ID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharMonadoArt6ID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharMonadoArt5ID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharMonTalentArtID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharMonadoArt4ID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharMonadoArt3ID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharMonadoArt2ID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharMonadoArt1ID)).BeginInit();
            this.GbxCharArtLevels.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharArtUnlockLvl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharArtPoints)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharArtLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharArtID)).BeginInit();
            this.GbxCharSetNormalArts.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharNormalArt8ID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharNormalArt7ID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharNormalArt6ID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharNormalArt5ID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharTalentArtID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharNormalArt4ID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharNormalArt3ID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharNormalArt2ID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharNormalArt1ID)).BeginInit();
            this.TabCharsSkillTrees.SuspendLayout();
            this.gbxDriverSkillPoints.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverCurrentSkillPoints)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverTotalSkillPoints)).BeginInit();
            this.gbxDriverHiddenSkills.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c5_Level)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c5_ColumnNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c4_Level)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c4_ColumnNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c3_Level)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c3_ColumnNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c2_Level)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c2_ColumnNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c1_Level)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c1_ColumnNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c5r1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c5r2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c5r3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c4r1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c4r2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c4r3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c3r1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c3r2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c3r3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c2r1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c2r2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c2r3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c1r1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c1r2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c1r3)).BeginInit();
            this.gbxDriverOvertSkills.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c5_Level)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c5_ColumnNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c4_Level)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c4_ColumnNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c3_Level)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c3_ColumnNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c2_Level)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c2_ColumnNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c1_Level)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c1_ColumnNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c5r1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c5r2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c5r3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c4r1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c4r2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c4r3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c3r1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c3r2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c3r3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c2r1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c2r2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c2r3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c1r1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c1r2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c1r3)).BeginInit();
            this.TabCharsSkillLinks.SuspendLayout();
            this.tbcDriver150Ext.SuspendLayout();
            this.tabDriver150ExtUnk_0x00B4.SuspendLayout();
            this.tabDriver150ExtUnk_0x0247.SuspendLayout();
            this.TabCharsUnknown.SuspendLayout();
            this.GbxCharUnk_0x0f8.SuspendLayout();
            this.GbxCharUnk_0x0a4.SuspendLayout();
            this.GbxCharUnk_0x06c.SuspendLayout();
            this.GbxCharUnk_0x02c.SuspendLayout();
            this.GbxCharUnk_0x010.SuspendLayout();
            this.TabItems.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.splitContainer5)).BeginInit();
            this.splitContainer5.Panel1.SuspendLayout();
            this.splitContainer5.Panel2.SuspendLayout();
            this.splitContainer5.SuspendLayout();
            this.GbxItemCheat.SuspendLayout();
            this.GbxItemAddNew.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.NudItemAddNewQty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudItemAddNewID)).BeginInit();
            this.GbxItemFind.SuspendLayout();
            this.TbcItems.SuspendLayout();
            this.TabWeaponBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.DgvWeaponBox)).BeginInit();
            this.TabHeadArmourBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.DgvHeadArmourBox)).BeginInit();
            this.TabTorsoArmourBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.DgvTorsoArmourBox)).BeginInit();
            this.TabArmArmourBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.DgvArmArmourBox)).BeginInit();
            this.TabLegArmourBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.DgvLegArmourBox)).BeginInit();
            this.TabFootArmourBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.DgvFootArmourBox)).BeginInit();
            this.TabCrystalBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.DgvCrystalBox)).BeginInit();
            this.TabGemBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.DgvGemBox)).BeginInit();
            this.TabCollectBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.DgvCollectBox)).BeginInit();
            this.TabMaterialBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.DgvMaterialBox)).BeginInit();
            this.TabKeyItemBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.DgvKeyItemBox)).BeginInit();
            this.TabArtsManBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.DgvArtsManBox)).BeginInit();
            this.TabItemsSerials.SuspendLayout();
            this.gbxItemBoxSerials.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.NudItemsSerial1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudItemsSerial2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudItemsSerial3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudItemsSerial4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudItemsSerial5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudItemsSerial6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudItemsSerial13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudItemsSerial7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudItemsSerial12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudItemsSerial8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudItemsSerial11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudItemsSerial9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudItemsSerial10)).BeginInit();
            this.TabAffChart.SuspendLayout();
            this.tbcFlagData.SuspendLayout();
            this.tabFlags1248.SuspendLayout();
            this.gbxFlags1Bit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.dgvFlags1Bit)).BeginInit();
            this.gbxFlags2Bit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.dgvFlags2Bit)).BeginInit();
            this.gbxFlags4Bit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.dgvFlags4Bit)).BeginInit();
            this.gbxFlags8Bit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.dgvFlags8Bit)).BeginInit();
            this.tabFlags1632.SuspendLayout();
            this.gbxFlags16Bit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.dgvFlags16Bit)).BeginInit();
            this.gbxFlags32Bit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.dgvFlags32Bit)).BeginInit();
            this.tabUnknown.SuspendLayout();
            this.TbcUnknown.SuspendLayout();
            this.TabUnkMain.SuspendLayout();
            this.GbxUnk_0x000000.SuspendLayout();
            this.GbxUnk_0x152331.SuspendLayout();
            this.TabUnk_0x000014.SuspendLayout();
            this.TabUnk_0x03b5b0.SuspendLayout();
            this.TabUnk_0x04693c.SuspendLayout();
            this.TabUnk_0x151b44.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.MenuStrip.SuspendLayout();
            this.ContextMenuStripItems.SuspendLayout();
            this.SuspendLayout();
            // 
            // BtnLoad
            // 
            this.BtnLoad.Location = new System.Drawing.Point (12, 27);
            this.BtnLoad.Name = "BtnLoad";
            this.BtnLoad.Size = new System.Drawing.Size (75, 23);
            this.BtnLoad.TabIndex = 0;
            this.BtnLoad.Text = "Load File...";
            this.BtnLoad.UseVisualStyleBackColor = true;
            this.BtnLoad.Click += new System.EventHandler (this.BtnLoad_Click);
            // 
            // LblSaveTimeDate
            // 
            this.LblSaveTimeDate.AutoSize = true;
            this.LblSaveTimeDate.Location = new System.Drawing.Point (6, 21);
            this.LblSaveTimeDate.Name = "LblSaveTimeDate";
            this.LblSaveTimeDate.Size = new System.Drawing.Size (33, 13);
            this.LblSaveTimeDate.TabIndex = 4;
            this.LblSaveTimeDate.Text = "Date:";
            // 
            // LblSaveTimeDateDash1
            // 
            this.LblSaveTimeDateDash1.AutoSize = true;
            this.LblSaveTimeDateDash1.Location = new System.Drawing.Point (95, 21);
            this.LblSaveTimeDateDash1.Name = "LblSaveTimeDateDash1";
            this.LblSaveTimeDateDash1.Size = new System.Drawing.Size (10, 13);
            this.LblSaveTimeDateDash1.TabIndex = 5;
            this.LblSaveTimeDateDash1.Text = "-";
            // 
            // LblSaveTimeUnkB
            // 
            this.LblSaveTimeUnkB.AutoSize = true;
            this.LblSaveTimeUnkB.Location = new System.Drawing.Point (260, 44);
            this.LblSaveTimeUnkB.Name = "LblSaveTimeUnkB";
            this.LblSaveTimeUnkB.Size = new System.Drawing.Size (37, 13);
            this.LblSaveTimeUnkB.TabIndex = 6;
            this.LblSaveTimeUnkB.Text = "UnkB:";
            // 
            // GbxTimeData
            // 
            this.GbxTimeData.Controls.Add (this.GbxPlayTime);
            this.GbxTimeData.Controls.Add (this.GbxInGameTime);
            this.GbxTimeData.Controls.Add (this.GbxSaveTime);
            this.GbxTimeData.Location = new System.Drawing.Point (6, 57);
            this.GbxTimeData.Name = "GbxTimeData";
            this.GbxTimeData.Size = new System.Drawing.Size (372, 201);
            this.GbxTimeData.TabIndex = 7;
            this.GbxTimeData.TabStop = false;
            this.GbxTimeData.Text = "Time Data";
            // 
            // GbxPlayTime
            // 
            this.GbxPlayTime.Controls.Add (this.NudPlayTimeSecond);
            this.GbxPlayTime.Controls.Add (this.LblPlayTimeTime);
            this.GbxPlayTime.Controls.Add (this.NudPlayTimeMinute);
            this.GbxPlayTime.Controls.Add (this.NudPlayTimeHour);
            this.GbxPlayTime.Controls.Add (this.LblPlayTimeColon2);
            this.GbxPlayTime.Controls.Add (this.LblPlayTimeColon1);
            this.GbxPlayTime.Location = new System.Drawing.Point (6, 145);
            this.GbxPlayTime.Name = "GbxPlayTime";
            this.GbxPlayTime.Size = new System.Drawing.Size (201, 46);
            this.GbxPlayTime.TabIndex = 37;
            this.GbxPlayTime.TabStop = false;
            this.GbxPlayTime.Text = "Total Play Time:";
            // 
            // NudPlayTimeSecond
            // 
            this.NudPlayTimeSecond.Location = new System.Drawing.Point (155, 19);
            this.NudPlayTimeSecond.Maximum = new decimal (new int[] {
            59,
            0,
            0,
            0});
            this.NudPlayTimeSecond.Name = "NudPlayTimeSecond";
            this.NudPlayTimeSecond.Size = new System.Drawing.Size (39, 20);
            this.NudPlayTimeSecond.TabIndex = 30;
            this.NudPlayTimeSecond.ValueChanged += new System.EventHandler (this.NudPlayTimeSecond_ValueChanged);
            // 
            // LblPlayTimeTime
            // 
            this.LblPlayTimeTime.AutoSize = true;
            this.LblPlayTimeTime.Location = new System.Drawing.Point (6, 21);
            this.LblPlayTimeTime.Name = "LblPlayTimeTime";
            this.LblPlayTimeTime.Size = new System.Drawing.Size (33, 13);
            this.LblPlayTimeTime.TabIndex = 16;
            this.LblPlayTimeTime.Text = "Time:";
            // 
            // NudPlayTimeMinute
            // 
            this.NudPlayTimeMinute.Location = new System.Drawing.Point (105, 19);
            this.NudPlayTimeMinute.Maximum = new decimal (new int[] {
            59,
            0,
            0,
            0});
            this.NudPlayTimeMinute.Name = "NudPlayTimeMinute";
            this.NudPlayTimeMinute.Size = new System.Drawing.Size (39, 20);
            this.NudPlayTimeMinute.TabIndex = 29;
            this.NudPlayTimeMinute.ValueChanged += new System.EventHandler (this.NudPlayTimeMinute_ValueChanged);
            // 
            // NudPlayTimeHour
            // 
            this.NudPlayTimeHour.Location = new System.Drawing.Point (45, 19);
            this.NudPlayTimeHour.Maximum = new decimal (new int[] {
            1048575,
            0,
            0,
            0});
            this.NudPlayTimeHour.Name = "NudPlayTimeHour";
            this.NudPlayTimeHour.Size = new System.Drawing.Size (49, 20);
            this.NudPlayTimeHour.TabIndex = 28;
            this.NudPlayTimeHour.ValueChanged += new System.EventHandler (this.NudPlayTimeHour_ValueChanged);
            // 
            // LblPlayTimeColon2
            // 
            this.LblPlayTimeColon2.AutoSize = true;
            this.LblPlayTimeColon2.Location = new System.Drawing.Point (145, 21);
            this.LblPlayTimeColon2.Name = "LblPlayTimeColon2";
            this.LblPlayTimeColon2.Size = new System.Drawing.Size (10, 13);
            this.LblPlayTimeColon2.TabIndex = 27;
            this.LblPlayTimeColon2.Text = ":";
            // 
            // LblPlayTimeColon1
            // 
            this.LblPlayTimeColon1.AutoSize = true;
            this.LblPlayTimeColon1.Location = new System.Drawing.Point (95, 21);
            this.LblPlayTimeColon1.Name = "LblPlayTimeColon1";
            this.LblPlayTimeColon1.Size = new System.Drawing.Size (10, 13);
            this.LblPlayTimeColon1.TabIndex = 26;
            this.LblPlayTimeColon1.Text = ":";
            // 
            // GbxInGameTime
            // 
            this.GbxInGameTime.Controls.Add (this.NudInGameTimeDay);
            this.GbxInGameTime.Controls.Add (this.LblInGameTimeDay);
            this.GbxInGameTime.Controls.Add (this.NudInGameTimeSecond);
            this.GbxInGameTime.Controls.Add (this.LblInGameTimeTime);
            this.GbxInGameTime.Controls.Add (this.NudInGameTimeMinute);
            this.GbxInGameTime.Controls.Add (this.NudInGameTimeHour);
            this.GbxInGameTime.Controls.Add (this.LblInGameTimeTimeColon2);
            this.GbxInGameTime.Controls.Add (this.LblInGameTimeTimeColon1);
            this.GbxInGameTime.Location = new System.Drawing.Point (6, 93);
            this.GbxInGameTime.Name = "GbxInGameTime";
            this.GbxInGameTime.Size = new System.Drawing.Size (297, 46);
            this.GbxInGameTime.TabIndex = 36;
            this.GbxInGameTime.TabStop = false;
            this.GbxInGameTime.Text = "Current In-Game Time";
            // 
            // NudInGameTimeDay
            // 
            this.NudInGameTimeDay.Location = new System.Drawing.Point (45, 19);
            this.NudInGameTimeDay.Maximum = new decimal (new int[] {
            32767,
            0,
            0,
            0});
            this.NudInGameTimeDay.Name = "NudInGameTimeDay";
            this.NudInGameTimeDay.Size = new System.Drawing.Size (49, 20);
            this.NudInGameTimeDay.TabIndex = 23;
            this.NudInGameTimeDay.ValueChanged += new System.EventHandler (this.NudInGameTimeDay_ValueChanged);
            // 
            // LblInGameTimeDay
            // 
            this.LblInGameTimeDay.AutoSize = true;
            this.LblInGameTimeDay.Location = new System.Drawing.Point (6, 21);
            this.LblInGameTimeDay.Name = "LblInGameTimeDay";
            this.LblInGameTimeDay.Size = new System.Drawing.Size (29, 13);
            this.LblInGameTimeDay.TabIndex = 4;
            this.LblInGameTimeDay.Text = "Day:";
            // 
            // NudInGameTimeSecond
            // 
            this.NudInGameTimeSecond.Location = new System.Drawing.Point (251, 19);
            this.NudInGameTimeSecond.Maximum = new decimal (new int[] {
            59,
            0,
            0,
            0});
            this.NudInGameTimeSecond.Name = "NudInGameTimeSecond";
            this.NudInGameTimeSecond.Size = new System.Drawing.Size (39, 20);
            this.NudInGameTimeSecond.TabIndex = 30;
            this.NudInGameTimeSecond.ValueChanged += new System.EventHandler (this.NudInGameTimeSecond_ValueChanged);
            // 
            // LblInGameTimeTime
            // 
            this.LblInGameTimeTime.AutoSize = true;
            this.LblInGameTimeTime.Location = new System.Drawing.Point (102, 21);
            this.LblInGameTimeTime.Name = "LblInGameTimeTime";
            this.LblInGameTimeTime.Size = new System.Drawing.Size (33, 13);
            this.LblInGameTimeTime.TabIndex = 16;
            this.LblInGameTimeTime.Text = "Time:";
            // 
            // NudInGameTimeMinute
            // 
            this.NudInGameTimeMinute.Location = new System.Drawing.Point (201, 19);
            this.NudInGameTimeMinute.Maximum = new decimal (new int[] {
            59,
            0,
            0,
            0});
            this.NudInGameTimeMinute.Name = "NudInGameTimeMinute";
            this.NudInGameTimeMinute.Size = new System.Drawing.Size (39, 20);
            this.NudInGameTimeMinute.TabIndex = 29;
            this.NudInGameTimeMinute.ValueChanged += new System.EventHandler (this.NudInGameTimeMinute_ValueChanged);
            // 
            // NudInGameTimeHour
            // 
            this.NudInGameTimeHour.Location = new System.Drawing.Point (141, 19);
            this.NudInGameTimeHour.Maximum = new decimal (new int[] {
            23,
            0,
            0,
            0});
            this.NudInGameTimeHour.Name = "NudInGameTimeHour";
            this.NudInGameTimeHour.Size = new System.Drawing.Size (49, 20);
            this.NudInGameTimeHour.TabIndex = 28;
            this.NudInGameTimeHour.ValueChanged += new System.EventHandler (this.NudInGameTimeHour_ValueChanged);
            // 
            // LblInGameTimeTimeColon2
            // 
            this.LblInGameTimeTimeColon2.AutoSize = true;
            this.LblInGameTimeTimeColon2.Location = new System.Drawing.Point (241, 21);
            this.LblInGameTimeTimeColon2.Name = "LblInGameTimeTimeColon2";
            this.LblInGameTimeTimeColon2.Size = new System.Drawing.Size (10, 13);
            this.LblInGameTimeTimeColon2.TabIndex = 27;
            this.LblInGameTimeTimeColon2.Text = ":";
            // 
            // LblInGameTimeTimeColon1
            // 
            this.LblInGameTimeTimeColon1.AutoSize = true;
            this.LblInGameTimeTimeColon1.Location = new System.Drawing.Point (191, 21);
            this.LblInGameTimeTimeColon1.Name = "LblInGameTimeTimeColon1";
            this.LblInGameTimeTimeColon1.Size = new System.Drawing.Size (10, 13);
            this.LblInGameTimeTimeColon1.TabIndex = 26;
            this.LblInGameTimeTimeColon1.Text = ":";
            // 
            // GbxSaveTime
            // 
            this.GbxSaveTime.Controls.Add (this.NudSaveTimeYear);
            this.GbxSaveTime.Controls.Add (this.NudSaveTimeUnkB);
            this.GbxSaveTime.Controls.Add (this.LblSaveTimeDateDash1);
            this.GbxSaveTime.Controls.Add (this.NudSaveTimeUnkA);
            this.GbxSaveTime.Controls.Add (this.LblSaveTimeUnkB);
            this.GbxSaveTime.Controls.Add (this.NudSaveTimeMSecond);
            this.GbxSaveTime.Controls.Add (this.LblSaveTimeDate);
            this.GbxSaveTime.Controls.Add (this.LblSaveTimeTimeDecimal);
            this.GbxSaveTime.Controls.Add (this.LblSaveTimeDateDash2);
            this.GbxSaveTime.Controls.Add (this.NudSaveTimeSecond);
            this.GbxSaveTime.Controls.Add (this.LblSaveTimeTime);
            this.GbxSaveTime.Controls.Add (this.NudSaveTimeMinute);
            this.GbxSaveTime.Controls.Add (this.LblSaveTimeUnkA);
            this.GbxSaveTime.Controls.Add (this.NudSaveTimeHour);
            this.GbxSaveTime.Controls.Add (this.NudSaveTimeMonth);
            this.GbxSaveTime.Controls.Add (this.LblSaveTimeTimeColon2);
            this.GbxSaveTime.Controls.Add (this.NudSaveTimeDay);
            this.GbxSaveTime.Controls.Add (this.LblSaveTimeTimeColon1);
            this.GbxSaveTime.Location = new System.Drawing.Point (6, 19);
            this.GbxSaveTime.Name = "GbxSaveTime";
            this.GbxSaveTime.Size = new System.Drawing.Size (359, 68);
            this.GbxSaveTime.TabIndex = 35;
            this.GbxSaveTime.TabStop = false;
            this.GbxSaveTime.Text = "Time Saved";
            // 
            // NudSaveTimeYear
            // 
            this.NudSaveTimeYear.Location = new System.Drawing.Point (45, 19);
            this.NudSaveTimeYear.Maximum = new decimal (new int[] {
            8191,
            0,
            0,
            0});
            this.NudSaveTimeYear.Name = "NudSaveTimeYear";
            this.NudSaveTimeYear.Size = new System.Drawing.Size (49, 20);
            this.NudSaveTimeYear.TabIndex = 23;
            this.NudSaveTimeYear.ValueChanged += new System.EventHandler (this.NudSaveTimeYear_ValueChanged);
            // 
            // NudSaveTimeUnkB
            // 
            this.NudSaveTimeUnkB.Hexadecimal = true;
            this.NudSaveTimeUnkB.Location = new System.Drawing.Point (303, 42);
            this.NudSaveTimeUnkB.Maximum = new decimal (new int[] {
            15,
            0,
            0,
            0});
            this.NudSaveTimeUnkB.Name = "NudSaveTimeUnkB";
            this.NudSaveTimeUnkB.Size = new System.Drawing.Size (49, 20);
            this.NudSaveTimeUnkB.TabIndex = 34;
            this.NudSaveTimeUnkB.ValueChanged += new System.EventHandler (this.NudSaveTimeUnkB_ValueChanged);
            // 
            // NudSaveTimeUnkA
            // 
            this.NudSaveTimeUnkA.Hexadecimal = true;
            this.NudSaveTimeUnkA.Location = new System.Drawing.Point (303, 19);
            this.NudSaveTimeUnkA.Maximum = new decimal (new int[] {
            511,
            0,
            0,
            0});
            this.NudSaveTimeUnkA.Name = "NudSaveTimeUnkA";
            this.NudSaveTimeUnkA.Size = new System.Drawing.Size (49, 20);
            this.NudSaveTimeUnkA.TabIndex = 33;
            this.NudSaveTimeUnkA.ValueChanged += new System.EventHandler (this.NudSaveTimeUnkA_ValueChanged);
            // 
            // NudSaveTimeMSecond
            // 
            this.NudSaveTimeMSecond.Location = new System.Drawing.Point (205, 42);
            this.NudSaveTimeMSecond.Maximum = new decimal (new int[] {
            999,
            0,
            0,
            0});
            this.NudSaveTimeMSecond.Name = "NudSaveTimeMSecond";
            this.NudSaveTimeMSecond.Size = new System.Drawing.Size (49, 20);
            this.NudSaveTimeMSecond.TabIndex = 32;
            this.NudSaveTimeMSecond.ValueChanged += new System.EventHandler (this.NudSaveTimeMSecond_ValueChanged);
            // 
            // LblSaveTimeTimeDecimal
            // 
            this.LblSaveTimeTimeDecimal.AutoSize = true;
            this.LblSaveTimeTimeDecimal.Location = new System.Drawing.Point (195, 44);
            this.LblSaveTimeTimeDecimal.Name = "LblSaveTimeTimeDecimal";
            this.LblSaveTimeTimeDecimal.Size = new System.Drawing.Size (10, 13);
            this.LblSaveTimeTimeDecimal.TabIndex = 31;
            this.LblSaveTimeTimeDecimal.Text = ".";
            // 
            // LblSaveTimeDateDash2
            // 
            this.LblSaveTimeDateDash2.AutoSize = true;
            this.LblSaveTimeDateDash2.Location = new System.Drawing.Point (145, 21);
            this.LblSaveTimeDateDash2.Name = "LblSaveTimeDateDash2";
            this.LblSaveTimeDateDash2.Size = new System.Drawing.Size (10, 13);
            this.LblSaveTimeDateDash2.TabIndex = 10;
            this.LblSaveTimeDateDash2.Text = "-";
            // 
            // NudSaveTimeSecond
            // 
            this.NudSaveTimeSecond.Location = new System.Drawing.Point (155, 42);
            this.NudSaveTimeSecond.Maximum = new decimal (new int[] {
            59,
            0,
            0,
            0});
            this.NudSaveTimeSecond.Name = "NudSaveTimeSecond";
            this.NudSaveTimeSecond.Size = new System.Drawing.Size (39, 20);
            this.NudSaveTimeSecond.TabIndex = 30;
            this.NudSaveTimeSecond.ValueChanged += new System.EventHandler (this.NudSaveTimeSecond_ValueChanged);
            // 
            // LblSaveTimeTime
            // 
            this.LblSaveTimeTime.AutoSize = true;
            this.LblSaveTimeTime.Location = new System.Drawing.Point (6, 44);
            this.LblSaveTimeTime.Name = "LblSaveTimeTime";
            this.LblSaveTimeTime.Size = new System.Drawing.Size (33, 13);
            this.LblSaveTimeTime.TabIndex = 16;
            this.LblSaveTimeTime.Text = "Time:";
            // 
            // NudSaveTimeMinute
            // 
            this.NudSaveTimeMinute.Location = new System.Drawing.Point (105, 42);
            this.NudSaveTimeMinute.Maximum = new decimal (new int[] {
            59,
            0,
            0,
            0});
            this.NudSaveTimeMinute.Name = "NudSaveTimeMinute";
            this.NudSaveTimeMinute.Size = new System.Drawing.Size (39, 20);
            this.NudSaveTimeMinute.TabIndex = 29;
            this.NudSaveTimeMinute.ValueChanged += new System.EventHandler (this.NudSaveTimeMinute_ValueChanged);
            // 
            // LblSaveTimeUnkA
            // 
            this.LblSaveTimeUnkA.AutoSize = true;
            this.LblSaveTimeUnkA.Location = new System.Drawing.Point (260, 21);
            this.LblSaveTimeUnkA.Name = "LblSaveTimeUnkA";
            this.LblSaveTimeUnkA.Size = new System.Drawing.Size (37, 13);
            this.LblSaveTimeUnkA.TabIndex = 21;
            this.LblSaveTimeUnkA.Text = "UnkA:";
            // 
            // NudSaveTimeHour
            // 
            this.NudSaveTimeHour.Location = new System.Drawing.Point (45, 42);
            this.NudSaveTimeHour.Maximum = new decimal (new int[] {
            23,
            0,
            0,
            0});
            this.NudSaveTimeHour.Name = "NudSaveTimeHour";
            this.NudSaveTimeHour.Size = new System.Drawing.Size (49, 20);
            this.NudSaveTimeHour.TabIndex = 28;
            this.NudSaveTimeHour.ValueChanged += new System.EventHandler (this.NudSaveTimeHour_ValueChanged);
            // 
            // NudSaveTimeMonth
            // 
            this.NudSaveTimeMonth.Location = new System.Drawing.Point (105, 19);
            this.NudSaveTimeMonth.Maximum = new decimal (new int[] {
            12,
            0,
            0,
            0});
            this.NudSaveTimeMonth.Name = "NudSaveTimeMonth";
            this.NudSaveTimeMonth.Size = new System.Drawing.Size (39, 20);
            this.NudSaveTimeMonth.TabIndex = 24;
            this.NudSaveTimeMonth.ValueChanged += new System.EventHandler (this.NudSaveTimeMonth_ValueChanged);
            // 
            // LblSaveTimeTimeColon2
            // 
            this.LblSaveTimeTimeColon2.AutoSize = true;
            this.LblSaveTimeTimeColon2.Location = new System.Drawing.Point (145, 44);
            this.LblSaveTimeTimeColon2.Name = "LblSaveTimeTimeColon2";
            this.LblSaveTimeTimeColon2.Size = new System.Drawing.Size (10, 13);
            this.LblSaveTimeTimeColon2.TabIndex = 27;
            this.LblSaveTimeTimeColon2.Text = ":";
            // 
            // NudSaveTimeDay
            // 
            this.NudSaveTimeDay.Location = new System.Drawing.Point (155, 19);
            this.NudSaveTimeDay.Maximum = new decimal (new int[] {
            31,
            0,
            0,
            0});
            this.NudSaveTimeDay.Name = "NudSaveTimeDay";
            this.NudSaveTimeDay.Size = new System.Drawing.Size (39, 20);
            this.NudSaveTimeDay.TabIndex = 25;
            this.NudSaveTimeDay.ValueChanged += new System.EventHandler (this.NudSaveTimeDay_ValueChanged);
            // 
            // LblSaveTimeTimeColon1
            // 
            this.LblSaveTimeTimeColon1.AutoSize = true;
            this.LblSaveTimeTimeColon1.Location = new System.Drawing.Point (95, 44);
            this.LblSaveTimeTimeColon1.Name = "LblSaveTimeTimeColon1";
            this.LblSaveTimeTimeColon1.Size = new System.Drawing.Size (10, 13);
            this.LblSaveTimeTimeColon1.TabIndex = 26;
            this.LblSaveTimeTimeColon1.Text = ":";
            // 
            // LblMoney
            // 
            this.LblMoney.AutoSize = true;
            this.LblMoney.Location = new System.Drawing.Point (6, 17);
            this.LblMoney.Name = "LblMoney";
            this.LblMoney.Size = new System.Drawing.Size (42, 13);
            this.LblMoney.TabIndex = 10;
            this.LblMoney.Text = "Money:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point (150, 17);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size (15, 13);
            this.label13.TabIndex = 12;
            this.label13.Text = "G";
            // 
            // TbcMain
            // 
            this.TbcMain.Controls.Add (this.TabMain);
            this.TbcMain.Controls.Add (this.TabChars);
            this.TbcMain.Controls.Add (this.TabItems);
            this.TbcMain.Controls.Add (this.TabAffChart);
            this.TbcMain.Controls.Add (this.TabTimeAtk);
            this.TbcMain.Controls.Add (this.tabUnknown);
            this.TbcMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TbcMain.Enabled = false;
            this.TbcMain.Location = new System.Drawing.Point (0, 0);
            this.TbcMain.Name = "TbcMain";
            this.TbcMain.SelectedIndex = 0;
            this.TbcMain.Size = new System.Drawing.Size (1264, 644);
            this.TbcMain.TabIndex = 32;
            // 
            // TabMain
            // 
            this.TabMain.BackColor = System.Drawing.Color.Transparent;
            this.TabMain.Controls.Add (this.GbxMoney);
            this.TabMain.Controls.Add (this.GbxParty);
            this.TabMain.Controls.Add (this.GbxTimeData);
            this.TabMain.Location = new System.Drawing.Point (4, 22);
            this.TabMain.Name = "TabMain";
            this.TabMain.Padding = new System.Windows.Forms.Padding (3);
            this.TabMain.Size = new System.Drawing.Size (1256, 618);
            this.TabMain.TabIndex = 0;
            this.TabMain.Text = "Main";
            // 
            // GbxMoney
            // 
            this.GbxMoney.Controls.Add (this.NudNoponstones);
            this.GbxMoney.Controls.Add (this.LblNoponstones);
            this.GbxMoney.Controls.Add (this.NudMoney);
            this.GbxMoney.Controls.Add (this.label13);
            this.GbxMoney.Controls.Add (this.LblMoney);
            this.GbxMoney.Location = new System.Drawing.Point (6, 6);
            this.GbxMoney.Name = "GbxMoney";
            this.GbxMoney.Size = new System.Drawing.Size (358, 45);
            this.GbxMoney.TabIndex = 35;
            this.GbxMoney.TabStop = false;
            this.GbxMoney.Text = "Money";
            // 
            // NudNoponstones
            // 
            this.NudNoponstones.Location = new System.Drawing.Point (263, 15);
            this.NudNoponstones.Maximum = new decimal (new int[] {
            99999999,
            0,
            0,
            0});
            this.NudNoponstones.Name = "NudNoponstones";
            this.NudNoponstones.Size = new System.Drawing.Size (90, 20);
            this.NudNoponstones.TabIndex = 35;
            this.NudNoponstones.ValueChanged += new System.EventHandler (this.NudNoponstones_ValueChanged);
            // 
            // LblNoponstones
            // 
            this.LblNoponstones.AutoSize = true;
            this.LblNoponstones.Location = new System.Drawing.Point (184, 17);
            this.LblNoponstones.Name = "LblNoponstones";
            this.LblNoponstones.Size = new System.Drawing.Size (73, 13);
            this.LblNoponstones.TabIndex = 34;
            this.LblNoponstones.Text = "Noponstones:";
            // 
            // NudMoney
            // 
            this.NudMoney.Location = new System.Drawing.Point (54, 15);
            this.NudMoney.Maximum = new decimal (new int[] {
            99999999,
            0,
            0,
            0});
            this.NudMoney.Name = "NudMoney";
            this.NudMoney.Size = new System.Drawing.Size (90, 20);
            this.NudMoney.TabIndex = 33;
            this.NudMoney.ValueChanged += new System.EventHandler (this.NudMoney_ValueChanged);
            // 
            // GbxParty
            // 
            this.GbxParty.Controls.Add (this.GbxPartyMembers);
            this.GbxParty.Location = new System.Drawing.Point (384, 6);
            this.GbxParty.Name = "GbxParty";
            this.GbxParty.Size = new System.Drawing.Size (274, 160);
            this.GbxParty.TabIndex = 32;
            this.GbxParty.TabStop = false;
            this.GbxParty.Text = "Party";
            // 
            // GbxPartyMembers
            // 
            this.GbxPartyMembers.Controls.Add (this.NudPartyMemberCount);
            this.GbxPartyMembers.Controls.Add (this.LblPartyMembersCount);
            this.GbxPartyMembers.Controls.Add (this.NudPartyMemberChar);
            this.GbxPartyMembers.Controls.Add (this.LblPartyMembersMember);
            this.GbxPartyMembers.Controls.Add (this.CbxPartyMemberChar);
            this.GbxPartyMembers.Controls.Add (this.LblPartyMembersChar);
            this.GbxPartyMembers.Controls.Add (this.CbxPartyMembers);
            this.GbxPartyMembers.Location = new System.Drawing.Point (6, 15);
            this.GbxPartyMembers.Name = "GbxPartyMembers";
            this.GbxPartyMembers.Size = new System.Drawing.Size (261, 139);
            this.GbxPartyMembers.TabIndex = 2;
            this.GbxPartyMembers.TabStop = false;
            this.GbxPartyMembers.Text = "Party Members";
            // 
            // NudPartyMemberCount
            // 
            this.NudPartyMemberCount.Enabled = false;
            this.NudPartyMemberCount.Location = new System.Drawing.Point (118, 74);
            this.NudPartyMemberCount.Maximum = new decimal (new int[] {
            27,
            0,
            0,
            0});
            this.NudPartyMemberCount.Name = "NudPartyMemberCount";
            this.NudPartyMemberCount.ReadOnly = true;
            this.NudPartyMemberCount.Size = new System.Drawing.Size (63, 20);
            this.NudPartyMemberCount.TabIndex = 159;
            // 
            // LblPartyMembersCount
            // 
            this.LblPartyMembersCount.AutoSize = true;
            this.LblPartyMembersCount.Location = new System.Drawing.Point (6, 76);
            this.LblPartyMembersCount.Name = "LblPartyMembersCount";
            this.LblPartyMembersCount.Size = new System.Drawing.Size (106, 13);
            this.LblPartyMembersCount.TabIndex = 158;
            this.LblPartyMembersCount.Text = "Party Member Count:";
            // 
            // NudPartyMemberChar
            // 
            this.NudPartyMemberChar.Location = new System.Drawing.Point (76, 41);
            this.NudPartyMemberChar.Maximum = new decimal (new int[] {
            27,
            0,
            0,
            0});
            this.NudPartyMemberChar.Name = "NudPartyMemberChar";
            this.NudPartyMemberChar.Size = new System.Drawing.Size (63, 20);
            this.NudPartyMemberChar.TabIndex = 157;
            this.NudPartyMemberChar.ValueChanged += new System.EventHandler (this.NudPartyMemberChar_ValueChanged);
            // 
            // LblPartyMembersMember
            // 
            this.LblPartyMembersMember.AutoSize = true;
            this.LblPartyMembersMember.Location = new System.Drawing.Point (6, 16);
            this.LblPartyMembersMember.Name = "LblPartyMembersMember";
            this.LblPartyMembersMember.Size = new System.Drawing.Size (48, 13);
            this.LblPartyMembersMember.TabIndex = 9;
            this.LblPartyMembersMember.Text = "Member:";
            // 
            // CbxPartyMemberChar
            // 
            this.CbxPartyMemberChar.FormattingEnabled = true;
            this.CbxPartyMemberChar.Location = new System.Drawing.Point (140, 41);
            this.CbxPartyMemberChar.Name = "CbxPartyMemberChar";
            this.CbxPartyMemberChar.Size = new System.Drawing.Size (115, 21);
            this.CbxPartyMemberChar.TabIndex = 4;
            this.CbxPartyMemberChar.SelectionChangeCommitted += new System.EventHandler (this.CbxPartyMemberChar_SelectionChangeCommitted);
            // 
            // LblPartyMembersChar
            // 
            this.LblPartyMembersChar.AutoSize = true;
            this.LblPartyMembersChar.Location = new System.Drawing.Point (6, 44);
            this.LblPartyMembersChar.Name = "LblPartyMembersChar";
            this.LblPartyMembersChar.Size = new System.Drawing.Size (56, 13);
            this.LblPartyMembersChar.TabIndex = 1;
            this.LblPartyMembersChar.Text = "Character:";
            // 
            // CbxPartyMembers
            // 
            this.CbxPartyMembers.FormattingEnabled = true;
            this.CbxPartyMembers.Location = new System.Drawing.Point (76, 13);
            this.CbxPartyMembers.Name = "CbxPartyMembers";
            this.CbxPartyMembers.Size = new System.Drawing.Size (121, 21);
            this.CbxPartyMembers.TabIndex = 0;
            this.CbxPartyMembers.SelectedIndexChanged += new System.EventHandler (this.CbxPartyMembers_SelectedIndexChanged);
            // 
            // TabChars
            // 
            this.TabChars.BackColor = System.Drawing.SystemColors.Control;
            this.TabChars.Controls.Add (this.splitContainer2);
            this.TabChars.Location = new System.Drawing.Point (4, 22);
            this.TabChars.Name = "TabChars";
            this.TabChars.Padding = new System.Windows.Forms.Padding (3);
            this.TabChars.Size = new System.Drawing.Size (1256, 618);
            this.TabChars.TabIndex = 1;
            this.TabChars.Text = "Characters";
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer2.IsSplitterFixed = true;
            this.splitContainer2.Location = new System.Drawing.Point (3, 3);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add (this.LbxChars);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add (this.TbcChars);
            this.splitContainer2.Size = new System.Drawing.Size (1250, 612);
            this.splitContainer2.SplitterDistance = 168;
            this.splitContainer2.TabIndex = 0;
            // 
            // LbxChars
            // 
            this.LbxChars.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LbxChars.FormattingEnabled = true;
            this.LbxChars.Location = new System.Drawing.Point (0, 0);
            this.LbxChars.Name = "LbxChars";
            this.LbxChars.Size = new System.Drawing.Size (168, 612);
            this.LbxChars.TabIndex = 0;
            this.LbxChars.SelectedIndexChanged += new System.EventHandler (this.LbxChars_SelectedIndexChanged);
            // 
            // TbcChars
            // 
            this.TbcChars.Controls.Add (this.TabCharsMain);
            this.TbcChars.Controls.Add (this.TabCharsArts);
            this.TbcChars.Controls.Add (this.TabCharsSkillTrees);
            this.TbcChars.Controls.Add (this.TabCharsSkillLinks);
            this.TbcChars.Controls.Add (this.TabCharsUnknown);
            this.TbcChars.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TbcChars.Location = new System.Drawing.Point (0, 0);
            this.TbcChars.Name = "TbcChars";
            this.TbcChars.SelectedIndex = 0;
            this.TbcChars.Size = new System.Drawing.Size (1078, 612);
            this.TbcChars.TabIndex = 0;
            // 
            // TabCharsMain
            // 
            this.TabCharsMain.Controls.Add (this.GbxCharExp);
            this.TabCharsMain.Controls.Add (this.GbxCharacter);
            this.TabCharsMain.Controls.Add (this.GbxCharEquipment);
            this.TabCharsMain.Controls.Add (this.GbxCharStats);
            this.TabCharsMain.Location = new System.Drawing.Point (4, 22);
            this.TabCharsMain.Name = "TabCharsMain";
            this.TabCharsMain.Padding = new System.Windows.Forms.Padding (3);
            this.TabCharsMain.Size = new System.Drawing.Size (1070, 586);
            this.TabCharsMain.TabIndex = 0;
            this.TabCharsMain.Text = "Main";
            this.TabCharsMain.UseVisualStyleBackColor = true;
            // 
            // GbxCharExp
            // 
            this.GbxCharExp.Controls.Add (this.NudCharExpertRsvExp);
            this.GbxCharExp.Controls.Add (this.LblCharExpertRsvExp);
            this.GbxCharExp.Controls.Add (this.NudCharExp);
            this.GbxCharExp.Controls.Add (this.LblCharExpertExp);
            this.GbxCharExp.Controls.Add (this.LblCharExp);
            this.GbxCharExp.Controls.Add (this.NudCharExpertExp);
            this.GbxCharExp.Location = new System.Drawing.Point (6, 137);
            this.GbxCharExp.Name = "GbxCharExp";
            this.GbxCharExp.Size = new System.Drawing.Size (245, 82);
            this.GbxCharExp.TabIndex = 46;
            this.GbxCharExp.TabStop = false;
            this.GbxCharExp.Text = "Experience Points";
            // 
            // NudCharExpertRsvExp
            // 
            this.NudCharExpertRsvExp.Location = new System.Drawing.Point (133, 53);
            this.NudCharExpertRsvExp.Maximum = new decimal (new int[] {
            -1,
            0,
            0,
            0});
            this.NudCharExpertRsvExp.Name = "NudCharExpertRsvExp";
            this.NudCharExpertRsvExp.Size = new System.Drawing.Size (85, 20);
            this.NudCharExpertRsvExp.TabIndex = 38;
            // 
            // LblCharExpertRsvExp
            // 
            this.LblCharExpertRsvExp.AutoSize = true;
            this.LblCharExpertRsvExp.Location = new System.Drawing.Point (8, 53);
            this.LblCharExpertRsvExp.Name = "LblCharExpertRsvExp";
            this.LblCharExpertRsvExp.Size = new System.Drawing.Size (116, 13);
            this.LblCharExpertRsvExp.TabIndex = 37;
            this.LblCharExpertRsvExp.Text = "Expert Mode Rsv. Exp:";
            // 
            // NudCharExp
            // 
            this.NudCharExp.Location = new System.Drawing.Point (133, 16);
            this.NudCharExp.Maximum = new decimal (new int[] {
            -1,
            0,
            0,
            0});
            this.NudCharExp.Name = "NudCharExp";
            this.NudCharExp.Size = new System.Drawing.Size (85, 20);
            this.NudCharExp.TabIndex = 35;
            this.NudCharExp.ValueChanged += new System.EventHandler (this.NudCharExp_ValueChanged);
            // 
            // LblCharExpertExp
            // 
            this.LblCharExpertExp.AutoSize = true;
            this.LblCharExpertExp.Location = new System.Drawing.Point (8, 36);
            this.LblCharExpertExp.Name = "LblCharExpertExp";
            this.LblCharExpertExp.Size = new System.Drawing.Size (91, 13);
            this.LblCharExpertExp.TabIndex = 18;
            this.LblCharExpertExp.Text = "Expert Mode Exp:";
            // 
            // LblCharExp
            // 
            this.LblCharExp.AutoSize = true;
            this.LblCharExp.Location = new System.Drawing.Point (8, 18);
            this.LblCharExp.Name = "LblCharExp";
            this.LblCharExp.Size = new System.Drawing.Size (95, 13);
            this.LblCharExp.TabIndex = 17;
            this.LblCharExp.Text = "Experience Points:";
            // 
            // NudCharExpertExp
            // 
            this.NudCharExpertExp.Location = new System.Drawing.Point (133, 34);
            this.NudCharExpertExp.Maximum = new decimal (new int[] {
            -1,
            0,
            0,
            0});
            this.NudCharExpertExp.Name = "NudCharExpertExp";
            this.NudCharExpertExp.Size = new System.Drawing.Size (85, 20);
            this.NudCharExpertExp.TabIndex = 36;
            this.NudCharExpertExp.ValueChanged += new System.EventHandler (this.NudCharExpertExp_ValueChanged);
            // 
            // GbxCharacter
            // 
            this.GbxCharacter.Controls.Add (this.ChkCharInParty);
            this.GbxCharacter.Controls.Add (this.LblCharID);
            this.GbxCharacter.Controls.Add (this.CbxCharID);
            this.GbxCharacter.Controls.Add (this.NudCharID);
            this.GbxCharacter.Location = new System.Drawing.Point (6, 6);
            this.GbxCharacter.Name = "GbxCharacter";
            this.GbxCharacter.Size = new System.Drawing.Size (245, 60);
            this.GbxCharacter.TabIndex = 45;
            this.GbxCharacter.TabStop = false;
            this.GbxCharacter.Text = "Character";
            // 
            // ChkCharInParty
            // 
            this.ChkCharInParty.AutoSize = true;
            this.ChkCharInParty.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ChkCharInParty.Enabled = false;
            this.ChkCharInParty.Location = new System.Drawing.Point (6, 39);
            this.ChkCharInParty.Name = "ChkCharInParty";
            this.ChkCharInParty.Size = new System.Drawing.Size (73, 17);
            this.ChkCharInParty.TabIndex = 42;
            this.ChkCharInParty.Text = "Is In Party";
            this.ChkCharInParty.UseVisualStyleBackColor = true;
            // 
            // LblCharID
            // 
            this.LblCharID.AutoSize = true;
            this.LblCharID.Location = new System.Drawing.Point (6, 16);
            this.LblCharID.Name = "LblCharID";
            this.LblCharID.Size = new System.Drawing.Size (70, 13);
            this.LblCharID.TabIndex = 37;
            this.LblCharID.Text = "Character ID:";
            // 
            // CbxCharID
            // 
            this.CbxCharID.Enabled = false;
            this.CbxCharID.FormattingEnabled = true;
            this.CbxCharID.Location = new System.Drawing.Point (131, 12);
            this.CbxCharID.Name = "CbxCharID";
            this.CbxCharID.Size = new System.Drawing.Size (103, 21);
            this.CbxCharID.TabIndex = 30;
            // 
            // NudCharID
            // 
            this.NudCharID.Enabled = false;
            this.NudCharID.Location = new System.Drawing.Point (82, 13);
            this.NudCharID.Maximum = new decimal (new int[] {
            27,
            0,
            0,
            0});
            this.NudCharID.Name = "NudCharID";
            this.NudCharID.Size = new System.Drawing.Size (43, 20);
            this.NudCharID.TabIndex = 41;
            // 
            // GbxCharEquipment
            // 
            this.GbxCharEquipment.Controls.Add (this.groupBox3);
            this.GbxCharEquipment.Controls.Add (this.groupBox2);
            this.GbxCharEquipment.Controls.Add (this.groupBox1);
            this.GbxCharEquipment.Controls.Add (this.GbxCharTorsoArmor);
            this.GbxCharEquipment.Controls.Add (this.GbxCharHeadArmour);
            this.GbxCharEquipment.Controls.Add (this.GbxCharWeapon);
            this.GbxCharEquipment.Location = new System.Drawing.Point (257, 6);
            this.GbxCharEquipment.Name = "GbxCharEquipment";
            this.GbxCharEquipment.Size = new System.Drawing.Size (355, 572);
            this.GbxCharEquipment.TabIndex = 43;
            this.GbxCharEquipment.TabStop = false;
            this.GbxCharEquipment.Text = "Equipment";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add (this.CbxCharFootArmourType);
            this.groupBox3.Controls.Add (this.CbxCharFootArmourID);
            this.groupBox3.Controls.Add (this.LblCharFootArmourID);
            this.groupBox3.Controls.Add (this.NudCharFootArmourIndex);
            this.groupBox3.Controls.Add (this.label15);
            this.groupBox3.Controls.Add (this.NudCharFootArmourType);
            this.groupBox3.Controls.Add (this.LblCharFootArmourIndex);
            this.groupBox3.Controls.Add (this.NudCharFootArmourID);
            this.groupBox3.Location = new System.Drawing.Point (6, 478);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size (343, 87);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Foot Armour";
            // 
            // CbxCharFootArmourType
            // 
            this.CbxCharFootArmourType.FormattingEnabled = true;
            this.CbxCharFootArmourType.Location = new System.Drawing.Point (134, 37);
            this.CbxCharFootArmourType.Name = "CbxCharFootArmourType";
            this.CbxCharFootArmourType.Size = new System.Drawing.Size (200, 21);
            this.CbxCharFootArmourType.TabIndex = 7;
            this.CbxCharFootArmourType.SelectionChangeCommitted += new System.EventHandler (this.CbxCharFootArmourType_SelectionChangeCommitted);
            // 
            // CbxCharFootArmourID
            // 
            this.CbxCharFootArmourID.Enabled = false;
            this.CbxCharFootArmourID.FormattingEnabled = true;
            this.CbxCharFootArmourID.Location = new System.Drawing.Point (134, 18);
            this.CbxCharFootArmourID.Name = "CbxCharFootArmourID";
            this.CbxCharFootArmourID.Size = new System.Drawing.Size (200, 21);
            this.CbxCharFootArmourID.TabIndex = 6;
            // 
            // LblCharFootArmourID
            // 
            this.LblCharFootArmourID.AutoSize = true;
            this.LblCharFootArmourID.Location = new System.Drawing.Point (12, 20);
            this.LblCharFootArmourID.Name = "LblCharFootArmourID";
            this.LblCharFootArmourID.Size = new System.Drawing.Size (44, 13);
            this.LblCharFootArmourID.TabIndex = 0;
            this.LblCharFootArmourID.Text = "Item ID:";
            // 
            // NudCharFootArmourIndex
            // 
            this.NudCharFootArmourIndex.Location = new System.Drawing.Point (62, 58);
            this.NudCharFootArmourIndex.Maximum = new decimal (new int[] {
            67108863,
            0,
            0,
            0});
            this.NudCharFootArmourIndex.Name = "NudCharFootArmourIndex";
            this.NudCharFootArmourIndex.Size = new System.Drawing.Size (66, 20);
            this.NudCharFootArmourIndex.TabIndex = 5;
            this.NudCharFootArmourIndex.ValueChanged += new System.EventHandler (this.NudCharFootArmourIndex_ValueChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point (12, 40);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size (34, 13);
            this.label15.TabIndex = 1;
            this.label15.Text = "Type:";
            // 
            // NudCharFootArmourType
            // 
            this.NudCharFootArmourType.Location = new System.Drawing.Point (62, 38);
            this.NudCharFootArmourType.Maximum = new decimal (new int[] {
            63,
            0,
            0,
            0});
            this.NudCharFootArmourType.Name = "NudCharFootArmourType";
            this.NudCharFootArmourType.Size = new System.Drawing.Size (66, 20);
            this.NudCharFootArmourType.TabIndex = 4;
            this.NudCharFootArmourType.ValueChanged += new System.EventHandler (this.NudCharFootArmourType_ValueChanged);
            // 
            // LblCharFootArmourIndex
            // 
            this.LblCharFootArmourIndex.AutoSize = true;
            this.LblCharFootArmourIndex.Location = new System.Drawing.Point (12, 60);
            this.LblCharFootArmourIndex.Name = "LblCharFootArmourIndex";
            this.LblCharFootArmourIndex.Size = new System.Drawing.Size (36, 13);
            this.LblCharFootArmourIndex.TabIndex = 2;
            this.LblCharFootArmourIndex.Text = "Index:";
            // 
            // NudCharFootArmourID
            // 
            this.NudCharFootArmourID.Enabled = false;
            this.NudCharFootArmourID.Location = new System.Drawing.Point (62, 18);
            this.NudCharFootArmourID.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.NudCharFootArmourID.Name = "NudCharFootArmourID";
            this.NudCharFootArmourID.Size = new System.Drawing.Size (66, 20);
            this.NudCharFootArmourID.TabIndex = 3;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add (this.CbxCharLegArmourType);
            this.groupBox2.Controls.Add (this.CbxCharLegArmourID);
            this.groupBox2.Controls.Add (this.LblCharLegArmourID);
            this.groupBox2.Controls.Add (this.NudCharLegArmourIndex);
            this.groupBox2.Controls.Add (this.label11);
            this.groupBox2.Controls.Add (this.NudCharLegArmourType);
            this.groupBox2.Controls.Add (this.LblCharLegArmourIndex);
            this.groupBox2.Controls.Add (this.NudCharLegArmourID);
            this.groupBox2.Location = new System.Drawing.Point (6, 385);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size (343, 87);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Leg Armour";
            // 
            // CbxCharLegArmourType
            // 
            this.CbxCharLegArmourType.FormattingEnabled = true;
            this.CbxCharLegArmourType.Location = new System.Drawing.Point (134, 37);
            this.CbxCharLegArmourType.Name = "CbxCharLegArmourType";
            this.CbxCharLegArmourType.Size = new System.Drawing.Size (200, 21);
            this.CbxCharLegArmourType.TabIndex = 7;
            this.CbxCharLegArmourType.SelectionChangeCommitted += new System.EventHandler (this.CbxCharLegArmourType_SelectionChangeCommitted);
            // 
            // CbxCharLegArmourID
            // 
            this.CbxCharLegArmourID.Enabled = false;
            this.CbxCharLegArmourID.FormattingEnabled = true;
            this.CbxCharLegArmourID.Location = new System.Drawing.Point (134, 18);
            this.CbxCharLegArmourID.Name = "CbxCharLegArmourID";
            this.CbxCharLegArmourID.Size = new System.Drawing.Size (200, 21);
            this.CbxCharLegArmourID.TabIndex = 6;
            // 
            // LblCharLegArmourID
            // 
            this.LblCharLegArmourID.AutoSize = true;
            this.LblCharLegArmourID.Location = new System.Drawing.Point (12, 20);
            this.LblCharLegArmourID.Name = "LblCharLegArmourID";
            this.LblCharLegArmourID.Size = new System.Drawing.Size (44, 13);
            this.LblCharLegArmourID.TabIndex = 0;
            this.LblCharLegArmourID.Text = "Item ID:";
            // 
            // NudCharLegArmourIndex
            // 
            this.NudCharLegArmourIndex.Location = new System.Drawing.Point (62, 58);
            this.NudCharLegArmourIndex.Maximum = new decimal (new int[] {
            67108863,
            0,
            0,
            0});
            this.NudCharLegArmourIndex.Name = "NudCharLegArmourIndex";
            this.NudCharLegArmourIndex.Size = new System.Drawing.Size (66, 20);
            this.NudCharLegArmourIndex.TabIndex = 5;
            this.NudCharLegArmourIndex.ValueChanged += new System.EventHandler (this.NudCharLegArmourIndex_ValueChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point (12, 40);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size (34, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "Type:";
            // 
            // NudCharLegArmourType
            // 
            this.NudCharLegArmourType.Location = new System.Drawing.Point (62, 38);
            this.NudCharLegArmourType.Maximum = new decimal (new int[] {
            63,
            0,
            0,
            0});
            this.NudCharLegArmourType.Name = "NudCharLegArmourType";
            this.NudCharLegArmourType.Size = new System.Drawing.Size (66, 20);
            this.NudCharLegArmourType.TabIndex = 4;
            this.NudCharLegArmourType.ValueChanged += new System.EventHandler (this.NudCharLegArmourType_ValueChanged);
            // 
            // LblCharLegArmourIndex
            // 
            this.LblCharLegArmourIndex.AutoSize = true;
            this.LblCharLegArmourIndex.Location = new System.Drawing.Point (12, 60);
            this.LblCharLegArmourIndex.Name = "LblCharLegArmourIndex";
            this.LblCharLegArmourIndex.Size = new System.Drawing.Size (36, 13);
            this.LblCharLegArmourIndex.TabIndex = 2;
            this.LblCharLegArmourIndex.Text = "Index:";
            // 
            // NudCharLegArmourID
            // 
            this.NudCharLegArmourID.Enabled = false;
            this.NudCharLegArmourID.Location = new System.Drawing.Point (62, 18);
            this.NudCharLegArmourID.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.NudCharLegArmourID.Name = "NudCharLegArmourID";
            this.NudCharLegArmourID.Size = new System.Drawing.Size (66, 20);
            this.NudCharLegArmourID.TabIndex = 3;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add (this.CbxCharArmArmourType);
            this.groupBox1.Controls.Add (this.CbxCharArmArmourID);
            this.groupBox1.Controls.Add (this.LblCharArmArmourID);
            this.groupBox1.Controls.Add (this.NudCharArmArmourIndex);
            this.groupBox1.Controls.Add (this.label8);
            this.groupBox1.Controls.Add (this.NudCharArmArmourType);
            this.groupBox1.Controls.Add (this.LblCharArmArmourIndex);
            this.groupBox1.Controls.Add (this.NudCharArmArmourID);
            this.groupBox1.Location = new System.Drawing.Point (6, 292);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size (343, 87);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Arm Armour";
            // 
            // CbxCharArmArmourType
            // 
            this.CbxCharArmArmourType.FormattingEnabled = true;
            this.CbxCharArmArmourType.Location = new System.Drawing.Point (134, 37);
            this.CbxCharArmArmourType.Name = "CbxCharArmArmourType";
            this.CbxCharArmArmourType.Size = new System.Drawing.Size (200, 21);
            this.CbxCharArmArmourType.TabIndex = 7;
            this.CbxCharArmArmourType.SelectionChangeCommitted += new System.EventHandler (this.CbxCharArmArmourType_SelectionChangeCommitted);
            // 
            // CbxCharArmArmourID
            // 
            this.CbxCharArmArmourID.Enabled = false;
            this.CbxCharArmArmourID.FormattingEnabled = true;
            this.CbxCharArmArmourID.Location = new System.Drawing.Point (134, 18);
            this.CbxCharArmArmourID.Name = "CbxCharArmArmourID";
            this.CbxCharArmArmourID.Size = new System.Drawing.Size (200, 21);
            this.CbxCharArmArmourID.TabIndex = 6;
            // 
            // LblCharArmArmourID
            // 
            this.LblCharArmArmourID.AutoSize = true;
            this.LblCharArmArmourID.Location = new System.Drawing.Point (12, 20);
            this.LblCharArmArmourID.Name = "LblCharArmArmourID";
            this.LblCharArmArmourID.Size = new System.Drawing.Size (44, 13);
            this.LblCharArmArmourID.TabIndex = 0;
            this.LblCharArmArmourID.Text = "Item ID:";
            // 
            // NudCharArmArmourIndex
            // 
            this.NudCharArmArmourIndex.Location = new System.Drawing.Point (62, 58);
            this.NudCharArmArmourIndex.Maximum = new decimal (new int[] {
            67108863,
            0,
            0,
            0});
            this.NudCharArmArmourIndex.Name = "NudCharArmArmourIndex";
            this.NudCharArmArmourIndex.Size = new System.Drawing.Size (66, 20);
            this.NudCharArmArmourIndex.TabIndex = 5;
            this.NudCharArmArmourIndex.ValueChanged += new System.EventHandler (this.NudCharArmArmourIndex_ValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point (12, 40);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size (34, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Type:";
            // 
            // NudCharArmArmourType
            // 
            this.NudCharArmArmourType.Location = new System.Drawing.Point (62, 38);
            this.NudCharArmArmourType.Maximum = new decimal (new int[] {
            63,
            0,
            0,
            0});
            this.NudCharArmArmourType.Name = "NudCharArmArmourType";
            this.NudCharArmArmourType.Size = new System.Drawing.Size (66, 20);
            this.NudCharArmArmourType.TabIndex = 4;
            this.NudCharArmArmourType.ValueChanged += new System.EventHandler (this.NudCharArmArmourType_ValueChanged);
            // 
            // LblCharArmArmourIndex
            // 
            this.LblCharArmArmourIndex.AutoSize = true;
            this.LblCharArmArmourIndex.Location = new System.Drawing.Point (12, 60);
            this.LblCharArmArmourIndex.Name = "LblCharArmArmourIndex";
            this.LblCharArmArmourIndex.Size = new System.Drawing.Size (36, 13);
            this.LblCharArmArmourIndex.TabIndex = 2;
            this.LblCharArmArmourIndex.Text = "Index:";
            // 
            // NudCharArmArmourID
            // 
            this.NudCharArmArmourID.Enabled = false;
            this.NudCharArmArmourID.Location = new System.Drawing.Point (62, 18);
            this.NudCharArmArmourID.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.NudCharArmArmourID.Name = "NudCharArmArmourID";
            this.NudCharArmArmourID.Size = new System.Drawing.Size (66, 20);
            this.NudCharArmArmourID.TabIndex = 3;
            // 
            // GbxCharTorsoArmor
            // 
            this.GbxCharTorsoArmor.Controls.Add (this.CbxCharTorsoArmourType);
            this.GbxCharTorsoArmor.Controls.Add (this.CbxCharTorsoArmourID);
            this.GbxCharTorsoArmor.Controls.Add (this.LblCharTorsoArmourID);
            this.GbxCharTorsoArmor.Controls.Add (this.NudCharTorsoArmourIndex);
            this.GbxCharTorsoArmor.Controls.Add (this.label5);
            this.GbxCharTorsoArmor.Controls.Add (this.NudCharTorsoArmourType);
            this.GbxCharTorsoArmor.Controls.Add (this.LblCharTorsoArmourIndex);
            this.GbxCharTorsoArmor.Controls.Add (this.NudCharTorsoArmourID);
            this.GbxCharTorsoArmor.Location = new System.Drawing.Point (6, 199);
            this.GbxCharTorsoArmor.Name = "GbxCharTorsoArmor";
            this.GbxCharTorsoArmor.Size = new System.Drawing.Size (343, 87);
            this.GbxCharTorsoArmor.TabIndex = 9;
            this.GbxCharTorsoArmor.TabStop = false;
            this.GbxCharTorsoArmor.Text = "Torso Armour";
            // 
            // CbxCharTorsoArmourType
            // 
            this.CbxCharTorsoArmourType.FormattingEnabled = true;
            this.CbxCharTorsoArmourType.Location = new System.Drawing.Point (134, 37);
            this.CbxCharTorsoArmourType.Name = "CbxCharTorsoArmourType";
            this.CbxCharTorsoArmourType.Size = new System.Drawing.Size (200, 21);
            this.CbxCharTorsoArmourType.TabIndex = 7;
            this.CbxCharTorsoArmourType.SelectionChangeCommitted += new System.EventHandler (this.CbxCharTorsoArmourType_SelectionChangeCommitted);
            // 
            // CbxCharTorsoArmourID
            // 
            this.CbxCharTorsoArmourID.Enabled = false;
            this.CbxCharTorsoArmourID.FormattingEnabled = true;
            this.CbxCharTorsoArmourID.Location = new System.Drawing.Point (134, 18);
            this.CbxCharTorsoArmourID.Name = "CbxCharTorsoArmourID";
            this.CbxCharTorsoArmourID.Size = new System.Drawing.Size (200, 21);
            this.CbxCharTorsoArmourID.TabIndex = 6;
            // 
            // LblCharTorsoArmourID
            // 
            this.LblCharTorsoArmourID.AutoSize = true;
            this.LblCharTorsoArmourID.Location = new System.Drawing.Point (12, 20);
            this.LblCharTorsoArmourID.Name = "LblCharTorsoArmourID";
            this.LblCharTorsoArmourID.Size = new System.Drawing.Size (44, 13);
            this.LblCharTorsoArmourID.TabIndex = 0;
            this.LblCharTorsoArmourID.Text = "Item ID:";
            // 
            // NudCharTorsoArmourIndex
            // 
            this.NudCharTorsoArmourIndex.Location = new System.Drawing.Point (62, 58);
            this.NudCharTorsoArmourIndex.Maximum = new decimal (new int[] {
            67108863,
            0,
            0,
            0});
            this.NudCharTorsoArmourIndex.Name = "NudCharTorsoArmourIndex";
            this.NudCharTorsoArmourIndex.Size = new System.Drawing.Size (66, 20);
            this.NudCharTorsoArmourIndex.TabIndex = 5;
            this.NudCharTorsoArmourIndex.ValueChanged += new System.EventHandler (this.NudCharTorsoArmourIndex_ValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point (12, 40);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size (34, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Type:";
            // 
            // NudCharTorsoArmourType
            // 
            this.NudCharTorsoArmourType.Location = new System.Drawing.Point (62, 38);
            this.NudCharTorsoArmourType.Maximum = new decimal (new int[] {
            63,
            0,
            0,
            0});
            this.NudCharTorsoArmourType.Name = "NudCharTorsoArmourType";
            this.NudCharTorsoArmourType.Size = new System.Drawing.Size (66, 20);
            this.NudCharTorsoArmourType.TabIndex = 4;
            this.NudCharTorsoArmourType.ValueChanged += new System.EventHandler (this.NudCharTorsoArmourType_ValueChanged);
            // 
            // LblCharTorsoArmourIndex
            // 
            this.LblCharTorsoArmourIndex.AutoSize = true;
            this.LblCharTorsoArmourIndex.Location = new System.Drawing.Point (12, 60);
            this.LblCharTorsoArmourIndex.Name = "LblCharTorsoArmourIndex";
            this.LblCharTorsoArmourIndex.Size = new System.Drawing.Size (36, 13);
            this.LblCharTorsoArmourIndex.TabIndex = 2;
            this.LblCharTorsoArmourIndex.Text = "Index:";
            // 
            // NudCharTorsoArmourID
            // 
            this.NudCharTorsoArmourID.Enabled = false;
            this.NudCharTorsoArmourID.Location = new System.Drawing.Point (62, 18);
            this.NudCharTorsoArmourID.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.NudCharTorsoArmourID.Name = "NudCharTorsoArmourID";
            this.NudCharTorsoArmourID.Size = new System.Drawing.Size (66, 20);
            this.NudCharTorsoArmourID.TabIndex = 3;
            // 
            // GbxCharHeadArmour
            // 
            this.GbxCharHeadArmour.Controls.Add (this.CbxCharHeadArmourType);
            this.GbxCharHeadArmour.Controls.Add (this.CbxCharHeadArmourID);
            this.GbxCharHeadArmour.Controls.Add (this.LblCharHeadArmourID);
            this.GbxCharHeadArmour.Controls.Add (this.NudCharHeadArmourIndex);
            this.GbxCharHeadArmour.Controls.Add (this.label2);
            this.GbxCharHeadArmour.Controls.Add (this.NudCharHeadArmourType);
            this.GbxCharHeadArmour.Controls.Add (this.LblCharHeadArmourIndex);
            this.GbxCharHeadArmour.Controls.Add (this.NudCharHeadArmourID);
            this.GbxCharHeadArmour.Location = new System.Drawing.Point (6, 106);
            this.GbxCharHeadArmour.Name = "GbxCharHeadArmour";
            this.GbxCharHeadArmour.Size = new System.Drawing.Size (343, 87);
            this.GbxCharHeadArmour.TabIndex = 8;
            this.GbxCharHeadArmour.TabStop = false;
            this.GbxCharHeadArmour.Text = "Head Armour";
            // 
            // CbxCharHeadArmourType
            // 
            this.CbxCharHeadArmourType.FormattingEnabled = true;
            this.CbxCharHeadArmourType.Location = new System.Drawing.Point (134, 37);
            this.CbxCharHeadArmourType.Name = "CbxCharHeadArmourType";
            this.CbxCharHeadArmourType.Size = new System.Drawing.Size (200, 21);
            this.CbxCharHeadArmourType.TabIndex = 7;
            this.CbxCharHeadArmourType.SelectionChangeCommitted += new System.EventHandler (this.CbxCharHeadArmourType_SelectionChangeCommitted);
            // 
            // CbxCharHeadArmourID
            // 
            this.CbxCharHeadArmourID.Enabled = false;
            this.CbxCharHeadArmourID.FormattingEnabled = true;
            this.CbxCharHeadArmourID.Location = new System.Drawing.Point (134, 18);
            this.CbxCharHeadArmourID.Name = "CbxCharHeadArmourID";
            this.CbxCharHeadArmourID.Size = new System.Drawing.Size (200, 21);
            this.CbxCharHeadArmourID.TabIndex = 6;
            // 
            // LblCharHeadArmourID
            // 
            this.LblCharHeadArmourID.AutoSize = true;
            this.LblCharHeadArmourID.Location = new System.Drawing.Point (12, 20);
            this.LblCharHeadArmourID.Name = "LblCharHeadArmourID";
            this.LblCharHeadArmourID.Size = new System.Drawing.Size (44, 13);
            this.LblCharHeadArmourID.TabIndex = 0;
            this.LblCharHeadArmourID.Text = "Item ID:";
            // 
            // NudCharHeadArmourIndex
            // 
            this.NudCharHeadArmourIndex.Location = new System.Drawing.Point (62, 58);
            this.NudCharHeadArmourIndex.Maximum = new decimal (new int[] {
            67108863,
            0,
            0,
            0});
            this.NudCharHeadArmourIndex.Name = "NudCharHeadArmourIndex";
            this.NudCharHeadArmourIndex.Size = new System.Drawing.Size (66, 20);
            this.NudCharHeadArmourIndex.TabIndex = 5;
            this.NudCharHeadArmourIndex.ValueChanged += new System.EventHandler (this.NudCharHeadArmourIndex_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point (12, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size (34, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Type:";
            // 
            // NudCharHeadArmourType
            // 
            this.NudCharHeadArmourType.Location = new System.Drawing.Point (62, 38);
            this.NudCharHeadArmourType.Maximum = new decimal (new int[] {
            63,
            0,
            0,
            0});
            this.NudCharHeadArmourType.Name = "NudCharHeadArmourType";
            this.NudCharHeadArmourType.Size = new System.Drawing.Size (66, 20);
            this.NudCharHeadArmourType.TabIndex = 4;
            this.NudCharHeadArmourType.ValueChanged += new System.EventHandler (this.NudCharHeadArmourType_ValueChanged);
            // 
            // LblCharHeadArmourIndex
            // 
            this.LblCharHeadArmourIndex.AutoSize = true;
            this.LblCharHeadArmourIndex.Location = new System.Drawing.Point (12, 60);
            this.LblCharHeadArmourIndex.Name = "LblCharHeadArmourIndex";
            this.LblCharHeadArmourIndex.Size = new System.Drawing.Size (36, 13);
            this.LblCharHeadArmourIndex.TabIndex = 2;
            this.LblCharHeadArmourIndex.Text = "Index:";
            // 
            // NudCharHeadArmourID
            // 
            this.NudCharHeadArmourID.Enabled = false;
            this.NudCharHeadArmourID.Location = new System.Drawing.Point (62, 18);
            this.NudCharHeadArmourID.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.NudCharHeadArmourID.Name = "NudCharHeadArmourID";
            this.NudCharHeadArmourID.Size = new System.Drawing.Size (66, 20);
            this.NudCharHeadArmourID.TabIndex = 3;
            // 
            // GbxCharWeapon
            // 
            this.GbxCharWeapon.Controls.Add (this.CbxCharWeaponType);
            this.GbxCharWeapon.Controls.Add (this.CbxCharWeaponID);
            this.GbxCharWeapon.Controls.Add (this.LblCharWeaponID);
            this.GbxCharWeapon.Controls.Add (this.NudCharWeaponIndex);
            this.GbxCharWeapon.Controls.Add (this.LblCharWeaponType);
            this.GbxCharWeapon.Controls.Add (this.NudCharWeaponType);
            this.GbxCharWeapon.Controls.Add (this.LblCharWeaponIndex);
            this.GbxCharWeapon.Controls.Add (this.NudCharWeaponID);
            this.GbxCharWeapon.Location = new System.Drawing.Point (6, 13);
            this.GbxCharWeapon.Name = "GbxCharWeapon";
            this.GbxCharWeapon.Size = new System.Drawing.Size (343, 87);
            this.GbxCharWeapon.TabIndex = 6;
            this.GbxCharWeapon.TabStop = false;
            this.GbxCharWeapon.Text = "Weapon";
            // 
            // CbxCharWeaponType
            // 
            this.CbxCharWeaponType.FormattingEnabled = true;
            this.CbxCharWeaponType.Location = new System.Drawing.Point (134, 37);
            this.CbxCharWeaponType.Name = "CbxCharWeaponType";
            this.CbxCharWeaponType.Size = new System.Drawing.Size (200, 21);
            this.CbxCharWeaponType.TabIndex = 7;
            this.CbxCharWeaponType.SelectionChangeCommitted += new System.EventHandler (this.CbxCharWeaponType_SelectionChangeCommitted);
            // 
            // CbxCharWeaponID
            // 
            this.CbxCharWeaponID.DropDownWidth = 225;
            this.CbxCharWeaponID.Enabled = false;
            this.CbxCharWeaponID.FormattingEnabled = true;
            this.CbxCharWeaponID.Location = new System.Drawing.Point (134, 18);
            this.CbxCharWeaponID.Name = "CbxCharWeaponID";
            this.CbxCharWeaponID.Size = new System.Drawing.Size (200, 21);
            this.CbxCharWeaponID.TabIndex = 6;
            // 
            // LblCharWeaponID
            // 
            this.LblCharWeaponID.AutoSize = true;
            this.LblCharWeaponID.Location = new System.Drawing.Point (12, 20);
            this.LblCharWeaponID.Name = "LblCharWeaponID";
            this.LblCharWeaponID.Size = new System.Drawing.Size (44, 13);
            this.LblCharWeaponID.TabIndex = 0;
            this.LblCharWeaponID.Text = "Item ID:";
            // 
            // NudCharWeaponIndex
            // 
            this.NudCharWeaponIndex.Location = new System.Drawing.Point (62, 58);
            this.NudCharWeaponIndex.Maximum = new decimal (new int[] {
            67108863,
            0,
            0,
            0});
            this.NudCharWeaponIndex.Name = "NudCharWeaponIndex";
            this.NudCharWeaponIndex.Size = new System.Drawing.Size (66, 20);
            this.NudCharWeaponIndex.TabIndex = 5;
            this.NudCharWeaponIndex.ValueChanged += new System.EventHandler (this.NudCharWeaponIndex_ValueChanged);
            // 
            // LblCharWeaponType
            // 
            this.LblCharWeaponType.AutoSize = true;
            this.LblCharWeaponType.Location = new System.Drawing.Point (12, 40);
            this.LblCharWeaponType.Name = "LblCharWeaponType";
            this.LblCharWeaponType.Size = new System.Drawing.Size (34, 13);
            this.LblCharWeaponType.TabIndex = 1;
            this.LblCharWeaponType.Text = "Type:";
            // 
            // NudCharWeaponType
            // 
            this.NudCharWeaponType.Location = new System.Drawing.Point (62, 38);
            this.NudCharWeaponType.Maximum = new decimal (new int[] {
            63,
            0,
            0,
            0});
            this.NudCharWeaponType.Name = "NudCharWeaponType";
            this.NudCharWeaponType.Size = new System.Drawing.Size (66, 20);
            this.NudCharWeaponType.TabIndex = 4;
            this.NudCharWeaponType.ValueChanged += new System.EventHandler (this.NudCharWeaponType_ValueChanged);
            // 
            // LblCharWeaponIndex
            // 
            this.LblCharWeaponIndex.AutoSize = true;
            this.LblCharWeaponIndex.Location = new System.Drawing.Point (12, 60);
            this.LblCharWeaponIndex.Name = "LblCharWeaponIndex";
            this.LblCharWeaponIndex.Size = new System.Drawing.Size (36, 13);
            this.LblCharWeaponIndex.TabIndex = 2;
            this.LblCharWeaponIndex.Text = "Index:";
            // 
            // NudCharWeaponID
            // 
            this.NudCharWeaponID.Enabled = false;
            this.NudCharWeaponID.Location = new System.Drawing.Point (62, 18);
            this.NudCharWeaponID.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.NudCharWeaponID.Name = "NudCharWeaponID";
            this.NudCharWeaponID.Size = new System.Drawing.Size (66, 20);
            this.NudCharWeaponID.TabIndex = 3;
            // 
            // GbxCharStats
            // 
            this.GbxCharStats.Controls.Add (this.NudCharExpertLevel);
            this.GbxCharStats.Controls.Add (this.LblExpertLvl);
            this.GbxCharStats.Controls.Add (this.NudCharLevel);
            this.GbxCharStats.Controls.Add (this.LblCharLevel);
            this.GbxCharStats.Location = new System.Drawing.Point (6, 72);
            this.GbxCharStats.Name = "GbxCharStats";
            this.GbxCharStats.Size = new System.Drawing.Size (245, 59);
            this.GbxCharStats.TabIndex = 36;
            this.GbxCharStats.TabStop = false;
            this.GbxCharStats.Text = "Base Stats";
            // 
            // NudCharExpertLevel
            // 
            this.NudCharExpertLevel.Location = new System.Drawing.Point (133, 29);
            this.NudCharExpertLevel.Maximum = new decimal (new int[] {
            99,
            0,
            0,
            0});
            this.NudCharExpertLevel.Name = "NudCharExpertLevel";
            this.NudCharExpertLevel.Size = new System.Drawing.Size (85, 20);
            this.NudCharExpertLevel.TabIndex = 26;
            this.NudCharExpertLevel.ValueChanged += new System.EventHandler (this.NudCharExpertLevel_ValueChanged);
            // 
            // LblExpertLvl
            // 
            this.LblExpertLvl.AutoSize = true;
            this.LblExpertLvl.Location = new System.Drawing.Point (7, 32);
            this.LblExpertLvl.Name = "LblExpertLvl";
            this.LblExpertLvl.Size = new System.Drawing.Size (102, 13);
            this.LblExpertLvl.TabIndex = 25;
            this.LblExpertLvl.Text = "Expert Mode Level: ";
            // 
            // NudCharLevel
            // 
            this.NudCharLevel.Location = new System.Drawing.Point (133, 11);
            this.NudCharLevel.Maximum = new decimal (new int[] {
            99,
            0,
            0,
            0});
            this.NudCharLevel.Name = "NudCharLevel";
            this.NudCharLevel.Size = new System.Drawing.Size (85, 20);
            this.NudCharLevel.TabIndex = 24;
            this.NudCharLevel.ValueChanged += new System.EventHandler (this.NudCharLevel_ValueChanged);
            // 
            // LblCharLevel
            // 
            this.LblCharLevel.AutoSize = true;
            this.LblCharLevel.Location = new System.Drawing.Point (7, 15);
            this.LblCharLevel.Name = "LblCharLevel";
            this.LblCharLevel.Size = new System.Drawing.Size (39, 13);
            this.LblCharLevel.TabIndex = 0;
            this.LblCharLevel.Text = "Level: ";
            // 
            // TabCharsArts
            // 
            this.TabCharsArts.Controls.Add (this.GbxCharSetMonadoArts);
            this.TabCharsArts.Controls.Add (this.GbxCharArtLevels);
            this.TabCharsArts.Controls.Add (this.GbxCharSetNormalArts);
            this.TabCharsArts.Location = new System.Drawing.Point (4, 22);
            this.TabCharsArts.Name = "TabCharsArts";
            this.TabCharsArts.Padding = new System.Windows.Forms.Padding (3);
            this.TabCharsArts.Size = new System.Drawing.Size (1070, 586);
            this.TabCharsArts.TabIndex = 1;
            this.TabCharsArts.Text = "Arts";
            this.TabCharsArts.UseVisualStyleBackColor = true;
            // 
            // GbxCharSetMonadoArts
            // 
            this.GbxCharSetMonadoArts.Controls.Add (this.CbxCharMonadoArt8ID);
            this.GbxCharSetMonadoArts.Controls.Add (this.NudCharMonadoArt8ID);
            this.GbxCharSetMonadoArts.Controls.Add (this.LblCharMonadoArt8ID);
            this.GbxCharSetMonadoArts.Controls.Add (this.CbxCharMonadoArt7ID);
            this.GbxCharSetMonadoArts.Controls.Add (this.CbxCharMonadoArt6ID);
            this.GbxCharSetMonadoArts.Controls.Add (this.CbxCharMonadoArt5ID);
            this.GbxCharSetMonadoArts.Controls.Add (this.NudCharMonadoArt7ID);
            this.GbxCharSetMonadoArts.Controls.Add (this.NudCharMonadoArt6ID);
            this.GbxCharSetMonadoArts.Controls.Add (this.NudCharMonadoArt5ID);
            this.GbxCharSetMonadoArts.Controls.Add (this.LblCharMonadoArt7ID);
            this.GbxCharSetMonadoArts.Controls.Add (this.LblCharMonadoArt5ID);
            this.GbxCharSetMonadoArts.Controls.Add (this.LblCharMonadoArt6ID);
            this.GbxCharSetMonadoArts.Controls.Add (this.CbxCharMonTalentArtID);
            this.GbxCharSetMonadoArts.Controls.Add (this.NudCharMonTalentArtID);
            this.GbxCharSetMonadoArts.Controls.Add (this.LblCharMonTalentArtID);
            this.GbxCharSetMonadoArts.Controls.Add (this.CbxCharMonadoArt4ID);
            this.GbxCharSetMonadoArts.Controls.Add (this.NudCharMonadoArt4ID);
            this.GbxCharSetMonadoArts.Controls.Add (this.LblCharMonadoArt4ID);
            this.GbxCharSetMonadoArts.Controls.Add (this.CbxCharMonadoArt3ID);
            this.GbxCharSetMonadoArts.Controls.Add (this.CbxCharMonadoArt2ID);
            this.GbxCharSetMonadoArts.Controls.Add (this.CbxCharMonadoArt1ID);
            this.GbxCharSetMonadoArts.Controls.Add (this.NudCharMonadoArt3ID);
            this.GbxCharSetMonadoArts.Controls.Add (this.NudCharMonadoArt2ID);
            this.GbxCharSetMonadoArts.Controls.Add (this.NudCharMonadoArt1ID);
            this.GbxCharSetMonadoArts.Controls.Add (this.LblCharMonadoArt3ID);
            this.GbxCharSetMonadoArts.Controls.Add (this.LblCharMonadoArt1ID);
            this.GbxCharSetMonadoArts.Controls.Add (this.LblCharMonadoArt2ID);
            this.GbxCharSetMonadoArts.Location = new System.Drawing.Point (380, 6);
            this.GbxCharSetMonadoArts.Name = "GbxCharSetMonadoArts";
            this.GbxCharSetMonadoArts.Size = new System.Drawing.Size (368, 263);
            this.GbxCharSetMonadoArts.TabIndex = 44;
            this.GbxCharSetMonadoArts.TabStop = false;
            this.GbxCharSetMonadoArts.Text = "Set Monado Arts";
            // 
            // CbxCharMonadoArt8ID
            // 
            this.CbxCharMonadoArt8ID.FormattingEnabled = true;
            this.CbxCharMonadoArt8ID.Location = new System.Drawing.Point (134, 231);
            this.CbxCharMonadoArt8ID.Name = "CbxCharMonadoArt8ID";
            this.CbxCharMonadoArt8ID.Size = new System.Drawing.Size (225, 21);
            this.CbxCharMonadoArt8ID.TabIndex = 37;
            this.CbxCharMonadoArt8ID.SelectionChangeCommitted += new System.EventHandler (this.CbxCharMonadoArt8ID_SelectionChangeCommitted);
            // 
            // NudCharMonadoArt8ID
            // 
            this.NudCharMonadoArt8ID.Location = new System.Drawing.Point (80, 231);
            this.NudCharMonadoArt8ID.Maximum = new decimal (new int[] {
            -1,
            0,
            0,
            0});
            this.NudCharMonadoArt8ID.Name = "NudCharMonadoArt8ID";
            this.NudCharMonadoArt8ID.Size = new System.Drawing.Size (48, 20);
            this.NudCharMonadoArt8ID.TabIndex = 36;
            this.NudCharMonadoArt8ID.ValueChanged += new System.EventHandler (this.NudCharMonadoArt8ID_ValueChanged);
            // 
            // LblCharMonadoArt8ID
            // 
            this.LblCharMonadoArt8ID.AutoSize = true;
            this.LblCharMonadoArt8ID.Location = new System.Drawing.Point (6, 234);
            this.LblCharMonadoArt8ID.Name = "LblCharMonadoArt8ID";
            this.LblCharMonadoArt8ID.Size = new System.Drawing.Size (74, 13);
            this.LblCharMonadoArt8ID.TabIndex = 35;
            this.LblCharMonadoArt8ID.Text = "Monado Art 8:";
            // 
            // CbxCharMonadoArt7ID
            // 
            this.CbxCharMonadoArt7ID.FormattingEnabled = true;
            this.CbxCharMonadoArt7ID.Location = new System.Drawing.Point (134, 205);
            this.CbxCharMonadoArt7ID.Name = "CbxCharMonadoArt7ID";
            this.CbxCharMonadoArt7ID.Size = new System.Drawing.Size (225, 21);
            this.CbxCharMonadoArt7ID.TabIndex = 34;
            this.CbxCharMonadoArt7ID.SelectionChangeCommitted += new System.EventHandler (this.CbxCharMonadoArt7ID_SelectionChangeCommitted);
            // 
            // CbxCharMonadoArt6ID
            // 
            this.CbxCharMonadoArt6ID.FormattingEnabled = true;
            this.CbxCharMonadoArt6ID.Location = new System.Drawing.Point (134, 179);
            this.CbxCharMonadoArt6ID.Name = "CbxCharMonadoArt6ID";
            this.CbxCharMonadoArt6ID.Size = new System.Drawing.Size (225, 21);
            this.CbxCharMonadoArt6ID.TabIndex = 33;
            this.CbxCharMonadoArt6ID.SelectionChangeCommitted += new System.EventHandler (this.CbxCharMonadoArt6ID_SelectionChangeCommitted);
            // 
            // CbxCharMonadoArt5ID
            // 
            this.CbxCharMonadoArt5ID.FormattingEnabled = true;
            this.CbxCharMonadoArt5ID.Location = new System.Drawing.Point (134, 153);
            this.CbxCharMonadoArt5ID.Name = "CbxCharMonadoArt5ID";
            this.CbxCharMonadoArt5ID.Size = new System.Drawing.Size (225, 21);
            this.CbxCharMonadoArt5ID.TabIndex = 32;
            this.CbxCharMonadoArt5ID.SelectionChangeCommitted += new System.EventHandler (this.CbxCharMonadoArt5ID_SelectionChangeCommitted);
            // 
            // NudCharMonadoArt7ID
            // 
            this.NudCharMonadoArt7ID.Location = new System.Drawing.Point (80, 205);
            this.NudCharMonadoArt7ID.Maximum = new decimal (new int[] {
            -1,
            0,
            0,
            0});
            this.NudCharMonadoArt7ID.Name = "NudCharMonadoArt7ID";
            this.NudCharMonadoArt7ID.Size = new System.Drawing.Size (48, 20);
            this.NudCharMonadoArt7ID.TabIndex = 31;
            this.NudCharMonadoArt7ID.ValueChanged += new System.EventHandler (this.NudCharMonadoArt7ID_ValueChanged);
            // 
            // NudCharMonadoArt6ID
            // 
            this.NudCharMonadoArt6ID.Location = new System.Drawing.Point (80, 179);
            this.NudCharMonadoArt6ID.Maximum = new decimal (new int[] {
            -1,
            0,
            0,
            0});
            this.NudCharMonadoArt6ID.Name = "NudCharMonadoArt6ID";
            this.NudCharMonadoArt6ID.Size = new System.Drawing.Size (48, 20);
            this.NudCharMonadoArt6ID.TabIndex = 30;
            this.NudCharMonadoArt6ID.ValueChanged += new System.EventHandler (this.NudCharMonadoArt6ID_ValueChanged);
            // 
            // NudCharMonadoArt5ID
            // 
            this.NudCharMonadoArt5ID.Location = new System.Drawing.Point (80, 153);
            this.NudCharMonadoArt5ID.Maximum = new decimal (new int[] {
            -1,
            0,
            0,
            0});
            this.NudCharMonadoArt5ID.Name = "NudCharMonadoArt5ID";
            this.NudCharMonadoArt5ID.Size = new System.Drawing.Size (48, 20);
            this.NudCharMonadoArt5ID.TabIndex = 29;
            this.NudCharMonadoArt5ID.ValueChanged += new System.EventHandler (this.NudCharMonadoArt5ID_ValueChanged);
            // 
            // LblCharMonadoArt7ID
            // 
            this.LblCharMonadoArt7ID.AutoSize = true;
            this.LblCharMonadoArt7ID.Location = new System.Drawing.Point (6, 208);
            this.LblCharMonadoArt7ID.Name = "LblCharMonadoArt7ID";
            this.LblCharMonadoArt7ID.Size = new System.Drawing.Size (74, 13);
            this.LblCharMonadoArt7ID.TabIndex = 28;
            this.LblCharMonadoArt7ID.Text = "Monado Art 7:";
            // 
            // LblCharMonadoArt5ID
            // 
            this.LblCharMonadoArt5ID.AutoSize = true;
            this.LblCharMonadoArt5ID.Location = new System.Drawing.Point (6, 156);
            this.LblCharMonadoArt5ID.Name = "LblCharMonadoArt5ID";
            this.LblCharMonadoArt5ID.Size = new System.Drawing.Size (74, 13);
            this.LblCharMonadoArt5ID.TabIndex = 26;
            this.LblCharMonadoArt5ID.Text = "Monado Art 5:";
            // 
            // LblCharMonadoArt6ID
            // 
            this.LblCharMonadoArt6ID.AutoSize = true;
            this.LblCharMonadoArt6ID.Location = new System.Drawing.Point (6, 182);
            this.LblCharMonadoArt6ID.Name = "LblCharMonadoArt6ID";
            this.LblCharMonadoArt6ID.Size = new System.Drawing.Size (74, 13);
            this.LblCharMonadoArt6ID.TabIndex = 27;
            this.LblCharMonadoArt6ID.Text = "Monado Art 6:";
            // 
            // CbxCharMonTalentArtID
            // 
            this.CbxCharMonTalentArtID.FormattingEnabled = true;
            this.CbxCharMonTalentArtID.Location = new System.Drawing.Point (134, 127);
            this.CbxCharMonTalentArtID.Name = "CbxCharMonTalentArtID";
            this.CbxCharMonTalentArtID.Size = new System.Drawing.Size (225, 21);
            this.CbxCharMonTalentArtID.TabIndex = 25;
            this.CbxCharMonTalentArtID.SelectionChangeCommitted += new System.EventHandler (this.CbxCharMonTalentArtID_SelectionChangeCommitted);
            // 
            // NudCharMonTalentArtID
            // 
            this.NudCharMonTalentArtID.Location = new System.Drawing.Point (80, 127);
            this.NudCharMonTalentArtID.Maximum = new decimal (new int[] {
            -1,
            0,
            0,
            0});
            this.NudCharMonTalentArtID.Name = "NudCharMonTalentArtID";
            this.NudCharMonTalentArtID.Size = new System.Drawing.Size (48, 20);
            this.NudCharMonTalentArtID.TabIndex = 24;
            this.NudCharMonTalentArtID.ValueChanged += new System.EventHandler (this.NudCharMonTalentArtID_ValueChanged);
            // 
            // LblCharMonTalentArtID
            // 
            this.LblCharMonTalentArtID.AutoSize = true;
            this.LblCharMonTalentArtID.Location = new System.Drawing.Point (6, 130);
            this.LblCharMonTalentArtID.Name = "LblCharMonTalentArtID";
            this.LblCharMonTalentArtID.Size = new System.Drawing.Size (56, 13);
            this.LblCharMonTalentArtID.TabIndex = 23;
            this.LblCharMonTalentArtID.Text = "Talent Art:";
            // 
            // CbxCharMonadoArt4ID
            // 
            this.CbxCharMonadoArt4ID.FormattingEnabled = true;
            this.CbxCharMonadoArt4ID.Location = new System.Drawing.Point (134, 101);
            this.CbxCharMonadoArt4ID.Name = "CbxCharMonadoArt4ID";
            this.CbxCharMonadoArt4ID.Size = new System.Drawing.Size (225, 21);
            this.CbxCharMonadoArt4ID.TabIndex = 22;
            this.CbxCharMonadoArt4ID.SelectionChangeCommitted += new System.EventHandler (this.CbxCharMonadoArt4ID_SelectionChangeCommitted);
            // 
            // NudCharMonadoArt4ID
            // 
            this.NudCharMonadoArt4ID.Location = new System.Drawing.Point (80, 101);
            this.NudCharMonadoArt4ID.Maximum = new decimal (new int[] {
            -1,
            0,
            0,
            0});
            this.NudCharMonadoArt4ID.Name = "NudCharMonadoArt4ID";
            this.NudCharMonadoArt4ID.Size = new System.Drawing.Size (48, 20);
            this.NudCharMonadoArt4ID.TabIndex = 21;
            this.NudCharMonadoArt4ID.ValueChanged += new System.EventHandler (this.NudCharMonadoArt4ID_ValueChanged);
            // 
            // LblCharMonadoArt4ID
            // 
            this.LblCharMonadoArt4ID.AutoSize = true;
            this.LblCharMonadoArt4ID.Location = new System.Drawing.Point (6, 104);
            this.LblCharMonadoArt4ID.Name = "LblCharMonadoArt4ID";
            this.LblCharMonadoArt4ID.Size = new System.Drawing.Size (74, 13);
            this.LblCharMonadoArt4ID.TabIndex = 20;
            this.LblCharMonadoArt4ID.Text = "Monado Art 4:";
            // 
            // CbxCharMonadoArt3ID
            // 
            this.CbxCharMonadoArt3ID.FormattingEnabled = true;
            this.CbxCharMonadoArt3ID.Location = new System.Drawing.Point (134, 75);
            this.CbxCharMonadoArt3ID.Name = "CbxCharMonadoArt3ID";
            this.CbxCharMonadoArt3ID.Size = new System.Drawing.Size (225, 21);
            this.CbxCharMonadoArt3ID.TabIndex = 19;
            this.CbxCharMonadoArt3ID.SelectionChangeCommitted += new System.EventHandler (this.CbxCharMonadoArt3ID_SelectionChangeCommitted);
            // 
            // CbxCharMonadoArt2ID
            // 
            this.CbxCharMonadoArt2ID.FormattingEnabled = true;
            this.CbxCharMonadoArt2ID.Location = new System.Drawing.Point (134, 49);
            this.CbxCharMonadoArt2ID.Name = "CbxCharMonadoArt2ID";
            this.CbxCharMonadoArt2ID.Size = new System.Drawing.Size (225, 21);
            this.CbxCharMonadoArt2ID.TabIndex = 18;
            this.CbxCharMonadoArt2ID.SelectionChangeCommitted += new System.EventHandler (this.CbxCharMonadoArt2ID_SelectionChangeCommitted);
            // 
            // CbxCharMonadoArt1ID
            // 
            this.CbxCharMonadoArt1ID.FormattingEnabled = true;
            this.CbxCharMonadoArt1ID.Location = new System.Drawing.Point (134, 23);
            this.CbxCharMonadoArt1ID.Name = "CbxCharMonadoArt1ID";
            this.CbxCharMonadoArt1ID.Size = new System.Drawing.Size (225, 21);
            this.CbxCharMonadoArt1ID.TabIndex = 17;
            this.CbxCharMonadoArt1ID.SelectionChangeCommitted += new System.EventHandler (this.CbxCharMonadoArt1ID_SelectionChangeCommitted);
            // 
            // NudCharMonadoArt3ID
            // 
            this.NudCharMonadoArt3ID.Location = new System.Drawing.Point (80, 75);
            this.NudCharMonadoArt3ID.Maximum = new decimal (new int[] {
            -1,
            0,
            0,
            0});
            this.NudCharMonadoArt3ID.Name = "NudCharMonadoArt3ID";
            this.NudCharMonadoArt3ID.Size = new System.Drawing.Size (48, 20);
            this.NudCharMonadoArt3ID.TabIndex = 14;
            this.NudCharMonadoArt3ID.ValueChanged += new System.EventHandler (this.NudCharMonadoArt3ID_ValueChanged);
            // 
            // NudCharMonadoArt2ID
            // 
            this.NudCharMonadoArt2ID.Location = new System.Drawing.Point (80, 49);
            this.NudCharMonadoArt2ID.Maximum = new decimal (new int[] {
            -1,
            0,
            0,
            0});
            this.NudCharMonadoArt2ID.Name = "NudCharMonadoArt2ID";
            this.NudCharMonadoArt2ID.Size = new System.Drawing.Size (48, 20);
            this.NudCharMonadoArt2ID.TabIndex = 13;
            this.NudCharMonadoArt2ID.ValueChanged += new System.EventHandler (this.NudCharMonadoArt2ID_ValueChanged);
            // 
            // NudCharMonadoArt1ID
            // 
            this.NudCharMonadoArt1ID.Location = new System.Drawing.Point (80, 23);
            this.NudCharMonadoArt1ID.Maximum = new decimal (new int[] {
            -1,
            0,
            0,
            0});
            this.NudCharMonadoArt1ID.Name = "NudCharMonadoArt1ID";
            this.NudCharMonadoArt1ID.Size = new System.Drawing.Size (48, 20);
            this.NudCharMonadoArt1ID.TabIndex = 12;
            this.NudCharMonadoArt1ID.ValueChanged += new System.EventHandler (this.NudCharMonadoArt1ID_ValueChanged);
            // 
            // LblCharMonadoArt3ID
            // 
            this.LblCharMonadoArt3ID.AutoSize = true;
            this.LblCharMonadoArt3ID.Location = new System.Drawing.Point (6, 78);
            this.LblCharMonadoArt3ID.Name = "LblCharMonadoArt3ID";
            this.LblCharMonadoArt3ID.Size = new System.Drawing.Size (74, 13);
            this.LblCharMonadoArt3ID.TabIndex = 3;
            this.LblCharMonadoArt3ID.Text = "Monado Art 3:";
            // 
            // LblCharMonadoArt1ID
            // 
            this.LblCharMonadoArt1ID.AutoSize = true;
            this.LblCharMonadoArt1ID.Location = new System.Drawing.Point (6, 26);
            this.LblCharMonadoArt1ID.Name = "LblCharMonadoArt1ID";
            this.LblCharMonadoArt1ID.Size = new System.Drawing.Size (74, 13);
            this.LblCharMonadoArt1ID.TabIndex = 1;
            this.LblCharMonadoArt1ID.Text = "Monado Art 1:";
            // 
            // LblCharMonadoArt2ID
            // 
            this.LblCharMonadoArt2ID.AutoSize = true;
            this.LblCharMonadoArt2ID.Location = new System.Drawing.Point (6, 52);
            this.LblCharMonadoArt2ID.Name = "LblCharMonadoArt2ID";
            this.LblCharMonadoArt2ID.Size = new System.Drawing.Size (74, 13);
            this.LblCharMonadoArt2ID.TabIndex = 2;
            this.LblCharMonadoArt2ID.Text = "Monado Art 2:";
            // 
            // GbxCharArtLevels
            // 
            this.GbxCharArtLevels.Controls.Add (this.CbxCharArtUnlockLvl);
            this.GbxCharArtLevels.Controls.Add (this.LblCharArtUnlkLvl);
            this.GbxCharArtLevels.Controls.Add (this.NudCharArtUnlockLvl);
            this.GbxCharArtLevels.Controls.Add (this.NudCharArtPoints);
            this.GbxCharArtLevels.Controls.Add (this.LblCharArtPoints);
            this.GbxCharArtLevels.Controls.Add (this.NudCharArtLevel);
            this.GbxCharArtLevels.Controls.Add (this.LblCharArtLevel);
            this.GbxCharArtLevels.Controls.Add (this.CbxCharArtID);
            this.GbxCharArtLevels.Controls.Add (this.LblCharArtID);
            this.GbxCharArtLevels.Controls.Add (this.NudCharArtID);
            this.GbxCharArtLevels.Location = new System.Drawing.Point (6, 275);
            this.GbxCharArtLevels.Name = "GbxCharArtLevels";
            this.GbxCharArtLevels.Size = new System.Drawing.Size (368, 120);
            this.GbxCharArtLevels.TabIndex = 44;
            this.GbxCharArtLevels.TabStop = false;
            this.GbxCharArtLevels.Text = "Character Art Levels";
            // 
            // CbxCharArtUnlockLvl
            // 
            this.CbxCharArtUnlockLvl.FormattingEnabled = true;
            this.CbxCharArtUnlockLvl.Location = new System.Drawing.Point (155, 40);
            this.CbxCharArtUnlockLvl.Name = "CbxCharArtUnlockLvl";
            this.CbxCharArtUnlockLvl.Size = new System.Drawing.Size (200, 21);
            this.CbxCharArtUnlockLvl.TabIndex = 22;
            this.CbxCharArtUnlockLvl.SelectionChangeCommitted += new System.EventHandler (this.CbxCharArtUnlockLvl_SelectionChangeCommitted);
            // 
            // LblCharArtUnlkLvl
            // 
            this.LblCharArtUnlkLvl.AutoSize = true;
            this.LblCharArtUnlkLvl.Location = new System.Drawing.Point (6, 43);
            this.LblCharArtUnlkLvl.Name = "LblCharArtUnlkLvl";
            this.LblCharArtUnlkLvl.Size = new System.Drawing.Size (89, 13);
            this.LblCharArtUnlkLvl.TabIndex = 21;
            this.LblCharArtUnlkLvl.Text = "Art Unlock Level:";
            // 
            // NudCharArtUnlockLvl
            // 
            this.NudCharArtUnlockLvl.Location = new System.Drawing.Point (100, 40);
            this.NudCharArtUnlockLvl.Maximum = new decimal (new int[] {
            742,
            0,
            0,
            0});
            this.NudCharArtUnlockLvl.Name = "NudCharArtUnlockLvl";
            this.NudCharArtUnlockLvl.ReadOnly = true;
            this.NudCharArtUnlockLvl.Size = new System.Drawing.Size (48, 20);
            this.NudCharArtUnlockLvl.TabIndex = 20;
            // 
            // NudCharArtPoints
            // 
            this.NudCharArtPoints.Location = new System.Drawing.Point (100, 92);
            this.NudCharArtPoints.Maximum = new decimal (new int[] {
            -1,
            0,
            0,
            0});
            this.NudCharArtPoints.Name = "NudCharArtPoints";
            this.NudCharArtPoints.Size = new System.Drawing.Size (100, 20);
            this.NudCharArtPoints.TabIndex = 19;
            // 
            // LblCharArtPoints
            // 
            this.LblCharArtPoints.AutoSize = true;
            this.LblCharArtPoints.Location = new System.Drawing.Point (6, 94);
            this.LblCharArtPoints.Name = "LblCharArtPoints";
            this.LblCharArtPoints.Size = new System.Drawing.Size (55, 13);
            this.LblCharArtPoints.TabIndex = 17;
            this.LblCharArtPoints.Text = "Art Points:";
            // 
            // NudCharArtLevel
            // 
            this.NudCharArtLevel.Location = new System.Drawing.Point (100, 66);
            this.NudCharArtLevel.Maximum = new decimal (new int[] {
            255,
            0,
            0,
            0});
            this.NudCharArtLevel.Name = "NudCharArtLevel";
            this.NudCharArtLevel.Size = new System.Drawing.Size (48, 20);
            this.NudCharArtLevel.TabIndex = 4;
            this.NudCharArtLevel.ValueChanged += new System.EventHandler (this.NudCharArtLevel_ValueChanged);
            // 
            // LblCharArtLevel
            // 
            this.LblCharArtLevel.AutoSize = true;
            this.LblCharArtLevel.Location = new System.Drawing.Point (6, 68);
            this.LblCharArtLevel.Name = "LblCharArtLevel";
            this.LblCharArtLevel.Size = new System.Drawing.Size (52, 13);
            this.LblCharArtLevel.TabIndex = 3;
            this.LblCharArtLevel.Text = "Art Level:";
            // 
            // CbxCharArtID
            // 
            this.CbxCharArtID.FormattingEnabled = true;
            this.CbxCharArtID.Location = new System.Drawing.Point (155, 13);
            this.CbxCharArtID.Name = "CbxCharArtID";
            this.CbxCharArtID.Size = new System.Drawing.Size (200, 21);
            this.CbxCharArtID.TabIndex = 2;
            this.CbxCharArtID.SelectionChangeCommitted += new System.EventHandler (this.CbxCharArtID_SelectionChangeCommitted);
            // 
            // LblCharArtID
            // 
            this.LblCharArtID.AutoSize = true;
            this.LblCharArtID.Location = new System.Drawing.Point (6, 16);
            this.LblCharArtID.Name = "LblCharArtID";
            this.LblCharArtID.Size = new System.Drawing.Size (37, 13);
            this.LblCharArtID.TabIndex = 1;
            this.LblCharArtID.Text = "Art ID:";
            // 
            // NudCharArtID
            // 
            this.NudCharArtID.Location = new System.Drawing.Point (100, 13);
            this.NudCharArtID.Maximum = new decimal (new int[] {
            742,
            0,
            0,
            0});
            this.NudCharArtID.Name = "NudCharArtID";
            this.NudCharArtID.Size = new System.Drawing.Size (48, 20);
            this.NudCharArtID.TabIndex = 0;
            this.NudCharArtID.ValueChanged += new System.EventHandler (this.NudCharArtID_ValueChanged);
            // 
            // GbxCharSetNormalArts
            // 
            this.GbxCharSetNormalArts.Controls.Add (this.CbxCharNormalArt8ID);
            this.GbxCharSetNormalArts.Controls.Add (this.NudCharNormalArt8ID);
            this.GbxCharSetNormalArts.Controls.Add (this.LblCharNormalArt8ID);
            this.GbxCharSetNormalArts.Controls.Add (this.CbxCharNormalArt7ID);
            this.GbxCharSetNormalArts.Controls.Add (this.CbxCharNormalArt6ID);
            this.GbxCharSetNormalArts.Controls.Add (this.CbxCharNormalArt5ID);
            this.GbxCharSetNormalArts.Controls.Add (this.NudCharNormalArt7ID);
            this.GbxCharSetNormalArts.Controls.Add (this.NudCharNormalArt6ID);
            this.GbxCharSetNormalArts.Controls.Add (this.NudCharNormalArt5ID);
            this.GbxCharSetNormalArts.Controls.Add (this.LblCharNormalArt7ID);
            this.GbxCharSetNormalArts.Controls.Add (this.LblCharNormalArt5ID);
            this.GbxCharSetNormalArts.Controls.Add (this.LblCharNormalArt6ID);
            this.GbxCharSetNormalArts.Controls.Add (this.CbxCharTalentArtID);
            this.GbxCharSetNormalArts.Controls.Add (this.NudCharTalentArtID);
            this.GbxCharSetNormalArts.Controls.Add (this.LblCharTalentArtID);
            this.GbxCharSetNormalArts.Controls.Add (this.CbxCharNormalArt4ID);
            this.GbxCharSetNormalArts.Controls.Add (this.NudCharNormalArt4ID);
            this.GbxCharSetNormalArts.Controls.Add (this.LblCharNormalArt4ID);
            this.GbxCharSetNormalArts.Controls.Add (this.CbxCharNormalArt3ID);
            this.GbxCharSetNormalArts.Controls.Add (this.CbxCharNormalArt2ID);
            this.GbxCharSetNormalArts.Controls.Add (this.CbxCharNormalArt1ID);
            this.GbxCharSetNormalArts.Controls.Add (this.NudCharNormalArt3ID);
            this.GbxCharSetNormalArts.Controls.Add (this.NudCharNormalArt2ID);
            this.GbxCharSetNormalArts.Controls.Add (this.NudCharNormalArt1ID);
            this.GbxCharSetNormalArts.Controls.Add (this.LblCharNormalArt3ID);
            this.GbxCharSetNormalArts.Controls.Add (this.LblCharNormalArt1ID);
            this.GbxCharSetNormalArts.Controls.Add (this.LblCharNormalArt2ID);
            this.GbxCharSetNormalArts.Location = new System.Drawing.Point (6, 6);
            this.GbxCharSetNormalArts.Name = "GbxCharSetNormalArts";
            this.GbxCharSetNormalArts.Size = new System.Drawing.Size (368, 263);
            this.GbxCharSetNormalArts.TabIndex = 43;
            this.GbxCharSetNormalArts.TabStop = false;
            this.GbxCharSetNormalArts.Text = "Set Normal Arts";
            // 
            // CbxCharNormalArt8ID
            // 
            this.CbxCharNormalArt8ID.FormattingEnabled = true;
            this.CbxCharNormalArt8ID.Location = new System.Drawing.Point (134, 231);
            this.CbxCharNormalArt8ID.Name = "CbxCharNormalArt8ID";
            this.CbxCharNormalArt8ID.Size = new System.Drawing.Size (225, 21);
            this.CbxCharNormalArt8ID.TabIndex = 37;
            this.CbxCharNormalArt8ID.SelectionChangeCommitted += new System.EventHandler (this.CbxCharNormalArt8ID_SelectionChangeCommitted);
            // 
            // NudCharNormalArt8ID
            // 
            this.NudCharNormalArt8ID.Location = new System.Drawing.Point (80, 231);
            this.NudCharNormalArt8ID.Maximum = new decimal (new int[] {
            -1,
            0,
            0,
            0});
            this.NudCharNormalArt8ID.Name = "NudCharNormalArt8ID";
            this.NudCharNormalArt8ID.Size = new System.Drawing.Size (48, 20);
            this.NudCharNormalArt8ID.TabIndex = 36;
            this.NudCharNormalArt8ID.ValueChanged += new System.EventHandler (this.NudCharNormalArt8ID_ValueChanged);
            // 
            // LblCharNormalArt8ID
            // 
            this.LblCharNormalArt8ID.AutoSize = true;
            this.LblCharNormalArt8ID.Location = new System.Drawing.Point (6, 234);
            this.LblCharNormalArt8ID.Name = "LblCharNormalArt8ID";
            this.LblCharNormalArt8ID.Size = new System.Drawing.Size (68, 13);
            this.LblCharNormalArt8ID.TabIndex = 35;
            this.LblCharNormalArt8ID.Text = "Normal Art 8:";
            // 
            // CbxCharNormalArt7ID
            // 
            this.CbxCharNormalArt7ID.FormattingEnabled = true;
            this.CbxCharNormalArt7ID.Location = new System.Drawing.Point (134, 205);
            this.CbxCharNormalArt7ID.Name = "CbxCharNormalArt7ID";
            this.CbxCharNormalArt7ID.Size = new System.Drawing.Size (225, 21);
            this.CbxCharNormalArt7ID.TabIndex = 34;
            this.CbxCharNormalArt7ID.SelectionChangeCommitted += new System.EventHandler (this.CbxCharNormalArt7ID_SelectionChangeCommitted);
            // 
            // CbxCharNormalArt6ID
            // 
            this.CbxCharNormalArt6ID.FormattingEnabled = true;
            this.CbxCharNormalArt6ID.Location = new System.Drawing.Point (134, 179);
            this.CbxCharNormalArt6ID.Name = "CbxCharNormalArt6ID";
            this.CbxCharNormalArt6ID.Size = new System.Drawing.Size (225, 21);
            this.CbxCharNormalArt6ID.TabIndex = 33;
            this.CbxCharNormalArt6ID.SelectionChangeCommitted += new System.EventHandler (this.CbxCharNormalArt6ID_SelectionChangeCommitted);
            // 
            // CbxCharNormalArt5ID
            // 
            this.CbxCharNormalArt5ID.FormattingEnabled = true;
            this.CbxCharNormalArt5ID.Location = new System.Drawing.Point (134, 153);
            this.CbxCharNormalArt5ID.Name = "CbxCharNormalArt5ID";
            this.CbxCharNormalArt5ID.Size = new System.Drawing.Size (225, 21);
            this.CbxCharNormalArt5ID.TabIndex = 32;
            this.CbxCharNormalArt5ID.SelectionChangeCommitted += new System.EventHandler (this.CbxCharNormalArt5ID_SelectionChangeCommitted);
            // 
            // NudCharNormalArt7ID
            // 
            this.NudCharNormalArt7ID.Location = new System.Drawing.Point (80, 205);
            this.NudCharNormalArt7ID.Maximum = new decimal (new int[] {
            -1,
            0,
            0,
            0});
            this.NudCharNormalArt7ID.Name = "NudCharNormalArt7ID";
            this.NudCharNormalArt7ID.Size = new System.Drawing.Size (48, 20);
            this.NudCharNormalArt7ID.TabIndex = 31;
            this.NudCharNormalArt7ID.ValueChanged += new System.EventHandler (this.NudCharNormalArt7ID_ValueChanged);
            // 
            // NudCharNormalArt6ID
            // 
            this.NudCharNormalArt6ID.Location = new System.Drawing.Point (80, 179);
            this.NudCharNormalArt6ID.Maximum = new decimal (new int[] {
            -1,
            0,
            0,
            0});
            this.NudCharNormalArt6ID.Name = "NudCharNormalArt6ID";
            this.NudCharNormalArt6ID.Size = new System.Drawing.Size (48, 20);
            this.NudCharNormalArt6ID.TabIndex = 30;
            this.NudCharNormalArt6ID.ValueChanged += new System.EventHandler (this.NudCharNormalArt6ID_ValueChanged);
            // 
            // NudCharNormalArt5ID
            // 
            this.NudCharNormalArt5ID.Location = new System.Drawing.Point (80, 153);
            this.NudCharNormalArt5ID.Maximum = new decimal (new int[] {
            -1,
            0,
            0,
            0});
            this.NudCharNormalArt5ID.Name = "NudCharNormalArt5ID";
            this.NudCharNormalArt5ID.Size = new System.Drawing.Size (48, 20);
            this.NudCharNormalArt5ID.TabIndex = 29;
            this.NudCharNormalArt5ID.ValueChanged += new System.EventHandler (this.NudCharNormalArt5ID_ValueChanged);
            // 
            // LblCharNormalArt7ID
            // 
            this.LblCharNormalArt7ID.AutoSize = true;
            this.LblCharNormalArt7ID.Location = new System.Drawing.Point (6, 208);
            this.LblCharNormalArt7ID.Name = "LblCharNormalArt7ID";
            this.LblCharNormalArt7ID.Size = new System.Drawing.Size (68, 13);
            this.LblCharNormalArt7ID.TabIndex = 28;
            this.LblCharNormalArt7ID.Text = "Normal Art 7:";
            // 
            // LblCharNormalArt5ID
            // 
            this.LblCharNormalArt5ID.AutoSize = true;
            this.LblCharNormalArt5ID.Location = new System.Drawing.Point (6, 156);
            this.LblCharNormalArt5ID.Name = "LblCharNormalArt5ID";
            this.LblCharNormalArt5ID.Size = new System.Drawing.Size (68, 13);
            this.LblCharNormalArt5ID.TabIndex = 26;
            this.LblCharNormalArt5ID.Text = "Normal Art 5:";
            // 
            // LblCharNormalArt6ID
            // 
            this.LblCharNormalArt6ID.AutoSize = true;
            this.LblCharNormalArt6ID.Location = new System.Drawing.Point (6, 182);
            this.LblCharNormalArt6ID.Name = "LblCharNormalArt6ID";
            this.LblCharNormalArt6ID.Size = new System.Drawing.Size (68, 13);
            this.LblCharNormalArt6ID.TabIndex = 27;
            this.LblCharNormalArt6ID.Text = "Normal Art 6:";
            // 
            // CbxCharTalentArtID
            // 
            this.CbxCharTalentArtID.FormattingEnabled = true;
            this.CbxCharTalentArtID.Location = new System.Drawing.Point (134, 127);
            this.CbxCharTalentArtID.Name = "CbxCharTalentArtID";
            this.CbxCharTalentArtID.Size = new System.Drawing.Size (225, 21);
            this.CbxCharTalentArtID.TabIndex = 25;
            this.CbxCharTalentArtID.SelectionChangeCommitted += new System.EventHandler (this.CbxCharTalentArtID_SelectionChangeCommitted);
            // 
            // NudCharTalentArtID
            // 
            this.NudCharTalentArtID.Location = new System.Drawing.Point (80, 127);
            this.NudCharTalentArtID.Maximum = new decimal (new int[] {
            -1,
            0,
            0,
            0});
            this.NudCharTalentArtID.Name = "NudCharTalentArtID";
            this.NudCharTalentArtID.Size = new System.Drawing.Size (48, 20);
            this.NudCharTalentArtID.TabIndex = 24;
            this.NudCharTalentArtID.ValueChanged += new System.EventHandler (this.NudCharTalentArtID_ValueChanged);
            // 
            // LblCharTalentArtID
            // 
            this.LblCharTalentArtID.AutoSize = true;
            this.LblCharTalentArtID.Location = new System.Drawing.Point (6, 130);
            this.LblCharTalentArtID.Name = "LblCharTalentArtID";
            this.LblCharTalentArtID.Size = new System.Drawing.Size (56, 13);
            this.LblCharTalentArtID.TabIndex = 23;
            this.LblCharTalentArtID.Text = "Talent Art:";
            // 
            // CbxCharNormalArt4ID
            // 
            this.CbxCharNormalArt4ID.FormattingEnabled = true;
            this.CbxCharNormalArt4ID.Location = new System.Drawing.Point (134, 101);
            this.CbxCharNormalArt4ID.Name = "CbxCharNormalArt4ID";
            this.CbxCharNormalArt4ID.Size = new System.Drawing.Size (225, 21);
            this.CbxCharNormalArt4ID.TabIndex = 22;
            this.CbxCharNormalArt4ID.SelectionChangeCommitted += new System.EventHandler (this.CbxCharNormalArt4ID_SelectionChangeCommitted);
            // 
            // NudCharNormalArt4ID
            // 
            this.NudCharNormalArt4ID.Location = new System.Drawing.Point (80, 101);
            this.NudCharNormalArt4ID.Maximum = new decimal (new int[] {
            -1,
            0,
            0,
            0});
            this.NudCharNormalArt4ID.Name = "NudCharNormalArt4ID";
            this.NudCharNormalArt4ID.Size = new System.Drawing.Size (48, 20);
            this.NudCharNormalArt4ID.TabIndex = 21;
            this.NudCharNormalArt4ID.ValueChanged += new System.EventHandler (this.NudCharNormalArt4ID_ValueChanged);
            // 
            // LblCharNormalArt4ID
            // 
            this.LblCharNormalArt4ID.AutoSize = true;
            this.LblCharNormalArt4ID.Location = new System.Drawing.Point (6, 104);
            this.LblCharNormalArt4ID.Name = "LblCharNormalArt4ID";
            this.LblCharNormalArt4ID.Size = new System.Drawing.Size (68, 13);
            this.LblCharNormalArt4ID.TabIndex = 20;
            this.LblCharNormalArt4ID.Text = "Normal Art 4:";
            // 
            // CbxCharNormalArt3ID
            // 
            this.CbxCharNormalArt3ID.FormattingEnabled = true;
            this.CbxCharNormalArt3ID.Location = new System.Drawing.Point (134, 75);
            this.CbxCharNormalArt3ID.Name = "CbxCharNormalArt3ID";
            this.CbxCharNormalArt3ID.Size = new System.Drawing.Size (225, 21);
            this.CbxCharNormalArt3ID.TabIndex = 19;
            this.CbxCharNormalArt3ID.SelectionChangeCommitted += new System.EventHandler (this.CbxCharNormalArt3ID_SelectionChangeCommitted);
            // 
            // CbxCharNormalArt2ID
            // 
            this.CbxCharNormalArt2ID.FormattingEnabled = true;
            this.CbxCharNormalArt2ID.Location = new System.Drawing.Point (134, 49);
            this.CbxCharNormalArt2ID.Name = "CbxCharNormalArt2ID";
            this.CbxCharNormalArt2ID.Size = new System.Drawing.Size (225, 21);
            this.CbxCharNormalArt2ID.TabIndex = 18;
            this.CbxCharNormalArt2ID.SelectionChangeCommitted += new System.EventHandler (this.CbxCharNormalArt2ID_SelectionChangeCommitted);
            // 
            // CbxCharNormalArt1ID
            // 
            this.CbxCharNormalArt1ID.FormattingEnabled = true;
            this.CbxCharNormalArt1ID.Location = new System.Drawing.Point (134, 23);
            this.CbxCharNormalArt1ID.Name = "CbxCharNormalArt1ID";
            this.CbxCharNormalArt1ID.Size = new System.Drawing.Size (225, 21);
            this.CbxCharNormalArt1ID.TabIndex = 17;
            this.CbxCharNormalArt1ID.SelectionChangeCommitted += new System.EventHandler (this.CbxCharNormalArt1ID_SelectionChangeCommitted);
            // 
            // NudCharNormalArt3ID
            // 
            this.NudCharNormalArt3ID.Location = new System.Drawing.Point (80, 75);
            this.NudCharNormalArt3ID.Maximum = new decimal (new int[] {
            -1,
            0,
            0,
            0});
            this.NudCharNormalArt3ID.Name = "NudCharNormalArt3ID";
            this.NudCharNormalArt3ID.Size = new System.Drawing.Size (48, 20);
            this.NudCharNormalArt3ID.TabIndex = 14;
            this.NudCharNormalArt3ID.ValueChanged += new System.EventHandler (this.NudCharNormalArt3ID_ValueChanged);
            // 
            // NudCharNormalArt2ID
            // 
            this.NudCharNormalArt2ID.Location = new System.Drawing.Point (80, 49);
            this.NudCharNormalArt2ID.Maximum = new decimal (new int[] {
            -1,
            0,
            0,
            0});
            this.NudCharNormalArt2ID.Name = "NudCharNormalArt2ID";
            this.NudCharNormalArt2ID.Size = new System.Drawing.Size (48, 20);
            this.NudCharNormalArt2ID.TabIndex = 13;
            this.NudCharNormalArt2ID.ValueChanged += new System.EventHandler (this.NudCharNormalArt2ID_ValueChanged);
            // 
            // NudCharNormalArt1ID
            // 
            this.NudCharNormalArt1ID.Location = new System.Drawing.Point (80, 23);
            this.NudCharNormalArt1ID.Maximum = new decimal (new int[] {
            -1,
            0,
            0,
            0});
            this.NudCharNormalArt1ID.Name = "NudCharNormalArt1ID";
            this.NudCharNormalArt1ID.Size = new System.Drawing.Size (48, 20);
            this.NudCharNormalArt1ID.TabIndex = 12;
            this.NudCharNormalArt1ID.ValueChanged += new System.EventHandler (this.NudCharNormalArt1ID_ValueChanged);
            // 
            // LblCharNormalArt3ID
            // 
            this.LblCharNormalArt3ID.AutoSize = true;
            this.LblCharNormalArt3ID.Location = new System.Drawing.Point (6, 78);
            this.LblCharNormalArt3ID.Name = "LblCharNormalArt3ID";
            this.LblCharNormalArt3ID.Size = new System.Drawing.Size (68, 13);
            this.LblCharNormalArt3ID.TabIndex = 3;
            this.LblCharNormalArt3ID.Text = "Normal Art 3:";
            // 
            // LblCharNormalArt1ID
            // 
            this.LblCharNormalArt1ID.AutoSize = true;
            this.LblCharNormalArt1ID.Location = new System.Drawing.Point (6, 26);
            this.LblCharNormalArt1ID.Name = "LblCharNormalArt1ID";
            this.LblCharNormalArt1ID.Size = new System.Drawing.Size (68, 13);
            this.LblCharNormalArt1ID.TabIndex = 1;
            this.LblCharNormalArt1ID.Text = "Normal Art 1:";
            // 
            // LblCharNormalArt2ID
            // 
            this.LblCharNormalArt2ID.AutoSize = true;
            this.LblCharNormalArt2ID.Location = new System.Drawing.Point (6, 52);
            this.LblCharNormalArt2ID.Name = "LblCharNormalArt2ID";
            this.LblCharNormalArt2ID.Size = new System.Drawing.Size (68, 13);
            this.LblCharNormalArt2ID.TabIndex = 2;
            this.LblCharNormalArt2ID.Text = "Normal Art 2:";
            // 
            // TabCharsSkillTrees
            // 
            this.TabCharsSkillTrees.Controls.Add (this.gbxDriverSkillPoints);
            this.TabCharsSkillTrees.Controls.Add (this.gbxDriverHiddenSkills);
            this.TabCharsSkillTrees.Controls.Add (this.gbxDriverOvertSkills);
            this.TabCharsSkillTrees.Location = new System.Drawing.Point (4, 22);
            this.TabCharsSkillTrees.Name = "TabCharsSkillTrees";
            this.TabCharsSkillTrees.Padding = new System.Windows.Forms.Padding (3);
            this.TabCharsSkillTrees.Size = new System.Drawing.Size (1070, 586);
            this.TabCharsSkillTrees.TabIndex = 3;
            this.TabCharsSkillTrees.Text = "Skill Trees";
            this.TabCharsSkillTrees.UseVisualStyleBackColor = true;
            // 
            // gbxDriverSkillPoints
            // 
            this.gbxDriverSkillPoints.Controls.Add (this.lblDriverCurrentSkillPoints);
            this.gbxDriverSkillPoints.Controls.Add (this.nudDriverCurrentSkillPoints);
            this.gbxDriverSkillPoints.Controls.Add (this.nudDriverTotalSkillPoints);
            this.gbxDriverSkillPoints.Controls.Add (this.lblDriverTotalSkillPoints);
            this.gbxDriverSkillPoints.Location = new System.Drawing.Point (17, 393);
            this.gbxDriverSkillPoints.Name = "gbxDriverSkillPoints";
            this.gbxDriverSkillPoints.Size = new System.Drawing.Size (195, 58);
            this.gbxDriverSkillPoints.TabIndex = 104;
            this.gbxDriverSkillPoints.TabStop = false;
            this.gbxDriverSkillPoints.Text = "Skill Points";
            // 
            // lblDriverCurrentSkillPoints
            // 
            this.lblDriverCurrentSkillPoints.AutoSize = true;
            this.lblDriverCurrentSkillPoints.Location = new System.Drawing.Point (8, 14);
            this.lblDriverCurrentSkillPoints.Name = "lblDriverCurrentSkillPoints";
            this.lblDriverCurrentSkillPoints.Size = new System.Drawing.Size (98, 13);
            this.lblDriverCurrentSkillPoints.TabIndex = 19;
            this.lblDriverCurrentSkillPoints.Text = "Current Skill Points:";
            // 
            // nudDriverCurrentSkillPoints
            // 
            this.nudDriverCurrentSkillPoints.Location = new System.Drawing.Point (108, 12);
            this.nudDriverCurrentSkillPoints.Maximum = new decimal (new int[] {
            -1,
            0,
            0,
            0});
            this.nudDriverCurrentSkillPoints.Name = "nudDriverCurrentSkillPoints";
            this.nudDriverCurrentSkillPoints.Size = new System.Drawing.Size (76, 20);
            this.nudDriverCurrentSkillPoints.TabIndex = 37;
            // 
            // nudDriverTotalSkillPoints
            // 
            this.nudDriverTotalSkillPoints.Location = new System.Drawing.Point (108, 32);
            this.nudDriverTotalSkillPoints.Maximum = new decimal (new int[] {
            -1,
            0,
            0,
            0});
            this.nudDriverTotalSkillPoints.Name = "nudDriverTotalSkillPoints";
            this.nudDriverTotalSkillPoints.Size = new System.Drawing.Size (76, 20);
            this.nudDriverTotalSkillPoints.TabIndex = 38;
            // 
            // lblDriverTotalSkillPoints
            // 
            this.lblDriverTotalSkillPoints.AutoSize = true;
            this.lblDriverTotalSkillPoints.Location = new System.Drawing.Point (8, 34);
            this.lblDriverTotalSkillPoints.Name = "lblDriverTotalSkillPoints";
            this.lblDriverTotalSkillPoints.Size = new System.Drawing.Size (88, 13);
            this.lblDriverTotalSkillPoints.TabIndex = 20;
            this.lblDriverTotalSkillPoints.Text = "Total Skill Points:";
            // 
            // gbxDriverHiddenSkills
            // 
            this.gbxDriverHiddenSkills.Controls.Add (this.nudDriverHiddenSkill_c5_Level);
            this.gbxDriverHiddenSkills.Controls.Add (this.lblDriverHiddenSkillsColumn5Level);
            this.gbxDriverHiddenSkills.Controls.Add (this.nudDriverHiddenSkill_c5_ColumnNo);
            this.gbxDriverHiddenSkills.Controls.Add (this.lblDriverHiddenSkillsColumn5No);
            this.gbxDriverHiddenSkills.Controls.Add (this.nudDriverHiddenSkill_c4_Level);
            this.gbxDriverHiddenSkills.Controls.Add (this.lblDriverHiddenSkillsColumn4Level);
            this.gbxDriverHiddenSkills.Controls.Add (this.nudDriverHiddenSkill_c4_ColumnNo);
            this.gbxDriverHiddenSkills.Controls.Add (this.lblDriverHiddenSkillsColumn4No);
            this.gbxDriverHiddenSkills.Controls.Add (this.nudDriverHiddenSkill_c3_Level);
            this.gbxDriverHiddenSkills.Controls.Add (this.lblDriverHiddenSkillsColumn3Level);
            this.gbxDriverHiddenSkills.Controls.Add (this.nudDriverHiddenSkill_c3_ColumnNo);
            this.gbxDriverHiddenSkills.Controls.Add (this.lblDriverHiddenSkillsColumn3No);
            this.gbxDriverHiddenSkills.Controls.Add (this.nudDriverHiddenSkill_c2_Level);
            this.gbxDriverHiddenSkills.Controls.Add (this.lblDriverHiddenSkillsColumn2Level);
            this.gbxDriverHiddenSkills.Controls.Add (this.nudDriverHiddenSkill_c2_ColumnNo);
            this.gbxDriverHiddenSkills.Controls.Add (this.lblDriverHiddenSkillsColumn2No);
            this.gbxDriverHiddenSkills.Controls.Add (this.nudDriverHiddenSkill_c1_Level);
            this.gbxDriverHiddenSkills.Controls.Add (this.lblDriverHiddenSkillsColumn1Level);
            this.gbxDriverHiddenSkills.Controls.Add (this.nudDriverHiddenSkill_c1_ColumnNo);
            this.gbxDriverHiddenSkills.Controls.Add (this.lblDriverHiddenSkillsColumn1No);
            this.gbxDriverHiddenSkills.Controls.Add (this.lblDriverHiddenSkillsRow3);
            this.gbxDriverHiddenSkills.Controls.Add (this.lblDriverHiddenSkillsRow2);
            this.gbxDriverHiddenSkills.Controls.Add (this.lblDriverHiddenSkillsRow1);
            this.gbxDriverHiddenSkills.Controls.Add (this.cbxDriverHiddenSkill_c5r3);
            this.gbxDriverHiddenSkills.Controls.Add (this.nudDriverHiddenSkill_c5r1);
            this.gbxDriverHiddenSkills.Controls.Add (this.cbxDriverHiddenSkill_c5r2);
            this.gbxDriverHiddenSkills.Controls.Add (this.nudDriverHiddenSkill_c5r2);
            this.gbxDriverHiddenSkills.Controls.Add (this.cbxDriverHiddenSkill_c5r1);
            this.gbxDriverHiddenSkills.Controls.Add (this.nudDriverHiddenSkill_c5r3);
            this.gbxDriverHiddenSkills.Controls.Add (this.cbxDriverHiddenSkill_c4r3);
            this.gbxDriverHiddenSkills.Controls.Add (this.nudDriverHiddenSkill_c4r1);
            this.gbxDriverHiddenSkills.Controls.Add (this.cbxDriverHiddenSkill_c4r2);
            this.gbxDriverHiddenSkills.Controls.Add (this.nudDriverHiddenSkill_c4r2);
            this.gbxDriverHiddenSkills.Controls.Add (this.cbxDriverHiddenSkill_c4r1);
            this.gbxDriverHiddenSkills.Controls.Add (this.nudDriverHiddenSkill_c4r3);
            this.gbxDriverHiddenSkills.Controls.Add (this.cbxDriverHiddenSkill_c3r3);
            this.gbxDriverHiddenSkills.Controls.Add (this.nudDriverHiddenSkill_c3r1);
            this.gbxDriverHiddenSkills.Controls.Add (this.cbxDriverHiddenSkill_c3r2);
            this.gbxDriverHiddenSkills.Controls.Add (this.nudDriverHiddenSkill_c3r2);
            this.gbxDriverHiddenSkills.Controls.Add (this.cbxDriverHiddenSkill_c3r1);
            this.gbxDriverHiddenSkills.Controls.Add (this.nudDriverHiddenSkill_c3r3);
            this.gbxDriverHiddenSkills.Controls.Add (this.cbxDriverHiddenSkill_c2r3);
            this.gbxDriverHiddenSkills.Controls.Add (this.nudDriverHiddenSkill_c2r1);
            this.gbxDriverHiddenSkills.Controls.Add (this.cbxDriverHiddenSkill_c2r2);
            this.gbxDriverHiddenSkills.Controls.Add (this.nudDriverHiddenSkill_c2r2);
            this.gbxDriverHiddenSkills.Controls.Add (this.cbxDriverHiddenSkill_c2r1);
            this.gbxDriverHiddenSkills.Controls.Add (this.nudDriverHiddenSkill_c2r3);
            this.gbxDriverHiddenSkills.Controls.Add (this.cbxDriverHiddenSkill_c1r3);
            this.gbxDriverHiddenSkills.Controls.Add (this.nudDriverHiddenSkill_c1r1);
            this.gbxDriverHiddenSkills.Controls.Add (this.cbxDriverHiddenSkill_c1r2);
            this.gbxDriverHiddenSkills.Controls.Add (this.nudDriverHiddenSkill_c1r2);
            this.gbxDriverHiddenSkills.Controls.Add (this.cbxDriverHiddenSkill_c1r1);
            this.gbxDriverHiddenSkills.Controls.Add (this.nudDriverHiddenSkill_c1r3);
            this.gbxDriverHiddenSkills.Location = new System.Drawing.Point (17, 243);
            this.gbxDriverHiddenSkills.Name = "gbxDriverHiddenSkills";
            this.gbxDriverHiddenSkills.Size = new System.Drawing.Size (915, 144);
            this.gbxDriverHiddenSkills.TabIndex = 106;
            this.gbxDriverHiddenSkills.TabStop = false;
            this.gbxDriverHiddenSkills.Text = "Hidden Skills";
            // 
            // nudDriverHiddenSkill_c5_Level
            // 
            this.nudDriverHiddenSkill_c5_Level.Location = new System.Drawing.Point (829, 112);
            this.nudDriverHiddenSkill_c5_Level.Name = "nudDriverHiddenSkill_c5_Level";
            this.nudDriverHiddenSkill_c5_Level.Size = new System.Drawing.Size (61, 20);
            this.nudDriverHiddenSkill_c5_Level.TabIndex = 98;
            // 
            // lblDriverHiddenSkillsColumn5Level
            // 
            this.lblDriverHiddenSkillsColumn5Level.AutoSize = true;
            this.lblDriverHiddenSkillsColumn5Level.Location = new System.Drawing.Point (761, 114);
            this.lblDriverHiddenSkillsColumn5Level.Name = "lblDriverHiddenSkillsColumn5Level";
            this.lblDriverHiddenSkillsColumn5Level.Size = new System.Drawing.Size (36, 13);
            this.lblDriverHiddenSkillsColumn5Level.TabIndex = 97;
            this.lblDriverHiddenSkillsColumn5Level.Text = "Level:";
            // 
            // nudDriverHiddenSkill_c5_ColumnNo
            // 
            this.nudDriverHiddenSkill_c5_ColumnNo.Location = new System.Drawing.Point (829, 86);
            this.nudDriverHiddenSkill_c5_ColumnNo.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.nudDriverHiddenSkill_c5_ColumnNo.Name = "nudDriverHiddenSkill_c5_ColumnNo";
            this.nudDriverHiddenSkill_c5_ColumnNo.Size = new System.Drawing.Size (61, 20);
            this.nudDriverHiddenSkill_c5_ColumnNo.TabIndex = 96;
            // 
            // lblDriverHiddenSkillsColumn5No
            // 
            this.lblDriverHiddenSkillsColumn5No.AutoSize = true;
            this.lblDriverHiddenSkillsColumn5No.Location = new System.Drawing.Point (761, 88);
            this.lblDriverHiddenSkillsColumn5No.Name = "lblDriverHiddenSkillsColumn5No";
            this.lblDriverHiddenSkillsColumn5No.Size = new System.Drawing.Size (62, 13);
            this.lblDriverHiddenSkillsColumn5No.TabIndex = 95;
            this.lblDriverHiddenSkillsColumn5No.Text = "Column No:";
            // 
            // nudDriverHiddenSkill_c4_Level
            // 
            this.nudDriverHiddenSkill_c4_Level.Location = new System.Drawing.Point (657, 112);
            this.nudDriverHiddenSkill_c4_Level.Name = "nudDriverHiddenSkill_c4_Level";
            this.nudDriverHiddenSkill_c4_Level.Size = new System.Drawing.Size (61, 20);
            this.nudDriverHiddenSkill_c4_Level.TabIndex = 94;
            // 
            // lblDriverHiddenSkillsColumn4Level
            // 
            this.lblDriverHiddenSkillsColumn4Level.AutoSize = true;
            this.lblDriverHiddenSkillsColumn4Level.Location = new System.Drawing.Point (589, 114);
            this.lblDriverHiddenSkillsColumn4Level.Name = "lblDriverHiddenSkillsColumn4Level";
            this.lblDriverHiddenSkillsColumn4Level.Size = new System.Drawing.Size (36, 13);
            this.lblDriverHiddenSkillsColumn4Level.TabIndex = 93;
            this.lblDriverHiddenSkillsColumn4Level.Text = "Level:";
            // 
            // nudDriverHiddenSkill_c4_ColumnNo
            // 
            this.nudDriverHiddenSkill_c4_ColumnNo.Location = new System.Drawing.Point (657, 86);
            this.nudDriverHiddenSkill_c4_ColumnNo.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.nudDriverHiddenSkill_c4_ColumnNo.Name = "nudDriverHiddenSkill_c4_ColumnNo";
            this.nudDriverHiddenSkill_c4_ColumnNo.Size = new System.Drawing.Size (61, 20);
            this.nudDriverHiddenSkill_c4_ColumnNo.TabIndex = 92;
            // 
            // lblDriverHiddenSkillsColumn4No
            // 
            this.lblDriverHiddenSkillsColumn4No.AutoSize = true;
            this.lblDriverHiddenSkillsColumn4No.Location = new System.Drawing.Point (589, 88);
            this.lblDriverHiddenSkillsColumn4No.Name = "lblDriverHiddenSkillsColumn4No";
            this.lblDriverHiddenSkillsColumn4No.Size = new System.Drawing.Size (62, 13);
            this.lblDriverHiddenSkillsColumn4No.TabIndex = 91;
            this.lblDriverHiddenSkillsColumn4No.Text = "Column No:";
            // 
            // nudDriverHiddenSkill_c3_Level
            // 
            this.nudDriverHiddenSkill_c3_Level.Location = new System.Drawing.Point (488, 112);
            this.nudDriverHiddenSkill_c3_Level.Name = "nudDriverHiddenSkill_c3_Level";
            this.nudDriverHiddenSkill_c3_Level.Size = new System.Drawing.Size (61, 20);
            this.nudDriverHiddenSkill_c3_Level.TabIndex = 90;
            // 
            // lblDriverHiddenSkillsColumn3Level
            // 
            this.lblDriverHiddenSkillsColumn3Level.AutoSize = true;
            this.lblDriverHiddenSkillsColumn3Level.Location = new System.Drawing.Point (420, 114);
            this.lblDriverHiddenSkillsColumn3Level.Name = "lblDriverHiddenSkillsColumn3Level";
            this.lblDriverHiddenSkillsColumn3Level.Size = new System.Drawing.Size (36, 13);
            this.lblDriverHiddenSkillsColumn3Level.TabIndex = 89;
            this.lblDriverHiddenSkillsColumn3Level.Text = "Level:";
            // 
            // nudDriverHiddenSkill_c3_ColumnNo
            // 
            this.nudDriverHiddenSkill_c3_ColumnNo.Location = new System.Drawing.Point (488, 86);
            this.nudDriverHiddenSkill_c3_ColumnNo.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.nudDriverHiddenSkill_c3_ColumnNo.Name = "nudDriverHiddenSkill_c3_ColumnNo";
            this.nudDriverHiddenSkill_c3_ColumnNo.Size = new System.Drawing.Size (61, 20);
            this.nudDriverHiddenSkill_c3_ColumnNo.TabIndex = 88;
            // 
            // lblDriverHiddenSkillsColumn3No
            // 
            this.lblDriverHiddenSkillsColumn3No.AutoSize = true;
            this.lblDriverHiddenSkillsColumn3No.Location = new System.Drawing.Point (420, 88);
            this.lblDriverHiddenSkillsColumn3No.Name = "lblDriverHiddenSkillsColumn3No";
            this.lblDriverHiddenSkillsColumn3No.Size = new System.Drawing.Size (62, 13);
            this.lblDriverHiddenSkillsColumn3No.TabIndex = 87;
            this.lblDriverHiddenSkillsColumn3No.Text = "Column No:";
            // 
            // nudDriverHiddenSkill_c2_Level
            // 
            this.nudDriverHiddenSkill_c2_Level.Location = new System.Drawing.Point (321, 112);
            this.nudDriverHiddenSkill_c2_Level.Name = "nudDriverHiddenSkill_c2_Level";
            this.nudDriverHiddenSkill_c2_Level.Size = new System.Drawing.Size (61, 20);
            this.nudDriverHiddenSkill_c2_Level.TabIndex = 86;
            // 
            // lblDriverHiddenSkillsColumn2Level
            // 
            this.lblDriverHiddenSkillsColumn2Level.AutoSize = true;
            this.lblDriverHiddenSkillsColumn2Level.Location = new System.Drawing.Point (253, 114);
            this.lblDriverHiddenSkillsColumn2Level.Name = "lblDriverHiddenSkillsColumn2Level";
            this.lblDriverHiddenSkillsColumn2Level.Size = new System.Drawing.Size (36, 13);
            this.lblDriverHiddenSkillsColumn2Level.TabIndex = 85;
            this.lblDriverHiddenSkillsColumn2Level.Text = "Level:";
            // 
            // nudDriverHiddenSkill_c2_ColumnNo
            // 
            this.nudDriverHiddenSkill_c2_ColumnNo.Location = new System.Drawing.Point (321, 86);
            this.nudDriverHiddenSkill_c2_ColumnNo.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.nudDriverHiddenSkill_c2_ColumnNo.Name = "nudDriverHiddenSkill_c2_ColumnNo";
            this.nudDriverHiddenSkill_c2_ColumnNo.Size = new System.Drawing.Size (61, 20);
            this.nudDriverHiddenSkill_c2_ColumnNo.TabIndex = 84;
            // 
            // lblDriverHiddenSkillsColumn2No
            // 
            this.lblDriverHiddenSkillsColumn2No.AutoSize = true;
            this.lblDriverHiddenSkillsColumn2No.Location = new System.Drawing.Point (253, 88);
            this.lblDriverHiddenSkillsColumn2No.Name = "lblDriverHiddenSkillsColumn2No";
            this.lblDriverHiddenSkillsColumn2No.Size = new System.Drawing.Size (62, 13);
            this.lblDriverHiddenSkillsColumn2No.TabIndex = 83;
            this.lblDriverHiddenSkillsColumn2No.Text = "Column No:";
            // 
            // nudDriverHiddenSkill_c1_Level
            // 
            this.nudDriverHiddenSkill_c1_Level.Location = new System.Drawing.Point (151, 112);
            this.nudDriverHiddenSkill_c1_Level.Name = "nudDriverHiddenSkill_c1_Level";
            this.nudDriverHiddenSkill_c1_Level.Size = new System.Drawing.Size (61, 20);
            this.nudDriverHiddenSkill_c1_Level.TabIndex = 82;
            // 
            // lblDriverHiddenSkillsColumn1Level
            // 
            this.lblDriverHiddenSkillsColumn1Level.AutoSize = true;
            this.lblDriverHiddenSkillsColumn1Level.Location = new System.Drawing.Point (83, 114);
            this.lblDriverHiddenSkillsColumn1Level.Name = "lblDriverHiddenSkillsColumn1Level";
            this.lblDriverHiddenSkillsColumn1Level.Size = new System.Drawing.Size (36, 13);
            this.lblDriverHiddenSkillsColumn1Level.TabIndex = 81;
            this.lblDriverHiddenSkillsColumn1Level.Text = "Level:";
            // 
            // nudDriverHiddenSkill_c1_ColumnNo
            // 
            this.nudDriverHiddenSkill_c1_ColumnNo.Location = new System.Drawing.Point (151, 86);
            this.nudDriverHiddenSkill_c1_ColumnNo.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.nudDriverHiddenSkill_c1_ColumnNo.Name = "nudDriverHiddenSkill_c1_ColumnNo";
            this.nudDriverHiddenSkill_c1_ColumnNo.Size = new System.Drawing.Size (61, 20);
            this.nudDriverHiddenSkill_c1_ColumnNo.TabIndex = 80;
            // 
            // lblDriverHiddenSkillsColumn1No
            // 
            this.lblDriverHiddenSkillsColumn1No.AutoSize = true;
            this.lblDriverHiddenSkillsColumn1No.Location = new System.Drawing.Point (83, 88);
            this.lblDriverHiddenSkillsColumn1No.Name = "lblDriverHiddenSkillsColumn1No";
            this.lblDriverHiddenSkillsColumn1No.Size = new System.Drawing.Size (62, 13);
            this.lblDriverHiddenSkillsColumn1No.TabIndex = 79;
            this.lblDriverHiddenSkillsColumn1No.Text = "Column No:";
            // 
            // lblDriverHiddenSkillsRow3
            // 
            this.lblDriverHiddenSkillsRow3.AutoSize = true;
            this.lblDriverHiddenSkillsRow3.Location = new System.Drawing.Point (6, 22);
            this.lblDriverHiddenSkillsRow3.Name = "lblDriverHiddenSkillsRow3";
            this.lblDriverHiddenSkillsRow3.Size = new System.Drawing.Size (41, 13);
            this.lblDriverHiddenSkillsRow3.TabIndex = 78;
            this.lblDriverHiddenSkillsRow3.Text = "Row 3:";
            // 
            // lblDriverHiddenSkillsRow2
            // 
            this.lblDriverHiddenSkillsRow2.AutoSize = true;
            this.lblDriverHiddenSkillsRow2.Location = new System.Drawing.Point (6, 42);
            this.lblDriverHiddenSkillsRow2.Name = "lblDriverHiddenSkillsRow2";
            this.lblDriverHiddenSkillsRow2.Size = new System.Drawing.Size (41, 13);
            this.lblDriverHiddenSkillsRow2.TabIndex = 77;
            this.lblDriverHiddenSkillsRow2.Text = "Row 2:";
            // 
            // lblDriverHiddenSkillsRow1
            // 
            this.lblDriverHiddenSkillsRow1.AutoSize = true;
            this.lblDriverHiddenSkillsRow1.Location = new System.Drawing.Point (6, 62);
            this.lblDriverHiddenSkillsRow1.Name = "lblDriverHiddenSkillsRow1";
            this.lblDriverHiddenSkillsRow1.Size = new System.Drawing.Size (41, 13);
            this.lblDriverHiddenSkillsRow1.TabIndex = 76;
            this.lblDriverHiddenSkillsRow1.Text = "Row 1:";
            // 
            // cbxDriverHiddenSkill_c5r3
            // 
            this.cbxDriverHiddenSkill_c5r3.FormattingEnabled = true;
            this.cbxDriverHiddenSkill_c5r3.Location = new System.Drawing.Point (788, 19);
            this.cbxDriverHiddenSkill_c5r3.Name = "cbxDriverHiddenSkill_c5r3";
            this.cbxDriverHiddenSkill_c5r3.Size = new System.Drawing.Size (121, 21);
            this.cbxDriverHiddenSkill_c5r3.TabIndex = 75;
            // 
            // nudDriverHiddenSkill_c5r1
            // 
            this.nudDriverHiddenSkill_c5r1.Location = new System.Drawing.Point (741, 59);
            this.nudDriverHiddenSkill_c5r1.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.nudDriverHiddenSkill_c5r1.Name = "nudDriverHiddenSkill_c5r1";
            this.nudDriverHiddenSkill_c5r1.Size = new System.Drawing.Size (48, 20);
            this.nudDriverHiddenSkill_c5r1.TabIndex = 70;
            // 
            // cbxDriverHiddenSkill_c5r2
            // 
            this.cbxDriverHiddenSkill_c5r2.FormattingEnabled = true;
            this.cbxDriverHiddenSkill_c5r2.Location = new System.Drawing.Point (788, 39);
            this.cbxDriverHiddenSkill_c5r2.Name = "cbxDriverHiddenSkill_c5r2";
            this.cbxDriverHiddenSkill_c5r2.Size = new System.Drawing.Size (121, 21);
            this.cbxDriverHiddenSkill_c5r2.TabIndex = 74;
            // 
            // nudDriverHiddenSkill_c5r2
            // 
            this.nudDriverHiddenSkill_c5r2.Location = new System.Drawing.Point (741, 39);
            this.nudDriverHiddenSkill_c5r2.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.nudDriverHiddenSkill_c5r2.Name = "nudDriverHiddenSkill_c5r2";
            this.nudDriverHiddenSkill_c5r2.Size = new System.Drawing.Size (48, 20);
            this.nudDriverHiddenSkill_c5r2.TabIndex = 71;
            // 
            // cbxDriverHiddenSkill_c5r1
            // 
            this.cbxDriverHiddenSkill_c5r1.FormattingEnabled = true;
            this.cbxDriverHiddenSkill_c5r1.Location = new System.Drawing.Point (788, 59);
            this.cbxDriverHiddenSkill_c5r1.Name = "cbxDriverHiddenSkill_c5r1";
            this.cbxDriverHiddenSkill_c5r1.Size = new System.Drawing.Size (121, 21);
            this.cbxDriverHiddenSkill_c5r1.TabIndex = 73;
            // 
            // nudDriverHiddenSkill_c5r3
            // 
            this.nudDriverHiddenSkill_c5r3.Location = new System.Drawing.Point (741, 19);
            this.nudDriverHiddenSkill_c5r3.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.nudDriverHiddenSkill_c5r3.Name = "nudDriverHiddenSkill_c5r3";
            this.nudDriverHiddenSkill_c5r3.Size = new System.Drawing.Size (48, 20);
            this.nudDriverHiddenSkill_c5r3.TabIndex = 72;
            // 
            // cbxDriverHiddenSkill_c4r3
            // 
            this.cbxDriverHiddenSkill_c4r3.FormattingEnabled = true;
            this.cbxDriverHiddenSkill_c4r3.Location = new System.Drawing.Point (619, 19);
            this.cbxDriverHiddenSkill_c4r3.Name = "cbxDriverHiddenSkill_c4r3";
            this.cbxDriverHiddenSkill_c4r3.Size = new System.Drawing.Size (121, 21);
            this.cbxDriverHiddenSkill_c4r3.TabIndex = 69;
            // 
            // nudDriverHiddenSkill_c4r1
            // 
            this.nudDriverHiddenSkill_c4r1.Location = new System.Drawing.Point (572, 59);
            this.nudDriverHiddenSkill_c4r1.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.nudDriverHiddenSkill_c4r1.Name = "nudDriverHiddenSkill_c4r1";
            this.nudDriverHiddenSkill_c4r1.Size = new System.Drawing.Size (48, 20);
            this.nudDriverHiddenSkill_c4r1.TabIndex = 64;
            // 
            // cbxDriverHiddenSkill_c4r2
            // 
            this.cbxDriverHiddenSkill_c4r2.FormattingEnabled = true;
            this.cbxDriverHiddenSkill_c4r2.Location = new System.Drawing.Point (619, 39);
            this.cbxDriverHiddenSkill_c4r2.Name = "cbxDriverHiddenSkill_c4r2";
            this.cbxDriverHiddenSkill_c4r2.Size = new System.Drawing.Size (121, 21);
            this.cbxDriverHiddenSkill_c4r2.TabIndex = 68;
            // 
            // nudDriverHiddenSkill_c4r2
            // 
            this.nudDriverHiddenSkill_c4r2.Location = new System.Drawing.Point (572, 39);
            this.nudDriverHiddenSkill_c4r2.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.nudDriverHiddenSkill_c4r2.Name = "nudDriverHiddenSkill_c4r2";
            this.nudDriverHiddenSkill_c4r2.Size = new System.Drawing.Size (48, 20);
            this.nudDriverHiddenSkill_c4r2.TabIndex = 65;
            // 
            // cbxDriverHiddenSkill_c4r1
            // 
            this.cbxDriverHiddenSkill_c4r1.FormattingEnabled = true;
            this.cbxDriverHiddenSkill_c4r1.Location = new System.Drawing.Point (619, 59);
            this.cbxDriverHiddenSkill_c4r1.Name = "cbxDriverHiddenSkill_c4r1";
            this.cbxDriverHiddenSkill_c4r1.Size = new System.Drawing.Size (121, 21);
            this.cbxDriverHiddenSkill_c4r1.TabIndex = 67;
            // 
            // nudDriverHiddenSkill_c4r3
            // 
            this.nudDriverHiddenSkill_c4r3.Location = new System.Drawing.Point (572, 19);
            this.nudDriverHiddenSkill_c4r3.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.nudDriverHiddenSkill_c4r3.Name = "nudDriverHiddenSkill_c4r3";
            this.nudDriverHiddenSkill_c4r3.Size = new System.Drawing.Size (48, 20);
            this.nudDriverHiddenSkill_c4r3.TabIndex = 66;
            // 
            // cbxDriverHiddenSkill_c3r3
            // 
            this.cbxDriverHiddenSkill_c3r3.FormattingEnabled = true;
            this.cbxDriverHiddenSkill_c3r3.Location = new System.Drawing.Point (450, 19);
            this.cbxDriverHiddenSkill_c3r3.Name = "cbxDriverHiddenSkill_c3r3";
            this.cbxDriverHiddenSkill_c3r3.Size = new System.Drawing.Size (121, 21);
            this.cbxDriverHiddenSkill_c3r3.TabIndex = 63;
            // 
            // nudDriverHiddenSkill_c3r1
            // 
            this.nudDriverHiddenSkill_c3r1.Location = new System.Drawing.Point (403, 59);
            this.nudDriverHiddenSkill_c3r1.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.nudDriverHiddenSkill_c3r1.Name = "nudDriverHiddenSkill_c3r1";
            this.nudDriverHiddenSkill_c3r1.Size = new System.Drawing.Size (48, 20);
            this.nudDriverHiddenSkill_c3r1.TabIndex = 58;
            // 
            // cbxDriverHiddenSkill_c3r2
            // 
            this.cbxDriverHiddenSkill_c3r2.FormattingEnabled = true;
            this.cbxDriverHiddenSkill_c3r2.Location = new System.Drawing.Point (450, 39);
            this.cbxDriverHiddenSkill_c3r2.Name = "cbxDriverHiddenSkill_c3r2";
            this.cbxDriverHiddenSkill_c3r2.Size = new System.Drawing.Size (121, 21);
            this.cbxDriverHiddenSkill_c3r2.TabIndex = 62;
            // 
            // nudDriverHiddenSkill_c3r2
            // 
            this.nudDriverHiddenSkill_c3r2.Location = new System.Drawing.Point (403, 39);
            this.nudDriverHiddenSkill_c3r2.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.nudDriverHiddenSkill_c3r2.Name = "nudDriverHiddenSkill_c3r2";
            this.nudDriverHiddenSkill_c3r2.Size = new System.Drawing.Size (48, 20);
            this.nudDriverHiddenSkill_c3r2.TabIndex = 59;
            // 
            // cbxDriverHiddenSkill_c3r1
            // 
            this.cbxDriverHiddenSkill_c3r1.FormattingEnabled = true;
            this.cbxDriverHiddenSkill_c3r1.Location = new System.Drawing.Point (450, 59);
            this.cbxDriverHiddenSkill_c3r1.Name = "cbxDriverHiddenSkill_c3r1";
            this.cbxDriverHiddenSkill_c3r1.Size = new System.Drawing.Size (121, 21);
            this.cbxDriverHiddenSkill_c3r1.TabIndex = 61;
            // 
            // nudDriverHiddenSkill_c3r3
            // 
            this.nudDriverHiddenSkill_c3r3.Location = new System.Drawing.Point (403, 19);
            this.nudDriverHiddenSkill_c3r3.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.nudDriverHiddenSkill_c3r3.Name = "nudDriverHiddenSkill_c3r3";
            this.nudDriverHiddenSkill_c3r3.Size = new System.Drawing.Size (48, 20);
            this.nudDriverHiddenSkill_c3r3.TabIndex = 60;
            // 
            // cbxDriverHiddenSkill_c2r3
            // 
            this.cbxDriverHiddenSkill_c2r3.FormattingEnabled = true;
            this.cbxDriverHiddenSkill_c2r3.Location = new System.Drawing.Point (281, 19);
            this.cbxDriverHiddenSkill_c2r3.Name = "cbxDriverHiddenSkill_c2r3";
            this.cbxDriverHiddenSkill_c2r3.Size = new System.Drawing.Size (121, 21);
            this.cbxDriverHiddenSkill_c2r3.TabIndex = 57;
            // 
            // nudDriverHiddenSkill_c2r1
            // 
            this.nudDriverHiddenSkill_c2r1.Location = new System.Drawing.Point (234, 59);
            this.nudDriverHiddenSkill_c2r1.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.nudDriverHiddenSkill_c2r1.Name = "nudDriverHiddenSkill_c2r1";
            this.nudDriverHiddenSkill_c2r1.Size = new System.Drawing.Size (48, 20);
            this.nudDriverHiddenSkill_c2r1.TabIndex = 52;
            // 
            // cbxDriverHiddenSkill_c2r2
            // 
            this.cbxDriverHiddenSkill_c2r2.FormattingEnabled = true;
            this.cbxDriverHiddenSkill_c2r2.Location = new System.Drawing.Point (281, 39);
            this.cbxDriverHiddenSkill_c2r2.Name = "cbxDriverHiddenSkill_c2r2";
            this.cbxDriverHiddenSkill_c2r2.Size = new System.Drawing.Size (121, 21);
            this.cbxDriverHiddenSkill_c2r2.TabIndex = 56;
            // 
            // nudDriverHiddenSkill_c2r2
            // 
            this.nudDriverHiddenSkill_c2r2.Location = new System.Drawing.Point (234, 39);
            this.nudDriverHiddenSkill_c2r2.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.nudDriverHiddenSkill_c2r2.Name = "nudDriverHiddenSkill_c2r2";
            this.nudDriverHiddenSkill_c2r2.Size = new System.Drawing.Size (48, 20);
            this.nudDriverHiddenSkill_c2r2.TabIndex = 53;
            // 
            // cbxDriverHiddenSkill_c2r1
            // 
            this.cbxDriverHiddenSkill_c2r1.FormattingEnabled = true;
            this.cbxDriverHiddenSkill_c2r1.Location = new System.Drawing.Point (281, 59);
            this.cbxDriverHiddenSkill_c2r1.Name = "cbxDriverHiddenSkill_c2r1";
            this.cbxDriverHiddenSkill_c2r1.Size = new System.Drawing.Size (121, 21);
            this.cbxDriverHiddenSkill_c2r1.TabIndex = 55;
            // 
            // nudDriverHiddenSkill_c2r3
            // 
            this.nudDriverHiddenSkill_c2r3.Location = new System.Drawing.Point (234, 19);
            this.nudDriverHiddenSkill_c2r3.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.nudDriverHiddenSkill_c2r3.Name = "nudDriverHiddenSkill_c2r3";
            this.nudDriverHiddenSkill_c2r3.Size = new System.Drawing.Size (48, 20);
            this.nudDriverHiddenSkill_c2r3.TabIndex = 54;
            // 
            // cbxDriverHiddenSkill_c1r3
            // 
            this.cbxDriverHiddenSkill_c1r3.FormattingEnabled = true;
            this.cbxDriverHiddenSkill_c1r3.Location = new System.Drawing.Point (112, 19);
            this.cbxDriverHiddenSkill_c1r3.Name = "cbxDriverHiddenSkill_c1r3";
            this.cbxDriverHiddenSkill_c1r3.Size = new System.Drawing.Size (121, 21);
            this.cbxDriverHiddenSkill_c1r3.TabIndex = 51;
            // 
            // nudDriverHiddenSkill_c1r1
            // 
            this.nudDriverHiddenSkill_c1r1.Location = new System.Drawing.Point (65, 59);
            this.nudDriverHiddenSkill_c1r1.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.nudDriverHiddenSkill_c1r1.Name = "nudDriverHiddenSkill_c1r1";
            this.nudDriverHiddenSkill_c1r1.Size = new System.Drawing.Size (48, 20);
            this.nudDriverHiddenSkill_c1r1.TabIndex = 46;
            // 
            // cbxDriverHiddenSkill_c1r2
            // 
            this.cbxDriverHiddenSkill_c1r2.FormattingEnabled = true;
            this.cbxDriverHiddenSkill_c1r2.Location = new System.Drawing.Point (112, 39);
            this.cbxDriverHiddenSkill_c1r2.Name = "cbxDriverHiddenSkill_c1r2";
            this.cbxDriverHiddenSkill_c1r2.Size = new System.Drawing.Size (121, 21);
            this.cbxDriverHiddenSkill_c1r2.TabIndex = 50;
            // 
            // nudDriverHiddenSkill_c1r2
            // 
            this.nudDriverHiddenSkill_c1r2.Location = new System.Drawing.Point (65, 39);
            this.nudDriverHiddenSkill_c1r2.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.nudDriverHiddenSkill_c1r2.Name = "nudDriverHiddenSkill_c1r2";
            this.nudDriverHiddenSkill_c1r2.Size = new System.Drawing.Size (48, 20);
            this.nudDriverHiddenSkill_c1r2.TabIndex = 47;
            // 
            // cbxDriverHiddenSkill_c1r1
            // 
            this.cbxDriverHiddenSkill_c1r1.FormattingEnabled = true;
            this.cbxDriverHiddenSkill_c1r1.Location = new System.Drawing.Point (112, 59);
            this.cbxDriverHiddenSkill_c1r1.Name = "cbxDriverHiddenSkill_c1r1";
            this.cbxDriverHiddenSkill_c1r1.Size = new System.Drawing.Size (121, 21);
            this.cbxDriverHiddenSkill_c1r1.TabIndex = 49;
            // 
            // nudDriverHiddenSkill_c1r3
            // 
            this.nudDriverHiddenSkill_c1r3.Location = new System.Drawing.Point (65, 19);
            this.nudDriverHiddenSkill_c1r3.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.nudDriverHiddenSkill_c1r3.Name = "nudDriverHiddenSkill_c1r3";
            this.nudDriverHiddenSkill_c1r3.Size = new System.Drawing.Size (48, 20);
            this.nudDriverHiddenSkill_c1r3.TabIndex = 48;
            // 
            // gbxDriverOvertSkills
            // 
            this.gbxDriverOvertSkills.Controls.Add (this.nudDriverOvertSkill_c5_Level);
            this.gbxDriverOvertSkills.Controls.Add (this.lblDriverOvertSkillsColumn5Level);
            this.gbxDriverOvertSkills.Controls.Add (this.nudDriverOvertSkill_c5_ColumnNo);
            this.gbxDriverOvertSkills.Controls.Add (this.lblDriverOvertSkillsColumn5No);
            this.gbxDriverOvertSkills.Controls.Add (this.nudDriverOvertSkill_c4_Level);
            this.gbxDriverOvertSkills.Controls.Add (this.lblDriverOvertSkillsColumn4Level);
            this.gbxDriverOvertSkills.Controls.Add (this.nudDriverOvertSkill_c4_ColumnNo);
            this.gbxDriverOvertSkills.Controls.Add (this.lblDriverOvertSkillsColumn4No);
            this.gbxDriverOvertSkills.Controls.Add (this.nudDriverOvertSkill_c3_Level);
            this.gbxDriverOvertSkills.Controls.Add (this.lblDriverOvertSkillsColumn3Level);
            this.gbxDriverOvertSkills.Controls.Add (this.nudDriverOvertSkill_c3_ColumnNo);
            this.gbxDriverOvertSkills.Controls.Add (this.lblDriverOvertSkillsColumn3No);
            this.gbxDriverOvertSkills.Controls.Add (this.nudDriverOvertSkill_c2_Level);
            this.gbxDriverOvertSkills.Controls.Add (this.lblDriverOvertSkillsColumn2Level);
            this.gbxDriverOvertSkills.Controls.Add (this.nudDriverOvertSkill_c2_ColumnNo);
            this.gbxDriverOvertSkills.Controls.Add (this.lblDriverOvertSkillsColumn2No);
            this.gbxDriverOvertSkills.Controls.Add (this.nudDriverOvertSkill_c1_Level);
            this.gbxDriverOvertSkills.Controls.Add (this.lblDriverOvertSkillsColumn1Level);
            this.gbxDriverOvertSkills.Controls.Add (this.nudDriverOvertSkill_c1_ColumnNo);
            this.gbxDriverOvertSkills.Controls.Add (this.lblDriverOvertSkillsColumn1No);
            this.gbxDriverOvertSkills.Controls.Add (this.lblDriverOvertSkillsRow3);
            this.gbxDriverOvertSkills.Controls.Add (this.lblDriverOvertSkillsRow2);
            this.gbxDriverOvertSkills.Controls.Add (this.lblDriverOvertSkillsRow1);
            this.gbxDriverOvertSkills.Controls.Add (this.cbxDriverOvertSkill_c5r3);
            this.gbxDriverOvertSkills.Controls.Add (this.nudDriverOvertSkill_c5r1);
            this.gbxDriverOvertSkills.Controls.Add (this.cbxDriverOvertSkill_c5r2);
            this.gbxDriverOvertSkills.Controls.Add (this.nudDriverOvertSkill_c5r2);
            this.gbxDriverOvertSkills.Controls.Add (this.cbxDriverOvertSkill_c5r1);
            this.gbxDriverOvertSkills.Controls.Add (this.nudDriverOvertSkill_c5r3);
            this.gbxDriverOvertSkills.Controls.Add (this.cbxDriverOvertSkill_c4r3);
            this.gbxDriverOvertSkills.Controls.Add (this.nudDriverOvertSkill_c4r1);
            this.gbxDriverOvertSkills.Controls.Add (this.cbxDriverOvertSkill_c4r2);
            this.gbxDriverOvertSkills.Controls.Add (this.nudDriverOvertSkill_c4r2);
            this.gbxDriverOvertSkills.Controls.Add (this.cbxDriverOvertSkill_c4r1);
            this.gbxDriverOvertSkills.Controls.Add (this.nudDriverOvertSkill_c4r3);
            this.gbxDriverOvertSkills.Controls.Add (this.cbxDriverOvertSkill_c3r3);
            this.gbxDriverOvertSkills.Controls.Add (this.nudDriverOvertSkill_c3r1);
            this.gbxDriverOvertSkills.Controls.Add (this.cbxDriverOvertSkill_c3r2);
            this.gbxDriverOvertSkills.Controls.Add (this.nudDriverOvertSkill_c3r2);
            this.gbxDriverOvertSkills.Controls.Add (this.cbxDriverOvertSkill_c3r1);
            this.gbxDriverOvertSkills.Controls.Add (this.nudDriverOvertSkill_c3r3);
            this.gbxDriverOvertSkills.Controls.Add (this.cbxDriverOvertSkill_c2r3);
            this.gbxDriverOvertSkills.Controls.Add (this.nudDriverOvertSkill_c2r1);
            this.gbxDriverOvertSkills.Controls.Add (this.cbxDriverOvertSkill_c2r2);
            this.gbxDriverOvertSkills.Controls.Add (this.nudDriverOvertSkill_c2r2);
            this.gbxDriverOvertSkills.Controls.Add (this.cbxDriverOvertSkill_c2r1);
            this.gbxDriverOvertSkills.Controls.Add (this.nudDriverOvertSkill_c2r3);
            this.gbxDriverOvertSkills.Controls.Add (this.cbxDriverOvertSkill_c1r3);
            this.gbxDriverOvertSkills.Controls.Add (this.nudDriverOvertSkill_c1r1);
            this.gbxDriverOvertSkills.Controls.Add (this.cbxDriverOvertSkill_c1r2);
            this.gbxDriverOvertSkills.Controls.Add (this.nudDriverOvertSkill_c1r2);
            this.gbxDriverOvertSkills.Controls.Add (this.cbxDriverOvertSkill_c1r1);
            this.gbxDriverOvertSkills.Controls.Add (this.nudDriverOvertSkill_c1r3);
            this.gbxDriverOvertSkills.Location = new System.Drawing.Point (17, 93);
            this.gbxDriverOvertSkills.Name = "gbxDriverOvertSkills";
            this.gbxDriverOvertSkills.Size = new System.Drawing.Size (915, 144);
            this.gbxDriverOvertSkills.TabIndex = 105;
            this.gbxDriverOvertSkills.TabStop = false;
            this.gbxDriverOvertSkills.Text = "Overt Skills";
            // 
            // nudDriverOvertSkill_c5_Level
            // 
            this.nudDriverOvertSkill_c5_Level.Location = new System.Drawing.Point (829, 112);
            this.nudDriverOvertSkill_c5_Level.Name = "nudDriverOvertSkill_c5_Level";
            this.nudDriverOvertSkill_c5_Level.Size = new System.Drawing.Size (61, 20);
            this.nudDriverOvertSkill_c5_Level.TabIndex = 98;
            // 
            // lblDriverOvertSkillsColumn5Level
            // 
            this.lblDriverOvertSkillsColumn5Level.AutoSize = true;
            this.lblDriverOvertSkillsColumn5Level.Location = new System.Drawing.Point (761, 114);
            this.lblDriverOvertSkillsColumn5Level.Name = "lblDriverOvertSkillsColumn5Level";
            this.lblDriverOvertSkillsColumn5Level.Size = new System.Drawing.Size (36, 13);
            this.lblDriverOvertSkillsColumn5Level.TabIndex = 97;
            this.lblDriverOvertSkillsColumn5Level.Text = "Level:";
            // 
            // nudDriverOvertSkill_c5_ColumnNo
            // 
            this.nudDriverOvertSkill_c5_ColumnNo.Location = new System.Drawing.Point (829, 86);
            this.nudDriverOvertSkill_c5_ColumnNo.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.nudDriverOvertSkill_c5_ColumnNo.Name = "nudDriverOvertSkill_c5_ColumnNo";
            this.nudDriverOvertSkill_c5_ColumnNo.Size = new System.Drawing.Size (61, 20);
            this.nudDriverOvertSkill_c5_ColumnNo.TabIndex = 96;
            // 
            // lblDriverOvertSkillsColumn5No
            // 
            this.lblDriverOvertSkillsColumn5No.AutoSize = true;
            this.lblDriverOvertSkillsColumn5No.Location = new System.Drawing.Point (761, 88);
            this.lblDriverOvertSkillsColumn5No.Name = "lblDriverOvertSkillsColumn5No";
            this.lblDriverOvertSkillsColumn5No.Size = new System.Drawing.Size (62, 13);
            this.lblDriverOvertSkillsColumn5No.TabIndex = 95;
            this.lblDriverOvertSkillsColumn5No.Text = "Column No:";
            // 
            // nudDriverOvertSkill_c4_Level
            // 
            this.nudDriverOvertSkill_c4_Level.Location = new System.Drawing.Point (657, 112);
            this.nudDriverOvertSkill_c4_Level.Name = "nudDriverOvertSkill_c4_Level";
            this.nudDriverOvertSkill_c4_Level.Size = new System.Drawing.Size (61, 20);
            this.nudDriverOvertSkill_c4_Level.TabIndex = 94;
            // 
            // lblDriverOvertSkillsColumn4Level
            // 
            this.lblDriverOvertSkillsColumn4Level.AutoSize = true;
            this.lblDriverOvertSkillsColumn4Level.Location = new System.Drawing.Point (589, 114);
            this.lblDriverOvertSkillsColumn4Level.Name = "lblDriverOvertSkillsColumn4Level";
            this.lblDriverOvertSkillsColumn4Level.Size = new System.Drawing.Size (36, 13);
            this.lblDriverOvertSkillsColumn4Level.TabIndex = 93;
            this.lblDriverOvertSkillsColumn4Level.Text = "Level:";
            // 
            // nudDriverOvertSkill_c4_ColumnNo
            // 
            this.nudDriverOvertSkill_c4_ColumnNo.Location = new System.Drawing.Point (657, 86);
            this.nudDriverOvertSkill_c4_ColumnNo.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.nudDriverOvertSkill_c4_ColumnNo.Name = "nudDriverOvertSkill_c4_ColumnNo";
            this.nudDriverOvertSkill_c4_ColumnNo.Size = new System.Drawing.Size (61, 20);
            this.nudDriverOvertSkill_c4_ColumnNo.TabIndex = 92;
            // 
            // lblDriverOvertSkillsColumn4No
            // 
            this.lblDriverOvertSkillsColumn4No.AutoSize = true;
            this.lblDriverOvertSkillsColumn4No.Location = new System.Drawing.Point (589, 88);
            this.lblDriverOvertSkillsColumn4No.Name = "lblDriverOvertSkillsColumn4No";
            this.lblDriverOvertSkillsColumn4No.Size = new System.Drawing.Size (62, 13);
            this.lblDriverOvertSkillsColumn4No.TabIndex = 91;
            this.lblDriverOvertSkillsColumn4No.Text = "Column No:";
            // 
            // nudDriverOvertSkill_c3_Level
            // 
            this.nudDriverOvertSkill_c3_Level.Location = new System.Drawing.Point (488, 112);
            this.nudDriverOvertSkill_c3_Level.Name = "nudDriverOvertSkill_c3_Level";
            this.nudDriverOvertSkill_c3_Level.Size = new System.Drawing.Size (61, 20);
            this.nudDriverOvertSkill_c3_Level.TabIndex = 90;
            // 
            // lblDriverOvertSkillsColumn3Level
            // 
            this.lblDriverOvertSkillsColumn3Level.AutoSize = true;
            this.lblDriverOvertSkillsColumn3Level.Location = new System.Drawing.Point (420, 114);
            this.lblDriverOvertSkillsColumn3Level.Name = "lblDriverOvertSkillsColumn3Level";
            this.lblDriverOvertSkillsColumn3Level.Size = new System.Drawing.Size (36, 13);
            this.lblDriverOvertSkillsColumn3Level.TabIndex = 89;
            this.lblDriverOvertSkillsColumn3Level.Text = "Level:";
            // 
            // nudDriverOvertSkill_c3_ColumnNo
            // 
            this.nudDriverOvertSkill_c3_ColumnNo.Location = new System.Drawing.Point (488, 86);
            this.nudDriverOvertSkill_c3_ColumnNo.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.nudDriverOvertSkill_c3_ColumnNo.Name = "nudDriverOvertSkill_c3_ColumnNo";
            this.nudDriverOvertSkill_c3_ColumnNo.Size = new System.Drawing.Size (61, 20);
            this.nudDriverOvertSkill_c3_ColumnNo.TabIndex = 88;
            // 
            // lblDriverOvertSkillsColumn3No
            // 
            this.lblDriverOvertSkillsColumn3No.AutoSize = true;
            this.lblDriverOvertSkillsColumn3No.Location = new System.Drawing.Point (420, 88);
            this.lblDriverOvertSkillsColumn3No.Name = "lblDriverOvertSkillsColumn3No";
            this.lblDriverOvertSkillsColumn3No.Size = new System.Drawing.Size (62, 13);
            this.lblDriverOvertSkillsColumn3No.TabIndex = 87;
            this.lblDriverOvertSkillsColumn3No.Text = "Column No:";
            // 
            // nudDriverOvertSkill_c2_Level
            // 
            this.nudDriverOvertSkill_c2_Level.Location = new System.Drawing.Point (321, 112);
            this.nudDriverOvertSkill_c2_Level.Name = "nudDriverOvertSkill_c2_Level";
            this.nudDriverOvertSkill_c2_Level.Size = new System.Drawing.Size (61, 20);
            this.nudDriverOvertSkill_c2_Level.TabIndex = 86;
            // 
            // lblDriverOvertSkillsColumn2Level
            // 
            this.lblDriverOvertSkillsColumn2Level.AutoSize = true;
            this.lblDriverOvertSkillsColumn2Level.Location = new System.Drawing.Point (253, 114);
            this.lblDriverOvertSkillsColumn2Level.Name = "lblDriverOvertSkillsColumn2Level";
            this.lblDriverOvertSkillsColumn2Level.Size = new System.Drawing.Size (36, 13);
            this.lblDriverOvertSkillsColumn2Level.TabIndex = 85;
            this.lblDriverOvertSkillsColumn2Level.Text = "Level:";
            // 
            // nudDriverOvertSkill_c2_ColumnNo
            // 
            this.nudDriverOvertSkill_c2_ColumnNo.Location = new System.Drawing.Point (321, 86);
            this.nudDriverOvertSkill_c2_ColumnNo.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.nudDriverOvertSkill_c2_ColumnNo.Name = "nudDriverOvertSkill_c2_ColumnNo";
            this.nudDriverOvertSkill_c2_ColumnNo.Size = new System.Drawing.Size (61, 20);
            this.nudDriverOvertSkill_c2_ColumnNo.TabIndex = 84;
            // 
            // lblDriverOvertSkillsColumn2No
            // 
            this.lblDriverOvertSkillsColumn2No.AutoSize = true;
            this.lblDriverOvertSkillsColumn2No.Location = new System.Drawing.Point (253, 88);
            this.lblDriverOvertSkillsColumn2No.Name = "lblDriverOvertSkillsColumn2No";
            this.lblDriverOvertSkillsColumn2No.Size = new System.Drawing.Size (62, 13);
            this.lblDriverOvertSkillsColumn2No.TabIndex = 83;
            this.lblDriverOvertSkillsColumn2No.Text = "Column No:";
            // 
            // nudDriverOvertSkill_c1_Level
            // 
            this.nudDriverOvertSkill_c1_Level.Location = new System.Drawing.Point (151, 112);
            this.nudDriverOvertSkill_c1_Level.Name = "nudDriverOvertSkill_c1_Level";
            this.nudDriverOvertSkill_c1_Level.Size = new System.Drawing.Size (61, 20);
            this.nudDriverOvertSkill_c1_Level.TabIndex = 82;
            // 
            // lblDriverOvertSkillsColumn1Level
            // 
            this.lblDriverOvertSkillsColumn1Level.AutoSize = true;
            this.lblDriverOvertSkillsColumn1Level.Location = new System.Drawing.Point (83, 114);
            this.lblDriverOvertSkillsColumn1Level.Name = "lblDriverOvertSkillsColumn1Level";
            this.lblDriverOvertSkillsColumn1Level.Size = new System.Drawing.Size (36, 13);
            this.lblDriverOvertSkillsColumn1Level.TabIndex = 81;
            this.lblDriverOvertSkillsColumn1Level.Text = "Level:";
            // 
            // nudDriverOvertSkill_c1_ColumnNo
            // 
            this.nudDriverOvertSkill_c1_ColumnNo.Location = new System.Drawing.Point (151, 86);
            this.nudDriverOvertSkill_c1_ColumnNo.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.nudDriverOvertSkill_c1_ColumnNo.Name = "nudDriverOvertSkill_c1_ColumnNo";
            this.nudDriverOvertSkill_c1_ColumnNo.Size = new System.Drawing.Size (61, 20);
            this.nudDriverOvertSkill_c1_ColumnNo.TabIndex = 80;
            // 
            // lblDriverOvertSkillsColumn1No
            // 
            this.lblDriverOvertSkillsColumn1No.AutoSize = true;
            this.lblDriverOvertSkillsColumn1No.Location = new System.Drawing.Point (83, 88);
            this.lblDriverOvertSkillsColumn1No.Name = "lblDriverOvertSkillsColumn1No";
            this.lblDriverOvertSkillsColumn1No.Size = new System.Drawing.Size (62, 13);
            this.lblDriverOvertSkillsColumn1No.TabIndex = 79;
            this.lblDriverOvertSkillsColumn1No.Text = "Column No:";
            // 
            // lblDriverOvertSkillsRow3
            // 
            this.lblDriverOvertSkillsRow3.AutoSize = true;
            this.lblDriverOvertSkillsRow3.Location = new System.Drawing.Point (6, 22);
            this.lblDriverOvertSkillsRow3.Name = "lblDriverOvertSkillsRow3";
            this.lblDriverOvertSkillsRow3.Size = new System.Drawing.Size (41, 13);
            this.lblDriverOvertSkillsRow3.TabIndex = 78;
            this.lblDriverOvertSkillsRow3.Text = "Row 3:";
            // 
            // lblDriverOvertSkillsRow2
            // 
            this.lblDriverOvertSkillsRow2.AutoSize = true;
            this.lblDriverOvertSkillsRow2.Location = new System.Drawing.Point (6, 42);
            this.lblDriverOvertSkillsRow2.Name = "lblDriverOvertSkillsRow2";
            this.lblDriverOvertSkillsRow2.Size = new System.Drawing.Size (41, 13);
            this.lblDriverOvertSkillsRow2.TabIndex = 77;
            this.lblDriverOvertSkillsRow2.Text = "Row 2:";
            // 
            // lblDriverOvertSkillsRow1
            // 
            this.lblDriverOvertSkillsRow1.AutoSize = true;
            this.lblDriverOvertSkillsRow1.Location = new System.Drawing.Point (6, 62);
            this.lblDriverOvertSkillsRow1.Name = "lblDriverOvertSkillsRow1";
            this.lblDriverOvertSkillsRow1.Size = new System.Drawing.Size (41, 13);
            this.lblDriverOvertSkillsRow1.TabIndex = 76;
            this.lblDriverOvertSkillsRow1.Text = "Row 1:";
            // 
            // cbxDriverOvertSkill_c5r3
            // 
            this.cbxDriverOvertSkill_c5r3.FormattingEnabled = true;
            this.cbxDriverOvertSkill_c5r3.Location = new System.Drawing.Point (788, 19);
            this.cbxDriverOvertSkill_c5r3.Name = "cbxDriverOvertSkill_c5r3";
            this.cbxDriverOvertSkill_c5r3.Size = new System.Drawing.Size (121, 21);
            this.cbxDriverOvertSkill_c5r3.TabIndex = 75;
            // 
            // nudDriverOvertSkill_c5r1
            // 
            this.nudDriverOvertSkill_c5r1.Location = new System.Drawing.Point (741, 59);
            this.nudDriverOvertSkill_c5r1.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.nudDriverOvertSkill_c5r1.Name = "nudDriverOvertSkill_c5r1";
            this.nudDriverOvertSkill_c5r1.Size = new System.Drawing.Size (48, 20);
            this.nudDriverOvertSkill_c5r1.TabIndex = 70;
            // 
            // cbxDriverOvertSkill_c5r2
            // 
            this.cbxDriverOvertSkill_c5r2.FormattingEnabled = true;
            this.cbxDriverOvertSkill_c5r2.Location = new System.Drawing.Point (788, 39);
            this.cbxDriverOvertSkill_c5r2.Name = "cbxDriverOvertSkill_c5r2";
            this.cbxDriverOvertSkill_c5r2.Size = new System.Drawing.Size (121, 21);
            this.cbxDriverOvertSkill_c5r2.TabIndex = 74;
            // 
            // nudDriverOvertSkill_c5r2
            // 
            this.nudDriverOvertSkill_c5r2.Location = new System.Drawing.Point (741, 39);
            this.nudDriverOvertSkill_c5r2.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.nudDriverOvertSkill_c5r2.Name = "nudDriverOvertSkill_c5r2";
            this.nudDriverOvertSkill_c5r2.Size = new System.Drawing.Size (48, 20);
            this.nudDriverOvertSkill_c5r2.TabIndex = 71;
            // 
            // cbxDriverOvertSkill_c5r1
            // 
            this.cbxDriverOvertSkill_c5r1.FormattingEnabled = true;
            this.cbxDriverOvertSkill_c5r1.Location = new System.Drawing.Point (788, 59);
            this.cbxDriverOvertSkill_c5r1.Name = "cbxDriverOvertSkill_c5r1";
            this.cbxDriverOvertSkill_c5r1.Size = new System.Drawing.Size (121, 21);
            this.cbxDriverOvertSkill_c5r1.TabIndex = 73;
            // 
            // nudDriverOvertSkill_c5r3
            // 
            this.nudDriverOvertSkill_c5r3.Location = new System.Drawing.Point (741, 19);
            this.nudDriverOvertSkill_c5r3.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.nudDriverOvertSkill_c5r3.Name = "nudDriverOvertSkill_c5r3";
            this.nudDriverOvertSkill_c5r3.Size = new System.Drawing.Size (48, 20);
            this.nudDriverOvertSkill_c5r3.TabIndex = 72;
            // 
            // cbxDriverOvertSkill_c4r3
            // 
            this.cbxDriverOvertSkill_c4r3.FormattingEnabled = true;
            this.cbxDriverOvertSkill_c4r3.Location = new System.Drawing.Point (619, 19);
            this.cbxDriverOvertSkill_c4r3.Name = "cbxDriverOvertSkill_c4r3";
            this.cbxDriverOvertSkill_c4r3.Size = new System.Drawing.Size (121, 21);
            this.cbxDriverOvertSkill_c4r3.TabIndex = 69;
            // 
            // nudDriverOvertSkill_c4r1
            // 
            this.nudDriverOvertSkill_c4r1.Location = new System.Drawing.Point (572, 59);
            this.nudDriverOvertSkill_c4r1.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.nudDriverOvertSkill_c4r1.Name = "nudDriverOvertSkill_c4r1";
            this.nudDriverOvertSkill_c4r1.Size = new System.Drawing.Size (48, 20);
            this.nudDriverOvertSkill_c4r1.TabIndex = 64;
            // 
            // cbxDriverOvertSkill_c4r2
            // 
            this.cbxDriverOvertSkill_c4r2.FormattingEnabled = true;
            this.cbxDriverOvertSkill_c4r2.Location = new System.Drawing.Point (619, 39);
            this.cbxDriverOvertSkill_c4r2.Name = "cbxDriverOvertSkill_c4r2";
            this.cbxDriverOvertSkill_c4r2.Size = new System.Drawing.Size (121, 21);
            this.cbxDriverOvertSkill_c4r2.TabIndex = 68;
            // 
            // nudDriverOvertSkill_c4r2
            // 
            this.nudDriverOvertSkill_c4r2.Location = new System.Drawing.Point (572, 39);
            this.nudDriverOvertSkill_c4r2.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.nudDriverOvertSkill_c4r2.Name = "nudDriverOvertSkill_c4r2";
            this.nudDriverOvertSkill_c4r2.Size = new System.Drawing.Size (48, 20);
            this.nudDriverOvertSkill_c4r2.TabIndex = 65;
            // 
            // cbxDriverOvertSkill_c4r1
            // 
            this.cbxDriverOvertSkill_c4r1.FormattingEnabled = true;
            this.cbxDriverOvertSkill_c4r1.Location = new System.Drawing.Point (619, 59);
            this.cbxDriverOvertSkill_c4r1.Name = "cbxDriverOvertSkill_c4r1";
            this.cbxDriverOvertSkill_c4r1.Size = new System.Drawing.Size (121, 21);
            this.cbxDriverOvertSkill_c4r1.TabIndex = 67;
            // 
            // nudDriverOvertSkill_c4r3
            // 
            this.nudDriverOvertSkill_c4r3.Location = new System.Drawing.Point (572, 19);
            this.nudDriverOvertSkill_c4r3.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.nudDriverOvertSkill_c4r3.Name = "nudDriverOvertSkill_c4r3";
            this.nudDriverOvertSkill_c4r3.Size = new System.Drawing.Size (48, 20);
            this.nudDriverOvertSkill_c4r3.TabIndex = 66;
            // 
            // cbxDriverOvertSkill_c3r3
            // 
            this.cbxDriverOvertSkill_c3r3.FormattingEnabled = true;
            this.cbxDriverOvertSkill_c3r3.Location = new System.Drawing.Point (450, 19);
            this.cbxDriverOvertSkill_c3r3.Name = "cbxDriverOvertSkill_c3r3";
            this.cbxDriverOvertSkill_c3r3.Size = new System.Drawing.Size (121, 21);
            this.cbxDriverOvertSkill_c3r3.TabIndex = 63;
            // 
            // nudDriverOvertSkill_c3r1
            // 
            this.nudDriverOvertSkill_c3r1.Location = new System.Drawing.Point (403, 59);
            this.nudDriverOvertSkill_c3r1.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.nudDriverOvertSkill_c3r1.Name = "nudDriverOvertSkill_c3r1";
            this.nudDriverOvertSkill_c3r1.Size = new System.Drawing.Size (48, 20);
            this.nudDriverOvertSkill_c3r1.TabIndex = 58;
            // 
            // cbxDriverOvertSkill_c3r2
            // 
            this.cbxDriverOvertSkill_c3r2.FormattingEnabled = true;
            this.cbxDriverOvertSkill_c3r2.Location = new System.Drawing.Point (450, 39);
            this.cbxDriverOvertSkill_c3r2.Name = "cbxDriverOvertSkill_c3r2";
            this.cbxDriverOvertSkill_c3r2.Size = new System.Drawing.Size (121, 21);
            this.cbxDriverOvertSkill_c3r2.TabIndex = 62;
            // 
            // nudDriverOvertSkill_c3r2
            // 
            this.nudDriverOvertSkill_c3r2.Location = new System.Drawing.Point (403, 39);
            this.nudDriverOvertSkill_c3r2.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.nudDriverOvertSkill_c3r2.Name = "nudDriverOvertSkill_c3r2";
            this.nudDriverOvertSkill_c3r2.Size = new System.Drawing.Size (48, 20);
            this.nudDriverOvertSkill_c3r2.TabIndex = 59;
            // 
            // cbxDriverOvertSkill_c3r1
            // 
            this.cbxDriverOvertSkill_c3r1.FormattingEnabled = true;
            this.cbxDriverOvertSkill_c3r1.Location = new System.Drawing.Point (450, 59);
            this.cbxDriverOvertSkill_c3r1.Name = "cbxDriverOvertSkill_c3r1";
            this.cbxDriverOvertSkill_c3r1.Size = new System.Drawing.Size (121, 21);
            this.cbxDriverOvertSkill_c3r1.TabIndex = 61;
            // 
            // nudDriverOvertSkill_c3r3
            // 
            this.nudDriverOvertSkill_c3r3.Location = new System.Drawing.Point (403, 19);
            this.nudDriverOvertSkill_c3r3.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.nudDriverOvertSkill_c3r3.Name = "nudDriverOvertSkill_c3r3";
            this.nudDriverOvertSkill_c3r3.Size = new System.Drawing.Size (48, 20);
            this.nudDriverOvertSkill_c3r3.TabIndex = 60;
            // 
            // cbxDriverOvertSkill_c2r3
            // 
            this.cbxDriverOvertSkill_c2r3.FormattingEnabled = true;
            this.cbxDriverOvertSkill_c2r3.Location = new System.Drawing.Point (281, 19);
            this.cbxDriverOvertSkill_c2r3.Name = "cbxDriverOvertSkill_c2r3";
            this.cbxDriverOvertSkill_c2r3.Size = new System.Drawing.Size (121, 21);
            this.cbxDriverOvertSkill_c2r3.TabIndex = 57;
            // 
            // nudDriverOvertSkill_c2r1
            // 
            this.nudDriverOvertSkill_c2r1.Location = new System.Drawing.Point (234, 59);
            this.nudDriverOvertSkill_c2r1.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.nudDriverOvertSkill_c2r1.Name = "nudDriverOvertSkill_c2r1";
            this.nudDriverOvertSkill_c2r1.Size = new System.Drawing.Size (48, 20);
            this.nudDriverOvertSkill_c2r1.TabIndex = 52;
            // 
            // cbxDriverOvertSkill_c2r2
            // 
            this.cbxDriverOvertSkill_c2r2.FormattingEnabled = true;
            this.cbxDriverOvertSkill_c2r2.Location = new System.Drawing.Point (281, 39);
            this.cbxDriverOvertSkill_c2r2.Name = "cbxDriverOvertSkill_c2r2";
            this.cbxDriverOvertSkill_c2r2.Size = new System.Drawing.Size (121, 21);
            this.cbxDriverOvertSkill_c2r2.TabIndex = 56;
            // 
            // nudDriverOvertSkill_c2r2
            // 
            this.nudDriverOvertSkill_c2r2.Location = new System.Drawing.Point (234, 39);
            this.nudDriverOvertSkill_c2r2.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.nudDriverOvertSkill_c2r2.Name = "nudDriverOvertSkill_c2r2";
            this.nudDriverOvertSkill_c2r2.Size = new System.Drawing.Size (48, 20);
            this.nudDriverOvertSkill_c2r2.TabIndex = 53;
            // 
            // cbxDriverOvertSkill_c2r1
            // 
            this.cbxDriverOvertSkill_c2r1.FormattingEnabled = true;
            this.cbxDriverOvertSkill_c2r1.Location = new System.Drawing.Point (281, 59);
            this.cbxDriverOvertSkill_c2r1.Name = "cbxDriverOvertSkill_c2r1";
            this.cbxDriverOvertSkill_c2r1.Size = new System.Drawing.Size (121, 21);
            this.cbxDriverOvertSkill_c2r1.TabIndex = 55;
            // 
            // nudDriverOvertSkill_c2r3
            // 
            this.nudDriverOvertSkill_c2r3.Location = new System.Drawing.Point (234, 19);
            this.nudDriverOvertSkill_c2r3.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.nudDriverOvertSkill_c2r3.Name = "nudDriverOvertSkill_c2r3";
            this.nudDriverOvertSkill_c2r3.Size = new System.Drawing.Size (48, 20);
            this.nudDriverOvertSkill_c2r3.TabIndex = 54;
            // 
            // cbxDriverOvertSkill_c1r3
            // 
            this.cbxDriverOvertSkill_c1r3.FormattingEnabled = true;
            this.cbxDriverOvertSkill_c1r3.Location = new System.Drawing.Point (112, 19);
            this.cbxDriverOvertSkill_c1r3.Name = "cbxDriverOvertSkill_c1r3";
            this.cbxDriverOvertSkill_c1r3.Size = new System.Drawing.Size (121, 21);
            this.cbxDriverOvertSkill_c1r3.TabIndex = 51;
            // 
            // nudDriverOvertSkill_c1r1
            // 
            this.nudDriverOvertSkill_c1r1.Location = new System.Drawing.Point (65, 59);
            this.nudDriverOvertSkill_c1r1.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.nudDriverOvertSkill_c1r1.Name = "nudDriverOvertSkill_c1r1";
            this.nudDriverOvertSkill_c1r1.Size = new System.Drawing.Size (48, 20);
            this.nudDriverOvertSkill_c1r1.TabIndex = 46;
            // 
            // cbxDriverOvertSkill_c1r2
            // 
            this.cbxDriverOvertSkill_c1r2.FormattingEnabled = true;
            this.cbxDriverOvertSkill_c1r2.Location = new System.Drawing.Point (112, 39);
            this.cbxDriverOvertSkill_c1r2.Name = "cbxDriverOvertSkill_c1r2";
            this.cbxDriverOvertSkill_c1r2.Size = new System.Drawing.Size (121, 21);
            this.cbxDriverOvertSkill_c1r2.TabIndex = 50;
            // 
            // nudDriverOvertSkill_c1r2
            // 
            this.nudDriverOvertSkill_c1r2.Location = new System.Drawing.Point (65, 39);
            this.nudDriverOvertSkill_c1r2.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.nudDriverOvertSkill_c1r2.Name = "nudDriverOvertSkill_c1r2";
            this.nudDriverOvertSkill_c1r2.Size = new System.Drawing.Size (48, 20);
            this.nudDriverOvertSkill_c1r2.TabIndex = 47;
            // 
            // cbxDriverOvertSkill_c1r1
            // 
            this.cbxDriverOvertSkill_c1r1.FormattingEnabled = true;
            this.cbxDriverOvertSkill_c1r1.Location = new System.Drawing.Point (112, 59);
            this.cbxDriverOvertSkill_c1r1.Name = "cbxDriverOvertSkill_c1r1";
            this.cbxDriverOvertSkill_c1r1.Size = new System.Drawing.Size (121, 21);
            this.cbxDriverOvertSkill_c1r1.TabIndex = 49;
            // 
            // nudDriverOvertSkill_c1r3
            // 
            this.nudDriverOvertSkill_c1r3.Location = new System.Drawing.Point (65, 19);
            this.nudDriverOvertSkill_c1r3.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.nudDriverOvertSkill_c1r3.Name = "nudDriverOvertSkill_c1r3";
            this.nudDriverOvertSkill_c1r3.Size = new System.Drawing.Size (48, 20);
            this.nudDriverOvertSkill_c1r3.TabIndex = 48;
            // 
            // TabCharsSkillLinks
            // 
            this.TabCharsSkillLinks.Controls.Add (this.tbcDriver150Ext);
            this.TabCharsSkillLinks.Location = new System.Drawing.Point (4, 22);
            this.TabCharsSkillLinks.Name = "TabCharsSkillLinks";
            this.TabCharsSkillLinks.Size = new System.Drawing.Size (1070, 586);
            this.TabCharsSkillLinks.TabIndex = 4;
            this.TabCharsSkillLinks.Text = "Skill Links";
            this.TabCharsSkillLinks.UseVisualStyleBackColor = true;
            // 
            // tbcDriver150Ext
            // 
            this.tbcDriver150Ext.Controls.Add (this.tabDriver150ExtUnk_0x00B4);
            this.tbcDriver150Ext.Controls.Add (this.tabDriver150ExtUnk_0x0247);
            this.tbcDriver150Ext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbcDriver150Ext.Location = new System.Drawing.Point (0, 0);
            this.tbcDriver150Ext.Name = "tbcDriver150Ext";
            this.tbcDriver150Ext.SelectedIndex = 0;
            this.tbcDriver150Ext.Size = new System.Drawing.Size (1070, 586);
            this.tbcDriver150Ext.TabIndex = 158;
            // 
            // tabDriver150ExtUnk_0x00B4
            // 
            this.tabDriver150ExtUnk_0x00B4.Controls.Add (this.hbxDriver150ExtUnk_0x00B4);
            this.tabDriver150ExtUnk_0x00B4.Location = new System.Drawing.Point (4, 22);
            this.tabDriver150ExtUnk_0x00B4.Name = "tabDriver150ExtUnk_0x00B4";
            this.tabDriver150ExtUnk_0x00B4.Padding = new System.Windows.Forms.Padding (3);
            this.tabDriver150ExtUnk_0x00B4.Size = new System.Drawing.Size (1062, 560);
            this.tabDriver150ExtUnk_0x00B4.TabIndex = 0;
            this.tabDriver150ExtUnk_0x00B4.Text = "Unk_0x00B4";
            this.tabDriver150ExtUnk_0x00B4.UseVisualStyleBackColor = true;
            // 
            // hbxDriver150ExtUnk_0x00B4
            // 
            this.hbxDriver150ExtUnk_0x00B4.ColumnInfoVisible = true;
            this.hbxDriver150ExtUnk_0x00B4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.hbxDriver150ExtUnk_0x00B4.Font = new System.Drawing.Font ("Segoe UI", 9F);
            this.hbxDriver150ExtUnk_0x00B4.LineInfoVisible = true;
            this.hbxDriver150ExtUnk_0x00B4.Location = new System.Drawing.Point (3, 3);
            this.hbxDriver150ExtUnk_0x00B4.Name = "hbxDriver150ExtUnk_0x00B4";
            this.hbxDriver150ExtUnk_0x00B4.ShadowSelectionColor = System.Drawing.Color.FromArgb (((int) (((byte) (100)))), ((int) (((byte) (60)))), ((int) (((byte) (188)))), ((int) (((byte) (255)))));
            this.hbxDriver150ExtUnk_0x00B4.Size = new System.Drawing.Size (1056, 554);
            this.hbxDriver150ExtUnk_0x00B4.StringViewVisible = true;
            this.hbxDriver150ExtUnk_0x00B4.TabIndex = 151;
            this.hbxDriver150ExtUnk_0x00B4.UseFixedBytesPerLine = true;
            this.hbxDriver150ExtUnk_0x00B4.VScrollBarVisible = true;
            // 
            // tabDriver150ExtUnk_0x0247
            // 
            this.tabDriver150ExtUnk_0x0247.Controls.Add (this.hbxDriver150ExtUnk_0x0247);
            this.tabDriver150ExtUnk_0x0247.Location = new System.Drawing.Point (4, 22);
            this.tabDriver150ExtUnk_0x0247.Name = "tabDriver150ExtUnk_0x0247";
            this.tabDriver150ExtUnk_0x0247.Padding = new System.Windows.Forms.Padding (3);
            this.tabDriver150ExtUnk_0x0247.Size = new System.Drawing.Size (1062, 560);
            this.tabDriver150ExtUnk_0x0247.TabIndex = 1;
            this.tabDriver150ExtUnk_0x0247.Text = "Unk_0x0247";
            this.tabDriver150ExtUnk_0x0247.UseVisualStyleBackColor = true;
            // 
            // hbxDriver150ExtUnk_0x0247
            // 
            this.hbxDriver150ExtUnk_0x0247.ColumnInfoVisible = true;
            this.hbxDriver150ExtUnk_0x0247.Dock = System.Windows.Forms.DockStyle.Fill;
            this.hbxDriver150ExtUnk_0x0247.Font = new System.Drawing.Font ("Segoe UI", 9F);
            this.hbxDriver150ExtUnk_0x0247.LineInfoVisible = true;
            this.hbxDriver150ExtUnk_0x0247.Location = new System.Drawing.Point (3, 3);
            this.hbxDriver150ExtUnk_0x0247.Name = "hbxDriver150ExtUnk_0x0247";
            this.hbxDriver150ExtUnk_0x0247.ShadowSelectionColor = System.Drawing.Color.FromArgb (((int) (((byte) (100)))), ((int) (((byte) (60)))), ((int) (((byte) (188)))), ((int) (((byte) (255)))));
            this.hbxDriver150ExtUnk_0x0247.Size = new System.Drawing.Size (1056, 554);
            this.hbxDriver150ExtUnk_0x0247.StringViewVisible = true;
            this.hbxDriver150ExtUnk_0x0247.TabIndex = 151;
            this.hbxDriver150ExtUnk_0x0247.UseFixedBytesPerLine = true;
            this.hbxDriver150ExtUnk_0x0247.VScrollBarVisible = true;
            // 
            // TabCharsUnknown
            // 
            this.TabCharsUnknown.AutoScroll = true;
            this.TabCharsUnknown.Controls.Add (this.GbxCharUnk_0x0f8);
            this.TabCharsUnknown.Controls.Add (this.GbxCharUnk_0x0a4);
            this.TabCharsUnknown.Controls.Add (this.GbxCharUnk_0x06c);
            this.TabCharsUnknown.Controls.Add (this.GbxCharUnk_0x02c);
            this.TabCharsUnknown.Controls.Add (this.GbxCharUnk_0x010);
            this.TabCharsUnknown.Location = new System.Drawing.Point (4, 22);
            this.TabCharsUnknown.Name = "TabCharsUnknown";
            this.TabCharsUnknown.Padding = new System.Windows.Forms.Padding (3);
            this.TabCharsUnknown.Size = new System.Drawing.Size (1070, 586);
            this.TabCharsUnknown.TabIndex = 2;
            this.TabCharsUnknown.Text = "Unknown";
            this.TabCharsUnknown.UseVisualStyleBackColor = true;
            // 
            // GbxCharUnk_0x0f8
            // 
            this.GbxCharUnk_0x0f8.Controls.Add (this.HbxCharUnk_0x0f8);
            this.GbxCharUnk_0x0f8.Location = new System.Drawing.Point (6, 265);
            this.GbxCharUnk_0x0f8.Name = "GbxCharUnk_0x0f8";
            this.GbxCharUnk_0x0f8.Size = new System.Drawing.Size (414, 127);
            this.GbxCharUnk_0x0f8.TabIndex = 154;
            this.GbxCharUnk_0x0f8.TabStop = false;
            this.GbxCharUnk_0x0f8.Text = "Unk5 (0x0f8)";
            // 
            // HbxCharUnk_0x0f8
            // 
            this.HbxCharUnk_0x0f8.ColumnInfoVisible = true;
            this.HbxCharUnk_0x0f8.Font = new System.Drawing.Font ("Segoe UI", 9F);
            this.HbxCharUnk_0x0f8.LineInfoVisible = true;
            this.HbxCharUnk_0x0f8.Location = new System.Drawing.Point (6, 19);
            this.HbxCharUnk_0x0f8.Name = "HbxCharUnk_0x0f8";
            this.HbxCharUnk_0x0f8.ShadowSelectionColor = System.Drawing.Color.FromArgb (((int) (((byte) (100)))), ((int) (((byte) (60)))), ((int) (((byte) (188)))), ((int) (((byte) (255)))));
            this.HbxCharUnk_0x0f8.Size = new System.Drawing.Size (400, 100);
            this.HbxCharUnk_0x0f8.TabIndex = 151;
            // 
            // GbxCharUnk_0x0a4
            // 
            this.GbxCharUnk_0x0a4.Controls.Add (this.HbxCharUnk_0x0a4);
            this.GbxCharUnk_0x0a4.Location = new System.Drawing.Point (6, 152);
            this.GbxCharUnk_0x0a4.Name = "GbxCharUnk_0x0a4";
            this.GbxCharUnk_0x0a4.Size = new System.Drawing.Size (414, 107);
            this.GbxCharUnk_0x0a4.TabIndex = 153;
            this.GbxCharUnk_0x0a4.TabStop = false;
            this.GbxCharUnk_0x0a4.Text = "Unk4 (0x0a4)";
            // 
            // HbxCharUnk_0x0a4
            // 
            this.HbxCharUnk_0x0a4.ColumnInfoVisible = true;
            this.HbxCharUnk_0x0a4.Font = new System.Drawing.Font ("Segoe UI", 9F);
            this.HbxCharUnk_0x0a4.LineInfoVisible = true;
            this.HbxCharUnk_0x0a4.Location = new System.Drawing.Point (6, 19);
            this.HbxCharUnk_0x0a4.Name = "HbxCharUnk_0x0a4";
            this.HbxCharUnk_0x0a4.ShadowSelectionColor = System.Drawing.Color.FromArgb (((int) (((byte) (100)))), ((int) (((byte) (60)))), ((int) (((byte) (188)))), ((int) (((byte) (255)))));
            this.HbxCharUnk_0x0a4.Size = new System.Drawing.Size (400, 80);
            this.HbxCharUnk_0x0a4.TabIndex = 151;
            // 
            // GbxCharUnk_0x06c
            // 
            this.GbxCharUnk_0x06c.Controls.Add (this.HbxCharUnk_0x06c);
            this.GbxCharUnk_0x06c.Location = new System.Drawing.Point (6, 79);
            this.GbxCharUnk_0x06c.Name = "GbxCharUnk_0x06c";
            this.GbxCharUnk_0x06c.Size = new System.Drawing.Size (414, 67);
            this.GbxCharUnk_0x06c.TabIndex = 154;
            this.GbxCharUnk_0x06c.TabStop = false;
            this.GbxCharUnk_0x06c.Text = "Unk3 (0x06c)";
            // 
            // HbxCharUnk_0x06c
            // 
            this.HbxCharUnk_0x06c.ColumnInfoVisible = true;
            this.HbxCharUnk_0x06c.Font = new System.Drawing.Font ("Segoe UI", 9F);
            this.HbxCharUnk_0x06c.LineInfoVisible = true;
            this.HbxCharUnk_0x06c.Location = new System.Drawing.Point (6, 19);
            this.HbxCharUnk_0x06c.Name = "HbxCharUnk_0x06c";
            this.HbxCharUnk_0x06c.ShadowSelectionColor = System.Drawing.Color.FromArgb (((int) (((byte) (100)))), ((int) (((byte) (60)))), ((int) (((byte) (188)))), ((int) (((byte) (255)))));
            this.HbxCharUnk_0x06c.Size = new System.Drawing.Size (400, 40);
            this.HbxCharUnk_0x06c.TabIndex = 151;
            // 
            // GbxCharUnk_0x02c
            // 
            this.GbxCharUnk_0x02c.Controls.Add (this.HbxCharUnk_0x02c);
            this.GbxCharUnk_0x02c.Location = new System.Drawing.Point (228, 6);
            this.GbxCharUnk_0x02c.Name = "GbxCharUnk_0x02c";
            this.GbxCharUnk_0x02c.Size = new System.Drawing.Size (216, 67);
            this.GbxCharUnk_0x02c.TabIndex = 153;
            this.GbxCharUnk_0x02c.TabStop = false;
            this.GbxCharUnk_0x02c.Text = "Unk2 (0x02c)";
            // 
            // HbxCharUnk_0x02c
            // 
            this.HbxCharUnk_0x02c.ColumnInfoVisible = true;
            this.HbxCharUnk_0x02c.Font = new System.Drawing.Font ("Segoe UI", 9F);
            this.HbxCharUnk_0x02c.LineInfoVisible = true;
            this.HbxCharUnk_0x02c.Location = new System.Drawing.Point (6, 19);
            this.HbxCharUnk_0x02c.Name = "HbxCharUnk_0x02c";
            this.HbxCharUnk_0x02c.ShadowSelectionColor = System.Drawing.Color.FromArgb (((int) (((byte) (100)))), ((int) (((byte) (60)))), ((int) (((byte) (188)))), ((int) (((byte) (255)))));
            this.HbxCharUnk_0x02c.Size = new System.Drawing.Size (200, 40);
            this.HbxCharUnk_0x02c.TabIndex = 151;
            // 
            // GbxCharUnk_0x010
            // 
            this.GbxCharUnk_0x010.Controls.Add (this.HbxCharUnk_0x010);
            this.GbxCharUnk_0x010.Location = new System.Drawing.Point (6, 6);
            this.GbxCharUnk_0x010.Name = "GbxCharUnk_0x010";
            this.GbxCharUnk_0x010.Size = new System.Drawing.Size (216, 67);
            this.GbxCharUnk_0x010.TabIndex = 152;
            this.GbxCharUnk_0x010.TabStop = false;
            this.GbxCharUnk_0x010.Text = "Unk1 (0x010)";
            // 
            // HbxCharUnk_0x010
            // 
            this.HbxCharUnk_0x010.ColumnInfoVisible = true;
            this.HbxCharUnk_0x010.Font = new System.Drawing.Font ("Segoe UI", 9F);
            this.HbxCharUnk_0x010.LineInfoVisible = true;
            this.HbxCharUnk_0x010.Location = new System.Drawing.Point (6, 19);
            this.HbxCharUnk_0x010.Name = "HbxCharUnk_0x010";
            this.HbxCharUnk_0x010.ShadowSelectionColor = System.Drawing.Color.FromArgb (((int) (((byte) (100)))), ((int) (((byte) (60)))), ((int) (((byte) (188)))), ((int) (((byte) (255)))));
            this.HbxCharUnk_0x010.Size = new System.Drawing.Size (200, 40);
            this.HbxCharUnk_0x010.TabIndex = 151;
            // 
            // TabItems
            // 
            this.TabItems.Controls.Add (this.splitContainer5);
            this.TabItems.Location = new System.Drawing.Point (4, 22);
            this.TabItems.Name = "TabItems";
            this.TabItems.Padding = new System.Windows.Forms.Padding (3);
            this.TabItems.Size = new System.Drawing.Size (1256, 618);
            this.TabItems.TabIndex = 3;
            this.TabItems.Text = "Items";
            this.TabItems.UseVisualStyleBackColor = true;
            // 
            // splitContainer5
            // 
            this.splitContainer5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer5.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer5.Location = new System.Drawing.Point (3, 3);
            this.splitContainer5.Name = "splitContainer5";
            this.splitContainer5.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer5.Panel1
            // 
            this.splitContainer5.Panel1.Controls.Add (this.LblItemSortTip);
            this.splitContainer5.Panel1.Controls.Add (this.GbxItemCheat);
            this.splitContainer5.Panel1.Controls.Add (this.GbxItemAddNew);
            this.splitContainer5.Panel1.Controls.Add (this.GbxItemFind);
            // 
            // splitContainer5.Panel2
            // 
            this.splitContainer5.Panel2.Controls.Add (this.TbcItems);
            this.splitContainer5.Size = new System.Drawing.Size (1250, 612);
            this.splitContainer5.SplitterDistance = 47;
            this.splitContainer5.TabIndex = 1;
            // 
            // LblItemSortTip
            // 
            this.LblItemSortTip.AutoSize = true;
            this.LblItemSortTip.Location = new System.Drawing.Point (828, 11);
            this.LblItemSortTip.Name = "LblItemSortTip";
            this.LblItemSortTip.Size = new System.Drawing.Size (399, 26);
            this.LblItemSortTip.TabIndex = 6;
            this.LblItemSortTip.Text = "Double-click ID, Name, Qty, Exists, Favourite, Serial, Equipped, Weight, Gem Slot" +
    "s,\r\nRank, Element, or Buffs column headers to sort Items by that field.";
            // 
            // GbxItemCheat
            // 
            this.GbxItemCheat.Controls.Add (this.BtnItemCheatMaxQty);
            this.GbxItemCheat.Location = new System.Drawing.Point (610, 3);
            this.GbxItemCheat.Name = "GbxItemCheat";
            this.GbxItemCheat.Size = new System.Drawing.Size (212, 40);
            this.GbxItemCheat.TabIndex = 5;
            this.GbxItemCheat.TabStop = false;
            this.GbxItemCheat.Text = "Easy-Cheat";
            // 
            // BtnItemCheatMaxQty
            // 
            this.BtnItemCheatMaxQty.Location = new System.Drawing.Point (6, 12);
            this.BtnItemCheatMaxQty.Name = "BtnItemCheatMaxQty";
            this.BtnItemCheatMaxQty.Size = new System.Drawing.Size (200, 23);
            this.BtnItemCheatMaxQty.TabIndex = 4;
            this.BtnItemCheatMaxQty.Text = "Max Out Qty for all Items in Current Box";
            this.BtnItemCheatMaxQty.UseVisualStyleBackColor = true;
            this.BtnItemCheatMaxQty.Click += new System.EventHandler (this.btnItemCheatMaxQty_Click);
            // 
            // GbxItemAddNew
            // 
            this.GbxItemAddNew.Controls.Add (this.BtnItemAddNew);
            this.GbxItemAddNew.Controls.Add (this.LblItemAddNewItemQty);
            this.GbxItemAddNew.Controls.Add (this.LblItemAddNewItemID);
            this.GbxItemAddNew.Controls.Add (this.NudItemAddNewQty);
            this.GbxItemAddNew.Controls.Add (this.CbxItemAddNewID);
            this.GbxItemAddNew.Controls.Add (this.NudItemAddNewID);
            this.GbxItemAddNew.Location = new System.Drawing.Point (239, 3);
            this.GbxItemAddNew.Name = "GbxItemAddNew";
            this.GbxItemAddNew.Size = new System.Drawing.Size (365, 40);
            this.GbxItemAddNew.TabIndex = 3;
            this.GbxItemAddNew.TabStop = false;
            this.GbxItemAddNew.Text = "Easy-Add New Item to Current Item Box";
            // 
            // BtnItemAddNew
            // 
            this.BtnItemAddNew.Location = new System.Drawing.Point (316, 12);
            this.BtnItemAddNew.Name = "BtnItemAddNew";
            this.BtnItemAddNew.Size = new System.Drawing.Size (43, 23);
            this.BtnItemAddNew.TabIndex = 2;
            this.BtnItemAddNew.Text = "Add";
            this.BtnItemAddNew.UseVisualStyleBackColor = true;
            this.BtnItemAddNew.Click += new System.EventHandler (this.btnItemAddNew_Click);
            // 
            // LblItemAddNewItemQty
            // 
            this.LblItemAddNewItemQty.AutoSize = true;
            this.LblItemAddNewItemQty.Location = new System.Drawing.Point (230, 16);
            this.LblItemAddNewItemQty.Name = "LblItemAddNewItemQty";
            this.LblItemAddNewItemQty.Size = new System.Drawing.Size (26, 13);
            this.LblItemAddNewItemQty.TabIndex = 4;
            this.LblItemAddNewItemQty.Text = "Qty:";
            // 
            // LblItemAddNewItemID
            // 
            this.LblItemAddNewItemID.AutoSize = true;
            this.LblItemAddNewItemID.Location = new System.Drawing.Point (6, 16);
            this.LblItemAddNewItemID.Name = "LblItemAddNewItemID";
            this.LblItemAddNewItemID.Size = new System.Drawing.Size (30, 13);
            this.LblItemAddNewItemID.TabIndex = 3;
            this.LblItemAddNewItemID.Text = "Item:";
            // 
            // NudItemAddNewQty
            // 
            this.NudItemAddNewQty.Location = new System.Drawing.Point (262, 13);
            this.NudItemAddNewQty.Maximum = new decimal (new int[] {
            999,
            0,
            0,
            0});
            this.NudItemAddNewQty.Minimum = new decimal (new int[] {
            1,
            0,
            0,
            0});
            this.NudItemAddNewQty.Name = "NudItemAddNewQty";
            this.NudItemAddNewQty.Size = new System.Drawing.Size (48, 20);
            this.NudItemAddNewQty.TabIndex = 2;
            this.NudItemAddNewQty.Value = new decimal (new int[] {
            1,
            0,
            0,
            0});
            // 
            // CbxItemAddNewID
            // 
            this.CbxItemAddNewID.FormattingEnabled = true;
            this.CbxItemAddNewID.Location = new System.Drawing.Point (103, 13);
            this.CbxItemAddNewID.Name = "CbxItemAddNewID";
            this.CbxItemAddNewID.Size = new System.Drawing.Size (121, 21);
            this.CbxItemAddNewID.TabIndex = 1;
            this.CbxItemAddNewID.SelectionChangeCommitted += new System.EventHandler (this.cbxItemAddNewID_SelectionChangeCommitted);
            // 
            // NudItemAddNewID
            // 
            this.NudItemAddNewID.Location = new System.Drawing.Point (42, 13);
            this.NudItemAddNewID.Maximum = new decimal (new int[] {
            65535,
            0,
            0,
            0});
            this.NudItemAddNewID.Name = "NudItemAddNewID";
            this.NudItemAddNewID.Size = new System.Drawing.Size (55, 20);
            this.NudItemAddNewID.TabIndex = 0;
            this.NudItemAddNewID.ValueChanged += new System.EventHandler (this.nudItemAddNewID_ValueChanged);
            // 
            // GbxItemFind
            // 
            this.GbxItemFind.Controls.Add (this.LblItemFindName);
            this.GbxItemFind.Controls.Add (this.BtnItemSearch);
            this.GbxItemFind.Controls.Add (this.TxtItemSearch);
            this.GbxItemFind.Location = new System.Drawing.Point (5, 3);
            this.GbxItemFind.Name = "GbxItemFind";
            this.GbxItemFind.Size = new System.Drawing.Size (228, 40);
            this.GbxItemFind.TabIndex = 2;
            this.GbxItemFind.TabStop = false;
            this.GbxItemFind.Text = "Find Existing Item in Current Item Box";
            // 
            // LblItemFindName
            // 
            this.LblItemFindName.AutoSize = true;
            this.LblItemFindName.Location = new System.Drawing.Point (6, 16);
            this.LblItemFindName.Name = "LblItemFindName";
            this.LblItemFindName.Size = new System.Drawing.Size (61, 13);
            this.LblItemFindName.TabIndex = 0;
            this.LblItemFindName.Text = "Find Name:";
            // 
            // BtnItemSearch
            // 
            this.BtnItemSearch.Location = new System.Drawing.Point (179, 12);
            this.BtnItemSearch.Name = "BtnItemSearch";
            this.BtnItemSearch.Size = new System.Drawing.Size (43, 23);
            this.BtnItemSearch.TabIndex = 1;
            this.BtnItemSearch.Text = "Find";
            this.BtnItemSearch.UseVisualStyleBackColor = true;
            this.BtnItemSearch.Click += new System.EventHandler (this.btnItemSearch_Click);
            // 
            // TxtItemSearch
            // 
            this.TxtItemSearch.Location = new System.Drawing.Point (73, 13);
            this.TxtItemSearch.Name = "TxtItemSearch";
            this.TxtItemSearch.Size = new System.Drawing.Size (100, 20);
            this.TxtItemSearch.TabIndex = 0;
            this.TxtItemSearch.KeyUp += new System.Windows.Forms.KeyEventHandler (this.txtItemSearch_KeyUp);
            // 
            // TbcItems
            // 
            this.TbcItems.Controls.Add (this.TabWeaponBox);
            this.TbcItems.Controls.Add (this.TabHeadArmourBox);
            this.TbcItems.Controls.Add (this.TabTorsoArmourBox);
            this.TbcItems.Controls.Add (this.TabArmArmourBox);
            this.TbcItems.Controls.Add (this.TabLegArmourBox);
            this.TbcItems.Controls.Add (this.TabFootArmourBox);
            this.TbcItems.Controls.Add (this.TabCrystalBox);
            this.TbcItems.Controls.Add (this.TabGemBox);
            this.TbcItems.Controls.Add (this.TabCollectBox);
            this.TbcItems.Controls.Add (this.TabMaterialBox);
            this.TbcItems.Controls.Add (this.TabKeyItemBox);
            this.TbcItems.Controls.Add (this.TabArtsManBox);
            this.TbcItems.Controls.Add (this.TabItemsSerials);
            this.TbcItems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TbcItems.Location = new System.Drawing.Point (0, 0);
            this.TbcItems.Name = "TbcItems";
            this.TbcItems.SelectedIndex = 0;
            this.TbcItems.Size = new System.Drawing.Size (1250, 561);
            this.TbcItems.TabIndex = 0;
            this.TbcItems.SelectedIndexChanged += new System.EventHandler (this.TbcItems_SelectedIndexChanged);
            // 
            // TabWeaponBox
            // 
            this.TabWeaponBox.AutoScroll = true;
            this.TabWeaponBox.Controls.Add (this.DgvWeaponBox);
            this.TabWeaponBox.Location = new System.Drawing.Point (4, 22);
            this.TabWeaponBox.Name = "TabWeaponBox";
            this.TabWeaponBox.Padding = new System.Windows.Forms.Padding (3);
            this.TabWeaponBox.Size = new System.Drawing.Size (1242, 535);
            this.TabWeaponBox.TabIndex = 0;
            this.TabWeaponBox.Text = "Weapons";
            this.TabWeaponBox.UseVisualStyleBackColor = true;
            // 
            // DgvWeaponBox
            // 
            this.DgvWeaponBox.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvWeaponBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DgvWeaponBox.Location = new System.Drawing.Point (3, 3);
            this.DgvWeaponBox.Name = "DgvWeaponBox";
            this.DgvWeaponBox.Size = new System.Drawing.Size (1236, 529);
            this.DgvWeaponBox.TabIndex = 0;
            this.DgvWeaponBox.CellContextMenuStripNeeded += new System.Windows.Forms.DataGridViewCellContextMenuStripNeededEventHandler (this.DgvItemBox_CellContextMenuStripNeeded);
            this.DgvWeaponBox.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler (this.DgvItemBox_CellValueChanged);
            this.DgvWeaponBox.ColumnHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler (this.DgvItemBox_ColumnHeaderMouseDoubleClick);
            // 
            // TabHeadArmourBox
            // 
            this.TabHeadArmourBox.Controls.Add (this.DgvHeadArmourBox);
            this.TabHeadArmourBox.Location = new System.Drawing.Point (4, 22);
            this.TabHeadArmourBox.Name = "TabHeadArmourBox";
            this.TabHeadArmourBox.Padding = new System.Windows.Forms.Padding (3);
            this.TabHeadArmourBox.Size = new System.Drawing.Size (1242, 535);
            this.TabHeadArmourBox.TabIndex = 1;
            this.TabHeadArmourBox.Text = "Head Armour";
            this.TabHeadArmourBox.UseVisualStyleBackColor = true;
            // 
            // DgvHeadArmourBox
            // 
            this.DgvHeadArmourBox.AllowUserToAddRows = false;
            this.DgvHeadArmourBox.AllowUserToDeleteRows = false;
            this.DgvHeadArmourBox.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvHeadArmourBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DgvHeadArmourBox.Location = new System.Drawing.Point (3, 3);
            this.DgvHeadArmourBox.Name = "DgvHeadArmourBox";
            this.DgvHeadArmourBox.Size = new System.Drawing.Size (1236, 529);
            this.DgvHeadArmourBox.TabIndex = 0;
            this.DgvHeadArmourBox.CellContextMenuStripNeeded += new System.Windows.Forms.DataGridViewCellContextMenuStripNeededEventHandler (this.DgvItemBox_CellContextMenuStripNeeded);
            this.DgvHeadArmourBox.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler (this.DgvItemBox_CellValueChanged);
            this.DgvHeadArmourBox.ColumnHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler (this.DgvItemBox_ColumnHeaderMouseDoubleClick);
            // 
            // TabTorsoArmourBox
            // 
            this.TabTorsoArmourBox.Controls.Add (this.DgvTorsoArmourBox);
            this.TabTorsoArmourBox.Location = new System.Drawing.Point (4, 22);
            this.TabTorsoArmourBox.Name = "TabTorsoArmourBox";
            this.TabTorsoArmourBox.Padding = new System.Windows.Forms.Padding (3);
            this.TabTorsoArmourBox.Size = new System.Drawing.Size (1242, 535);
            this.TabTorsoArmourBox.TabIndex = 2;
            this.TabTorsoArmourBox.Text = "Torso Armour";
            this.TabTorsoArmourBox.UseVisualStyleBackColor = true;
            // 
            // DgvTorsoArmourBox
            // 
            this.DgvTorsoArmourBox.AllowUserToAddRows = false;
            this.DgvTorsoArmourBox.AllowUserToDeleteRows = false;
            this.DgvTorsoArmourBox.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvTorsoArmourBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DgvTorsoArmourBox.Location = new System.Drawing.Point (3, 3);
            this.DgvTorsoArmourBox.Name = "DgvTorsoArmourBox";
            this.DgvTorsoArmourBox.Size = new System.Drawing.Size (1236, 529);
            this.DgvTorsoArmourBox.TabIndex = 0;
            this.DgvTorsoArmourBox.CellContextMenuStripNeeded += new System.Windows.Forms.DataGridViewCellContextMenuStripNeededEventHandler (this.DgvItemBox_CellContextMenuStripNeeded);
            this.DgvTorsoArmourBox.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler (this.DgvItemBox_CellValueChanged);
            this.DgvTorsoArmourBox.ColumnHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler (this.DgvItemBox_ColumnHeaderMouseDoubleClick);
            // 
            // TabArmArmourBox
            // 
            this.TabArmArmourBox.Controls.Add (this.DgvArmArmourBox);
            this.TabArmArmourBox.Location = new System.Drawing.Point (4, 22);
            this.TabArmArmourBox.Name = "TabArmArmourBox";
            this.TabArmArmourBox.Padding = new System.Windows.Forms.Padding (3);
            this.TabArmArmourBox.Size = new System.Drawing.Size (1242, 535);
            this.TabArmArmourBox.TabIndex = 3;
            this.TabArmArmourBox.Text = "Arm Armour";
            this.TabArmArmourBox.UseVisualStyleBackColor = true;
            // 
            // DgvArmArmourBox
            // 
            this.DgvArmArmourBox.AllowUserToAddRows = false;
            this.DgvArmArmourBox.AllowUserToDeleteRows = false;
            this.DgvArmArmourBox.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvArmArmourBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DgvArmArmourBox.Location = new System.Drawing.Point (3, 3);
            this.DgvArmArmourBox.Name = "DgvArmArmourBox";
            this.DgvArmArmourBox.Size = new System.Drawing.Size (1236, 529);
            this.DgvArmArmourBox.TabIndex = 0;
            this.DgvArmArmourBox.CellContextMenuStripNeeded += new System.Windows.Forms.DataGridViewCellContextMenuStripNeededEventHandler (this.DgvItemBox_CellContextMenuStripNeeded);
            this.DgvArmArmourBox.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler (this.DgvItemBox_CellValueChanged);
            this.DgvArmArmourBox.ColumnHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler (this.DgvItemBox_ColumnHeaderMouseDoubleClick);
            // 
            // TabLegArmourBox
            // 
            this.TabLegArmourBox.Controls.Add (this.DgvLegArmourBox);
            this.TabLegArmourBox.Location = new System.Drawing.Point (4, 22);
            this.TabLegArmourBox.Name = "TabLegArmourBox";
            this.TabLegArmourBox.Padding = new System.Windows.Forms.Padding (3);
            this.TabLegArmourBox.Size = new System.Drawing.Size (1242, 535);
            this.TabLegArmourBox.TabIndex = 4;
            this.TabLegArmourBox.Text = "Leg Armour";
            this.TabLegArmourBox.UseVisualStyleBackColor = true;
            // 
            // DgvLegArmourBox
            // 
            this.DgvLegArmourBox.AllowUserToAddRows = false;
            this.DgvLegArmourBox.AllowUserToDeleteRows = false;
            this.DgvLegArmourBox.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvLegArmourBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DgvLegArmourBox.Location = new System.Drawing.Point (3, 3);
            this.DgvLegArmourBox.Name = "DgvLegArmourBox";
            this.DgvLegArmourBox.Size = new System.Drawing.Size (1236, 529);
            this.DgvLegArmourBox.TabIndex = 0;
            this.DgvLegArmourBox.CellContextMenuStripNeeded += new System.Windows.Forms.DataGridViewCellContextMenuStripNeededEventHandler (this.DgvItemBox_CellContextMenuStripNeeded);
            this.DgvLegArmourBox.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler (this.DgvItemBox_CellValueChanged);
            this.DgvLegArmourBox.ColumnHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler (this.DgvItemBox_ColumnHeaderMouseDoubleClick);
            // 
            // TabFootArmourBox
            // 
            this.TabFootArmourBox.Controls.Add (this.DgvFootArmourBox);
            this.TabFootArmourBox.Location = new System.Drawing.Point (4, 22);
            this.TabFootArmourBox.Name = "TabFootArmourBox";
            this.TabFootArmourBox.Padding = new System.Windows.Forms.Padding (3);
            this.TabFootArmourBox.Size = new System.Drawing.Size (1242, 535);
            this.TabFootArmourBox.TabIndex = 5;
            this.TabFootArmourBox.Text = "Foot Armour";
            this.TabFootArmourBox.UseVisualStyleBackColor = true;
            // 
            // DgvFootArmourBox
            // 
            this.DgvFootArmourBox.AllowUserToAddRows = false;
            this.DgvFootArmourBox.AllowUserToDeleteRows = false;
            this.DgvFootArmourBox.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvFootArmourBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DgvFootArmourBox.Location = new System.Drawing.Point (3, 3);
            this.DgvFootArmourBox.Name = "DgvFootArmourBox";
            this.DgvFootArmourBox.Size = new System.Drawing.Size (1236, 529);
            this.DgvFootArmourBox.TabIndex = 0;
            this.DgvFootArmourBox.CellContextMenuStripNeeded += new System.Windows.Forms.DataGridViewCellContextMenuStripNeededEventHandler (this.DgvItemBox_CellContextMenuStripNeeded);
            this.DgvFootArmourBox.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler (this.DgvItemBox_CellValueChanged);
            this.DgvFootArmourBox.ColumnHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler (this.DgvItemBox_ColumnHeaderMouseDoubleClick);
            // 
            // TabCrystalBox
            // 
            this.TabCrystalBox.Controls.Add (this.DgvCrystalBox);
            this.TabCrystalBox.Location = new System.Drawing.Point (4, 22);
            this.TabCrystalBox.Name = "TabCrystalBox";
            this.TabCrystalBox.Padding = new System.Windows.Forms.Padding (3);
            this.TabCrystalBox.Size = new System.Drawing.Size (1242, 535);
            this.TabCrystalBox.TabIndex = 6;
            this.TabCrystalBox.Text = "Crystals";
            this.TabCrystalBox.UseVisualStyleBackColor = true;
            // 
            // DgvCrystalBox
            // 
            this.DgvCrystalBox.AllowUserToAddRows = false;
            this.DgvCrystalBox.AllowUserToDeleteRows = false;
            this.DgvCrystalBox.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvCrystalBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DgvCrystalBox.Location = new System.Drawing.Point (3, 3);
            this.DgvCrystalBox.Name = "DgvCrystalBox";
            this.DgvCrystalBox.Size = new System.Drawing.Size (1236, 529);
            this.DgvCrystalBox.TabIndex = 0;
            this.DgvCrystalBox.CellContextMenuStripNeeded += new System.Windows.Forms.DataGridViewCellContextMenuStripNeededEventHandler (this.DgvItemBox_CellContextMenuStripNeeded);
            this.DgvCrystalBox.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler (this.DgvItemBox_CellValueChanged);
            // 
            // TabGemBox
            // 
            this.TabGemBox.Controls.Add (this.DgvGemBox);
            this.TabGemBox.Location = new System.Drawing.Point (4, 22);
            this.TabGemBox.Name = "TabGemBox";
            this.TabGemBox.Padding = new System.Windows.Forms.Padding (3);
            this.TabGemBox.Size = new System.Drawing.Size (1242, 535);
            this.TabGemBox.TabIndex = 7;
            this.TabGemBox.Text = "Gems";
            this.TabGemBox.UseVisualStyleBackColor = true;
            // 
            // DgvGemBox
            // 
            this.DgvGemBox.AllowUserToAddRows = false;
            this.DgvGemBox.AllowUserToDeleteRows = false;
            this.DgvGemBox.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvGemBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DgvGemBox.Location = new System.Drawing.Point (3, 3);
            this.DgvGemBox.Name = "DgvGemBox";
            this.DgvGemBox.Size = new System.Drawing.Size (1236, 529);
            this.DgvGemBox.TabIndex = 0;
            this.DgvGemBox.CellContextMenuStripNeeded += new System.Windows.Forms.DataGridViewCellContextMenuStripNeededEventHandler (this.DgvItemBox_CellContextMenuStripNeeded);
            this.DgvGemBox.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler (this.DgvItemBox_CellValueChanged);
            this.DgvGemBox.ColumnHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler (this.DgvItemBox_ColumnHeaderMouseDoubleClick);
            // 
            // TabCollectBox
            // 
            this.TabCollectBox.Controls.Add (this.DgvCollectBox);
            this.TabCollectBox.Location = new System.Drawing.Point (4, 22);
            this.TabCollectBox.Name = "TabCollectBox";
            this.TabCollectBox.Padding = new System.Windows.Forms.Padding (3);
            this.TabCollectBox.Size = new System.Drawing.Size (1242, 535);
            this.TabCollectBox.TabIndex = 8;
            this.TabCollectBox.Text = "Collectables";
            this.TabCollectBox.UseVisualStyleBackColor = true;
            // 
            // DgvCollectBox
            // 
            this.DgvCollectBox.AllowUserToAddRows = false;
            this.DgvCollectBox.AllowUserToDeleteRows = false;
            this.DgvCollectBox.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvCollectBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DgvCollectBox.Location = new System.Drawing.Point (3, 3);
            this.DgvCollectBox.Name = "DgvCollectBox";
            this.DgvCollectBox.Size = new System.Drawing.Size (1236, 529);
            this.DgvCollectBox.TabIndex = 0;
            this.DgvCollectBox.CellContextMenuStripNeeded += new System.Windows.Forms.DataGridViewCellContextMenuStripNeededEventHandler (this.DgvItemBox_CellContextMenuStripNeeded);
            this.DgvCollectBox.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler (this.DgvItemBox_CellValueChanged);
            this.DgvCollectBox.ColumnHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler (this.DgvItemBox_ColumnHeaderMouseDoubleClick);
            // 
            // TabMaterialBox
            // 
            this.TabMaterialBox.Controls.Add (this.DgvMaterialBox);
            this.TabMaterialBox.Location = new System.Drawing.Point (4, 22);
            this.TabMaterialBox.Name = "TabMaterialBox";
            this.TabMaterialBox.Padding = new System.Windows.Forms.Padding (3);
            this.TabMaterialBox.Size = new System.Drawing.Size (1242, 535);
            this.TabMaterialBox.TabIndex = 9;
            this.TabMaterialBox.Text = "Materials";
            this.TabMaterialBox.UseVisualStyleBackColor = true;
            // 
            // DgvMaterialBox
            // 
            this.DgvMaterialBox.AllowUserToAddRows = false;
            this.DgvMaterialBox.AllowUserToDeleteRows = false;
            this.DgvMaterialBox.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvMaterialBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DgvMaterialBox.Location = new System.Drawing.Point (3, 3);
            this.DgvMaterialBox.Name = "DgvMaterialBox";
            this.DgvMaterialBox.Size = new System.Drawing.Size (1236, 529);
            this.DgvMaterialBox.TabIndex = 0;
            this.DgvMaterialBox.CellContextMenuStripNeeded += new System.Windows.Forms.DataGridViewCellContextMenuStripNeededEventHandler (this.DgvItemBox_CellContextMenuStripNeeded);
            this.DgvMaterialBox.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler (this.DgvItemBox_CellValueChanged);
            this.DgvMaterialBox.ColumnHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler (this.DgvItemBox_ColumnHeaderMouseDoubleClick);
            // 
            // TabKeyItemBox
            // 
            this.TabKeyItemBox.AutoScroll = true;
            this.TabKeyItemBox.Controls.Add (this.DgvKeyItemBox);
            this.TabKeyItemBox.Location = new System.Drawing.Point (4, 22);
            this.TabKeyItemBox.Name = "TabKeyItemBox";
            this.TabKeyItemBox.Padding = new System.Windows.Forms.Padding (3);
            this.TabKeyItemBox.Size = new System.Drawing.Size (1242, 535);
            this.TabKeyItemBox.TabIndex = 10;
            this.TabKeyItemBox.Text = "Key Items";
            this.TabKeyItemBox.UseVisualStyleBackColor = true;
            // 
            // DgvKeyItemBox
            // 
            this.DgvKeyItemBox.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvKeyItemBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DgvKeyItemBox.Location = new System.Drawing.Point (3, 3);
            this.DgvKeyItemBox.Name = "DgvKeyItemBox";
            this.DgvKeyItemBox.Size = new System.Drawing.Size (1236, 529);
            this.DgvKeyItemBox.TabIndex = 0;
            this.DgvKeyItemBox.CellContextMenuStripNeeded += new System.Windows.Forms.DataGridViewCellContextMenuStripNeededEventHandler (this.DgvItemBox_CellContextMenuStripNeeded);
            this.DgvKeyItemBox.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler (this.DgvItemBox_CellValueChanged);
            this.DgvKeyItemBox.ColumnHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler (this.DgvItemBox_ColumnHeaderMouseDoubleClick);
            // 
            // TabArtsManBox
            // 
            this.TabArtsManBox.Controls.Add (this.DgvArtsManBox);
            this.TabArtsManBox.Location = new System.Drawing.Point (4, 22);
            this.TabArtsManBox.Name = "TabArtsManBox";
            this.TabArtsManBox.Padding = new System.Windows.Forms.Padding (3);
            this.TabArtsManBox.Size = new System.Drawing.Size (1242, 535);
            this.TabArtsManBox.TabIndex = 11;
            this.TabArtsManBox.Text = "Arts Manuals";
            this.TabArtsManBox.UseVisualStyleBackColor = true;
            // 
            // DgvArtsManBox
            // 
            this.DgvArtsManBox.AllowUserToAddRows = false;
            this.DgvArtsManBox.AllowUserToDeleteRows = false;
            this.DgvArtsManBox.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvArtsManBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DgvArtsManBox.Location = new System.Drawing.Point (3, 3);
            this.DgvArtsManBox.Name = "DgvArtsManBox";
            this.DgvArtsManBox.Size = new System.Drawing.Size (1236, 529);
            this.DgvArtsManBox.TabIndex = 0;
            this.DgvArtsManBox.CellContextMenuStripNeeded += new System.Windows.Forms.DataGridViewCellContextMenuStripNeededEventHandler (this.DgvItemBox_CellContextMenuStripNeeded);
            this.DgvArtsManBox.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler (this.DgvItemBox_CellValueChanged);
            this.DgvArtsManBox.ColumnHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler (this.DgvItemBox_ColumnHeaderMouseDoubleClick);
            // 
            // TabItemsSerials
            // 
            this.TabItemsSerials.Controls.Add (this.gbxItemBoxSerials);
            this.TabItemsSerials.Location = new System.Drawing.Point (4, 22);
            this.TabItemsSerials.Name = "TabItemsSerials";
            this.TabItemsSerials.Size = new System.Drawing.Size (1242, 535);
            this.TabItemsSerials.TabIndex = 18;
            this.TabItemsSerials.Text = "Serials";
            this.TabItemsSerials.UseVisualStyleBackColor = true;
            // 
            // gbxItemBoxSerials
            // 
            this.gbxItemBoxSerials.Controls.Add (this.NudItemsSerial1);
            this.gbxItemBoxSerials.Controls.Add (this.LblItemsSerial1);
            this.gbxItemBoxSerials.Controls.Add (this.LblItemsSerial2);
            this.gbxItemBoxSerials.Controls.Add (this.NudItemsSerial2);
            this.gbxItemBoxSerials.Controls.Add (this.LblItemsSerial3);
            this.gbxItemBoxSerials.Controls.Add (this.NudItemsSerial3);
            this.gbxItemBoxSerials.Controls.Add (this.LblItemsSerial4);
            this.gbxItemBoxSerials.Controls.Add (this.NudItemsSerial4);
            this.gbxItemBoxSerials.Controls.Add (this.LblItemsSerial5);
            this.gbxItemBoxSerials.Controls.Add (this.NudItemsSerial5);
            this.gbxItemBoxSerials.Controls.Add (this.LblItemsSerial6);
            this.gbxItemBoxSerials.Controls.Add (this.NudItemsSerial6);
            this.gbxItemBoxSerials.Controls.Add (this.LblItemsSerial7);
            this.gbxItemBoxSerials.Controls.Add (this.NudItemsSerial13);
            this.gbxItemBoxSerials.Controls.Add (this.NudItemsSerial7);
            this.gbxItemBoxSerials.Controls.Add (this.LblItemsSerial13);
            this.gbxItemBoxSerials.Controls.Add (this.LblItemsSerial8);
            this.gbxItemBoxSerials.Controls.Add (this.NudItemsSerial12);
            this.gbxItemBoxSerials.Controls.Add (this.NudItemsSerial8);
            this.gbxItemBoxSerials.Controls.Add (this.LblItemsSerial12);
            this.gbxItemBoxSerials.Controls.Add (this.LblItemsSerial9);
            this.gbxItemBoxSerials.Controls.Add (this.NudItemsSerial11);
            this.gbxItemBoxSerials.Controls.Add (this.NudItemsSerial9);
            this.gbxItemBoxSerials.Controls.Add (this.LblItemsSerial11);
            this.gbxItemBoxSerials.Controls.Add (this.LblItemsSerial10);
            this.gbxItemBoxSerials.Controls.Add (this.NudItemsSerial10);
            this.gbxItemBoxSerials.Location = new System.Drawing.Point (12, 15);
            this.gbxItemBoxSerials.Name = "gbxItemBoxSerials";
            this.gbxItemBoxSerials.Size = new System.Drawing.Size (557, 148);
            this.gbxItemBoxSerials.TabIndex = 38;
            this.gbxItemBoxSerials.TabStop = false;
            this.gbxItemBoxSerials.Text = "Item Box Serials";
            // 
            // NudItemsSerial1
            // 
            this.NudItemsSerial1.Location = new System.Drawing.Point (85, 14);
            this.NudItemsSerial1.Maximum = new decimal (new int[] {
            -1,
            0,
            0,
            0});
            this.NudItemsSerial1.Name = "NudItemsSerial1";
            this.NudItemsSerial1.Size = new System.Drawing.Size (100, 20);
            this.NudItemsSerial1.TabIndex = 1;
            this.NudItemsSerial1.ValueChanged += new System.EventHandler (this.nudItemsSerial1_ValueChanged);
            // 
            // LblItemsSerial1
            // 
            this.LblItemsSerial1.AutoSize = true;
            this.LblItemsSerial1.Location = new System.Drawing.Point (6, 16);
            this.LblItemsSerial1.Name = "LblItemsSerial1";
            this.LblItemsSerial1.Size = new System.Drawing.Size (56, 13);
            this.LblItemsSerial1.TabIndex = 0;
            this.LblItemsSerial1.Text = "Weapons:";
            // 
            // LblItemsSerial2
            // 
            this.LblItemsSerial2.AutoSize = true;
            this.LblItemsSerial2.Location = new System.Drawing.Point (6, 42);
            this.LblItemsSerial2.Name = "LblItemsSerial2";
            this.LblItemsSerial2.Size = new System.Drawing.Size (37, 13);
            this.LblItemsSerial2.TabIndex = 2;
            this.LblItemsSerial2.Text = "Gems:";
            // 
            // NudItemsSerial2
            // 
            this.NudItemsSerial2.Location = new System.Drawing.Point (85, 40);
            this.NudItemsSerial2.Maximum = new decimal (new int[] {
            -1,
            0,
            0,
            0});
            this.NudItemsSerial2.Name = "NudItemsSerial2";
            this.NudItemsSerial2.Size = new System.Drawing.Size (100, 20);
            this.NudItemsSerial2.TabIndex = 3;
            this.NudItemsSerial2.ValueChanged += new System.EventHandler (this.nudItemsSerial2_ValueChanged);
            // 
            // LblItemsSerial3
            // 
            this.LblItemsSerial3.AutoSize = true;
            this.LblItemsSerial3.Location = new System.Drawing.Point (6, 68);
            this.LblItemsSerial3.Name = "LblItemsSerial3";
            this.LblItemsSerial3.Size = new System.Drawing.Size (72, 13);
            this.LblItemsSerial3.TabIndex = 4;
            this.LblItemsSerial3.Text = "Head Armour:";
            // 
            // NudItemsSerial3
            // 
            this.NudItemsSerial3.Location = new System.Drawing.Point (85, 66);
            this.NudItemsSerial3.Maximum = new decimal (new int[] {
            -1,
            0,
            0,
            0});
            this.NudItemsSerial3.Name = "NudItemsSerial3";
            this.NudItemsSerial3.Size = new System.Drawing.Size (100, 20);
            this.NudItemsSerial3.TabIndex = 5;
            this.NudItemsSerial3.ValueChanged += new System.EventHandler (this.nudItemsSerial3_ValueChanged);
            // 
            // LblItemsSerial4
            // 
            this.LblItemsSerial4.AutoSize = true;
            this.LblItemsSerial4.Location = new System.Drawing.Point (6, 94);
            this.LblItemsSerial4.Name = "LblItemsSerial4";
            this.LblItemsSerial4.Size = new System.Drawing.Size (73, 13);
            this.LblItemsSerial4.TabIndex = 6;
            this.LblItemsSerial4.Text = "Torso Armour:";
            // 
            // NudItemsSerial4
            // 
            this.NudItemsSerial4.Location = new System.Drawing.Point (85, 92);
            this.NudItemsSerial4.Maximum = new decimal (new int[] {
            -1,
            0,
            0,
            0});
            this.NudItemsSerial4.Name = "NudItemsSerial4";
            this.NudItemsSerial4.Size = new System.Drawing.Size (100, 20);
            this.NudItemsSerial4.TabIndex = 7;
            this.NudItemsSerial4.ValueChanged += new System.EventHandler (this.nudItemsSerial4_ValueChanged);
            // 
            // LblItemsSerial5
            // 
            this.LblItemsSerial5.AutoSize = true;
            this.LblItemsSerial5.Location = new System.Drawing.Point (6, 120);
            this.LblItemsSerial5.Name = "LblItemsSerial5";
            this.LblItemsSerial5.Size = new System.Drawing.Size (64, 13);
            this.LblItemsSerial5.TabIndex = 8;
            this.LblItemsSerial5.Text = "Arm Armour:";
            // 
            // NudItemsSerial5
            // 
            this.NudItemsSerial5.Location = new System.Drawing.Point (85, 118);
            this.NudItemsSerial5.Maximum = new decimal (new int[] {
            -1,
            0,
            0,
            0});
            this.NudItemsSerial5.Name = "NudItemsSerial5";
            this.NudItemsSerial5.Size = new System.Drawing.Size (100, 20);
            this.NudItemsSerial5.TabIndex = 9;
            this.NudItemsSerial5.ValueChanged += new System.EventHandler (this.nudItemsSerial5_ValueChanged);
            // 
            // LblItemsSerial6
            // 
            this.LblItemsSerial6.AutoSize = true;
            this.LblItemsSerial6.Location = new System.Drawing.Point (191, 16);
            this.LblItemsSerial6.Name = "LblItemsSerial6";
            this.LblItemsSerial6.Size = new System.Drawing.Size (64, 13);
            this.LblItemsSerial6.TabIndex = 10;
            this.LblItemsSerial6.Text = "Leg Armour:";
            // 
            // NudItemsSerial6
            // 
            this.NudItemsSerial6.Location = new System.Drawing.Point (264, 14);
            this.NudItemsSerial6.Maximum = new decimal (new int[] {
            -1,
            0,
            0,
            0});
            this.NudItemsSerial6.Name = "NudItemsSerial6";
            this.NudItemsSerial6.Size = new System.Drawing.Size (100, 20);
            this.NudItemsSerial6.TabIndex = 11;
            this.NudItemsSerial6.ValueChanged += new System.EventHandler (this.nudItemsSerial6_ValueChanged);
            // 
            // LblItemsSerial7
            // 
            this.LblItemsSerial7.AutoSize = true;
            this.LblItemsSerial7.Location = new System.Drawing.Point (191, 42);
            this.LblItemsSerial7.Name = "LblItemsSerial7";
            this.LblItemsSerial7.Size = new System.Drawing.Size (67, 13);
            this.LblItemsSerial7.TabIndex = 12;
            this.LblItemsSerial7.Text = "Foot Armour:";
            // 
            // NudItemsSerial13
            // 
            this.NudItemsSerial13.Location = new System.Drawing.Point (447, 66);
            this.NudItemsSerial13.Maximum = new decimal (new int[] {
            -1,
            0,
            0,
            0});
            this.NudItemsSerial13.Name = "NudItemsSerial13";
            this.NudItemsSerial13.Size = new System.Drawing.Size (100, 20);
            this.NudItemsSerial13.TabIndex = 25;
            this.NudItemsSerial13.ValueChanged += new System.EventHandler (this.nudItemsSerial13_ValueChanged);
            // 
            // NudItemsSerial7
            // 
            this.NudItemsSerial7.Location = new System.Drawing.Point (264, 40);
            this.NudItemsSerial7.Maximum = new decimal (new int[] {
            -1,
            0,
            0,
            0});
            this.NudItemsSerial7.Name = "NudItemsSerial7";
            this.NudItemsSerial7.Size = new System.Drawing.Size (100, 20);
            this.NudItemsSerial7.TabIndex = 13;
            this.NudItemsSerial7.ValueChanged += new System.EventHandler (this.nudItemsSerial7_ValueChanged);
            // 
            // LblItemsSerial13
            // 
            this.LblItemsSerial13.AutoSize = true;
            this.LblItemsSerial13.Location = new System.Drawing.Point (370, 68);
            this.LblItemsSerial13.Name = "LblItemsSerial13";
            this.LblItemsSerial13.Size = new System.Drawing.Size (56, 13);
            this.LblItemsSerial13.TabIndex = 24;
            this.LblItemsSerial13.Text = "Unknown:";
            // 
            // LblItemsSerial8
            // 
            this.LblItemsSerial8.AutoSize = true;
            this.LblItemsSerial8.Location = new System.Drawing.Point (191, 68);
            this.LblItemsSerial8.Name = "LblItemsSerial8";
            this.LblItemsSerial8.Size = new System.Drawing.Size (46, 13);
            this.LblItemsSerial8.TabIndex = 14;
            this.LblItemsSerial8.Text = "Crystals:";
            // 
            // NudItemsSerial12
            // 
            this.NudItemsSerial12.Location = new System.Drawing.Point (447, 40);
            this.NudItemsSerial12.Maximum = new decimal (new int[] {
            -1,
            0,
            0,
            0});
            this.NudItemsSerial12.Name = "NudItemsSerial12";
            this.NudItemsSerial12.Size = new System.Drawing.Size (100, 20);
            this.NudItemsSerial12.TabIndex = 23;
            this.NudItemsSerial12.ValueChanged += new System.EventHandler (this.nudItemsSerial12_ValueChanged);
            // 
            // NudItemsSerial8
            // 
            this.NudItemsSerial8.Location = new System.Drawing.Point (264, 66);
            this.NudItemsSerial8.Maximum = new decimal (new int[] {
            -1,
            0,
            0,
            0});
            this.NudItemsSerial8.Name = "NudItemsSerial8";
            this.NudItemsSerial8.Size = new System.Drawing.Size (100, 20);
            this.NudItemsSerial8.TabIndex = 15;
            this.NudItemsSerial8.ValueChanged += new System.EventHandler (this.nudItemsSerial8_ValueChanged);
            // 
            // LblItemsSerial12
            // 
            this.LblItemsSerial12.AutoSize = true;
            this.LblItemsSerial12.Location = new System.Drawing.Point (370, 42);
            this.LblItemsSerial12.Name = "LblItemsSerial12";
            this.LblItemsSerial12.Size = new System.Drawing.Size (71, 13);
            this.LblItemsSerial12.TabIndex = 22;
            this.LblItemsSerial12.Text = "Arts Manuals:";
            // 
            // LblItemsSerial9
            // 
            this.LblItemsSerial9.AutoSize = true;
            this.LblItemsSerial9.Location = new System.Drawing.Point (191, 94);
            this.LblItemsSerial9.Name = "LblItemsSerial9";
            this.LblItemsSerial9.Size = new System.Drawing.Size (67, 13);
            this.LblItemsSerial9.TabIndex = 16;
            this.LblItemsSerial9.Text = "Collectables:";
            // 
            // NudItemsSerial11
            // 
            this.NudItemsSerial11.Location = new System.Drawing.Point (447, 14);
            this.NudItemsSerial11.Maximum = new decimal (new int[] {
            -1,
            0,
            0,
            0});
            this.NudItemsSerial11.Name = "NudItemsSerial11";
            this.NudItemsSerial11.Size = new System.Drawing.Size (100, 20);
            this.NudItemsSerial11.TabIndex = 21;
            this.NudItemsSerial11.ValueChanged += new System.EventHandler (this.nudItemsSerial11_ValueChanged);
            // 
            // NudItemsSerial9
            // 
            this.NudItemsSerial9.Location = new System.Drawing.Point (264, 92);
            this.NudItemsSerial9.Maximum = new decimal (new int[] {
            -1,
            0,
            0,
            0});
            this.NudItemsSerial9.Name = "NudItemsSerial9";
            this.NudItemsSerial9.Size = new System.Drawing.Size (100, 20);
            this.NudItemsSerial9.TabIndex = 17;
            this.NudItemsSerial9.ValueChanged += new System.EventHandler (this.nudItemsSerial9_ValueChanged);
            // 
            // LblItemsSerial11
            // 
            this.LblItemsSerial11.AutoSize = true;
            this.LblItemsSerial11.Location = new System.Drawing.Point (370, 16);
            this.LblItemsSerial11.Name = "LblItemsSerial11";
            this.LblItemsSerial11.Size = new System.Drawing.Size (56, 13);
            this.LblItemsSerial11.TabIndex = 20;
            this.LblItemsSerial11.Text = "Key Items:";
            // 
            // LblItemsSerial10
            // 
            this.LblItemsSerial10.AutoSize = true;
            this.LblItemsSerial10.Location = new System.Drawing.Point (191, 120);
            this.LblItemsSerial10.Name = "LblItemsSerial10";
            this.LblItemsSerial10.Size = new System.Drawing.Size (52, 13);
            this.LblItemsSerial10.TabIndex = 18;
            this.LblItemsSerial10.Text = "Materials:";
            // 
            // NudItemsSerial10
            // 
            this.NudItemsSerial10.Location = new System.Drawing.Point (264, 118);
            this.NudItemsSerial10.Maximum = new decimal (new int[] {
            -1,
            0,
            0,
            0});
            this.NudItemsSerial10.Name = "NudItemsSerial10";
            this.NudItemsSerial10.Size = new System.Drawing.Size (100, 20);
            this.NudItemsSerial10.TabIndex = 19;
            this.NudItemsSerial10.ValueChanged += new System.EventHandler (this.nudItemsSerial10_ValueChanged);
            // 
            // TabAffChart
            // 
            this.TabAffChart.Controls.Add (this.tbcFlagData);
            this.TabAffChart.Location = new System.Drawing.Point (4, 22);
            this.TabAffChart.Name = "TabAffChart";
            this.TabAffChart.Size = new System.Drawing.Size (1256, 618);
            this.TabAffChart.TabIndex = 8;
            this.TabAffChart.Text = "Affinity Chart";
            this.TabAffChart.UseVisualStyleBackColor = true;
            // 
            // tbcFlagData
            // 
            this.tbcFlagData.Controls.Add (this.tabFlags1248);
            this.tbcFlagData.Controls.Add (this.tabFlags1632);
            this.tbcFlagData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbcFlagData.Location = new System.Drawing.Point (0, 0);
            this.tbcFlagData.Name = "tbcFlagData";
            this.tbcFlagData.SelectedIndex = 0;
            this.tbcFlagData.Size = new System.Drawing.Size (1256, 618);
            this.tbcFlagData.TabIndex = 8;
            // 
            // tabFlags1248
            // 
            this.tabFlags1248.Controls.Add (this.gbxFlags1Bit);
            this.tabFlags1248.Controls.Add (this.gbxFlags2Bit);
            this.tabFlags1248.Controls.Add (this.gbxFlags4Bit);
            this.tabFlags1248.Controls.Add (this.gbxFlags8Bit);
            this.tabFlags1248.Location = new System.Drawing.Point (4, 22);
            this.tabFlags1248.Name = "tabFlags1248";
            this.tabFlags1248.Padding = new System.Windows.Forms.Padding (3);
            this.tabFlags1248.Size = new System.Drawing.Size (1248, 592);
            this.tabFlags1248.TabIndex = 0;
            this.tabFlags1248.Text = "1-Bit 2-Bit 4-Bit 8-Bit";
            this.tabFlags1248.UseVisualStyleBackColor = true;
            // 
            // gbxFlags1Bit
            // 
            this.gbxFlags1Bit.Controls.Add (this.dgvFlags1Bit);
            this.gbxFlags1Bit.Location = new System.Drawing.Point (6, 6);
            this.gbxFlags1Bit.Name = "gbxFlags1Bit";
            this.gbxFlags1Bit.Size = new System.Drawing.Size (358, 538);
            this.gbxFlags1Bit.TabIndex = 2;
            this.gbxFlags1Bit.TabStop = false;
            this.gbxFlags1Bit.Text = "1-Bit Flags";
            // 
            // dgvFlags1Bit
            // 
            this.dgvFlags1Bit.Location = new System.Drawing.Point (0, 0);
            this.dgvFlags1Bit.Name = "dgvFlags1Bit";
            this.dgvFlags1Bit.Size = new System.Drawing.Size (240, 150);
            this.dgvFlags1Bit.TabIndex = 0;
            // 
            // gbxFlags2Bit
            // 
            this.gbxFlags2Bit.Controls.Add (this.dgvFlags2Bit);
            this.gbxFlags2Bit.Location = new System.Drawing.Point (370, 6);
            this.gbxFlags2Bit.Name = "gbxFlags2Bit";
            this.gbxFlags2Bit.Size = new System.Drawing.Size (160, 538);
            this.gbxFlags2Bit.TabIndex = 3;
            this.gbxFlags2Bit.TabStop = false;
            this.gbxFlags2Bit.Text = "2-Bit Flags";
            // 
            // dgvFlags2Bit
            // 
            this.dgvFlags2Bit.Location = new System.Drawing.Point (0, 0);
            this.dgvFlags2Bit.Name = "dgvFlags2Bit";
            this.dgvFlags2Bit.Size = new System.Drawing.Size (240, 150);
            this.dgvFlags2Bit.TabIndex = 0;
            // 
            // gbxFlags4Bit
            // 
            this.gbxFlags4Bit.Controls.Add (this.dgvFlags4Bit);
            this.gbxFlags4Bit.Location = new System.Drawing.Point (536, 6);
            this.gbxFlags4Bit.Name = "gbxFlags4Bit";
            this.gbxFlags4Bit.Size = new System.Drawing.Size (160, 538);
            this.gbxFlags4Bit.TabIndex = 4;
            this.gbxFlags4Bit.TabStop = false;
            this.gbxFlags4Bit.Text = "4-Bit Flags";
            // 
            // dgvFlags4Bit
            // 
            this.dgvFlags4Bit.Location = new System.Drawing.Point (0, 0);
            this.dgvFlags4Bit.Name = "dgvFlags4Bit";
            this.dgvFlags4Bit.Size = new System.Drawing.Size (240, 150);
            this.dgvFlags4Bit.TabIndex = 0;
            // 
            // gbxFlags8Bit
            // 
            this.gbxFlags8Bit.Controls.Add (this.dgvFlags8Bit);
            this.gbxFlags8Bit.Location = new System.Drawing.Point (702, 6);
            this.gbxFlags8Bit.Name = "gbxFlags8Bit";
            this.gbxFlags8Bit.Size = new System.Drawing.Size (253, 538);
            this.gbxFlags8Bit.TabIndex = 5;
            this.gbxFlags8Bit.TabStop = false;
            this.gbxFlags8Bit.Text = "8-Bit Flags";
            // 
            // dgvFlags8Bit
            // 
            this.dgvFlags8Bit.Location = new System.Drawing.Point (0, 0);
            this.dgvFlags8Bit.Name = "dgvFlags8Bit";
            this.dgvFlags8Bit.Size = new System.Drawing.Size (240, 150);
            this.dgvFlags8Bit.TabIndex = 0;
            // 
            // tabFlags1632
            // 
            this.tabFlags1632.Controls.Add (this.gbxFlags16Bit);
            this.tabFlags1632.Controls.Add (this.gbxFlags32Bit);
            this.tabFlags1632.Location = new System.Drawing.Point (4, 22);
            this.tabFlags1632.Name = "tabFlags1632";
            this.tabFlags1632.Padding = new System.Windows.Forms.Padding (3);
            this.tabFlags1632.Size = new System.Drawing.Size (1248, 592);
            this.tabFlags1632.TabIndex = 1;
            this.tabFlags1632.Text = "16-Bit 32-Bit";
            this.tabFlags1632.UseVisualStyleBackColor = true;
            // 
            // gbxFlags16Bit
            // 
            this.gbxFlags16Bit.Controls.Add (this.dgvFlags16Bit);
            this.gbxFlags16Bit.Location = new System.Drawing.Point (6, 6);
            this.gbxFlags16Bit.Name = "gbxFlags16Bit";
            this.gbxFlags16Bit.Size = new System.Drawing.Size (265, 538);
            this.gbxFlags16Bit.TabIndex = 6;
            this.gbxFlags16Bit.TabStop = false;
            this.gbxFlags16Bit.Text = "16-Bit Flags";
            // 
            // dgvFlags16Bit
            // 
            this.dgvFlags16Bit.Location = new System.Drawing.Point (0, 0);
            this.dgvFlags16Bit.Name = "dgvFlags16Bit";
            this.dgvFlags16Bit.Size = new System.Drawing.Size (240, 150);
            this.dgvFlags16Bit.TabIndex = 0;
            // 
            // gbxFlags32Bit
            // 
            this.gbxFlags32Bit.Controls.Add (this.dgvFlags32Bit);
            this.gbxFlags32Bit.Location = new System.Drawing.Point (274, 6);
            this.gbxFlags32Bit.Name = "gbxFlags32Bit";
            this.gbxFlags32Bit.Size = new System.Drawing.Size (377, 538);
            this.gbxFlags32Bit.TabIndex = 7;
            this.gbxFlags32Bit.TabStop = false;
            this.gbxFlags32Bit.Text = "32-Bit Flags";
            // 
            // dgvFlags32Bit
            // 
            this.dgvFlags32Bit.Location = new System.Drawing.Point (0, 0);
            this.dgvFlags32Bit.Name = "dgvFlags32Bit";
            this.dgvFlags32Bit.Size = new System.Drawing.Size (240, 150);
            this.dgvFlags32Bit.TabIndex = 0;
            // 
            // TabTimeAtk
            // 
            this.TabTimeAtk.Location = new System.Drawing.Point (4, 22);
            this.TabTimeAtk.Name = "TabTimeAtk";
            this.TabTimeAtk.Padding = new System.Windows.Forms.Padding (3);
            this.TabTimeAtk.Size = new System.Drawing.Size (1256, 618);
            this.TabTimeAtk.TabIndex = 5;
            this.TabTimeAtk.Text = "Time Attack";
            this.TabTimeAtk.UseVisualStyleBackColor = true;
            // 
            // tabUnknown
            // 
            this.tabUnknown.AutoScroll = true;
            this.tabUnknown.Controls.Add (this.TbcUnknown);
            this.tabUnknown.Location = new System.Drawing.Point (4, 22);
            this.tabUnknown.Name = "tabUnknown";
            this.tabUnknown.Padding = new System.Windows.Forms.Padding (3);
            this.tabUnknown.Size = new System.Drawing.Size (1256, 618);
            this.tabUnknown.TabIndex = 6;
            this.tabUnknown.Text = "Unknown";
            this.tabUnknown.UseVisualStyleBackColor = true;
            // 
            // TbcUnknown
            // 
            this.TbcUnknown.Controls.Add (this.TabUnkMain);
            this.TbcUnknown.Controls.Add (this.TabUnk_0x000014);
            this.TbcUnknown.Controls.Add (this.TabUnk_0x03b5b0);
            this.TbcUnknown.Controls.Add (this.TabUnk_0x04693c);
            this.TbcUnknown.Controls.Add (this.TabUnk_0x151b44);
            this.TbcUnknown.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TbcUnknown.Location = new System.Drawing.Point (3, 3);
            this.TbcUnknown.Name = "TbcUnknown";
            this.TbcUnknown.SelectedIndex = 0;
            this.TbcUnknown.Size = new System.Drawing.Size (1250, 612);
            this.TbcUnknown.TabIndex = 166;
            // 
            // TabUnkMain
            // 
            this.TabUnkMain.AutoScroll = true;
            this.TabUnkMain.Controls.Add (this.GbxUnk_0x000000);
            this.TabUnkMain.Controls.Add (this.GbxUnk_0x152331);
            this.TabUnkMain.Location = new System.Drawing.Point (4, 22);
            this.TabUnkMain.Name = "TabUnkMain";
            this.TabUnkMain.Padding = new System.Windows.Forms.Padding (3);
            this.TabUnkMain.Size = new System.Drawing.Size (1242, 586);
            this.TabUnkMain.TabIndex = 0;
            this.TabUnkMain.Text = "Main";
            this.TabUnkMain.UseVisualStyleBackColor = true;
            // 
            // GbxUnk_0x000000
            // 
            this.GbxUnk_0x000000.Controls.Add (this.HbxUnk_0x000000);
            this.GbxUnk_0x000000.Location = new System.Drawing.Point (6, 6);
            this.GbxUnk_0x000000.Name = "GbxUnk_0x000000";
            this.GbxUnk_0x000000.Size = new System.Drawing.Size (217, 67);
            this.GbxUnk_0x000000.TabIndex = 154;
            this.GbxUnk_0x000000.TabStop = false;
            this.GbxUnk_0x000000.Text = "Unk_0x000000";
            // 
            // HbxUnk_0x000000
            // 
            this.HbxUnk_0x000000.ColumnInfoVisible = true;
            this.HbxUnk_0x000000.Font = new System.Drawing.Font ("Segoe UI", 9F);
            this.HbxUnk_0x000000.LineInfoVisible = true;
            this.HbxUnk_0x000000.Location = new System.Drawing.Point (6, 19);
            this.HbxUnk_0x000000.Name = "HbxUnk_0x000000";
            this.HbxUnk_0x000000.ShadowSelectionColor = System.Drawing.Color.FromArgb (((int) (((byte) (100)))), ((int) (((byte) (60)))), ((int) (((byte) (188)))), ((int) (((byte) (255)))));
            this.HbxUnk_0x000000.Size = new System.Drawing.Size (200, 40);
            this.HbxUnk_0x000000.TabIndex = 151;
            // 
            // GbxUnk_0x152331
            // 
            this.GbxUnk_0x152331.Controls.Add (this.HbxUnk_0x152331);
            this.GbxUnk_0x152331.Location = new System.Drawing.Point (229, 6);
            this.GbxUnk_0x152331.Name = "GbxUnk_0x152331";
            this.GbxUnk_0x152331.Size = new System.Drawing.Size (409, 67);
            this.GbxUnk_0x152331.TabIndex = 159;
            this.GbxUnk_0x152331.TabStop = false;
            this.GbxUnk_0x152331.Text = "Unk_0x152331";
            // 
            // HbxUnk_0x152331
            // 
            this.HbxUnk_0x152331.ColumnInfoVisible = true;
            this.HbxUnk_0x152331.Font = new System.Drawing.Font ("Segoe UI", 9F);
            this.HbxUnk_0x152331.LineInfoVisible = true;
            this.HbxUnk_0x152331.Location = new System.Drawing.Point (6, 19);
            this.HbxUnk_0x152331.Name = "HbxUnk_0x152331";
            this.HbxUnk_0x152331.ShadowSelectionColor = System.Drawing.Color.FromArgb (((int) (((byte) (100)))), ((int) (((byte) (60)))), ((int) (((byte) (188)))), ((int) (((byte) (255)))));
            this.HbxUnk_0x152331.Size = new System.Drawing.Size (396, 41);
            this.HbxUnk_0x152331.TabIndex = 151;
            // 
            // TabUnk_0x000014
            // 
            this.TabUnk_0x000014.Controls.Add (this.HbxUnk_0x000014);
            this.TabUnk_0x000014.Location = new System.Drawing.Point (4, 22);
            this.TabUnk_0x000014.Name = "TabUnk_0x000014";
            this.TabUnk_0x000014.Padding = new System.Windows.Forms.Padding (3);
            this.TabUnk_0x000014.Size = new System.Drawing.Size (1242, 586);
            this.TabUnk_0x000014.TabIndex = 1;
            this.TabUnk_0x000014.Text = "Unk_0x000014";
            this.TabUnk_0x000014.UseVisualStyleBackColor = true;
            // 
            // HbxUnk_0x000014
            // 
            this.HbxUnk_0x000014.ColumnInfoVisible = true;
            this.HbxUnk_0x000014.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HbxUnk_0x000014.Font = new System.Drawing.Font ("Segoe UI", 9F);
            this.HbxUnk_0x000014.LineInfoVisible = true;
            this.HbxUnk_0x000014.Location = new System.Drawing.Point (3, 3);
            this.HbxUnk_0x000014.Name = "HbxUnk_0x000014";
            this.HbxUnk_0x000014.ShadowSelectionColor = System.Drawing.Color.FromArgb (((int) (((byte) (100)))), ((int) (((byte) (60)))), ((int) (((byte) (188)))), ((int) (((byte) (255)))));
            this.HbxUnk_0x000014.Size = new System.Drawing.Size (1236, 580);
            this.HbxUnk_0x000014.StringViewVisible = true;
            this.HbxUnk_0x000014.TabIndex = 151;
            this.HbxUnk_0x000014.UseFixedBytesPerLine = true;
            this.HbxUnk_0x000014.VScrollBarVisible = true;
            // 
            // TabUnk_0x03b5b0
            // 
            this.TabUnk_0x03b5b0.Controls.Add (this.HbxUnk_0x03b5b0);
            this.TabUnk_0x03b5b0.Location = new System.Drawing.Point (4, 22);
            this.TabUnk_0x03b5b0.Name = "TabUnk_0x03b5b0";
            this.TabUnk_0x03b5b0.Size = new System.Drawing.Size (1242, 586);
            this.TabUnk_0x03b5b0.TabIndex = 2;
            this.TabUnk_0x03b5b0.Text = "Unk_0x03b5b0";
            this.TabUnk_0x03b5b0.UseVisualStyleBackColor = true;
            // 
            // HbxUnk_0x03b5b0
            // 
            this.HbxUnk_0x03b5b0.ColumnInfoVisible = true;
            this.HbxUnk_0x03b5b0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HbxUnk_0x03b5b0.Font = new System.Drawing.Font ("Segoe UI", 9F);
            this.HbxUnk_0x03b5b0.LineInfoVisible = true;
            this.HbxUnk_0x03b5b0.Location = new System.Drawing.Point (0, 0);
            this.HbxUnk_0x03b5b0.Name = "HbxUnk_0x03b5b0";
            this.HbxUnk_0x03b5b0.ShadowSelectionColor = System.Drawing.Color.FromArgb (((int) (((byte) (100)))), ((int) (((byte) (60)))), ((int) (((byte) (188)))), ((int) (((byte) (255)))));
            this.HbxUnk_0x03b5b0.Size = new System.Drawing.Size (1242, 586);
            this.HbxUnk_0x03b5b0.StringViewVisible = true;
            this.HbxUnk_0x03b5b0.TabIndex = 151;
            this.HbxUnk_0x03b5b0.UseFixedBytesPerLine = true;
            this.HbxUnk_0x03b5b0.VScrollBarVisible = true;
            // 
            // TabUnk_0x04693c
            // 
            this.TabUnk_0x04693c.Controls.Add (this.HbxUnk_0x04693c);
            this.TabUnk_0x04693c.Location = new System.Drawing.Point (4, 22);
            this.TabUnk_0x04693c.Name = "TabUnk_0x04693c";
            this.TabUnk_0x04693c.Size = new System.Drawing.Size (1242, 586);
            this.TabUnk_0x04693c.TabIndex = 3;
            this.TabUnk_0x04693c.Text = "Unk_0x04693c";
            this.TabUnk_0x04693c.UseVisualStyleBackColor = true;
            // 
            // HbxUnk_0x04693c
            // 
            this.HbxUnk_0x04693c.ColumnInfoVisible = true;
            this.HbxUnk_0x04693c.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HbxUnk_0x04693c.Font = new System.Drawing.Font ("Segoe UI", 9F);
            this.HbxUnk_0x04693c.LineInfoVisible = true;
            this.HbxUnk_0x04693c.Location = new System.Drawing.Point (0, 0);
            this.HbxUnk_0x04693c.Name = "HbxUnk_0x04693c";
            this.HbxUnk_0x04693c.ShadowSelectionColor = System.Drawing.Color.FromArgb (((int) (((byte) (100)))), ((int) (((byte) (60)))), ((int) (((byte) (188)))), ((int) (((byte) (255)))));
            this.HbxUnk_0x04693c.Size = new System.Drawing.Size (1242, 586);
            this.HbxUnk_0x04693c.StringViewVisible = true;
            this.HbxUnk_0x04693c.TabIndex = 152;
            this.HbxUnk_0x04693c.UseFixedBytesPerLine = true;
            this.HbxUnk_0x04693c.VScrollBarVisible = true;
            // 
            // TabUnk_0x151b44
            // 
            this.TabUnk_0x151b44.Controls.Add (this.HbxUnk_0x151b44);
            this.TabUnk_0x151b44.Location = new System.Drawing.Point (4, 22);
            this.TabUnk_0x151b44.Name = "TabUnk_0x151b44";
            this.TabUnk_0x151b44.Size = new System.Drawing.Size (1242, 586);
            this.TabUnk_0x151b44.TabIndex = 4;
            this.TabUnk_0x151b44.Text = "Unk_0x151b44";
            this.TabUnk_0x151b44.UseVisualStyleBackColor = true;
            // 
            // HbxUnk_0x151b44
            // 
            this.HbxUnk_0x151b44.ColumnInfoVisible = true;
            this.HbxUnk_0x151b44.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HbxUnk_0x151b44.Font = new System.Drawing.Font ("Segoe UI", 9F);
            this.HbxUnk_0x151b44.LineInfoVisible = true;
            this.HbxUnk_0x151b44.Location = new System.Drawing.Point (0, 0);
            this.HbxUnk_0x151b44.Name = "HbxUnk_0x151b44";
            this.HbxUnk_0x151b44.ShadowSelectionColor = System.Drawing.Color.FromArgb (((int) (((byte) (100)))), ((int) (((byte) (60)))), ((int) (((byte) (188)))), ((int) (((byte) (255)))));
            this.HbxUnk_0x151b44.Size = new System.Drawing.Size (1242, 586);
            this.HbxUnk_0x151b44.StringViewVisible = true;
            this.HbxUnk_0x151b44.TabIndex = 153;
            this.HbxUnk_0x151b44.UseFixedBytesPerLine = true;
            this.HbxUnk_0x151b44.VScrollBarVisible = true;
            // 
            // OfdSaveFile
            // 
            this.OfdSaveFile.Filter = "Xenoblade Chronicles DE Save Files|*.sav|All FIles|*.*";
            // 
            // BtnSave
            // 
            this.BtnSave.Enabled = false;
            this.BtnSave.Location = new System.Drawing.Point (93, 27);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size (84, 23);
            this.BtnSave.TabIndex = 33;
            this.BtnSave.Text = "Save File As...";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler (this.BtnSave_Click);
            // 
            // SfdSaveFile
            // 
            this.SfdSaveFile.Filter = "Xenoblade Chronicles DE Save Files|*.sav|All FIles|*.*";
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer3.IsSplitterFixed = true;
            this.splitContainer3.Location = new System.Drawing.Point (0, 0);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add (this.BtnLoad);
            this.splitContainer3.Panel1.Controls.Add (this.BtnSave);
            this.splitContainer3.Panel1.Controls.Add (this.MenuStrip);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add (this.TbcMain);
            this.splitContainer3.Size = new System.Drawing.Size (1264, 701);
            this.splitContainer3.SplitterDistance = 53;
            this.splitContainer3.TabIndex = 34;
            // 
            // MenuStrip
            // 
            this.MenuStrip.Items.AddRange (new System.Windows.Forms.ToolStripItem[] {
            this.FileToolStripMenuItem,
            this.SettingsToolStripMenuItem,
            this.HelpToolStripMenuItem});
            this.MenuStrip.Location = new System.Drawing.Point (0, 0);
            this.MenuStrip.Name = "MenuStrip";
            this.MenuStrip.Size = new System.Drawing.Size (1264, 24);
            this.MenuStrip.TabIndex = 34;
            this.MenuStrip.Text = "Menu Strip";
            // 
            // FileToolStripMenuItem
            // 
            this.FileToolStripMenuItem.DropDownItems.AddRange (new System.Windows.Forms.ToolStripItem[] {
            this.LoadFileToolStripMenuItem,
            this.SaveFileToolStripMenuItem,
            this.ExitToolStripMenuItem});
            this.FileToolStripMenuItem.Name = "FileToolStripMenuItem";
            this.FileToolStripMenuItem.Size = new System.Drawing.Size (37, 20);
            this.FileToolStripMenuItem.Text = "File";
            // 
            // LoadFileToolStripMenuItem
            // 
            this.LoadFileToolStripMenuItem.Name = "LoadFileToolStripMenuItem";
            this.LoadFileToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys) ((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.LoadFileToolStripMenuItem.Size = new System.Drawing.Size (184, 22);
            this.LoadFileToolStripMenuItem.Text = "Load File...";
            this.LoadFileToolStripMenuItem.Click += new System.EventHandler (this.LoadFileToolStripMenuItem_Click);
            // 
            // SaveFileToolStripMenuItem
            // 
            this.SaveFileToolStripMenuItem.Enabled = false;
            this.SaveFileToolStripMenuItem.Name = "SaveFileToolStripMenuItem";
            this.SaveFileToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys) ((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.SaveFileToolStripMenuItem.Size = new System.Drawing.Size (184, 22);
            this.SaveFileToolStripMenuItem.Text = "Save File As...";
            this.SaveFileToolStripMenuItem.Click += new System.EventHandler (this.SaveFileToolStripMenuItem_Click);
            // 
            // ExitToolStripMenuItem
            // 
            this.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem";
            this.ExitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys) ((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.ExitToolStripMenuItem.Size = new System.Drawing.Size (184, 22);
            this.ExitToolStripMenuItem.Text = "Exit";
            this.ExitToolStripMenuItem.Click += new System.EventHandler (this.ExitToolStripMenuItem_Click);
            // 
            // SettingsToolStripMenuItem
            // 
            this.SettingsToolStripMenuItem.DropDownItems.AddRange (new System.Windows.Forms.ToolStripItem[] {
            this.AllowEditOfROValsToolStripMenuItem});
            this.SettingsToolStripMenuItem.Name = "SettingsToolStripMenuItem";
            this.SettingsToolStripMenuItem.Size = new System.Drawing.Size (61, 20);
            this.SettingsToolStripMenuItem.Text = "Settings";
            // 
            // AllowEditOfROValsToolStripMenuItem
            // 
            this.AllowEditOfROValsToolStripMenuItem.Name = "AllowEditOfROValsToolStripMenuItem";
            this.AllowEditOfROValsToolStripMenuItem.Size = new System.Drawing.Size (253, 22);
            this.AllowEditOfROValsToolStripMenuItem.Text = "Allow Editing of Read-Only Values";
            this.AllowEditOfROValsToolStripMenuItem.Click += new System.EventHandler (this.AllowEditOfROValsToolStripMenuItem_Click);
            // 
            // HelpToolStripMenuItem
            // 
            this.HelpToolStripMenuItem.DropDownItems.AddRange (new System.Windows.Forms.ToolStripItem[] {
            this.FaqToolStripMenuItem,
            this.AboutToolStripMenuItem});
            this.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem";
            this.HelpToolStripMenuItem.Size = new System.Drawing.Size (44, 20);
            this.HelpToolStripMenuItem.Text = "Help";
            // 
            // FaqToolStripMenuItem
            // 
            this.FaqToolStripMenuItem.Name = "FaqToolStripMenuItem";
            this.FaqToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.FaqToolStripMenuItem.Size = new System.Drawing.Size (132, 22);
            this.FaqToolStripMenuItem.Text = "FAQ";
            this.FaqToolStripMenuItem.Click += new System.EventHandler (this.fAQToolStripMenuItem_Click);
            // 
            // AboutToolStripMenuItem
            // 
            this.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem";
            this.AboutToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F12;
            this.AboutToolStripMenuItem.Size = new System.Drawing.Size (132, 22);
            this.AboutToolStripMenuItem.Text = "About";
            this.AboutToolStripMenuItem.Click += new System.EventHandler (this.aboutToolStripMenuItem_Click);
            // 
            // ContextMenuStripItems
            // 
            this.ContextMenuStripItems.Items.AddRange (new System.Windows.Forms.ToolStripItem[] {
            this.tsmiItemGetNewSerial,
            this.tsmiItemUpdateTime});
            this.ContextMenuStripItems.Name = "cmsItems";
            this.ContextMenuStripItems.Size = new System.Drawing.Size (243, 48);
            this.ContextMenuStripItems.Text = "Item Options";
            // 
            // tsmiItemGetNewSerial
            // 
            this.tsmiItemGetNewSerial.Name = "tsmiItemGetNewSerial";
            this.tsmiItemGetNewSerial.Size = new System.Drawing.Size (242, 22);
            this.tsmiItemGetNewSerial.Text = "Apply Unused Serial to this Item";
            this.tsmiItemGetNewSerial.Click += new System.EventHandler (this.tsmiItemGetNewSerial_Click);
            // 
            // tsmiItemUpdateTime
            // 
            this.tsmiItemUpdateTime.Name = "tsmiItemUpdateTime";
            this.tsmiItemUpdateTime.Size = new System.Drawing.Size (242, 22);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF (6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size (1264, 701);
            this.Controls.Add (this.splitContainer3);
            this.Icon = ((System.Drawing.Icon) (resources.GetObject ("$this.Icon")));
            this.MainMenuStrip = this.MenuStrip;
            this.MinimumSize = new System.Drawing.Size (1280, 740);
            this.Name = "FrmMain";
            this.Text = "XCDESaveEditor";
            this.Load += new System.EventHandler (this.frmMain_Load);
            this.GbxTimeData.ResumeLayout (false);
            this.GbxPlayTime.ResumeLayout (false);
            this.GbxPlayTime.PerformLayout();
            ((System.ComponentModel.ISupportInitialize) (this.NudPlayTimeSecond)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudPlayTimeMinute)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudPlayTimeHour)).EndInit();
            this.GbxInGameTime.ResumeLayout (false);
            this.GbxInGameTime.PerformLayout();
            ((System.ComponentModel.ISupportInitialize) (this.NudInGameTimeDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudInGameTimeSecond)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudInGameTimeMinute)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudInGameTimeHour)).EndInit();
            this.GbxSaveTime.ResumeLayout (false);
            this.GbxSaveTime.PerformLayout();
            ((System.ComponentModel.ISupportInitialize) (this.NudSaveTimeYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudSaveTimeUnkB)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudSaveTimeUnkA)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudSaveTimeMSecond)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudSaveTimeSecond)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudSaveTimeMinute)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudSaveTimeHour)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudSaveTimeMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudSaveTimeDay)).EndInit();
            this.TbcMain.ResumeLayout (false);
            this.TabMain.ResumeLayout (false);
            this.GbxMoney.ResumeLayout (false);
            this.GbxMoney.PerformLayout();
            ((System.ComponentModel.ISupportInitialize) (this.NudNoponstones)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudMoney)).EndInit();
            this.GbxParty.ResumeLayout (false);
            this.GbxPartyMembers.ResumeLayout (false);
            this.GbxPartyMembers.PerformLayout();
            ((System.ComponentModel.ISupportInitialize) (this.NudPartyMemberCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudPartyMemberChar)).EndInit();
            this.TabChars.ResumeLayout (false);
            this.splitContainer2.Panel1.ResumeLayout (false);
            this.splitContainer2.Panel2.ResumeLayout (false);
            ((System.ComponentModel.ISupportInitialize) (this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout (false);
            this.TbcChars.ResumeLayout (false);
            this.TabCharsMain.ResumeLayout (false);
            this.GbxCharExp.ResumeLayout (false);
            this.GbxCharExp.PerformLayout();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharExpertRsvExp)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharExp)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharExpertExp)).EndInit();
            this.GbxCharacter.ResumeLayout (false);
            this.GbxCharacter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharID)).EndInit();
            this.GbxCharEquipment.ResumeLayout (false);
            this.groupBox3.ResumeLayout (false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharFootArmourIndex)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharFootArmourType)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharFootArmourID)).EndInit();
            this.groupBox2.ResumeLayout (false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharLegArmourIndex)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharLegArmourType)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharLegArmourID)).EndInit();
            this.groupBox1.ResumeLayout (false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharArmArmourIndex)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharArmArmourType)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharArmArmourID)).EndInit();
            this.GbxCharTorsoArmor.ResumeLayout (false);
            this.GbxCharTorsoArmor.PerformLayout();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharTorsoArmourIndex)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharTorsoArmourType)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharTorsoArmourID)).EndInit();
            this.GbxCharHeadArmour.ResumeLayout (false);
            this.GbxCharHeadArmour.PerformLayout();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharHeadArmourIndex)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharHeadArmourType)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharHeadArmourID)).EndInit();
            this.GbxCharWeapon.ResumeLayout (false);
            this.GbxCharWeapon.PerformLayout();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharWeaponIndex)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharWeaponType)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharWeaponID)).EndInit();
            this.GbxCharStats.ResumeLayout (false);
            this.GbxCharStats.PerformLayout();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharExpertLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharLevel)).EndInit();
            this.TabCharsArts.ResumeLayout (false);
            this.GbxCharSetMonadoArts.ResumeLayout (false);
            this.GbxCharSetMonadoArts.PerformLayout();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharMonadoArt8ID)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharMonadoArt7ID)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharMonadoArt6ID)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharMonadoArt5ID)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharMonTalentArtID)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharMonadoArt4ID)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharMonadoArt3ID)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharMonadoArt2ID)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharMonadoArt1ID)).EndInit();
            this.GbxCharArtLevels.ResumeLayout (false);
            this.GbxCharArtLevels.PerformLayout();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharArtUnlockLvl)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharArtPoints)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharArtLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharArtID)).EndInit();
            this.GbxCharSetNormalArts.ResumeLayout (false);
            this.GbxCharSetNormalArts.PerformLayout();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharNormalArt8ID)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharNormalArt7ID)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharNormalArt6ID)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharNormalArt5ID)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharTalentArtID)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharNormalArt4ID)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharNormalArt3ID)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharNormalArt2ID)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudCharNormalArt1ID)).EndInit();
            this.TabCharsSkillTrees.ResumeLayout (false);
            this.gbxDriverSkillPoints.ResumeLayout (false);
            this.gbxDriverSkillPoints.PerformLayout();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverCurrentSkillPoints)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverTotalSkillPoints)).EndInit();
            this.gbxDriverHiddenSkills.ResumeLayout (false);
            this.gbxDriverHiddenSkills.PerformLayout();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c5_Level)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c5_ColumnNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c4_Level)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c4_ColumnNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c3_Level)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c3_ColumnNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c2_Level)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c2_ColumnNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c1_Level)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c1_ColumnNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c5r1)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c5r2)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c5r3)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c4r1)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c4r2)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c4r3)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c3r1)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c3r2)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c3r3)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c2r1)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c2r2)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c2r3)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c1r1)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c1r2)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverHiddenSkill_c1r3)).EndInit();
            this.gbxDriverOvertSkills.ResumeLayout (false);
            this.gbxDriverOvertSkills.PerformLayout();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c5_Level)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c5_ColumnNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c4_Level)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c4_ColumnNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c3_Level)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c3_ColumnNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c2_Level)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c2_ColumnNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c1_Level)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c1_ColumnNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c5r1)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c5r2)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c5r3)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c4r1)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c4r2)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c4r3)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c3r1)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c3r2)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c3r3)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c2r1)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c2r2)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c2r3)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c1r1)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c1r2)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudDriverOvertSkill_c1r3)).EndInit();
            this.TabCharsSkillLinks.ResumeLayout (false);
            this.tbcDriver150Ext.ResumeLayout (false);
            this.tabDriver150ExtUnk_0x00B4.ResumeLayout (false);
            this.tabDriver150ExtUnk_0x0247.ResumeLayout (false);
            this.TabCharsUnknown.ResumeLayout (false);
            this.GbxCharUnk_0x0f8.ResumeLayout (false);
            this.GbxCharUnk_0x0a4.ResumeLayout (false);
            this.GbxCharUnk_0x06c.ResumeLayout (false);
            this.GbxCharUnk_0x02c.ResumeLayout (false);
            this.GbxCharUnk_0x010.ResumeLayout (false);
            this.TabItems.ResumeLayout (false);
            this.splitContainer5.Panel1.ResumeLayout (false);
            this.splitContainer5.Panel1.PerformLayout();
            this.splitContainer5.Panel2.ResumeLayout (false);
            ((System.ComponentModel.ISupportInitialize) (this.splitContainer5)).EndInit();
            this.splitContainer5.ResumeLayout (false);
            this.GbxItemCheat.ResumeLayout (false);
            this.GbxItemAddNew.ResumeLayout (false);
            this.GbxItemAddNew.PerformLayout();
            ((System.ComponentModel.ISupportInitialize) (this.NudItemAddNewQty)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudItemAddNewID)).EndInit();
            this.GbxItemFind.ResumeLayout (false);
            this.GbxItemFind.PerformLayout();
            this.TbcItems.ResumeLayout (false);
            this.TabWeaponBox.ResumeLayout (false);
            ((System.ComponentModel.ISupportInitialize) (this.DgvWeaponBox)).EndInit();
            this.TabHeadArmourBox.ResumeLayout (false);
            ((System.ComponentModel.ISupportInitialize) (this.DgvHeadArmourBox)).EndInit();
            this.TabTorsoArmourBox.ResumeLayout (false);
            ((System.ComponentModel.ISupportInitialize) (this.DgvTorsoArmourBox)).EndInit();
            this.TabArmArmourBox.ResumeLayout (false);
            ((System.ComponentModel.ISupportInitialize) (this.DgvArmArmourBox)).EndInit();
            this.TabLegArmourBox.ResumeLayout (false);
            ((System.ComponentModel.ISupportInitialize) (this.DgvLegArmourBox)).EndInit();
            this.TabFootArmourBox.ResumeLayout (false);
            ((System.ComponentModel.ISupportInitialize) (this.DgvFootArmourBox)).EndInit();
            this.TabCrystalBox.ResumeLayout (false);
            ((System.ComponentModel.ISupportInitialize) (this.DgvCrystalBox)).EndInit();
            this.TabGemBox.ResumeLayout (false);
            ((System.ComponentModel.ISupportInitialize) (this.DgvGemBox)).EndInit();
            this.TabCollectBox.ResumeLayout (false);
            ((System.ComponentModel.ISupportInitialize) (this.DgvCollectBox)).EndInit();
            this.TabMaterialBox.ResumeLayout (false);
            ((System.ComponentModel.ISupportInitialize) (this.DgvMaterialBox)).EndInit();
            this.TabKeyItemBox.ResumeLayout (false);
            ((System.ComponentModel.ISupportInitialize) (this.DgvKeyItemBox)).EndInit();
            this.TabArtsManBox.ResumeLayout (false);
            ((System.ComponentModel.ISupportInitialize) (this.DgvArtsManBox)).EndInit();
            this.TabItemsSerials.ResumeLayout (false);
            this.gbxItemBoxSerials.ResumeLayout (false);
            this.gbxItemBoxSerials.PerformLayout();
            ((System.ComponentModel.ISupportInitialize) (this.NudItemsSerial1)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudItemsSerial2)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudItemsSerial3)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudItemsSerial4)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudItemsSerial5)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudItemsSerial6)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudItemsSerial13)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudItemsSerial7)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudItemsSerial12)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudItemsSerial8)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudItemsSerial11)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudItemsSerial9)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.NudItemsSerial10)).EndInit();
            this.TabAffChart.ResumeLayout (false);
            this.tbcFlagData.ResumeLayout (false);
            this.tabFlags1248.ResumeLayout (false);
            this.gbxFlags1Bit.ResumeLayout (false);
            ((System.ComponentModel.ISupportInitialize) (this.dgvFlags1Bit)).EndInit();
            this.gbxFlags2Bit.ResumeLayout (false);
            ((System.ComponentModel.ISupportInitialize) (this.dgvFlags2Bit)).EndInit();
            this.gbxFlags4Bit.ResumeLayout (false);
            ((System.ComponentModel.ISupportInitialize) (this.dgvFlags4Bit)).EndInit();
            this.gbxFlags8Bit.ResumeLayout (false);
            ((System.ComponentModel.ISupportInitialize) (this.dgvFlags8Bit)).EndInit();
            this.tabFlags1632.ResumeLayout (false);
            this.gbxFlags16Bit.ResumeLayout (false);
            ((System.ComponentModel.ISupportInitialize) (this.dgvFlags16Bit)).EndInit();
            this.gbxFlags32Bit.ResumeLayout (false);
            ((System.ComponentModel.ISupportInitialize) (this.dgvFlags32Bit)).EndInit();
            this.tabUnknown.ResumeLayout (false);
            this.TbcUnknown.ResumeLayout (false);
            this.TabUnkMain.ResumeLayout (false);
            this.GbxUnk_0x000000.ResumeLayout (false);
            this.GbxUnk_0x152331.ResumeLayout (false);
            this.TabUnk_0x000014.ResumeLayout (false);
            this.TabUnk_0x03b5b0.ResumeLayout (false);
            this.TabUnk_0x04693c.ResumeLayout (false);
            this.TabUnk_0x151b44.ResumeLayout (false);
            this.splitContainer3.Panel1.ResumeLayout (false);
            this.splitContainer3.Panel1.PerformLayout();
            this.splitContainer3.Panel2.ResumeLayout (false);
            ((System.ComponentModel.ISupportInitialize) (this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout (false);
            this.MenuStrip.ResumeLayout (false);
            this.MenuStrip.PerformLayout();
            this.ContextMenuStripItems.ResumeLayout (false);
            this.ResumeLayout (false);

        }

        #endregion

        private System.Windows.Forms.Button BtnLoad;
        private System.Windows.Forms.Label LblSaveTimeDate;
        private System.Windows.Forms.Label LblSaveTimeDateDash1;
        private System.Windows.Forms.Label LblSaveTimeUnkB;
        private System.Windows.Forms.GroupBox GbxTimeData;
        private System.Windows.Forms.Label LblSaveTimeDateDash2;
        private System.Windows.Forms.Label LblSaveTimeTime;
        private System.Windows.Forms.Label LblSaveTimeUnkA;
        private System.Windows.Forms.Label LblMoney;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TabControl TbcMain;
        private System.Windows.Forms.TabPage TabMain;
        private System.Windows.Forms.OpenFileDialog OfdSaveFile;
        private System.Windows.Forms.TabPage TabItems;
        private System.Windows.Forms.GroupBox GbxParty;
        private System.Windows.Forms.GroupBox GbxPartyMembers;
        private System.Windows.Forms.ComboBox CbxPartyMembers;
        private System.Windows.Forms.ComboBox CbxPartyMemberChar;
        private System.Windows.Forms.Label LblPartyMembersChar;
        private System.Windows.Forms.TabControl TbcItems;
        private System.Windows.Forms.TabPage TabWeaponBox;
        private System.Windows.Forms.TabPage TabHeadArmourBox;
        private System.Windows.Forms.DataGridView DgvHeadArmourBox;
        private System.Windows.Forms.TabPage TabTorsoArmourBox;
        private System.Windows.Forms.TabPage TabArmArmourBox;
        private System.Windows.Forms.TabPage TabLegArmourBox;
        private System.Windows.Forms.TabPage TabFootArmourBox;
        private System.Windows.Forms.TabPage TabCrystalBox;
        private System.Windows.Forms.TabPage TabGemBox;
        private System.Windows.Forms.TabPage TabCollectBox;
        private System.Windows.Forms.TabPage TabMaterialBox;
        private System.Windows.Forms.TabPage TabKeyItemBox;
        private System.Windows.Forms.TabPage TabArtsManBox;
        private System.Windows.Forms.DataGridView DgvTorsoArmourBox;
        private System.Windows.Forms.DataGridView DgvArmArmourBox;
        private System.Windows.Forms.DataGridView DgvLegArmourBox;
        private System.Windows.Forms.DataGridView DgvFootArmourBox;
        private System.Windows.Forms.DataGridView DgvCrystalBox;
        private System.Windows.Forms.DataGridView DgvGemBox;
        private System.Windows.Forms.DataGridView DgvCollectBox;
        private System.Windows.Forms.DataGridView DgvMaterialBox;
        private System.Windows.Forms.DataGridView DgvKeyItemBox;
        private System.Windows.Forms.DataGridView DgvArtsManBox;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.SaveFileDialog SfdSaveFile;
        private System.Windows.Forms.NumericUpDown NudMoney;
        private System.Windows.Forms.TabPage TabChars;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.ListBox LbxChars;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.DataGridView DgvWeaponBox;
        private System.Windows.Forms.TabPage TabItemsSerials;
        private System.Windows.Forms.NumericUpDown NudItemsSerial13;
        private System.Windows.Forms.Label LblItemsSerial13;
        private System.Windows.Forms.NumericUpDown NudItemsSerial12;
        private System.Windows.Forms.Label LblItemsSerial12;
        private System.Windows.Forms.NumericUpDown NudItemsSerial11;
        private System.Windows.Forms.Label LblItemsSerial11;
        private System.Windows.Forms.NumericUpDown NudItemsSerial10;
        private System.Windows.Forms.Label LblItemsSerial10;
        private System.Windows.Forms.NumericUpDown NudItemsSerial9;
        private System.Windows.Forms.Label LblItemsSerial9;
        private System.Windows.Forms.NumericUpDown NudItemsSerial8;
        private System.Windows.Forms.Label LblItemsSerial8;
        private System.Windows.Forms.NumericUpDown NudItemsSerial7;
        private System.Windows.Forms.Label LblItemsSerial7;
        private System.Windows.Forms.NumericUpDown NudItemsSerial6;
        private System.Windows.Forms.Label LblItemsSerial6;
        private System.Windows.Forms.NumericUpDown NudItemsSerial5;
        private System.Windows.Forms.Label LblItemsSerial5;
        private System.Windows.Forms.NumericUpDown NudItemsSerial4;
        private System.Windows.Forms.Label LblItemsSerial4;
        private System.Windows.Forms.NumericUpDown NudItemsSerial3;
        private System.Windows.Forms.Label LblItemsSerial3;
        private System.Windows.Forms.NumericUpDown NudItemsSerial2;
        private System.Windows.Forms.Label LblItemsSerial2;
        private System.Windows.Forms.NumericUpDown NudItemsSerial1;
        private System.Windows.Forms.Label LblItemsSerial1;
        private System.Windows.Forms.GroupBox gbxItemBoxSerials;
        private System.Windows.Forms.Label LblPartyMembersMember;
        private System.Windows.Forms.NumericUpDown NudPartyMemberChar;
        private System.Windows.Forms.TabPage TabTimeAtk;
        private System.Windows.Forms.TabPage tabUnknown;
        private System.Windows.Forms.GroupBox GbxUnk_0x000000;
        private Be.Windows.Forms.HexBox HbxUnk_0x000000;
        private System.Windows.Forms.GroupBox GbxUnk_0x152331;
        private Be.Windows.Forms.HexBox HbxUnk_0x152331;
        private Be.Windows.Forms.HexBox HbxUnk_0x000014;
        private Be.Windows.Forms.HexBox HbxUnk_0x03b5b0;
        private System.Windows.Forms.TabControl TbcUnknown;
        private System.Windows.Forms.TabPage TabUnkMain;
        private System.Windows.Forms.TabPage TabUnk_0x000014;
        private System.Windows.Forms.TabPage TabUnk_0x03b5b0;
        private System.Windows.Forms.TabPage TabAffChart;
        private System.Windows.Forms.NumericUpDown NudSaveTimeDay;
        private System.Windows.Forms.NumericUpDown NudSaveTimeMonth;
        private System.Windows.Forms.NumericUpDown NudSaveTimeYear;
        private System.Windows.Forms.NumericUpDown NudSaveTimeSecond;
        private System.Windows.Forms.NumericUpDown NudSaveTimeMinute;
        private System.Windows.Forms.NumericUpDown NudSaveTimeHour;
        private System.Windows.Forms.Label LblSaveTimeTimeColon2;
        private System.Windows.Forms.Label LblSaveTimeTimeColon1;
        private System.Windows.Forms.NumericUpDown NudSaveTimeMSecond;
        private System.Windows.Forms.Label LblSaveTimeTimeDecimal;
        private System.Windows.Forms.NumericUpDown NudSaveTimeUnkB;
        private System.Windows.Forms.NumericUpDown NudSaveTimeUnkA;
        private System.Windows.Forms.GroupBox GbxSaveTime;
        private System.Windows.Forms.GroupBox GbxInGameTime;
        private System.Windows.Forms.NumericUpDown NudInGameTimeDay;
        private System.Windows.Forms.Label LblInGameTimeDay;
        private System.Windows.Forms.NumericUpDown NudInGameTimeSecond;
        private System.Windows.Forms.Label LblInGameTimeTime;
        private System.Windows.Forms.NumericUpDown NudInGameTimeMinute;
        private System.Windows.Forms.NumericUpDown NudInGameTimeHour;
        private System.Windows.Forms.Label LblInGameTimeTimeColon2;
        private System.Windows.Forms.Label LblInGameTimeTimeColon1;
        private System.Windows.Forms.GroupBox GbxPlayTime;
        private System.Windows.Forms.NumericUpDown NudPlayTimeSecond;
        private System.Windows.Forms.Label LblPlayTimeTime;
        private System.Windows.Forms.NumericUpDown NudPlayTimeMinute;
        private System.Windows.Forms.NumericUpDown NudPlayTimeHour;
        private System.Windows.Forms.Label LblPlayTimeColon2;
        private System.Windows.Forms.Label LblPlayTimeColon1;
        private System.Windows.Forms.GroupBox GbxMoney;
        private System.Windows.Forms.NumericUpDown NudNoponstones;
        private System.Windows.Forms.Label LblNoponstones;
        private System.Windows.Forms.MenuStrip MenuStrip;
        private System.Windows.Forms.ToolStripMenuItem FileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem LoadFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SaveFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem HelpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem FaqToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem AboutToolStripMenuItem;
        private System.Windows.Forms.SplitContainer splitContainer5;
        private System.Windows.Forms.Button BtnItemSearch;
        private System.Windows.Forms.TextBox TxtItemSearch;
        private System.Windows.Forms.DataGridView dgvFlags1Bit;
        private System.Windows.Forms.DataGridView dgvFlags2Bit;
        private System.Windows.Forms.GroupBox gbxFlags2Bit;
        private System.Windows.Forms.GroupBox gbxFlags1Bit;
        private System.Windows.Forms.GroupBox gbxFlags32Bit;
        private System.Windows.Forms.DataGridView dgvFlags32Bit;
        private System.Windows.Forms.GroupBox gbxFlags16Bit;
        private System.Windows.Forms.DataGridView dgvFlags16Bit;
        private System.Windows.Forms.GroupBox gbxFlags8Bit;
        private System.Windows.Forms.DataGridView dgvFlags8Bit;
        private System.Windows.Forms.GroupBox gbxFlags4Bit;
        private System.Windows.Forms.DataGridView dgvFlags4Bit;
        private System.Windows.Forms.TabControl tbcFlagData;
        private System.Windows.Forms.TabPage tabFlags1248;
        private System.Windows.Forms.TabPage tabFlags1632;
        private System.Windows.Forms.ContextMenuStrip ContextMenuStripItems;
        private System.Windows.Forms.ToolStripMenuItem tsmiItemGetNewSerial;
        private System.Windows.Forms.ToolStripMenuItem tsmiItemUpdateTime;
        private System.Windows.Forms.GroupBox GbxItemAddNew;
        private System.Windows.Forms.GroupBox GbxItemFind;
        private System.Windows.Forms.Label LblItemFindName;
        private System.Windows.Forms.Button BtnItemAddNew;
        private System.Windows.Forms.Label LblItemAddNewItemQty;
        private System.Windows.Forms.Label LblItemAddNewItemID;
        private System.Windows.Forms.NumericUpDown NudItemAddNewQty;
        private System.Windows.Forms.ComboBox CbxItemAddNewID;
        private System.Windows.Forms.NumericUpDown NudItemAddNewID;
        private System.Windows.Forms.ToolStripMenuItem ExitToolStripMenuItem;
        private System.Windows.Forms.GroupBox GbxItemCheat;
        private System.Windows.Forms.Button BtnItemCheatMaxQty;
        private System.Windows.Forms.Label LblItemSortTip;
        private System.Windows.Forms.ToolStripMenuItem SettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem AllowEditOfROValsToolStripMenuItem;
        private System.Windows.Forms.Label LblPartyMembersCount;
        private System.Windows.Forms.NumericUpDown NudPartyMemberCount;
        private System.Windows.Forms.TabControl TbcChars;
        private System.Windows.Forms.TabPage TabCharsMain;
        private System.Windows.Forms.GroupBox GbxCharExp;
        private System.Windows.Forms.NumericUpDown NudCharExpertRsvExp;
        private System.Windows.Forms.Label LblCharExpertRsvExp;
        private System.Windows.Forms.NumericUpDown NudCharExp;
        private System.Windows.Forms.Label LblCharExpertExp;
        private System.Windows.Forms.Label LblCharExp;
        private System.Windows.Forms.NumericUpDown NudCharExpertExp;
        private System.Windows.Forms.GroupBox GbxCharacter;
        private System.Windows.Forms.CheckBox ChkCharInParty;
        private System.Windows.Forms.Label LblCharID;
        private System.Windows.Forms.ComboBox CbxCharID;
        private System.Windows.Forms.NumericUpDown NudCharID;
        private System.Windows.Forms.GroupBox GbxCharEquipment;
        private System.Windows.Forms.GroupBox GbxCharWeapon;
        private System.Windows.Forms.ComboBox CbxCharWeaponType;
        private System.Windows.Forms.ComboBox CbxCharWeaponID;
        private System.Windows.Forms.Label LblCharWeaponID;
        private System.Windows.Forms.NumericUpDown NudCharWeaponIndex;
        private System.Windows.Forms.Label LblCharWeaponType;
        private System.Windows.Forms.NumericUpDown NudCharWeaponType;
        private System.Windows.Forms.Label LblCharWeaponIndex;
        private System.Windows.Forms.NumericUpDown NudCharWeaponID;
        private System.Windows.Forms.GroupBox GbxCharStats;
        private System.Windows.Forms.NumericUpDown NudCharExpertLevel;
        private System.Windows.Forms.Label LblExpertLvl;
        private System.Windows.Forms.NumericUpDown NudCharLevel;
        private System.Windows.Forms.Label LblCharLevel;
        private System.Windows.Forms.TabPage TabCharsArts;
        private System.Windows.Forms.GroupBox GbxCharArtLevels;
        private System.Windows.Forms.NumericUpDown NudCharArtLevel;
        private System.Windows.Forms.Label LblCharArtLevel;
        private System.Windows.Forms.ComboBox CbxCharArtID;
        private System.Windows.Forms.Label LblCharArtID;
        private System.Windows.Forms.NumericUpDown NudCharArtID;
        private System.Windows.Forms.GroupBox GbxCharSetNormalArts;
        private System.Windows.Forms.ComboBox CbxCharNormalArt1ID;
        private System.Windows.Forms.NumericUpDown NudCharNormalArt3ID;
        private System.Windows.Forms.NumericUpDown NudCharNormalArt2ID;
        private System.Windows.Forms.NumericUpDown NudCharNormalArt1ID;
        private System.Windows.Forms.Label LblCharNormalArt3ID;
        private System.Windows.Forms.Label LblCharNormalArt1ID;
        private System.Windows.Forms.Label LblCharNormalArt2ID;
        private System.Windows.Forms.TabPage TabCharsSkillTrees;
        private System.Windows.Forms.GroupBox gbxDriverSkillPoints;
        private System.Windows.Forms.Label lblDriverCurrentSkillPoints;
        private System.Windows.Forms.NumericUpDown nudDriverCurrentSkillPoints;
        private System.Windows.Forms.NumericUpDown nudDriverTotalSkillPoints;
        private System.Windows.Forms.Label lblDriverTotalSkillPoints;
        private System.Windows.Forms.GroupBox gbxDriverHiddenSkills;
        private System.Windows.Forms.NumericUpDown nudDriverHiddenSkill_c5_Level;
        private System.Windows.Forms.Label lblDriverHiddenSkillsColumn5Level;
        private System.Windows.Forms.NumericUpDown nudDriverHiddenSkill_c5_ColumnNo;
        private System.Windows.Forms.Label lblDriverHiddenSkillsColumn5No;
        private System.Windows.Forms.NumericUpDown nudDriverHiddenSkill_c4_Level;
        private System.Windows.Forms.Label lblDriverHiddenSkillsColumn4Level;
        private System.Windows.Forms.NumericUpDown nudDriverHiddenSkill_c4_ColumnNo;
        private System.Windows.Forms.Label lblDriverHiddenSkillsColumn4No;
        private System.Windows.Forms.NumericUpDown nudDriverHiddenSkill_c3_Level;
        private System.Windows.Forms.Label lblDriverHiddenSkillsColumn3Level;
        private System.Windows.Forms.NumericUpDown nudDriverHiddenSkill_c3_ColumnNo;
        private System.Windows.Forms.Label lblDriverHiddenSkillsColumn3No;
        private System.Windows.Forms.NumericUpDown nudDriverHiddenSkill_c2_Level;
        private System.Windows.Forms.Label lblDriverHiddenSkillsColumn2Level;
        private System.Windows.Forms.NumericUpDown nudDriverHiddenSkill_c2_ColumnNo;
        private System.Windows.Forms.Label lblDriverHiddenSkillsColumn2No;
        private System.Windows.Forms.NumericUpDown nudDriverHiddenSkill_c1_Level;
        private System.Windows.Forms.Label lblDriverHiddenSkillsColumn1Level;
        private System.Windows.Forms.NumericUpDown nudDriverHiddenSkill_c1_ColumnNo;
        private System.Windows.Forms.Label lblDriverHiddenSkillsColumn1No;
        private System.Windows.Forms.Label lblDriverHiddenSkillsRow3;
        private System.Windows.Forms.Label lblDriverHiddenSkillsRow2;
        private System.Windows.Forms.Label lblDriverHiddenSkillsRow1;
        private System.Windows.Forms.ComboBox cbxDriverHiddenSkill_c5r3;
        private System.Windows.Forms.NumericUpDown nudDriverHiddenSkill_c5r1;
        private System.Windows.Forms.ComboBox cbxDriverHiddenSkill_c5r2;
        private System.Windows.Forms.NumericUpDown nudDriverHiddenSkill_c5r2;
        private System.Windows.Forms.ComboBox cbxDriverHiddenSkill_c5r1;
        private System.Windows.Forms.NumericUpDown nudDriverHiddenSkill_c5r3;
        private System.Windows.Forms.ComboBox cbxDriverHiddenSkill_c4r3;
        private System.Windows.Forms.NumericUpDown nudDriverHiddenSkill_c4r1;
        private System.Windows.Forms.ComboBox cbxDriverHiddenSkill_c4r2;
        private System.Windows.Forms.NumericUpDown nudDriverHiddenSkill_c4r2;
        private System.Windows.Forms.ComboBox cbxDriverHiddenSkill_c4r1;
        private System.Windows.Forms.NumericUpDown nudDriverHiddenSkill_c4r3;
        private System.Windows.Forms.ComboBox cbxDriverHiddenSkill_c3r3;
        private System.Windows.Forms.NumericUpDown nudDriverHiddenSkill_c3r1;
        private System.Windows.Forms.ComboBox cbxDriverHiddenSkill_c3r2;
        private System.Windows.Forms.NumericUpDown nudDriverHiddenSkill_c3r2;
        private System.Windows.Forms.ComboBox cbxDriverHiddenSkill_c3r1;
        private System.Windows.Forms.NumericUpDown nudDriverHiddenSkill_c3r3;
        private System.Windows.Forms.ComboBox cbxDriverHiddenSkill_c2r3;
        private System.Windows.Forms.NumericUpDown nudDriverHiddenSkill_c2r1;
        private System.Windows.Forms.ComboBox cbxDriverHiddenSkill_c2r2;
        private System.Windows.Forms.NumericUpDown nudDriverHiddenSkill_c2r2;
        private System.Windows.Forms.ComboBox cbxDriverHiddenSkill_c2r1;
        private System.Windows.Forms.NumericUpDown nudDriverHiddenSkill_c2r3;
        private System.Windows.Forms.ComboBox cbxDriverHiddenSkill_c1r3;
        private System.Windows.Forms.NumericUpDown nudDriverHiddenSkill_c1r1;
        private System.Windows.Forms.ComboBox cbxDriverHiddenSkill_c1r2;
        private System.Windows.Forms.NumericUpDown nudDriverHiddenSkill_c1r2;
        private System.Windows.Forms.ComboBox cbxDriverHiddenSkill_c1r1;
        private System.Windows.Forms.NumericUpDown nudDriverHiddenSkill_c1r3;
        private System.Windows.Forms.GroupBox gbxDriverOvertSkills;
        private System.Windows.Forms.NumericUpDown nudDriverOvertSkill_c5_Level;
        private System.Windows.Forms.Label lblDriverOvertSkillsColumn5Level;
        private System.Windows.Forms.NumericUpDown nudDriverOvertSkill_c5_ColumnNo;
        private System.Windows.Forms.Label lblDriverOvertSkillsColumn5No;
        private System.Windows.Forms.NumericUpDown nudDriverOvertSkill_c4_Level;
        private System.Windows.Forms.Label lblDriverOvertSkillsColumn4Level;
        private System.Windows.Forms.NumericUpDown nudDriverOvertSkill_c4_ColumnNo;
        private System.Windows.Forms.Label lblDriverOvertSkillsColumn4No;
        private System.Windows.Forms.NumericUpDown nudDriverOvertSkill_c3_Level;
        private System.Windows.Forms.Label lblDriverOvertSkillsColumn3Level;
        private System.Windows.Forms.NumericUpDown nudDriverOvertSkill_c3_ColumnNo;
        private System.Windows.Forms.Label lblDriverOvertSkillsColumn3No;
        private System.Windows.Forms.NumericUpDown nudDriverOvertSkill_c2_Level;
        private System.Windows.Forms.Label lblDriverOvertSkillsColumn2Level;
        private System.Windows.Forms.NumericUpDown nudDriverOvertSkill_c2_ColumnNo;
        private System.Windows.Forms.Label lblDriverOvertSkillsColumn2No;
        private System.Windows.Forms.NumericUpDown nudDriverOvertSkill_c1_Level;
        private System.Windows.Forms.Label lblDriverOvertSkillsColumn1Level;
        private System.Windows.Forms.NumericUpDown nudDriverOvertSkill_c1_ColumnNo;
        private System.Windows.Forms.Label lblDriverOvertSkillsColumn1No;
        private System.Windows.Forms.Label lblDriverOvertSkillsRow3;
        private System.Windows.Forms.Label lblDriverOvertSkillsRow2;
        private System.Windows.Forms.Label lblDriverOvertSkillsRow1;
        private System.Windows.Forms.ComboBox cbxDriverOvertSkill_c5r3;
        private System.Windows.Forms.NumericUpDown nudDriverOvertSkill_c5r1;
        private System.Windows.Forms.ComboBox cbxDriverOvertSkill_c5r2;
        private System.Windows.Forms.NumericUpDown nudDriverOvertSkill_c5r2;
        private System.Windows.Forms.ComboBox cbxDriverOvertSkill_c5r1;
        private System.Windows.Forms.NumericUpDown nudDriverOvertSkill_c5r3;
        private System.Windows.Forms.ComboBox cbxDriverOvertSkill_c4r3;
        private System.Windows.Forms.NumericUpDown nudDriverOvertSkill_c4r1;
        private System.Windows.Forms.ComboBox cbxDriverOvertSkill_c4r2;
        private System.Windows.Forms.NumericUpDown nudDriverOvertSkill_c4r2;
        private System.Windows.Forms.ComboBox cbxDriverOvertSkill_c4r1;
        private System.Windows.Forms.NumericUpDown nudDriverOvertSkill_c4r3;
        private System.Windows.Forms.ComboBox cbxDriverOvertSkill_c3r3;
        private System.Windows.Forms.NumericUpDown nudDriverOvertSkill_c3r1;
        private System.Windows.Forms.ComboBox cbxDriverOvertSkill_c3r2;
        private System.Windows.Forms.NumericUpDown nudDriverOvertSkill_c3r2;
        private System.Windows.Forms.ComboBox cbxDriverOvertSkill_c3r1;
        private System.Windows.Forms.NumericUpDown nudDriverOvertSkill_c3r3;
        private System.Windows.Forms.ComboBox cbxDriverOvertSkill_c2r3;
        private System.Windows.Forms.NumericUpDown nudDriverOvertSkill_c2r1;
        private System.Windows.Forms.ComboBox cbxDriverOvertSkill_c2r2;
        private System.Windows.Forms.NumericUpDown nudDriverOvertSkill_c2r2;
        private System.Windows.Forms.ComboBox cbxDriverOvertSkill_c2r1;
        private System.Windows.Forms.NumericUpDown nudDriverOvertSkill_c2r3;
        private System.Windows.Forms.ComboBox cbxDriverOvertSkill_c1r3;
        private System.Windows.Forms.NumericUpDown nudDriverOvertSkill_c1r1;
        private System.Windows.Forms.ComboBox cbxDriverOvertSkill_c1r2;
        private System.Windows.Forms.NumericUpDown nudDriverOvertSkill_c1r2;
        private System.Windows.Forms.ComboBox cbxDriverOvertSkill_c1r1;
        private System.Windows.Forms.NumericUpDown nudDriverOvertSkill_c1r3;
        private System.Windows.Forms.TabPage TabCharsSkillLinks;
        private System.Windows.Forms.TabControl tbcDriver150Ext;
        private System.Windows.Forms.TabPage tabDriver150ExtUnk_0x00B4;
        private Be.Windows.Forms.HexBox hbxDriver150ExtUnk_0x00B4;
        private System.Windows.Forms.TabPage tabDriver150ExtUnk_0x0247;
        private Be.Windows.Forms.HexBox hbxDriver150ExtUnk_0x0247;
        private System.Windows.Forms.TabPage TabCharsUnknown;
        private System.Windows.Forms.GroupBox GbxCharUnk_0x0f8;
        private Be.Windows.Forms.HexBox HbxCharUnk_0x0f8;
        private System.Windows.Forms.GroupBox GbxCharUnk_0x0a4;
        private Be.Windows.Forms.HexBox HbxCharUnk_0x0a4;
        private System.Windows.Forms.GroupBox GbxCharUnk_0x06c;
        private Be.Windows.Forms.HexBox HbxCharUnk_0x06c;
        private System.Windows.Forms.GroupBox GbxCharUnk_0x02c;
        private Be.Windows.Forms.HexBox HbxCharUnk_0x02c;
        private System.Windows.Forms.GroupBox GbxCharUnk_0x010;
        private Be.Windows.Forms.HexBox HbxCharUnk_0x010;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox CbxCharFootArmourType;
        private System.Windows.Forms.ComboBox CbxCharFootArmourID;
        private System.Windows.Forms.Label LblCharFootArmourID;
        private System.Windows.Forms.NumericUpDown NudCharFootArmourIndex;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.NumericUpDown NudCharFootArmourType;
        private System.Windows.Forms.Label LblCharFootArmourIndex;
        private System.Windows.Forms.NumericUpDown NudCharFootArmourID;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox CbxCharLegArmourType;
        private System.Windows.Forms.ComboBox CbxCharLegArmourID;
        private System.Windows.Forms.Label LblCharLegArmourID;
        private System.Windows.Forms.NumericUpDown NudCharLegArmourIndex;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown NudCharLegArmourType;
        private System.Windows.Forms.Label LblCharLegArmourIndex;
        private System.Windows.Forms.NumericUpDown NudCharLegArmourID;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox CbxCharArmArmourType;
        private System.Windows.Forms.ComboBox CbxCharArmArmourID;
        private System.Windows.Forms.Label LblCharArmArmourID;
        private System.Windows.Forms.NumericUpDown NudCharArmArmourIndex;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown NudCharArmArmourType;
        private System.Windows.Forms.Label LblCharArmArmourIndex;
        private System.Windows.Forms.NumericUpDown NudCharArmArmourID;
        private System.Windows.Forms.GroupBox GbxCharTorsoArmor;
        private System.Windows.Forms.ComboBox CbxCharTorsoArmourType;
        private System.Windows.Forms.ComboBox CbxCharTorsoArmourID;
        private System.Windows.Forms.Label LblCharTorsoArmourID;
        private System.Windows.Forms.NumericUpDown NudCharTorsoArmourIndex;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown NudCharTorsoArmourType;
        private System.Windows.Forms.Label LblCharTorsoArmourIndex;
        private System.Windows.Forms.NumericUpDown NudCharTorsoArmourID;
        private System.Windows.Forms.GroupBox GbxCharHeadArmour;
        private System.Windows.Forms.ComboBox CbxCharHeadArmourType;
        private System.Windows.Forms.ComboBox CbxCharHeadArmourID;
        private System.Windows.Forms.Label LblCharHeadArmourID;
        private System.Windows.Forms.NumericUpDown NudCharHeadArmourIndex;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown NudCharHeadArmourType;
        private System.Windows.Forms.Label LblCharHeadArmourIndex;
        private System.Windows.Forms.NumericUpDown NudCharHeadArmourID;
        private System.Windows.Forms.NumericUpDown NudCharArtPoints;
        private System.Windows.Forms.Label LblCharArtPoints;
        private System.Windows.Forms.ComboBox CbxCharNormalArt3ID;
        private System.Windows.Forms.ComboBox CbxCharNormalArt2ID;
        private System.Windows.Forms.ComboBox CbxCharNormalArt4ID;
        private System.Windows.Forms.NumericUpDown NudCharNormalArt4ID;
        private System.Windows.Forms.Label LblCharNormalArt4ID;
        private System.Windows.Forms.ComboBox CbxCharNormalArt8ID;
        private System.Windows.Forms.NumericUpDown NudCharNormalArt8ID;
        private System.Windows.Forms.Label LblCharNormalArt8ID;
        private System.Windows.Forms.ComboBox CbxCharNormalArt7ID;
        private System.Windows.Forms.ComboBox CbxCharNormalArt6ID;
        private System.Windows.Forms.ComboBox CbxCharNormalArt5ID;
        private System.Windows.Forms.NumericUpDown NudCharNormalArt7ID;
        private System.Windows.Forms.NumericUpDown NudCharNormalArt6ID;
        private System.Windows.Forms.NumericUpDown NudCharNormalArt5ID;
        private System.Windows.Forms.Label LblCharNormalArt7ID;
        private System.Windows.Forms.Label LblCharNormalArt5ID;
        private System.Windows.Forms.Label LblCharNormalArt6ID;
        private System.Windows.Forms.ComboBox CbxCharTalentArtID;
        private System.Windows.Forms.NumericUpDown NudCharTalentArtID;
        private System.Windows.Forms.Label LblCharTalentArtID;
        private System.Windows.Forms.GroupBox GbxCharSetMonadoArts;
        private System.Windows.Forms.ComboBox CbxCharMonadoArt8ID;
        private System.Windows.Forms.NumericUpDown NudCharMonadoArt8ID;
        private System.Windows.Forms.Label LblCharMonadoArt8ID;
        private System.Windows.Forms.ComboBox CbxCharMonadoArt7ID;
        private System.Windows.Forms.ComboBox CbxCharMonadoArt6ID;
        private System.Windows.Forms.ComboBox CbxCharMonadoArt5ID;
        private System.Windows.Forms.NumericUpDown NudCharMonadoArt7ID;
        private System.Windows.Forms.NumericUpDown NudCharMonadoArt6ID;
        private System.Windows.Forms.NumericUpDown NudCharMonadoArt5ID;
        private System.Windows.Forms.Label LblCharMonadoArt7ID;
        private System.Windows.Forms.Label LblCharMonadoArt5ID;
        private System.Windows.Forms.Label LblCharMonadoArt6ID;
        private System.Windows.Forms.ComboBox CbxCharMonTalentArtID;
        private System.Windows.Forms.NumericUpDown NudCharMonTalentArtID;
        private System.Windows.Forms.Label LblCharMonTalentArtID;
        private System.Windows.Forms.ComboBox CbxCharMonadoArt4ID;
        private System.Windows.Forms.NumericUpDown NudCharMonadoArt4ID;
        private System.Windows.Forms.Label LblCharMonadoArt4ID;
        private System.Windows.Forms.ComboBox CbxCharMonadoArt3ID;
        private System.Windows.Forms.ComboBox CbxCharMonadoArt2ID;
        private System.Windows.Forms.ComboBox CbxCharMonadoArt1ID;
        private System.Windows.Forms.NumericUpDown NudCharMonadoArt3ID;
        private System.Windows.Forms.NumericUpDown NudCharMonadoArt2ID;
        private System.Windows.Forms.NumericUpDown NudCharMonadoArt1ID;
        private System.Windows.Forms.Label LblCharMonadoArt3ID;
        private System.Windows.Forms.Label LblCharMonadoArt1ID;
        private System.Windows.Forms.Label LblCharMonadoArt2ID;
        private System.Windows.Forms.ComboBox CbxCharArtUnlockLvl;
        private System.Windows.Forms.Label LblCharArtUnlkLvl;
        private System.Windows.Forms.NumericUpDown NudCharArtUnlockLvl;
        private System.Windows.Forms.TabPage TabUnk_0x04693c;
        private System.Windows.Forms.TabPage TabUnk_0x151b44;
        private Be.Windows.Forms.HexBox HbxUnk_0x04693c;
        private Be.Windows.Forms.HexBox HbxUnk_0x151b44;
    }
}
