/*
 * Copyright (C) 2020 damysteryman
 * Copyright (C) 2022 NamelessMofo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;

namespace XCDESaveEditor
{
    /*
    *   PartyMember Structure
    *   Data for an individual Party Member / Player Character
    *
    *   Offset  Type        Name                    Description
    *   0x000   uint32      Level                   = Character's Level
    *   0x004   uint32      EXP                     = Character's amount of EXP
    *   0x008   uint32      AP                      = Character's amount of AP
    *   0x00c   uint32      AffinityCoins           = Character's No. of Affinity Coins
    *   0x010   uint8[4]    Unk1                    = ???   [UNKNOWN, PLEASE INVESTIGATE]
    *   0x014   ItemID      HeadEquip               = Equipped Head Armour Item Type + INDEX in inventory (NOT Item's actual ID)
    *   0x018   ItemID      TorsoEquip              = Equipped Torso Armour Item Type + INDEX in inventory (NOT Item's actual ID)
    *   0x01c   ItemID      ArmEquip                = Equipped Arm Armour Item Type + INDEX in inventory (NOT Item's actual ID)
    *   0x020   ItemID      LegEquip                = Equipped Leg Armour Item Type + INDEX in inventory (NOT Item's actual ID)
    *   0x024   ItemID      FootEquip               = Equipped Foot Armour Item Type + INDEX in inventory (NOT Item's actual ID)
    *   0x028   ItemID      WeaponEquip             = Equipped Weapon Item Type + INDEX in inventory (NOT Item's actual ID)
    *   0x02c   uint8[4]    Unk2                    = ???   [UNKNOWN, PLEASE INVESTIGATE]
    *   0x030   uint32      HeadCosmetic            = ID of equipped Head Cosmetic
    *   0x034   uint32      TorsoCosmetic           = ID of equipped Torso Cosmetic
    *   0x038   uint32      ArmCosmetic             = ID of equipped Arm Cosmetic
    *   0x03c   uint32      LegCosmetic             = ID of equipped Leg Cosmetic
    *   0x040   uint32      FootCosmetic            = ID of equipped Foot Cosmetic
    *   0x044   uint32      WeaponCosmetic          = ID of equipped Weapon Cosmetic
    *   0x048   uint16[9]   Arts                    = IDs of Character's set arts, 1st ID is Talent Art, IDs 2~9 are normal set Arts
    *   0x05a   uint16[9]   MonadoArts              = same as Arts, but 2nd set for when "Activate Monado" is used
    *   0x06c   uint8[0xc]  Unk3                    = ???   [UNKNOWN, PLEASE INVESTIGATE]
    *   0x078   uint32      SelectedSkillTreeIndex  = Index of the Character's Currently Selected Skill Tree
    *   0x07c   uint32[5]   SkillTreeSPs            = Skill Points (SP) for each of the Character's Skill Trees
    *   0x090   uint32[5]   SkillTreeUnlockedSkills = The No. of Skills Unlocked in each of the Character's Skill Trees
    *   0x0a4   uint8[0x25] Unk4                    = ???   [UNKNOWN, PLEASE INVESTIGATE]
    *   0x0c9   uint[35]    SkillLinkIDs            = The IDs of each Skill Link Skill on a Character's Skill Link Menu
    *   0x0ec   uint32      ExpertModeLevel         = Character's Level used for Expert Mode feature
    *   0x0f0   uint32      ExpertModeEXP           = Character's EXP used for Expert Mode feature
    *   0x0f4   uint32      ExpertModeReserveEXP    = Character's amount of Reserve EXP saved up for Expert Mode feature
    *   0x0f8   uint8[0x40] Unk5                    = ???   [UNKNOWN, PLEASE INVESTIGATE]
    */
    public class PartyMember : ISaveObject
    {
        public const uint Size = 0x138;

        public uint     Level                   { get; set; }
        public uint     EXP                     { get; set; }
        public uint     AP                      { get; set; }
        public uint     AffinityCoins           { get; set; }
        public byte[]   Unk1                    { get; set; }
        public ItemID   HeadEquip               { get; set; }
        public ItemID   TorsoEquip              { get; set; }
        public ItemID   ArmEquip                { get; set; }
        public ItemID   LegEquip                { get; set; }
        public ItemID   FootEquip               { get; set; }
        public ItemID   WeaponEquip             { get; set; }
        public byte[]   Unk2                    { get; set; }
        public uint     HeadCosmetic            { get; set; }
        public uint     TorsoCosmetic           { get; set; }
        public uint     ArmCosmetic             { get; set; }
        public uint     LegCosmetic             { get; set; }
        public uint     FootCosmetic            { get; set; }
        public uint     WeaponCosmetic          { get; set; }
        public ushort[] Arts                    { get; set; } = new ushort [9];
        public ushort[] MonadoArts              { get; set; } = new ushort [9];
        public byte[]   Unk3                    { get; set; }
        public uint     SelectedSkillTreeIndex  { get; set; }
        public uint[]   SkillTreeSPs            { get; set; } = new uint [5];
        public uint[]   SkillTreeUnlockedSkills { get; set; } = new uint [5];
        public byte[]   Unk4                    { get; set; }
        public byte[]   SkillLinkIDs            { get; set; } = new byte [35];
        public uint     ExpertModeLevel         { get; set; }
        public uint     ExpertModeEXP           { get; set; }
        public uint     ExpertModeReserveEXP    { get; set; }
        public byte[]   Unk5                    { get; set; }

        private static readonly Dictionary<string, uint> loc = new Dictionary<string, uint>()
        {
            { "Level",                   0x00 },
            { "EXP",                     0x04 },
            { "AP",                      0x08 },
            { "AffinityCoins",           0x0c },
            { "Unk1",                    0x10 },
            { "HeadEquip",               0x14 },
            { "TorsoEquip",              0x18 },
            { "ArmEquip",                0x1c },
            { "LegEquip",                0x20 },
            { "FootEquip" ,              0x24 },
            { "WeaponEquip",             0x28 },
            { "Unk2",                    0x2c },
            { "HeadCosmetic",            0x30 },
            { "TorsoCosmetic",           0x34 },
            { "ArmCosmetic",             0x38 },
            { "LegCosmetic",             0x3c },
            { "FootCosmetic",            0x40 },
            { "WeaponCosmetic",          0x44 },
            { "Arts",                    0x48 },
            { "MonadoArts",              0x5a },
            { "Unk3",                    0x6c },
            { "SelectedSkillTreeIndex",  0x78 },
            { "SkillTreeSPs",            0x7c },
            { "SkillTreeUnlockedSkills", 0x90 },
            { "Unk4",                    0xa4 },
            { "SkillLinkIDs",            0xc9 },
            { "ExpertModeLevel",         0xec },
            { "ExpertModeEXP",           0xf0 },
            { "ExpertModeReserveEXP",    0xf4 },
            { "Unk5",                    0xf8 }
        };

        public PartyMember (byte[] data)
        {
            Level           = BitConverter.ToUInt32 (data.GetByteSubArray (loc["Level"], loc["EXP"] - loc["Level"]), 0);
            EXP             = BitConverter.ToUInt32 (data.GetByteSubArray (loc["EXP"], loc["AP"] - loc["EXP"]), 0);
            AP              = BitConverter.ToUInt32 (data.GetByteSubArray (loc["AP"], loc["AffinityCoins"] - loc["AP"]), 0);
            AffinityCoins   = BitConverter.ToUInt32 (data.GetByteSubArray (loc["AffinityCoins"], loc["Unk1"] - loc["AffinityCoins"]), 0);
            Unk1            = data.GetByteSubArray (loc["Unk1"], loc["HeadEquip"] - loc["Unk1"]);
            HeadEquip       = new ItemID (data.GetByteSubArray (loc["HeadEquip"], loc["TorsoEquip"] - loc["HeadEquip"]));
            TorsoEquip      = new ItemID (data.GetByteSubArray (loc["TorsoEquip"], loc["ArmEquip"] - loc["TorsoEquip"]));
            ArmEquip        = new ItemID (data.GetByteSubArray (loc["ArmEquip"], loc["LegEquip"] - loc["ArmEquip"]));
            LegEquip        = new ItemID (data.GetByteSubArray (loc["LegEquip"], loc["FootEquip"] - loc["LegEquip"]));
            FootEquip       = new ItemID (data.GetByteSubArray (loc["FootEquip"], loc["WeaponEquip"] - loc["FootEquip"]));
            WeaponEquip     = new ItemID (data.GetByteSubArray (loc["WeaponEquip"], loc["Unk2"] - loc["WeaponEquip"]));
            Unk2            = data.GetByteSubArray (loc["Unk2"], loc["HeadCosmetic"] - loc["Unk2"]);
            HeadCosmetic    = BitConverter.ToUInt32 (data.GetByteSubArray (loc["HeadCosmetic"], loc["TorsoCosmetic"] - loc["HeadCosmetic"]), 0);
            TorsoCosmetic   = BitConverter.ToUInt32 (data.GetByteSubArray (loc["TorsoCosmetic"], loc["ArmCosmetic"] - loc["TorsoCosmetic"]), 0);
            ArmCosmetic     = BitConverter.ToUInt32 (data.GetByteSubArray (loc["ArmCosmetic"], loc["LegCosmetic"] - loc["ArmCosmetic"]), 0);
            LegCosmetic     = BitConverter.ToUInt32 (data.GetByteSubArray (loc["LegCosmetic"], loc["FootCosmetic"] - loc["LegCosmetic"]), 0);
            FootCosmetic    = BitConverter.ToUInt32 (data.GetByteSubArray (loc["FootCosmetic"], loc["WeaponCosmetic"] - loc["FootCosmetic"]), 0);
            WeaponCosmetic  = BitConverter.ToUInt32 (data.GetByteSubArray (loc["WeaponCosmetic"], loc["Arts"] - loc["WeaponCosmetic"]), 0);
            for (uint i = 0; i < Arts.Length; i++)
                Arts[i]     = BitConverter.ToUInt16 (data.GetByteSubArray(loc["Arts"] + (i * sizeof (UInt16)), sizeof (UInt16)), 0);
            for (uint i = 0; i < MonadoArts.Length; i++)
                MonadoArts[i] = BitConverter.ToUInt16 (data.GetByteSubArray (loc["MonadoArts"] + (i * sizeof (UInt16)), sizeof (UInt16)), 0);
            Unk3            = data.GetByteSubArray (loc["Unk3"], loc["SelectedSkillTreeIndex"] - loc["Unk3"]);
            SelectedSkillTreeIndex = BitConverter.ToUInt32 (data.GetByteSubArray (loc["SelectedSkillTreeIndex"], loc["SkillTreeSPs"] - loc["SelectedSkillTreeIndex"]), 0);
            for (uint i = 0; i < SkillTreeSPs.Length; i++)
                SkillTreeSPs[i] = BitConverter.ToUInt32 (data.GetByteSubArray (loc["SkillTreeSPs"] + (i * sizeof (UInt32)), sizeof (UInt32)), 0);
            for (uint i = 0; i < SkillTreeUnlockedSkills.Length; i++)
                SkillTreeUnlockedSkills[i] = BitConverter.ToUInt32 (data.GetByteSubArray (loc["SkillTreeUnlockedSkills"] + (i * sizeof (UInt32)), sizeof (UInt32)), 0);
            Unk4            = data.GetByteSubArray (loc["Unk4"], loc["SkillLinkIDs"] - loc["Unk4"]);
            for (uint i = 0; i < SkillLinkIDs.Length; i++)
                SkillLinkIDs[i] = data[loc["SkillLinkIDs"] + i];
            ExpertModeLevel = BitConverter.ToUInt32 (data.GetByteSubArray (loc["ExpertModeLevel"], loc["ExpertModeEXP"] - loc["ExpertModeLevel"]), 0);
            ExpertModeEXP   = BitConverter.ToUInt32 (data.GetByteSubArray (loc["ExpertModeEXP"], loc["ExpertModeReserveEXP"] - loc["ExpertModeEXP"]), 0);
            ExpertModeReserveEXP = BitConverter.ToUInt32 (data.GetByteSubArray (loc["ExpertModeReserveEXP"], loc["Unk5"] - loc["ExpertModeReserveEXP"]), 0);
            Unk5            = data.GetByteSubArray (loc["Unk5"], Size - loc["Unk5"]);
        }

        public byte[] ToRawData()
        {
            List<byte> result = new List<byte>();

            result.AddRange (BitConverter.GetBytes (Level));
            result.AddRange (BitConverter.GetBytes (EXP));
            result.AddRange (BitConverter.GetBytes (AP));
            result.AddRange (BitConverter.GetBytes (AffinityCoins));
            result.AddRange (Unk1);
            result.AddRange (HeadEquip.ToRawData());
            result.AddRange (TorsoEquip.ToRawData());
            result.AddRange (ArmEquip.ToRawData());
            result.AddRange (LegEquip.ToRawData());
            result.AddRange (FootEquip.ToRawData());
            result.AddRange (WeaponEquip.ToRawData());
            result.AddRange (Unk2);
            result.AddRange (BitConverter.GetBytes (HeadCosmetic));
            result.AddRange (BitConverter.GetBytes (TorsoCosmetic));
            result.AddRange (BitConverter.GetBytes (ArmCosmetic));
            result.AddRange (BitConverter.GetBytes (LegCosmetic));
            result.AddRange (BitConverter.GetBytes (FootCosmetic));
            result.AddRange (BitConverter.GetBytes (WeaponCosmetic));
            foreach (UInt16 a in Arts)
                result.AddRange (BitConverter.GetBytes (a));
            foreach (UInt16 ma in MonadoArts)
                result.AddRange (BitConverter.GetBytes (ma));
            result.AddRange (Unk3);
            result.AddRange (BitConverter.GetBytes (SelectedSkillTreeIndex));
            foreach (UInt32 sp in SkillTreeSPs)
                result.AddRange (BitConverter.GetBytes (sp));
            foreach (UInt32 su in SkillTreeUnlockedSkills)
                result.AddRange (BitConverter.GetBytes (su));
            result.AddRange (Unk4);
            result.AddRange (SkillLinkIDs);
            result.AddRange (BitConverter.GetBytes (ExpertModeLevel));
            result.AddRange (BitConverter.GetBytes (ExpertModeEXP));
            result.AddRange (BitConverter.GetBytes (ExpertModeReserveEXP));
            result.AddRange (Unk5);

            return result.ToArray();
        }
    }
}
