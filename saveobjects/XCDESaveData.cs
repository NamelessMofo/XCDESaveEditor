/*
 * Copyright (C) 2020 damysteryman
 * Copyright (C) 2022 NamelessMofo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;

namespace XCDESaveEditor
{
    /*
    *   XCDESaveData Structure
    *   Save file for Xenoblade Chronchles: Definitive Edition [WORK IN PROGRESS, PLEASE RESEARCH]
    *
    *   Offset      Type                Name             Description
    *   0x000000    uint8[4]            Unk_0x000000     = ???   [UNKNOWN, PLEASE INVESTIGATE]
    *   0x000004    uint32              PlayTime         = Total Play Time (in Seconds)
    *   0x000008    uint64              SaveTime         = Real (Wall Clock) Time of Save (in a Compressed Format)
    *   0x000010    uint32              Noponstones      = No. of Noponstones
    *   0x000014    uint8[0x3afc]       Unk_0x000014     = ???   [UNKNOWN, PLEASE INVESTIGATE]
    *   0x003b10    EquipItem[500]      WeaponBox        = Weapons in inventory (500pcs)
    *   0x0098d0    EquipItem[500]      HeadArmourBox    = Head Equipment in Inventory (500pcs)
    *   0x00f690    EquipItem[500]      TorsoArmourBox   = Torso Equipment in Inventory (500pcs)
    *   0x015450    EquipItem[500]      ArmArmourBox     = Arm Equipment in Inventory (500pcs)
    *   0x01b210    EquipItem[500]      LegArmourBox     = Leg Equipment in Inventory (500pcs)
    *   0x020fd0    EquipItem[500]      FootArmourBox    = Foot/Auxiliary Equipment in Inventory (500pcs)
    *   0x026d90    CrystalItem[500]    CrystalBox       = Crystals in Inventory (500pcs)
    *   0x02c380    CrystalItem[500]    GemBox           = Gems in Inventory (500pcs)
    *   0x031970    Item[500]           CollectableBox   = Collectables in Inventory (500pcs)
    *   0x034080    Item[500]           MaterialBox      = Materials in Inventory (500pcs)
    *   0x036790    Item[500]           KeyItemBox       = Key items in Inventory (500pcs)
    *   0x038ea0    Item[500]           ArtsManualBox    = Arts Manuals in Inventory (500pcs)
    *   0x03b5b0    uint8[0xb358]       Unk_0x03b5b0     = ???   [UNKNOWN, PLEASE INVESTIGATE]
    *   0x046908    uint32[13]          Serials          = Serials for all inventory boxes (seems to be 1 extra?)
    *   0x04693c    uint8[0x10b204]     Unk_0x04693c     = ???   [UNKNOWN, PLEASE INVESTIGATE]
    *   0x151b40    uint32              Money            = Amount of Money
    *   0x151b44    uint8[0x7d4]        Unk_0x151b44     = ???   [UNKNOWN, PLEASE INVESTIGATE]
    *   0x152318    Party               Party            = Party structure, No. and IDs of characters in party
    *   0x152331    uint8[0x37]         Unk_0x152331     = ???   [UNKNOWN, PLEASE INVESTIGATE]
    *   0x152368    PartyMember[16]     PartyMembers     = Party Member Character Data
    *   0x1536e8    ArtsLevel[188]      ArtsLevels       = Arts Levels + Unlock Levels for each Art (except Ponspector Arts)
    *
    *   Note about mapping of Serials to corresponding item boxes:
    *   0: Weapons, 1: Gems, 2: Head Armour, 3: Torso Armour, 4: Arm Armour, 5: Leg Armour, 6: Foot Armour,
    *   7: Crystals, 8: Collectables, 9: Materials, 10: Key Items, 11: Arts Manuals, 12: Unknown / Unused
    */
    public class XCDESaveData : ISaveObject
    {
        public Dictionary<uint, string> ItemTypes;
        public const int Size = 0x153860;

        public byte[] Unk_0x000000 { get; set; }
        public ElapseTime PlayTime { get; set; }
        public RealTime SaveTime { get; set; }
        public uint Noponstones { get; set; }
        public byte[] Unk_0x000014 { get; set; }
        public EquipItem[] WeaponBox { get; set; } = new EquipItem[500];
        public EquipItem[] HeadArmourBox { get; set; } = new EquipItem[500];
        public EquipItem[] TorsoArmourBox { get; set; } = new EquipItem[500];
        public EquipItem[] ArmArmourBox { get; set; } = new EquipItem[500];
        public EquipItem[] LegArmourBox { get; set; } = new EquipItem[500];
        public EquipItem[] FootArmourBox { get; set; } = new EquipItem[500];
        public CrystalItem[] CrystalBox { get; set; } = new CrystalItem[500];
        public CrystalItem[] GemBox { get; set; } = new CrystalItem[500];
        public Item[] CollectableBox { get; set; } = new Item[500];
        public Item[] MaterialBox { get; set; } = new Item[500];
        public Item[] KeyItemBox { get; set; } = new Item[500];
        public Item[] ArtsManualBox { get; set; } = new Item[500];
        public byte[] Unk_0x03b5b0 { get; set; }
        public uint[] Serials { get; set; } = new uint[13];
        public byte[] Unk_0x04693c { get; set; }
        public uint Money { get; set; }
        public byte[] Unk_0x151b44 { get; set; }
        public Party Party { get; set; }
        public byte[] Unk_0x152331 { get; set; }
        public PartyMember[] PartyMembers { get; set; } = new PartyMember[16];
        public ArtsLevel[] ArtsLevels { get; set; } = new ArtsLevel[188];

        // Placeholders for future enhancements
        public GameTime InGameTime { get; set; }

        private static readonly Dictionary<string, uint> loc = new Dictionary<string, uint> ()
        {
            { "Unk_0x000000",   0x000000 },
            { "PlayTime",       0x000004 },
            { "SaveTime",       0x000008 },
            { "Noponstones",    0x000010 },
            { "Unk_0x000014",   0x000014 },
            { "WeaponBox",      0x003b10 },
            { "HeadArmourBox",  0x0098d0 },
            { "TorsoArmourBox", 0x00f690 },
            { "ArmArmourBox",   0x015450 },
            { "LegArmourBox",   0x01b210 },
            { "FootArmourBox",  0x020fd0 },
            { "CrystalBox",     0x026d90 },
            { "GemBox",         0x02c380 },
            { "CollectableBox", 0x031970 },
            { "MaterialBox",    0x034080 },
            { "KeyItemBox",     0x036790 },
            { "ArtsManualBox",  0x038ea0 },
            { "Unk_0x03b5b0",   0x03b5b0 },
            { "Serials",        0x046908 },
            { "Unk_0x04693c",   0x04693c },
            { "Money",          0x151b40 },
            { "Unk_0x151b44",   0x151b44 },
            { "Party",          0x152318 },
            { "Unk_0x152331",   0x152331 },
            { "PartyMembers",   0x152368 },
            { "ArtsLevels",     0x1536e8 }
        };

        public XCDESaveData (byte[] data)
        {
            Unk_0x000000 = data.GetByteSubArray (loc["Unk_0x000000"], 4);
            PlayTime = new ElapseTime (data.GetByteSubArray (loc["PlayTime"], ElapseTime.Size));
            SaveTime = new RealTime (data.GetByteSubArray (loc["SaveTime"], RealTime.Size));
            Noponstones = BitConverter.ToUInt32 (data.GetByteSubArray (loc["Noponstones"], 4), 0);
            Unk_0x000014 = data.GetByteSubArray (loc["Unk_0x000014"], loc["WeaponBox"] - loc["Unk_0x000014"]);
            for (uint i = 0; i < WeaponBox.Length; i++)
                WeaponBox[i] = new EquipItem (data.GetByteSubArray (loc["WeaponBox"] + (i * EquipItem.Size), EquipItem.Size));
            for (uint i = 0; i < HeadArmourBox.Length; i++)
                HeadArmourBox[i] = new EquipItem (data.GetByteSubArray (loc["HeadArmourBox"] + (i * EquipItem.Size), EquipItem.Size));
            for (uint i = 0; i < TorsoArmourBox.Length; i++)
                TorsoArmourBox[i] = new EquipItem (data.GetByteSubArray (loc["TorsoArmourBox"] + (i * EquipItem.Size), EquipItem.Size));
            for (uint i = 0; i < ArmArmourBox.Length; i++)
                ArmArmourBox[i] = new EquipItem (data.GetByteSubArray (loc["ArmArmourBox"] + (i * EquipItem.Size), EquipItem.Size));
            for (uint i = 0; i < LegArmourBox.Length; i++)
                LegArmourBox[i] = new EquipItem (data.GetByteSubArray (loc["LegArmourBox"] + (i * EquipItem.Size), EquipItem.Size));
            for (uint i = 0; i < FootArmourBox.Length; i++)
                FootArmourBox[i] = new EquipItem (data.GetByteSubArray (loc["FootArmourBox"] + (i * EquipItem.Size), EquipItem.Size));
            for (uint i = 0; i < CrystalBox.Length; i++)
                CrystalBox[i] = new CrystalItem (data.GetByteSubArray (loc["CrystalBox"] + (i * CrystalItem.Size), CrystalItem.Size));
            for (uint i = 0; i < GemBox.Length; i++)
                GemBox[i] = new CrystalItem (data.GetByteSubArray (loc["GemBox"] + (i * CrystalItem.Size), CrystalItem.Size));
            for (uint i = 0; i < CollectableBox.Length; i++)
                CollectableBox[i] = new Item (data.GetByteSubArray (loc["CollectableBox"] + (i * Item.Size), Item.Size));
            for (uint i = 0; i < MaterialBox.Length; i++)
                MaterialBox[i] = new Item (data.GetByteSubArray (loc["MaterialBox"] + (i * Item.Size), Item.Size));
            for (uint i = 0; i < KeyItemBox.Length; i++)
                KeyItemBox[i] = new Item (data.GetByteSubArray (loc["KeyItemBox"] + (i * Item.Size), Item.Size));
            for (uint i = 0; i < ArtsManualBox.Length; i++)
                ArtsManualBox[i] = new Item (data.GetByteSubArray (loc["ArtsManualBox"] + (i * Item.Size), Item.Size));
            Unk_0x03b5b0 = data.GetByteSubArray (loc["Unk_0x03b5b0"], loc["Serials"] - loc["Unk_0x03b5b0"]);
            for (uint i = 0; i < Serials.Length; i++)
                Serials[i] = BitConverter.ToUInt32 (data.GetByteSubArray (loc["Serials"] + (i * 4), 4), 0);
            Unk_0x04693c = data.GetByteSubArray (loc["Unk_0x04693c"], loc["Money"] - loc["Unk_0x04693c"]);
            Money = BitConverter.ToUInt32 (data.GetByteSubArray (loc["Money"], 4), 0);
            Unk_0x151b44 = data.GetByteSubArray (loc["Unk_0x151b44"], loc["Party"] - loc["Unk_0x151b44"]);
            Party = new Party (data.GetByteSubArray (loc["Party"], Party.Size));
            Unk_0x152331 = data.GetByteSubArray (loc["Unk_0x152331"], loc["PartyMembers"] - loc["Unk_0x152331"]);
            for (uint i = 0; i < PartyMembers.Length; i++)
                PartyMembers[i] = new PartyMember (data.GetByteSubArray (loc["PartyMembers"] + (i * PartyMember.Size), PartyMember.Size));
            for (uint i = 0; i < ArtsLevels.Length; i++)
                ArtsLevels[i] = new ArtsLevel (data.GetByteSubArray (loc["ArtsLevels"] + (i * ArtsLevel.Size), ArtsLevel.Size));

            // Placeholders for future enhancements
            InGameTime = new GameTime ();
        }

        public byte[] ToRawData ()
        {
            List<byte> result = new List<byte> ();

            result.AddRange (Unk_0x000000);
            result.AddRange (PlayTime.ToRawData ());
            result.AddRange (SaveTime.ToRawData ());
            result.AddRange (BitConverter.GetBytes (Noponstones));
            result.AddRange (Unk_0x000014);
            foreach (EquipItem i in WeaponBox)
                result.AddRange (i.ToRawData ());
            foreach (EquipItem i in HeadArmourBox)
                result.AddRange (i.ToRawData ());
            foreach (EquipItem i in TorsoArmourBox)
                result.AddRange (i.ToRawData ());
            foreach (EquipItem i in ArmArmourBox)
                result.AddRange (i.ToRawData ());
            foreach (EquipItem i in LegArmourBox)
                result.AddRange (i.ToRawData ());
            foreach (EquipItem i in FootArmourBox)
                result.AddRange (i.ToRawData ());
            foreach (CrystalItem i in CrystalBox)
                result.AddRange (i.ToRawData ());
            foreach (CrystalItem i in GemBox)
                result.AddRange (i.ToRawData ());
            foreach (Item i in CollectableBox)
                result.AddRange (i.ToRawData ());
            foreach (Item i in MaterialBox)
                result.AddRange (i.ToRawData ());
            foreach (Item i in KeyItemBox)
                result.AddRange (i.ToRawData ());
            foreach (Item i in ArtsManualBox)
                result.AddRange (i.ToRawData ());
            result.AddRange (Unk_0x03b5b0);
            foreach (UInt32 ser in Serials)
                result.AddRange (BitConverter.GetBytes (ser));
            result.AddRange (Unk_0x04693c);
            result.AddRange (BitConverter.GetBytes (Money));
            result.AddRange (Unk_0x151b44);
            result.AddRange (Party.ToRawData ());
            result.AddRange (Unk_0x152331);
            foreach (PartyMember p in PartyMembers)
                result.AddRange (p.ToRawData ());
            foreach (ArtsLevel a in ArtsLevels)
                result.AddRange (a.ToRawData ());

            return result.ToArray ();
        }
    }
}
