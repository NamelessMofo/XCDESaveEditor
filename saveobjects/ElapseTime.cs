using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XCDESaveEditor
{
    public class ElapseTime : ISaveObject
    {
        public const int Size = 4;

        public UInt16 Hours   { get; set; }
        public Byte   Minutes { get; set; }
        public Byte   Seconds { get; set; }

        public ElapseTime (Byte[] data)
        {
            UInt32 value = BitConverter.ToUInt32 (data, 0);

            Hours   = (UInt16) (value / 3600);
            Minutes = (Byte) ((value % 3600) / 60);
            Seconds = (Byte) (value % 60);
        }

        public ElapseTime()
        {
            Hours   = 0;
            Minutes = 0;
            Seconds = 0;
        }

        public Byte[] ToRawData()
        {
            return BitConverter.GetBytes ((UInt32) (((UInt32) Hours * 3600) + ((UInt32) Minutes * 60) + ((UInt32) Seconds)));
        }

        public override string ToString()
        {
            return Hours + ":" + Minutes.ToString("D2") + ":" + Seconds.ToString("D2");
        }
    }
}
